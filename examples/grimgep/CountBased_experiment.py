import os.path as osp
import torch
import multiworld.envs.mujoco as mwmj
import rlkit.util.hyperparameter as hyp
# from multiworld.envs.mujoco.cameras import sawyer_door_env_camera_v0
from rlkit.launchers.launcher_util import run_experiment
import rlkit.torch.vae.vae_schedules as vae_schedules
from rlkit.launchers.skewfit_experiments import \
    skewfit_full_experiment
from rlkit.torch.vae.conv_vae import imsize48_default_architecture
from itertools import product
import copy
import sys


if __name__ == "__main__":
    assert len(sys.argv) >= 2
    id = int(sys.argv[-2])
    exp_name = str(sys.argv[-1])

    variant = dict(
        algorithm='Skew-Fit-SAC',
        double_algo=False,
        online_vae_exploration=False,
        imsize=48,
        env_id='PlaygroundRGB-v1',
        base_env_kwargs=dict(
            continuous=True,
            debug_mode=False,
            rooms_mode=True,
            debug_rooms_mode=True,
            goal_generation_rooms=("OBJECTS",),
            available_rooms=("START", "OBJECTS", "TV"),
            number_of_random_room_colors=...,
            colored_start_room=True,
            air_wrong_room_distance=True,
            doors=True,
            door_size=0.5,
            teleport_random_room=False,
            random_color_per_rollout=...,
            random_ball_per_rollout=...,
            random_ball_location=True,
            noise_random_color=False,
            corridor_random_room=False,

            number_of_TV_room_colors=...,
            random_TV_location=True,
            random_TV_location_per_rollout=False,
            TV_color_per_rollout=True,

            white_obj_room=False,
            gray_obj_room=True,
            obj_room_flipped_actions=False,
            obj_room_rotated_actions=False,
            doors_handle=False,

            block_dog=True,
            grow=False,
            block_water=False,

            deterministic_obj=True,
            whiten=True,

            use_big_margin=False,
            use_medium_margin=False,
            use_custom_margin=True,
            custom_margin_value=...,

            use_small_objs=False,
            use_huge_objs=False,
            use_custom_objs=True,
            custom_objs_ratio=...,

            red_gripper=True,
            static=True,
            smooth_colors=False,

            pos_step_size=0.5,
            static_eval_set_size=...,
    ),
        init_camera=None,
        skewfit_variant=dict(
            save_video=True,
            save_video_period=50,
            custom_goal_sampler='replay_buffer',
            online_vae_trainer_kwargs=dict(
                beta=20,
                lr=1e-3,
            ),
            qf_kwargs=dict(
                hidden_sizes=[400, 300],
            ),
            policy_kwargs=dict(
                hidden_sizes=[400, 300],
            ),
            max_path_length=...,
            algo_kwargs=dict(
                batch_size=1024,
                num_epochs=600,
                # num_eval_steps_per_epoch=20*50,  # 20 goals
                # num_expl_steps_per_train_loop=10*50,
                # min_num_steps_before_training=200*50,
                num_eval_steps_per_epoch=...,  # 20 goals
                num_expl_steps_per_train_loop=...,
                min_num_steps_before_training=...,
                num_trains_per_train_loop=1000,
                vae_training_schedule=vae_schedules.custom_schedule_3,
                oracle_data=False,
                vae_save_period=50,
                parallel_vae_train=False
            ),
            twin_sac_trainer_kwargs=dict(
                reward_scale=1,
                discount=0.99,
                soft_target_tau=1e-3,
                target_update_period=1,
                use_automatic_entropy_tuning=True,
            ),
            replay_buffer_kwargs=dict(
                start_skew_epoch=20,
                dump_reprs=False,
                # start_skew_epoch=100,
                max_size=int(100000),
                fraction_goals_rollout_goals=0.2,
                fraction_goals_env_goals=0.5,
                # exploration_rewards_type='None',
                # vae_priority_type='vae_prob',
                # priority_function_kwargs=dict(
                #     sampling_method='importance_sampling',
                #     decoder_distribution='gaussian_identity_variance',
                #     num_latents_to_sample=10,
                # ),
                exploration_rewards_type='count_based',
                vae_priority_type='count_based',
                power=...,
                replacement=False,
                regions=...,
                # expert_clusters=...,
                n_clusters=...,
                expert_LP=...,
                lp_history_size=...,
                hard_cluster_sample=False,
                alp_gmm=False,
                alp_gmm_k=1,
                hard_prior=True,
                clamp_by_initial=True,
                cluster_space=...,
                reshaped_img_size=2,
                lp_temp=...,
                clustering=...,
                uniform_LP=...,
                custom_expert_LP=False,
                normalize_by_mean_of_vars=False,
                relabeling_goal_sampling_mode='custom_goal_sampler',
                cluster_dqn_train=...,
            ),
            exploration_goal_sampling_mode='custom_goal_sampler',
            # exploration_goal_sampling_mode='env',
            # evaluation_goal_sampling_mode='presampled',
            evaluation_goal_sampling_mode='static_eval',
            training_mode='train',
            testing_mode='test',
            reward_params=dict(
                type='latent_distance',
            ),
            observation_key='latent_observation',
            desired_goal_key='latent_desired_goal',
            presampled_goals_path=osp.join(
                osp.dirname(mwmj.__file__),
                "goals",
                "door_goals.npy",
            ),
            # presample_goals=True,
            presample_goals=False,
            vae_wrapped_env_kwargs=dict(
                sample_from_true_prior=True,
            ),
        ),
        train_vae_variant=dict(
            representation_size=16,
            beta=20,
            num_epochs=0,
            dump_skew_debug_plots=False,
            decoder_activation='gaussian',
            # decoder_activation='sigmoid',
            generate_vae_dataset_kwargs=dict(
                N=2,
                test_p=.9,
                use_cached=True,
                show=False,
                oracle_dataset=False,
                n_random_steps=1,
                non_presampled_goal_img_is_garbage=True,
            ),
            vae_kwargs=dict(
                decoder_distribution='gaussian_identity_variance',
                input_channels=3,
                architecture=imsize48_default_architecture,
            ),
            algo_kwargs=dict(
                lr=1e-3,
            ),
            save_period=50,
        ),
    )

    n_seeds = 1
    mode = 'local'

    # obj_size, power, step_size
    power = [-0.75]  # CountBased
    col_prs = [True]
    ball_prs = [False]

    # apprs = ["cb", "cl_LP"]  # CountBased
    apprs = ["cb", "cl_UNI", "cl_LP"]  # CountBased

    c_algos = ['aaic']
    max_steps = [50]

    configurations = []
    for appr in apprs:
        for pow in power:
            for col_pr in col_prs:
                for ball_pr in ball_prs:
                    for m_st in max_steps:
                        for c_algo in c_algos:
                            c = (m_st, c_algo, col_pr, ball_pr, appr, pow)
                            configurations.append(c)

    configurations = configurations * 10  # ten seeds
    assert len(configurations) == 10*len(apprs)
    m_st, c_algo, col_pr, ball_pr, appr, pow = configurations[id]
    print("configuration:", configurations[id])
    num_of_rand_colors = 5

    variant_current = copy.deepcopy(variant)
    variant_current['skewfit_variant']['max_path_length'] = m_st
    variant_current['skewfit_variant']['algo_kwargs']['num_eval_steps_per_epoch'] = 25 * m_st  # 25 goals
    variant_current['skewfit_variant']['algo_kwargs']['num_expl_steps_per_train_loop'] = 10 * m_st
    variant_current['skewfit_variant']['algo_kwargs']['min_num_steps_before_training'] = 200 * m_st
    variant_current['base_env_kwargs']['static_eval_set_size'] = 25

    if appr == "rig":
        pow = 0
    elif appr == "cb":
        pow = -0.25

    variant_current['skewfit_variant']['replay_buffer_kwargs']['power'] = pow
    variant_current['skewfit_variant']['replay_buffer_kwargs']['cluster_space'] = "clustering_vae"
    variant_current['skewfit_variant']['replay_buffer_kwargs']['regions'] = appr not in ["rig", "cb"]
    # variant_current['skewfit_variant']['replay_buffer_kwargs']['expert_clusters'] = False
    variant_current['skewfit_variant']['replay_buffer_kwargs']['expert_LP'] = "cl_exp_LP" in appr
    variant_current['skewfit_variant']['replay_buffer_kwargs']['uniform_LP'] = "cl_UNI" in appr
    variant_current['skewfit_variant']['replay_buffer_kwargs']['cluster_dqn_train'] = "dqn_lp" in appr

    if appr in ["rig", "cb"]:
        n_clusters = 1
    else:
        n_clusters = 20

    variant_current['skewfit_variant']['replay_buffer_kwargs']['n_clusters'] = n_clusters
    variant_current['skewfit_variant']['replay_buffer_kwargs']['clustering'] = c_algo
    variant_current['skewfit_variant']['replay_buffer_kwargs']['lp_history_size'] = None

    lp_temp = 5
    variant_current['skewfit_variant']['replay_buffer_kwargs']['lp_temp'] = lp_temp
    variant_current['base_env_kwargs']['custom_objs_ratio'] = 1.3
    variant_current['base_env_kwargs']['custom_margin_value'] = 0.75
    variant_current['base_env_kwargs']['random_color_per_rollout'] = col_pr
    variant_current['base_env_kwargs']['random_ball_per_rollout'] = ball_pr
    variant_current['base_env_kwargs']['number_of_random_room_colors'] = num_of_rand_colors
    variant_current['base_env_kwargs']['number_of_TV_room_colors'] = num_of_rand_colors

    exp_prefix = exp_name + "_temp_{}_apr_{}_algo_{}_pow_{}".format(lp_temp, appr, "vae_"+c_algo, abs(pow))
    print("exp_prefix:", exp_prefix)

    for _ in range(n_seeds):
        run_experiment(
            skewfit_full_experiment,
            exp_prefix=exp_prefix,
            mode=mode,
            variant=variant_current,
            use_gpu=torch.cuda.is_available(),
        )
