import os.path as osp
import torch
import multiworld.envs.mujoco as mwmj
import rlkit.util.hyperparameter as hyp
# from multiworld.envs.mujoco.cameras import sawyer_door_env_camera_v0
from rlkit.launchers.launcher_util import run_experiment
import rlkit.torch.vae.vae_schedules as vae_schedules
from rlkit.launchers.skewfit_experiments import \
   skewfit_full_experiment
from rlkit.torch.vae.conv_vae import imsize48_default_architecture
import copy
import sys


if __name__ == "__main__":
    if len(sys.argv) >= 2:
        id = int(sys.argv[-2])
        exp_name = str(sys.argv[-1])

    variant = dict(
        algorithm='Skew-Fit-SAC',
        double_algo=False,
        online_vae_exploration=False,
        imsize=48,
        env_id='PlaygroundRGB-v1',
        base_env_kwargs=dict(
            continuous=True,

            # debug_mode=True,
            # available_room="OBJECTS",
            # rooms_mode=False,
            static_eval_set_size=25,

            debug_mode=False,
            colored_start_room=True,
            air_wrong_room_distance=True,
            rooms_mode=True,
            debug_rooms_mode=True,
            # goal_generation_rooms=("START", "OBJECTS", "RANDOM"),
            goal_generation_rooms=("OBJECTS",),
            available_rooms=("START", "OBJECTS", "RANDOM", "TV"),
            number_of_random_room_colors=5,
            number_of_TV_room_colors=5,
            doors=True,
            door_size=0.5,
            teleport_random_room=False,
            random_color_per_rollout=True,
            random_ball_per_rollout=False,
            noise_random_color=False,
            corridor_random_room=False,
            random_ball_location=False,

            random_TV_location=True,
            random_TV_location_per_rollout=False,
            TV_color_per_rollout=True,

            white_obj_room=False,
            gray_obj_room=True,
            block_dog=True,
            grow=False,
            block_water=False,
            deterministic_obj=True,
            whiten=True,

            use_big_margin=False,
            use_medium_margin=False,
            use_custom_margin=True,
            custom_margin_value=0.75,

            use_small_objs=False,
            use_huge_objs=False,
            use_custom_objs=True,
            custom_objs_ratio=1.3,

            red_gripper=True,
            static=True,
            smooth_colors=False,

            pos_step_size=0.5,
        ),
        init_camera=None,
        skewfit_variant=dict(
            save_video=True,
            save_video_period=50,
            custom_goal_sampler='replay_buffer',
            online_vae_trainer_kwargs=dict(
                beta=20,
                lr=1e-3,
            ),
            qf_kwargs=dict(
                hidden_sizes=[400, 300],
            ),
            policy_kwargs=dict(
                hidden_sizes=[400, 300],
            ),
            # max_path_length=100,
            # max_path_length=50,
            max_path_length=5,  # todo: DEBUG

            algo_kwargs=dict(
                batch_size=1024,
                # num_epochs=1000,
                num_epochs=1000,
                # num_eval_steps_per_epoch=500,
                # num_eval_steps_per_epoch=1000,  # 20 goals
                # num_expl_steps_per_train_loop=500,
                # num_trains_per_train_loop=1000,
                # min_num_steps_before_training=10000,
                # vae_training_schedule=vae_schedules.custom_schedule,

                # # todo: DEBUG
                num_eval_steps_per_epoch=25*5,
                num_expl_steps_per_train_loop=100,
                num_trains_per_train_loop=100,
                min_num_steps_before_training=10,
                vae_training_schedule=vae_schedules.debug_schedule,

                oracle_data=False,
                vae_save_period=50,
                parallel_vae_train=False
            ),
            twin_sac_trainer_kwargs=dict(
                reward_scale=1,
                discount=0.99,
                soft_target_tau=1e-3,
                target_update_period=1,
                use_automatic_entropy_tuning=True,
            ),
            replay_buffer_kwargs=dict(
                start_skew_epoch=3,
                max_size=int(100000),
                fraction_goals_rollout_goals=0.2,
                fraction_goals_env_goals=0.5,
                hard_prior=True,

                exploration_rewards_type='None',
                vae_priority_type='vae_prob',
                priority_function_kwargs=dict(
                    sampling_method='importance_sampling',
                    decoder_distribution='gaussian_identity_variance',
                    num_latents_to_sample=10,
                ),

                # # TODO: DEBUG!
                # exploration_rewards_type='count_based',
                # vae_priority_type='count_based',


                # TODO: DEBUG!
                # power=-1,
                power=-0.75,
                # power=-0.25,
                # power=0,

                # use_log_probs=True,
                dump_reprs=False,
                replacement=False,
                regions=True,
                expert_clusters=False,
                expert_LP=False,
                cluster_space="clustering_vae",
                clustering="aaic",
                n_clusters=15,
                lp_history_size=None,
                clamp_by_initial=True,
                uniform_LP=False,
                custom_expert_LP=False,
                normalize_by_mean_of_vars=False,
                relabeling_goal_sampling_mode='custom_goal_sampler',
                cluster_dqn_train=False,
            ),
            exploration_goal_sampling_mode='custom_goal_sampler',
            # exploration_goal_sampling_mode='env',
            # evaluation_goal_sampling_mode='presampled',
            evaluation_goal_sampling_mode='static_eval',
            training_mode='train',
            testing_mode='test',
            reward_params=dict(
                type='latent_distance',
            ),
            observation_key='latent_observation',
            desired_goal_key='latent_desired_goal',
            presampled_goals_path=osp.join(
                osp.dirname(mwmj.__file__),
                "goals",
                "door_goals.npy",
            ),
            # presample_goals=True,
            presample_goals=False,
            vae_wrapped_env_kwargs=dict(
                sample_from_true_prior=True,
            ),
        ),
        train_vae_variant=dict(
            representation_size=16,
            beta=20,
            num_epochs=0,
            dump_skew_debug_plots=False,
            decoder_activation='gaussian',
            # decoder_activation='sigmoid',
            generate_vae_dataset_kwargs=dict(
                N=2,
                test_p=.9,
                use_cached=True,
                show=False,
                oracle_dataset=False,
                n_random_steps=1,
                non_presampled_goal_img_is_garbage=True,
            ),
            vae_kwargs=dict(
                decoder_distribution='gaussian_identity_variance',
                input_channels=3,
                architecture=imsize48_default_architecture,
            ),
            algo_kwargs=dict(
                lr=1e-3,
            ),
            save_period=50,
        ),
    )

    search_space = {
    }
    sweeper = hyp.DeterministicHyperparameterSweeper(
        search_space, default_parameters=variant,
    )

    n_seeds = 1
    mode = 'local'
    exp_prefix = 'dev-{}'.format(
        __file__.replace('/', '-').replace('_', '-').split('.')[0]
    )

    for exp_id, variant in enumerate(sweeper.iterate_hyperparameters()):

        variant_current = copy.deepcopy(variant)

        for s in range(n_seeds):
            run_experiment(
                skewfit_full_experiment,
                # exp_prefix="door_buffer_wo_repl_pow_1",
                # exp_prefix="door_buffer_START_RANDOM_OBJECTS_75_margin_wo_repl_pow_00_huge_objs",
                # exp_prefix="test_sequential_{}_{}".format(id, exp_name),
                exp_prefix="TEST",
                # exp_prefix="LP_measure_clusters_sampling",
                mode=mode,
                variant=variant_current,
                use_gpu=torch.cuda.is_available(),
            )

