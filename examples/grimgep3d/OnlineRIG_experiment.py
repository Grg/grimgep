import os.path as osp
import torch

import multiworld.envs.mujoco as mwmj

import rlkit.util.hyperparameter as hyp
# from multiworld.envs.mujoco.cameras import sawyer_door_env_camera_v0
from rlkit.launchers.launcher_util import run_experiment
import rlkit.torch.vae.vae_schedules as vae_schedules
from rlkit.launchers.skewfit_experiments import \
    skewfit_full_experiment
from rlkit.torch.vae.conv_vae import imsize48_default_architecture
from itertools import product
import copy
import sys


if __name__ == "__main__":
    assert len(sys.argv) >= 3
    id = int(sys.argv[-3])
    exp_name = str(sys.argv[-2])
    n_seeds = int(sys.argv[-1])

    # we only want one sequential seed
    assert n_seeds == 1
    print("id_{}_exp_name_{}_seq_n_seeds_{}".format(id, exp_name, n_seeds))

    variant = dict(
        algorithm='Skew-Fit-SAC',
        double_algo=False,
        online_vae_exploration=False,
        imsize=48,
        env_id='MiniWorld-ThreeRoomsTVRicher-v0',
        # env_id='MiniWorld-ThreeRoomsTV-v0',
        # env_id='MiniWorld-OfficeRoom-v0',
        base_env_kwargs=dict(
            TV_active=...,
            TV_type=...,
            auto_change_image_prob=0.1,
            # static_initial_position=False,
            static_initial_position=True,
            start_room="TV",
            # is this parameter needed? used for generating DONE
            # intentionally set to > m_st so that no DONEs appear
            max_episode_steps=100,

            # we'll need this for clustering
            # obs_width=320,
            # obs_height=320,

            obs_width=48,
            obs_height=48,
            window_width=800,
            window_height=600,
            domain_rand=False,
            static_eval_set_size=...,
        ),
        init_camera=None,
        skewfit_variant=dict(
            render=False,
            save_video=True,
            save_video_period=50,
            custom_goal_sampler='replay_buffer',
            online_vae_trainer_kwargs=dict(
                beta=20,
                lr=1e-3,
            ),
            qf_kwargs=dict(
                hidden_sizes=[400, 300],
            ),
            policy_kwargs=dict(
                hidden_sizes=[400, 300],
            ),
            max_path_length=...,
            algo_kwargs=dict(
                batch_size=1024,
                num_epochs=2000,
                # num_eval_steps_per_epoch=20*50,  # 20 goals
                # num_expl_steps_per_train_loop=10*50,
                # min_num_steps_before_training=200*50,
                num_eval_steps_per_epoch=...,  # 20 goals
                num_expl_steps_per_train_loop=...,
                min_num_steps_before_training=...,
                num_trains_per_train_loop=1000,
                vae_training_schedule=vae_schedules.custom_schedule_3,
                oracle_data=False,
                vae_save_period=50,
                parallel_vae_train=False,
                algo_snapshot_interval=700,
            ),
            twin_sac_trainer_kwargs=dict(
                reward_scale=1,
                discount=0.99,
                soft_target_tau=1e-3,
                target_update_period=1,
                use_automatic_entropy_tuning=True,
            ),
            replay_buffer_kwargs=dict(
                start_skew_epoch=20,
                dump_reprs=False,
                # start_skew_epoch=100,
                max_size=int(100000),
                fraction_goals_rollout_goals=0.2,
                fraction_goals_env_goals=0.5,
                exploration_rewards_type='None',
                vae_priority_type='vae_prob',
                priority_function_kwargs=dict(
                    sampling_method='importance_sampling',
                    decoder_distribution='gaussian_identity_variance',
                    num_latents_to_sample=10,
                ),
                power=...,
                replacement=False,
                regions=...,
                n_clusters=...,
                aaic_possible_ns=...,
                expert_LP=...,
                lp_history_size=...,
                hard_cluster_sample=False,
                alp_gmm=False,
                alp_gmm_k=1,
                hard_prior=True,
                clamp_by_initial=True,
                cluster_space=...,
                reshaped_img_size=2,
                lp_temp=...,
                clustering=...,
                pca_n=...,
                uniform_LP=...,
                custom_expert_LP=False,
                normalize_by_mean_of_vars=False,
                relabeling_goal_sampling_mode='custom_goal_sampler',
                cluster_dqn_train=...,
            ),
            exploration_goal_sampling_mode='custom_goal_sampler',
            evaluation_goal_sampling_mode='static_eval',
            training_mode='train',
            testing_mode='test',
            reward_params=dict(
                type='latent_distance',
            ),
            observation_key='latent_observation',
            desired_goal_key='latent_desired_goal',
            presampled_goals_path=osp.join(
                osp.dirname(mwmj.__file__),
                "goals",
                "door_goals.npy",
            ),
            presample_goals=False,
            vae_wrapped_env_kwargs=dict(
                sample_from_true_prior=True,
            ),
        ),
        train_vae_variant=dict(
            representation_size=16,
            beta=20,
            num_epochs=0,
            dump_skew_debug_plots=False,
            decoder_activation='gaussian',
            # decoder_activation='sigmoid',
            generate_vae_dataset_kwargs=dict(
                N=2,
                test_p=.9,
                use_cached=True,
                show=False,
                oracle_dataset=False,
                n_random_steps=1,
                non_presampled_goal_img_is_garbage=True,
            ),
            vae_kwargs=dict(
                decoder_distribution='gaussian_identity_variance',
                input_channels=3,
                architecture=imsize48_default_architecture,
            ),
            algo_kwargs=dict(
                lr=1e-3,
            ),
            save_period=50,
        ),
    )

    mode = 'local'

    power = [0.0]

    # apprs = ["rig", "cl_UNI", "cl_LP"]
    apprs = ["rig", "cl_LP"]

    tv_acts = [True]
    tv_types = ["imagenet_very_small"]

    pca_ns = [25]
    ns_clusters = [25]

    history_sizes = [50]

    experiment_tag = "_V2_"
    configurations = []
    for appr in apprs:
        for pow in power:
            for tv_act in tv_acts:
                for tv_type in tv_types:
                    for pca_n in pca_ns:
                        for n_clusters in ns_clusters:
                            for history_size in history_sizes:
                                c = (pca_n, n_clusters, tv_act, tv_type, appr, pow, history_size)
                                configurations.append(c)

    assert len(configurations) == 2
    configurations = configurations * 20  # max twenty seeds

    pca_n, n_clusters, tv_active, tv_type, appr, pow, history_size = configurations[id]
    print("configuration:", configurations[id])
    print(
        "\t pca_n {}\n\t n_cluster {}\n\t tv_active {}\n\t tv_type {}\n\t appr {}\n\t pow {}\n\t history_size {}\n".format(
            pca_n, n_clusters, tv_active, tv_type, appr, pow, history_size
        )
    )

    # n_clusters = 25
    # pca_n = 25
    clustering = "pca_gmm"
    cluster_space = "pretrained_features"
    m_st = 50

    eval_set_size = 20
    variant['skewfit_variant']['max_path_length'] = m_st
    variant['skewfit_variant']['algo_kwargs']['num_eval_steps_per_epoch'] = eval_set_size * m_st  # 25 goals
    variant['skewfit_variant']['algo_kwargs']['num_expl_steps_per_train_loop'] = 10 * m_st
    variant['skewfit_variant']['algo_kwargs']['min_num_steps_before_training'] = 200 * m_st
    variant['base_env_kwargs']['static_eval_set_size'] = eval_set_size
    variant['base_env_kwargs']['TV_active'] = tv_active
    variant['base_env_kwargs']['TV_type'] = tv_type

    assert "cb" not in appr
    if appr == "rig":
        pow = 0
    elif appr == "skf":
        pass

    assert pow <= 0.0
    variant['skewfit_variant']['replay_buffer_kwargs']['power'] = pow
    variant['skewfit_variant']['replay_buffer_kwargs']['cluster_space'] = cluster_space
    variant['skewfit_variant']['replay_buffer_kwargs']['regions'] = appr not in ["rig", "cb", "skf"]
    variant['skewfit_variant']['replay_buffer_kwargs']['expert_LP'] = False
    variant['skewfit_variant']['replay_buffer_kwargs']['uniform_LP'] = "cl_UNI" in appr
    variant['skewfit_variant']['replay_buffer_kwargs']['cluster_dqn_train'] = False

    assert "cb" not in appr
    assert "skf" not in appr
    if appr in ["rig", "cb", "skf"]:
        n_clusters = 1

    assert appr in ["rig", "skf", "cl_UNI", "cl_LP"]

    variant['skewfit_variant']['replay_buffer_kwargs']['n_clusters'] = n_clusters
    variant['skewfit_variant']['replay_buffer_kwargs']['aaic_possible_ns'] = None
    variant['skewfit_variant']['replay_buffer_kwargs']['lp_history_size'] = history_size
    variant['skewfit_variant']['replay_buffer_kwargs']['clustering'] = clustering
    variant['skewfit_variant']['replay_buffer_kwargs']['pca_n'] = pca_n

    lp_temp = 5
    variant['skewfit_variant']['replay_buffer_kwargs']['lp_temp'] = lp_temp

    exp_prefix = exp_name + experiment_tag + "_{}_pca_n_{}_n_clus_{}_tv_{}_pow_{}_tv_act_{}_history_{}".format(
        appr, pca_n, n_clusters,
        tv_type, abs(pow), tv_active,
        history_size
    )

    for _ in range(n_seeds):
        variant_current = copy.deepcopy(variant)
        run_experiment(
            skewfit_full_experiment,
            exp_prefix=exp_prefix,
            mode=mode,
            variant=variant_current,
            use_gpu=torch.cuda.is_available(),
       )