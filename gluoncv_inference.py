import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from itertools import chain
import gym
import time
import envs.gym_miniworld
import hashlib
from mpl_toolkits.axes_grid1 import ImageGrid

from sklearn.decomposition import PCA
from sklearn.mixture import GaussianMixture
from pathlib import Path
import argparse
import numpy as np
from PIL import Image
import torch
import pickle

from gluoncv import model_zoo, data, utils
from gluoncv.data.transforms.presets.imagenet import transform_eval as imagenet_transform_eval
from mxnet import image, nd

from sklearn.cluster import KMeans
from collections import defaultdict
from collections import Counter

def hashmd5(s):
    return hashlib.md5(s.encode('utf-8')).hexdigest()

def any_of_ents_prob(ent_names_list, int_rews_normalized, cluster_vis_ent_names):
    if ent_names_list is None:
        goals_have_ent = np.array([len(vents) == 0 for vents in cluster_vis_ent_names])

    else:
        goals_have_ent = np.array([
            any([
                en in vents for en in ent_names_list
            ]) for vents in cluster_vis_ent_names
        ])

    return np.dot(int_rews_normalized, goals_have_ent)


def evaluate_clustering(pred_cls, expert_clusters, output_dir, draw_clusters=False):
    pred_cls = np.array(pred_cls)
    expert_clusters = np.array(expert_clusters)

    separation = defaultdict(list)
    clus_precision = defaultdict(float)

    for c in np.unique(pred_cls):
        if c < 0: continue
        separation[c] = list(np.where(pred_cls == c)[0])
        most_common_exp_cl, n_occurances = Counter(expert_clusters[separation[c]]).most_common(1)[0]
        prec = n_occurances / len(separation[c])
        clus_precision[c] = prec

        if draw_clusters:
            # todo: vertical labels
            counter = Counter(expert_clusters[separation[c]])

            ks = list(counter.keys())

            ids, _ = zip(*enumerate(ks))
            plt.bar(
                ids,
                [counter[k] for k in ks],
                align='center'
            )
            plt.xticks(ids, ks, rotation=90)

            cluster_image_path = output_dir / "cluster_{}".format(c)
            plt.savefig(cluster_image_path, dpi=300)
            plt.clf()
            plt.close()
            print("saved hist: ", cluster_image_path)

    print("clustering precision:", np.mean(list(clus_precision.values())))

    return clus_precision

def simulate_sampling(pred_cls, expert_clusters):
    ...


def generate_images_for_replay_buffer(buffer_path, im_size, overwrite_cache=False):
    buffer_name = Path(args.replay_buffer_path).with_suffix("").name

    H, W = im_size, im_size
    env = gym.make(
        'MiniWorld-ThreeRoomsTVRicher-v0',
        # is this parameter needed? used for generating DONE
        # intentionally set to > m_st so that no DONEs appear
        # max_episode_steps=100,
        # TV_active=True,
        # TV_type="imagenet_very_small",
        # obs_width=W,
        # obs_height=H,
        # # window_width=W,
        # # window_height=H,
        # domain_rand=False,
        # static_eval_set_size=20,
        # static_initial_position=True,
        # start_room="TV"

        TV_active=True,
        TV_type="imagenet_very_small",
        auto_change_image_prob=0.1,
        # static_initial_position=False,
        static_initial_position=True,
        start_room="TV",
        max_episode_steps=100,
        obs_width=W,
        obs_height=H,
        window_width=W,
        window_height=H,
        domain_rand=False,
        static_eval_set_size=20,
    )
    env.reset()

    buffer = np.load(buffer_path, allow_pickle=True).item()
    replay_buffer_expert_states_loaded = [env.vector_to_state_dict(s) for s in buffer['expert_states']]
    print("{} expert states to load loaded".format(len(replay_buffer_expert_states_loaded)))


    print("env_loaded")
    env_cache = "dummy_env_cache"
    images_dir = Path("./replay_buffer_visualization/{}_{}_{}".format(buffer_name, H, env_cache))
    images_dir.mkdir(exist_ok=True)
    pickle_path = images_dir / 'replay_buffer_exp_states.pickle'

    if (images_dir.exists() and len(list(images_dir.glob("obs_*.jpeg"))) == len(replay_buffer_expert_states_loaded) and pickle_path.exists()) and not overwrite_cache:
        # images_already_generated
        print("Images for {} already generated".format(images_dir))
        with open(pickle_path, 'rb') as file:
                replay_buffer_expert_states = pickle.load(file)
    else:
        replay_buffer_expert_states = []
        for id, state in enumerate(replay_buffer_expert_states_loaded):
            print("image generated: [{}/{}]".format(id, len(replay_buffer_expert_states_loaded)))
            env.set_context(state)

            replay_buffer_expert_states.append(env.create_info_d())
            image = env.get_image(W, H)
            im = Image.fromarray(image)

            image_path = images_dir / ("obs_"+str(id)+".jpeg")
            im.save(image_path)

        with open(pickle_path, 'wb') as file:
            pickle.dump(replay_buffer_expert_states, file, protocol=pickle.HIGHEST_PROTOCOL)



    expert_clusters = [str(env.goal_dict_to_ent_names(es)) for es in replay_buffer_expert_states]
    replay_buffer_vis_ent_names = [env.goal_dict_to_ent_names(es) for es in replay_buffer_expert_states]
    replay_buffer_intrinsic_rewards = buffer["intrinsic_rewards"]

    print("Num expert clusters : {}".format(len(set(expert_clusters))))

    return images_dir, expert_clusters, env_cache, replay_buffer_vis_ent_names, replay_buffer_intrinsic_rewards


def generate_images_for_sampled_goals(buffer_path, im_size):
    buffer_name = Path(args.replay_buffer_path).with_suffix("").name

    H, W = im_size, im_size
    env = gym.make(
        'MiniWorld-ThreeRoomsTVRicher-v0',
        # is this parameter needed? used for generating DONE
        # intentionally set to > m_st so that no DONEs appear
        # max_episode_steps=100,
        # TV_active=True,
        # TV_type="imagenet_very_small",
        # obs_width=W,
        # obs_height=H,
        # # window_width=W,
        # # window_height=H,
        # domain_rand=False,
        # static_eval_set_size=20,
        # static_initial_position=True,
        # start_room="TV"

        TV_active=True,
        TV_type="imagenet_very_small",
        auto_change_image_prob=0.1,
        # static_initial_position=False,
        static_initial_position=True,
        start_room="TV",
        max_episode_steps=100,
        obs_width=W,
        obs_height=H,
        window_width=W,
        window_height=H,
        domain_rand=False,
        static_eval_set_size=20,
    )
    env.reset()

    buffer = np.load(buffer_path, allow_pickle=True).item()
    replay_buffer_expert_goals_loaded = buffer['history']['goal_expert_dicts']

    print("env_loaded")
    env_cache = "dummy_env_cache"
    images_dir = Path("./replay_buffer_visualization/goals_{}_{}_{}".format(buffer_name, H, env_cache))
    images_dir.mkdir(exist_ok=True)
    pickle_path = images_dir / 'replay_buffer_exp_goals.pickle'

    if images_dir.exists() and len(list(images_dir.glob("obs_*.jpeg"))) == len(replay_buffer_expert_goals_loaded) and pickle_path.exists():
        # images_already_generated
        print("Images for {} already generated".format(images_dir))
        with open(pickle_path, 'rb') as file:
            goals_expert_states = pickle.load(file)
    else:
        goals_expert_states = []
        for id, state in enumerate(replay_buffer_expert_goals_loaded):
            print("goals generated: [{}/{}]".format(id, len(replay_buffer_expert_goals_loaded)))
            env.set_context(state)

            goals_expert_states.append(env.create_info_d())
            image = env.get_image(W, H)
            im = Image.fromarray(image)

            image_path = images_dir / ("obs_"+str(id)+".jpeg")
            im.save(image_path)

        with open(pickle_path, 'wb') as file:
            pickle.dump(goals_expert_states, file, protocol=pickle.HIGHEST_PROTOCOL)

    goals_vis_ent_names = [env.goal_dict_to_ent_names(es) for es in goals_expert_states]
    goals_performances = buffer["history"]["vae_perfs"]

    return images_dir, env_cache, goals_vis_ent_names, goals_expert_states, goals_performances

def calc_lats(net, images_path, ids, output_dir=None):
    iter = 0
    lat_xs = []
    batch_size = 5
    if len(ids) % batch_size != 0:
        # fallback
        batch_size = 1

    s = time.time()
    inf_times = []
    for batch_id_start in ids[::batch_size]:
        e = time.time()
        print("time:", e-s)
        batch_ids = list(range(batch_id_start, batch_id_start+batch_size))
        image_paths = [images_path / Path("obs_" + str(id) + ".jpeg") for id in batch_ids]
        print("iter: ", iter, " image: ", image_paths[0])
        iter += 1
        
        if "yolo" in args.model:
            x, orig_img = data.transforms.presets.yolo.load_test(map(str, image_paths), short=args.size)
        
        elif "rcnn" in args.model:
            x, orig_img = data.transforms.presets.rcnn.load_test(map(str, image_paths), short=args.size)
        
        else:
            raise DeprecationWarning()
            # orig_img = image.imread(str(image_path))
            # x = imagenet_transform_eval(orig_img)

        detection_model = "yolo" in net.name or "rcnn" in net.name  # todo: does this work ?
        
        if detection_model:
            # detection
            assert "yolo" in net.name
            start = time.time()
            lat_x = net.stages(
                nd.concatenate(x) if batch_size != 1 else x
            ).mean([-1, -2]).squeeze()
            end = time.time()

            inf_time = (end-start)/batch_size
            inf_times.append(inf_time)
            avg_inf_time = np.mean(inf_times)
            # 100 -> 0.0005
            # 10 -> 0.002
            # 2 -> 0.009
            print("image time(bs={}): {}".format(batch_size, avg_inf_time))

            if batch_size != 1:
                lat_xs.extend(lat_x.asnumpy())
            else:
                lat_xs.append(lat_x.asnumpy())

            if args.draw:
                # uncomment to plot results
                box_ids, scores, bboxes = net(x)
                fig = plt.figure()
                ax = fig.add_subplot(111)
                
                utils.viz.plot_bbox(orig_img, bboxes[0], scores[0], box_ids[0], class_names=net.classes, thresh=0.1,
                                    ax=ax)
                
                output_path = output_dir / image_path.name
                print("savefig at: ", output_path)
                fig.savefig(str(output_path))
                plt.close(fig)
        else:
            raise DeprecationWarning()
            res = net(x)
            res = res.asnumpy().reshape(-1)
            cls = net.classes[res.argmax()]
            conf = res.max()
            title = "cls: {} conf: {}".format(cls, conf)
            print(title)
            
            if args.draw:
                plt.imshow(orig_img)
                plt.title(title)
                plt.show()

        s = time.time()
    return np.array(lat_xs)


def construct_PCA(lat_xs, n_components=2):
    # PCA
    s = time.time()
    pca = PCA(n_components=n_components)
    lat_xs = np.array(lat_xs)
    res_pca = pca.fit_transform(lat_xs)
    e = time.time()
    print("pca time:", e-s)
    return res_pca, pca


def draw_pca(ids, res_pca, output_dir):
    plt.scatter(ids, res_pca[:, 0], c=res_pca[:, 1], s=1)
    output_path = output_dir / "latent"
    plt.savefig(str(output_path), dpi=400)
    plt.clf()
    
    plt.scatter(res_pca[:, 0], res_pca[:, 1], c=ids, s=1)
    output_path = output_dir / "latent_time"
    plt.savefig(str(output_path), dpi=400)
    plt.clf()


def cluster_latents_KM(lat_xs, n_clusters, drop_ratio, pca_n=None):
    # possible_ns = list(range(1, 20, 2))

    if pca_n is not None and pca_n > 0:
        lat_xs, pca = construct_PCA(lat_xs, n_components=pca_n)

    kmean = KMeans(n_clusters=n_clusters)
    predicted_clusters = kmean.fit_predict(lat_xs)

    # drop low prob
    k = round(len(lat_xs) * drop_ratio)
    distances = np.sum((kmean.cluster_centers_[predicted_clusters] - lat_xs) ** 2, axis=1)
    scores = 1.0 / (distances+1e-5)
    bottom_k_ind = np.argpartition(scores, k)[:k]
    predicted_clusters[bottom_k_ind] = -1


    # drop clusters
    # cluster_drop_ratio = 0.1
    # unique_clusters, counts = np.unique(predicted_clusters, return_counts=True)
    # n_drop = int(np.floor(len(unique_clusters)*cluster_drop_ratio))
    # drop_clusters = set(np.argsort(counts)[-n_drop:])
    # predicted_clusters = [-1 if c in drop_clusters else c for c in predicted_clusters]
    cluster_algo = lambda x: kmean.predict(pca.transform(x))

    return predicted_clusters, n_clusters, cluster_algo


def cluster_latents(lat_xs, n_clusters, drop_ratio, pca_n=None):
    # possible_ns = list(range(1, 20, 2))
    if pca_n is not None and pca_n > 0:
        lat_xs, pca = construct_PCA(lat_xs, n_components=pca_n)

    possible_ns = [n_clusters]
    # tol = 1e-4
    # clus_max_iter = 1000

    # tol = 1e-1
    # clus_max_iter = 10
    tol = 1e-2
    clus_max_iter = 100

    gmms = [GaussianMixture(
        n_components=n, max_iter=clus_max_iter, tol=tol, verbose=0,
    ).fit(lat_xs) for n in possible_ns]
    
    fitnesses = []
    d = 3
    params_per_gmm = (d * d - d) / 2 + 2 * d + 1
    n = 20 * params_per_gmm - 1
    
    def get_nb_gmm_params(gmm):
        # assumes full covariance
        # see https://stats.stackexchange.com/questions/229293/the-number-of-parameters-in-gaussian-mixture-model
        nb_gmms = gmm.get_params()['n_components']
        params_per_gmm = (d * d - d) / 2 + 2 * d + 1
        return nb_gmms * params_per_gmm - 1
    
    for l, m in enumerate(gmms):
        k = get_nb_gmm_params(m)
        penalty = (2 * k * (k + 1)) / (n - k - 1)
        # fitnesses.append(0.1*m.aic(X_train) + 0.9*penalty)
        fitnesses.append(m.aic(lat_xs) + penalty)
    
    gmm = gmms[np.argmin(fitnesses)]
    n_clusters = gmm.n_components
    predicted_clusters = gmm.predict(lat_xs)

    # drop low prob
    k = round(len(lat_xs)*drop_ratio)
    scores = gmm.score_samples(lat_xs)
    bottom_k_ind = np.argpartition(scores, k)[:k]
    predicted_clusters[bottom_k_ind] = -1

    cluster_algo = lambda x: gmm.predict(pca.transform(x))

    # drop clusters
    # cluster_drop_ratio = 0.1
    # unique_clusters, counts = np.unique(predicted_clusters, return_counts=True)
    # n_drop = int(np.floor(len(unique_clusters)*cluster_drop_ratio))
    # drop_clusters = set(np.argsort(counts)[-n_drop:])
    # predicted_clusters = [-1 if c in drop_clusters else c for c in predicted_clusters]

    return predicted_clusters, n_clusters, cluster_algo


def draw_clusters(ids, pred_cls, output_dir, n_clusters, images_path):
    plt.scatter(ids, pred_cls, s=1)
    output_path = output_dir / "latent_clusters_time"
    print(output_path)
    plt.grid(True)
    plt.savefig(str(output_path), dpi=400)
    plt.clf()
    # # cluster grids
    # for c_id in range(n_clusters):
    #     if c_id < 0: continue  # skip dropped
    #     c_ids = np.array(ids)[pred_cls == c_id]
    #
    #     if len(c_ids) == 0:
    #         continue
    #
    #     rows = int(np.ceil(np.sqrt(len(c_ids))))
    #     cols = rows + 1
    #
    #     fig = plt.figure(figsize=(rows, cols))
    #     grid = ImageGrid(fig, 111, nrows_ncols=(rows, cols), axes_pad=0.1)
    #
    #     for ax, im_id in zip(grid, c_ids):
    #         image_path = images_path / Path("obs_" + str(im_id) + ".jpeg")
    #         _, orig_img = data.transforms.presets.yolo.load_test(str(image_path), short=args.size)
    #         ax.imshow(orig_img)
    #
    #     output_path = output_dir / "cluster_{}".format(c_id)
    #
    #     plt.savefig(output_path, dpi=500)
    #     print("saved c_id: ", c_id)
    #     plt.clf()
    #     plt.close(fig)
    #
    # cluster grids
    for c_id in range(n_clusters):
        if c_id < 0: continue  # skip dropped
        c_ids = np.array(ids)[pred_cls == c_id]

        if len(c_ids) == 0:
            continue

        for im_id in c_ids:
            image_path = images_path / Path("obs_" + str(im_id) + ".jpeg")
            _, orig_img = data.transforms.presets.yolo.load_test(str(image_path), short=args.size)
            plt.imshow(orig_img)

            cluster_dir = output_dir / "cluster_{}".format(c_id)
            cluster_dir.mkdir(exist_ok=True)

            output_path = cluster_dir / "im_{}".format(im_id)

            plt.savefig(output_path, dpi=50)
            plt.clf()
            plt.close()

        print("saved for c_id: ", c_id)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    # parser.add_argument('--dataset-dir', type=str, default='./observations/MiniWorld-ThreeRoomsTVRicher-v0_H_320_overlay')
    # parser.add_argument('--replay-buffer-path', type=str, default='./replay_buffer_saved/replay_buffer_embeddings_400.npy')
    # parser.add_argument('--replay-buffer-path', type=str, default='./replay_buffer_saved/replay_buffer_embeddings_100.npy')
    parser.add_argument('--replay-buffer-path', type=str, default='./replay_buffer_saved/replay_buffer_embeddings_240.npy')
    parser.add_argument('--model', type=str, help='choose a model from https://cv.gluon.ai/model_zoo/index.html', default='yolo3_darknet53_coco')
    # parser.add_argument('--model', type=str, help='choose a model from https://cv.gluon.ai/model_zoo/index.html', default='ResNet18_v1')
    parser.add_argument('--size', type=int, help='size of the shorter side for detection (recommended: 320, 416, 608)', default=48)
    parser.add_argument('--n-clusters', type=int, help='number of clusters', default=25)
    parser.add_argument('--temp', type=int, help='number of clusters', default=5)
    parser.add_argument('--drop-ratio', type=float, help='drop ratio', default=0.0001)
    parser.add_argument('--pca-n', type=int, help='number of pca dimensions', default=25)
    parser.add_argument('--history-size', type=int, help='history len', default=None)
    parser.add_argument('--top_view', action='store_true', help='show the top view instead of the agent view')
    parser.add_argument('--no-LP', action='store_true', help='no LP calculation', default=False)
    parser.add_argument('--km', action='store_true', help='show the top view instead of the agent view', default=False)
    parser.add_argument('--draw', action='store_true', help='display results', default=False)
    parser.add_argument('--draw-clusters', action='store_true', help='draw clusters', default=False)
    parser.add_argument('--overwrite-cache', action='store_true', help='ignore cache', default=False)

    args = parser.parse_args()

    buffer_name = Path(args.replay_buffer_path).with_suffix("").name
    print("buffer_name:", buffer_name)

    use_LP = not args.no_LP

    # generate images from replay_buffer
    images_path, expert_clusters, env_cache, replay_buffer_vis_ent_names, replay_buffer_intrinsic_rewards \
        = generate_images_for_replay_buffer(args.replay_buffer_path, args.size, args.overwrite_cache)


    # images_path = Path("./replay_buffer_visualization/{}".format(buffer_name))
    # assert images_path == images_path_

    # todo: use listdir
    ids = sorted([int(str(p).split("_")[-1].split(".")[0]) for p in list(images_path.glob("obs_*jpeg"))])

    output_dir = Path('./clustering_results/') / "{}_{}_{}".format(buffer_name, args.size, env_cache)
    output_dir.mkdir(exist_ok=True)

    # load model
    net = model_zoo.get_model(args.model, pretrained=True)

    print("calc lats")

    dat_hash = hashmd5(str(net)) + hashmd5(str(images_path)) + hashmd5(str(ids))
    cache_path = "./cache/calc_lats_{}".format(dat_hash)

    if Path(cache_path+".npy").exists() and not args.overwrite_cache:
        print("loading_from: ", cache_path+".npy")
        lat_xs = np.load(cache_path+".npy")
    else:
        lat_xs = calc_lats(net, images_path, ids, output_dir)
        np.save(cache_path, lat_xs)
        print("saved to: ", cache_path+".npy")


    # clustering
    print("clustering")
    s = time.time()
    if args.km:
        pred_cls, n_clusters, cluster_algo = cluster_latents_KM(lat_xs, args.n_clusters, args.drop_ratio, args.pca_n)
    else:
        pred_cls, n_clusters, cluster_algo = cluster_latents(lat_xs, args.n_clusters, args.drop_ratio, args.pca_n)
    e = time.time()
    print("clustering time: {}".format(e-s))


    print("evaluate")
    clustering_precision = evaluate_clustering(pred_cls, expert_clusters, output_dir, args.draw_clusters)

    if use_LP:
        goals_path, env_cache, goals_vis_ent_names, goals_expert_states, goals_performances = generate_images_for_sampled_goals(
            args.replay_buffer_path, args.size)
        goal_ids = sorted([int(str(p).split("_")[-1].split(".")[0]) for p in list(goals_path.glob("obs_*jpeg"))])
        goals_lat_xs = calc_lats(net, goals_path, goal_ids)

        goals_clusters = cluster_algo(goals_lat_xs)
        assert len(goals_clusters) == len(goals_performances)

        # LP estimations
        per_step_performances = np.split(
            np.array(list(zip(goals_clusters, goals_performances))),
            indices_or_sections=len(goals_performances) // 10
        )

        # LP computation from the code

        # performances
        cluster_perf_steps_dicts = []
        for i, (step_cs_ps) in enumerate(per_step_performances):
            # k: cluster_id p: performances from this step
            step_cluster_perf_dict = defaultdict(list)
            for c, p in step_cs_ps:
                step_cluster_perf_dict[c].append(p)

            # whole performance history for the current vae
            cluster_perf_steps_dicts.append(step_cluster_perf_dict)

        cluster_perf_dict = defaultdict(list)
        cluster_var_dict = defaultdict(list)
        for step_perf_dict in cluster_perf_steps_dicts:
            for c, p in step_perf_dict.items():
                assert type(p) == list
                cluster_perf_dict[c].append(np.mean(p))

        # unstepped
        unstepped_cluster_perf_dict = defaultdict(list)
        for step_perf_dict in cluster_perf_steps_dicts:
            for c, p in step_perf_dict.items():
                assert type(p) == list
                unstepped_cluster_perf_dict[c].extend(p)

        def lp_for_perfs(perfs):
            clamp_by_initial = True
            history_size = args.history_size

            if clamp_by_initial:
                perfs = np.clip(perfs, -np.inf, perfs[0])

            if history_size is not None and history_size > 0:
                perfs = perfs[-history_size:]

            half = max(len(perfs) // 2, 1)
            lp = np.abs(np.mean(perfs[:half]) - np.mean(perfs[-half:]))
            return lp

        # LP
        lp_dict = defaultdict(float)

        for c, perfs in cluster_perf_dict.items():
            lp_dict[c] = lp_for_perfs(perfs)

        temp = args.temp
        lp_dict_orig = lp_dict.copy()
        if temp is not None:
            lp_dict = defaultdict(float, {k: v**temp for k,v in lp_dict.items()})


    print("simulate sampling")
    # todo: move code below to fn ->  simulate_sampling(pred_cls, expert_clusters)
    tv_ons = []
    perc_tv_ons = []
    exp_tv_on_probs = dict()
    exp_TV_ents_probs = dict()
    exp_GALLERY_ents_probs = dict()
    exp_OFFICE_ents_probs = dict()
    exp_no_ents_probs = dict()

    cluster_entities = dict()
    available_ent_names = []
    ent_probs = {}
    for c_id in range(n_clusters):
        ent_probs[c_id]={}

        if c_id < 0: continue  # skip dropped

        c_ids = np.array(ids)[pred_cls == c_id]
        # c_perfs = goals_performances[goals_clusters == c_id]

        if len(c_ids) == 0: continue

        cluster_vis_ent_names = np.array(replay_buffer_vis_ent_names, dtype="object")[c_ids]
        cluster_vis_ent_names = [["no"] if len(vents) == 0 else vents for vents in cluster_vis_ent_names]

        cluster_has_tv_on = any(["tv_ON" in vents for vents in cluster_vis_ent_names])
        cluster_entities[c_id] = cluster_vis_ent_names

        goals_have_tv_on = np.array(["tv_ON" in vents for vents in cluster_vis_ent_names])
        cluster_perc_has_tv_on = goals_have_tv_on.mean()
        int_rews = replay_buffer_intrinsic_rewards[c_ids]
        int_rews_normalized = int_rews/np.sum(int_rews)

        cluster_expected_tv_on_prob = np.dot(int_rews_normalized, goals_have_tv_on)

        TV_ent_names = ["tv_ON", "tv_OFF", "clock", "couch"]
        GALLERY_ent_names = ["impression", "starry_sky", "starry_night", "bench", "wooden_chair"]
        OFFICE_ent_names = ["office_desk","office_chair", "box", "whiteboard", "potted_plant", "airduct_grate"]
        wall_ent_names = ["no"]


        tv_ons.append(cluster_has_tv_on)
        perc_tv_ons.append(cluster_perc_has_tv_on)
        exp_tv_on_probs[c_id] = cluster_expected_tv_on_prob
        exp_TV_ents_probs[c_id] = any_of_ents_prob(
            ent_names_list=TV_ent_names,
            int_rews_normalized=int_rews_normalized,
            cluster_vis_ent_names=cluster_vis_ent_names
        )
        exp_GALLERY_ents_probs[c_id] = any_of_ents_prob(
            ent_names_list=GALLERY_ent_names,
            int_rews_normalized=int_rews_normalized,
            cluster_vis_ent_names=cluster_vis_ent_names
        )
        exp_OFFICE_ents_probs[c_id] = any_of_ents_prob(
            ent_names_list=OFFICE_ent_names,
            int_rews_normalized=int_rews_normalized,
            cluster_vis_ent_names=cluster_vis_ent_names
        )
        exp_no_ents_probs[c_id] = any_of_ents_prob(
            ent_names_list=wall_ent_names,
            int_rews_normalized=int_rews_normalized,
            cluster_vis_ent_names=cluster_vis_ent_names
        )
        assert sum([
            list(d.values())[0] for d in [
                exp_no_ents_probs, exp_TV_ents_probs, exp_OFFICE_ents_probs, exp_GALLERY_ents_probs
        ]]) > 0.99

    if use_LP:
        # draw cluster performances
        r = int(np.sqrt(n_clusters))
        # r=3
        c = n_clusters // r
        if r*c != n_clusters:
            r+=1

        fig = plt.figure(figsize=(c*2, r*2))
        grid = ImageGrid(
            fig, 111,  # similar to subplot(111)
            nrows_ncols=(r, c),  # creates rxc grid of axes
            axes_pad=0.3,  # pad between axes in inch.
            # share_y=True,
            share_all=True,
            aspect=False,
        )

        perf_color = "blue"
        lp_color = "gray"
        for c_id in range(n_clusters):
            # draw plot
            if c_id not in cluster_entities: continue


            perfs = cluster_perf_dict[c_id]
            counter = Counter(chain(*cluster_entities[c_id]))
            most_common_ent, mce_n = counter.most_common(1)[0]
            perc_of_goals_with_mce = mce_n/len(cluster_entities[c_id])
            assert perc_of_goals_with_mce <= 1.0

            ax = grid[c_id]
            # ax.set_title("cl: {}".format(c))
            # num of ent_names
            lp = lp_dict[c_id]

            ax.plot(perfs, c=perf_color, label="Perf")

            lps = [lp_for_perfs(perfs[:i+1]) for i in range(len(perfs))]

            ax.plot(lps, c=lp_color, label="LP", linestyle=":")
            # uns_perfs = unstepped_cluster_perf_dict[c_id]

            ax.set_title("{}_{:0.2f}_LP_{:0.2f}".format(most_common_ent, perc_of_goals_with_mce, lp))
            plt.sca(ax)

        plt.legend(handles=[
            mpatches.Patch(color=lp_color, label='LP'),
            mpatches.Patch(color=perf_color, label='Perf')
        ])

        plt.tight_layout()
        plt.savefig("./clustering_results/cluster_perfs.png")

    max_perc_tv_on = np.mean(tv_ons)

    print("max_perc_tv_on:", max_perc_tv_on)
    print("rig_exp_perc_tv_on:", np.mean(perc_tv_ons))

    if use_LP:
        tot_LP = sum(lp_dict.values())
        print("LP_exp_tv_on_prob:", np.sum([
            (lp_dict[c_id]/tot_LP)*exp_tv_on_probs[c_id] for c_id in range(n_clusters) if c_id in exp_tv_on_probs]
        ))
        print("LP_exp_TV_room:", np.sum([
            (lp_dict[c_id]/tot_LP)*exp_TV_ents_probs[c_id] for c_id in range(n_clusters) if c_id in exp_TV_ents_probs]
        ))
        print("LP_exp_GALLERY_room:", np.sum([
            (lp_dict[c_id]/tot_LP)*exp_GALLERY_ents_probs[c_id] for c_id in range(n_clusters) if c_id in exp_GALLERY_ents_probs]
        ))
        print("LP_exp_OFFICE_room:", np.sum([
            (lp_dict[c_id]/tot_LP)*exp_OFFICE_ents_probs[c_id] for c_id in range(n_clusters) if c_id in exp_OFFICE_ents_probs]
        ))
        print("exp_no_ents:", np.sum([
            (lp_dict[c_id]/tot_LP)*exp_no_ents_probs[c_id] for c_id in range(n_clusters) if c_id in exp_no_ents_probs]
        ))

    print("UNI_exp_tv_on_prob:", np.mean(list(exp_tv_on_probs.values())))
    print("UNI_exp_TV_room:", np.mean(list(exp_TV_ents_probs.values())))
    print("UNI_exp_GALLERY_room:", np.mean(list(exp_GALLERY_ents_probs.values())))
    print("UNI_exp_OFFICE_room:", np.mean(list(exp_OFFICE_ents_probs.values())))
    print("UNI_exp_no_ents:", np.mean(list(exp_no_ents_probs.values())))
