import matplotlib.pyplot as plt
import numpy as np


class GRBFTrajectory(object):
    def __init__(self, n_dims, sigma, steps_per_basis, max_basis):
        self.n_dims = n_dims
        self.sigma = sigma
        self.alpha = - 1. / (2. * self.sigma ** 2.)
        self.steps_per_basis = steps_per_basis
        self.max_basis = max_basis
        self.precomputed_gaussian = np.zeros(2 * self.max_basis * self.steps_per_basis)
        for i in range(2 * self.max_basis * self.steps_per_basis):
            self.precomputed_gaussian[i] = self.gaussian(self.max_basis * self.steps_per_basis, i)

    def gaussian(self, center, t):
        return np.exp(self.alpha * (center - t) ** 2.)

    def trajectory(self, weights):
        n_basis = len(weights) // self.n_dims
        weights = np.reshape(weights, (n_basis, self.n_dims)).T
        steps = self.steps_per_basis * n_basis
        traj = np.zeros((steps, self.n_dims))
        for step in range(steps):
            g = self.precomputed_gaussian[self.max_basis * self.steps_per_basis + self.steps_per_basis - 1 - step::self.steps_per_basis][:n_basis]
            traj[step] = np.dot(weights, g)
        return np.clip(traj, -1., 1.)

    def plot(self, traj):
        plt.plot(traj)
        plt.ylim([-1.05, 1.05])


# trajectory_generator = GRBFTrajectory(4, 3, 6, 5)
# plt.figure()
# plt.plot(trajectory_generator.precomputed_gaussian)
# plt.show()

# m = 2. * np.random.random(20) - 1.
#
# traj = trajectory_generator.trajectory(m)
#
# plt.plot(traj, lw=3)
# plt.legend(["Joint 1",
#             "Joint 2",
#             "Joint 3",
#             "Joint 4"], ncol=2, fontsize=16)
# plt.xlabel("Timesteps", fontsize=20)
# plt.ylabel("Joint angles (deg)", fontsize=20)
# plt.xticks(fontsize=16)
# plt.yticks(fontsize=16)
# # plt.ylim([-185, 185])
#
# plt.show()