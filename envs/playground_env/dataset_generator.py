import sys

sys.path.append('../../')
from src.playground_env.reward_function import sample_descriptions_from_state, get_reward_from_state
from src.playground_env.descriptions import train_descriptions, test_descriptions, extra_descriptions
from src.playground_env.rbf_controllers import GRBFTrajectory
import numpy as np
import os
import gym
import pickle
import time

NB_EPS = 50000
DIM_ACT = 3
NB_STEPS = 50

dataset_name = 'dataset_playground_2obj_no_hard_goals'
if 'flowers' in os.environ['HOME']:
    path = '/home/flowers/Desktop/{}.pk'.format(dataset_name)
elif 'tkarch' in os.environ['HOME']:
    path = '/projets/flowers2/tristan/data/raw/{}.pk'.format(dataset_name)
else:
    path = '/projets/flowers/cedric/data/{}.pk'.format(dataset_name)

trajectory_generator = GRBFTrajectory(n_dims=DIM_ACT,
                                      sigma=3,
                                      steps_per_basis=6,
                                      max_basis=10)

env = gym.make('PlaygroundNavigation-v1')
id2description = dict()
description2id = dict()
id_description = 0

all_descr = train_descriptions + test_descriptions
all_descriptions_ids = []
all_obs = []

times = []
t_i = time.time()
for ep in range(NB_EPS):

    if not (ep + 1) % 500:
        times.append(time.time() - t_i)
        t_i = time.time()
        print((ep - 1) / NB_EPS, ', ETA: ', (NB_EPS - ep) / 500 * np.mean(times))
    m = np.clip(2. * np.random.random(10 * DIM_ACT) - 1., -1, 1)
    actions = trajectory_generator.trajectory(m)
    # actions = np.random.uniform(-1, 1, [NB_STEPS, DIM_ACT])

    obs_episode = []
    descriptions_ids_episode = []

    init_obs = env.reset()
    d = all_descr[ep % len(all_descr)]
    obs_episode.append(env.unwrapped.reset_with_goal(d))
    train_descr, test_descr, extra_descr = sample_descriptions_from_state(obs_episode[-1], method='all', modes=[1, 2])
    sampled_descriptions = train_descr.copy() + test_descr.copy()
    descriptions_ids = []
    for description in sampled_descriptions:
        if description not in id2description.values():
            id2description[id_description] = description
            description2id[description] = id_description
            id_description += 1
        descriptions_ids.append(description2id[description])
    descriptions_ids_episode.append(descriptions_ids)


    for t in range(NB_STEPS):
        out = env.step(actions[t])
        obs_episode.append(out[0].copy())
        train_descr, test_descr, extra_descr = sample_descriptions_from_state(obs_episode[-1], method='all',
                                                                              modes=[1, 2])
        sampled_descriptions = train_descr.copy() + test_descr.copy()
        descriptions_ids = []
        for description in sampled_descriptions:
            if description not in id2description.values():
                id2description[id_description] = description
                description2id[description] = id_description
                id_description += 1
            descriptions_ids.append(description2id[description])
        descriptions_ids_episode.append(descriptions_ids)

    all_descriptions_ids.append(descriptions_ids_episode)
    all_obs.append(obs_episode)


dataset = dict(obs=np.array(all_obs), descriptions_ids=all_descriptions_ids, id2description=id2description,
               description2id=description2id)
with open(path, 'wb') as f:
    pickle.dump(dataset, f, protocol=4)
stop = 1
