from __future__ import division
from itertools import product
import cv2
import copy
import random
import os
import gym
from gym import spaces
from collections import OrderedDict, defaultdict
from multiworld.envs.env_util import create_stats_ordered_dict, get_stat_in_paths
from enum import Enum

from envs.playground_env.objects import *
# from envs.playground_env.reward_function import *


class PlayGroundRGB(gym.Env):
    metadata = {
        'render.modes': ['human', 'rgb_array'],
        'video.frames_per_second': 30
    }

    '''
        Playground Environement:
        set reward_screen to True to visualize modular reward function predictions
        set viz_data_collection to True to visualize Social Partner interactions 
    '''

    class Room(Enum):
        START = 0
        RANDOM = 1
        TV = 2
        OBJECTS = 3

    def __init__(self,
                 n_timesteps=50,
                 render=False,
                 human=False,
                 continuous=True,
                 debug_mode=False,
                 available_room="START",
                 rooms_mode=True,
                 debug_rooms_mode=False,
                 available_rooms=("START",),
                 teleport_random_room=False,
                 random_ball_location=False,
                 random_TV_location=False,
                 square_objects_room=False,
                 number_of_random_room_colors=None,
                 number_of_TV_room_colors=None,
                 square_size=0.5,
                 cross_width=0.75,
                 cross_objects_room=False,
                 corridor_random_room=False,
                 goal_generation_rooms=None,
                 random_start_room=False,
                 block_dog=True,
                 block_water=False,
                 grow=True,
                 use_small_objs=False,
                 use_huge_objs=False,
                 use_custom_objs=True,
                 custom_objs_ratio=1,
                 red_gripper=False,
                 deterministic_obj=False,
                 white_obj_room=False,
                 colored_start_room=False,
                 gray_obj_room=False,
                 obj_room_flipped_actions=False,
                 obj_room_rotated_actions=False,
                 whiten=False,
                 static=True,
                 smooth_colors=False,
                 use_big_margin=False,
                 use_medium_margin=False,
                 use_custom_margin=False,
                 custom_margin_value=None,
                 pos_step_size=0.25,
                 random_color_per_rollout=False,
                 random_ball_per_rollout=False,
                 random_TV_location_per_rollout=False,
                 TV_color_per_rollout=False,
                 noise_random_color=False,
                 noise_TV_color=False,
                 doors=False,
                 shifted_doors=False,
                 doors_handle=False,
                 door_size=0.5,
                 shifted_door=False,
                 static_eval_set_size=None,
                 air_wrong_room_distance=False,
                 oracle_reward_allowed_gap=0.20,
             ):

        self.continuous = continuous
        self.air_wrong_room_distance = air_wrong_room_distance
        assert self.continuous
        print("warn: you changed coordinate setting when moving to a new room in step()")

        assert sum([use_small_objs, use_huge_objs, use_custom_objs]) <= 1
        assert not (white_obj_room and gray_obj_room)

        assert sum([use_custom_margin, use_medium_margin, use_big_margin]) <= 1
        self.margin = 0.3
        if use_big_margin:
            self.margin = 1.0
        if use_medium_margin:
            self.margin = 0.5
        if use_custom_margin:
            self.margin = custom_margin_value

        self.pos_step_size = pos_step_size
        self.wrong_room_distance = 3 * (MAX_COORDINATE - self.margin)
        self.obj_room_flipped_actions = obj_room_flipped_actions
        self.obj_room_rotated_actions = obj_room_rotated_actions

        self.oracle_reward_allowed_gap = oracle_reward_allowed_gap


        # obj sizes
        self.gripper_ratio = 0.25
        self.closed_gripper_ratio = 0.22
        self.ratio_size = int(SCREEN_SIZE * 1.4)  # this is multiplied by min_max sizes

        self.doors = doors
        self.shifted_doors = shifted_doors
        self.doors_handle = doors_handle
        self.door_size = door_size
        self.shifted_door = shifted_door

        assert not HD
        if use_small_objs:
            # cca ratio 0.6
            self.gripper_ratio = 0.15
            self.closed_gripper_ratio = 0.13
            self.ratio_size = int(SCREEN_SIZE * 3 / 5)  # this is multiplied by min_max sizes
        elif use_huge_objs:
            # cca ratio 1.6
            self.gripper_ratio = 0.4
            self.closed_gripper_ratio = 0.35
            self.ratio_size = int(SCREEN_SIZE * 20 / 9)  # this is multiplied by min_max sizes
        elif use_custom_objs:
            self.custom_objs_ratio = custom_objs_ratio
            self.gripper_ratio = custom_objs_ratio*self.gripper_ratio
            self.closed_gripper_ratio = custom_objs_ratio*self.closed_gripper_ratio
            self.ratio_size = int(custom_objs_ratio*self.ratio_size)

        self.random_color_per_rollout = random_color_per_rollout
        self.random_ball_per_rollout = random_ball_per_rollout
        self.random_TV_location_per_rollout = random_TV_location_per_rollout
        self.TV_color_per_rollout = TV_color_per_rollout

        self.number_of_TV_room_colors = number_of_TV_room_colors
        self.number_of_random_room_colors = number_of_random_room_colors

        if self.number_of_random_room_colors is not None:

            # start_room_init_color = [190, 175, 45]
            # obj_room_init_color = [120, 120, 100]

            # [9, 30, 241],
            # [200, 12, 31],
            # [19, 222, 23],
            # [11, 208, 219],
            # [212, 13, 254],

            self.random_room_possible_colors = [
                [9, 73, 241],
                [200, 96, 31],
                [19, 222, 23],
                [11, 208, 219],
                [212, 13, 254],
            ][:self.number_of_random_room_colors]

        if self.number_of_TV_room_colors is not None:
            if "RANDOM" in available_rooms:
                self.TV_room_possible_colors = list(255-np.array([
                   [9, 73, 241],
                   [200, 96, 31],
                   [19, 222, 23],
                   [11, 208, 219],
                   [212, 13, 254],
                ]))[:self.number_of_TV_room_colors]
            else:
                self.TV_room_possible_colors = [
                    [9, 73, 241],
                    [200, 96, 31],
                    [19, 222, 23],
                    [11, 208, 219],
                    [212, 13, 254],
                ][:self.number_of_TV_room_colors]

        self.noise_random_color = noise_random_color
        self.noise_TV_color = noise_TV_color
        self.square_objects_room = square_objects_room
        self.cross_objects_room = cross_objects_room
        assert not(square_objects_room and cross_objects_room)
        self.square_size = square_size
        self.cross_width = cross_width

        self.gripper_size = self.gripper_ratio * 2

        self.sq_border = self.gripper_size/2+self.square_size/2
        self.cross_border = self.cross_width/2-self.gripper_size/2

        self.deterministic_obj = deterministic_obj
        self.whiten = whiten
        self.red_gripper = red_gripper
        self.smooth_colors = smooth_colors
        self.random_start_room = random_start_room
        self.teleport_random_room = teleport_random_room
        self.random_ball_location = random_ball_location
        self.random_TV_location = random_TV_location

        self.debug_mode = debug_mode
        assert not self.debug_mode
        if self.debug_mode:
            raise DeprecationWarning("Not used anymore")
            # self.available_room = {
            #     "START": self.Room.START,
            #     "RANDOM": self.Room.RANDOM,
            #     "TV": self.Room.TV,
            #     "OBJECTS": self.Room.OBJECTS
            # }[available_room]
            # print("debug available room:", self.available_room)

        self.debug_rooms_mode = debug_rooms_mode
        if rooms_mode:
            assert debug_rooms_mode

        self.rooms_mode = rooms_mode
        if self.rooms_mode:

            assert rooms_mode
            assert not debug_mode
            str_to_room = {
                "START": self.Room.START,
                "RANDOM": self.Room.RANDOM,
                "TV": self.Room.TV,
                "OBJECTS": self.Room.OBJECTS
            }
            self.available_rooms = [str_to_room[room] for room in available_rooms]

            if goal_generation_rooms is None:
                goal_generation_rooms = copy.deepcopy(available_rooms)
            assert type(goal_generation_rooms) in [tuple, list]
            self.goal_generation_rooms = [str_to_room[room] for room in goal_generation_rooms]
            print("aval:", self.available_rooms)
            print("gen:", self.goal_generation_rooms)
            for g in self.goal_generation_rooms:
                assert g in self.available_rooms

        if self.rooms_mode:
            assert not self.debug_mode

        self.corridor_random_room = corridor_random_room
        if self.corridor_random_room:
            raise DeprecationWarning("not used")
            # assert self.rooms_mode
            # assert self.Room.RANDOM in self.available_rooms

        self.block_dog = block_dog
        self.block_water = block_water

        self.grow = grow

        self.static = static
        self.image_obs = True
        self.random_init = random_gripper_location
        self.n_timesteps = n_timesteps
        self.human = human
        self.render_mode = render
        self.obs_shape = (SCREEN_SIZE, SCREEN_SIZE, 3)

        self.nb_obj = N_OBJECTS_IN_SCENE
        self.dim_obj = DIM_OBJ
        self.inds_objs = [np.arange(n_inds_before_obj_inds + self.dim_obj * i_obj,
                                    n_inds_before_obj_inds + self.dim_obj * (i_obj + 1)) for i_obj in
                          range(self.nb_obj)]
        self.n_half_obs = self.nb_obj * self.dim_obj + n_inds_before_obj_inds
        self.inds_grasped_obj = np.array([])
        self.n_obs = self.n_half_obs * 2

        # We define the spaces
        if self.continuous:
            self.n_act = 3
            self.action_space = spaces.Box(low=-np.ones(self.n_act),
                                           high=np.ones(self.n_act),
                                           dtype=np.float32)
        else:
            self.action_space = spaces.Discrete(5)

        # self.observation_space = spaces.Box(low=-np.ones(self.n_obs),
        #                                     high=np.ones(self.n_obs),
        #                                     dtype=np.float32)
        self.obs_box = spaces.Box(
            low=0, high=255, shape=(SCREEN_SIZE, SCREEN_SIZE, 3), dtype=np.uint8)

        self.goal_box = spaces.Box(
            np.array([-1] * 24),
            np.array([1] * 24),
        )

        self.observation_space = spaces.Dict([
            ('observation', self.obs_box),
            ('state_observation', self.obs_box),
            ('desired_goal', self.goal_box),
            ('state_desired_goal', self.goal_box),
            ('achieved_goal', self.goal_box),
            ('state_achieved_goal', self.goal_box),
        ])

        # Main agent

        self.pos_step_size = self.pos_step_size
        self.pos_init = np.array([0., 0.])
        self.pos_init_random_random = 0.6

        init_pos_d_w = [np.array((1.0, 1.0)), np.array((-0.5, -0.5))]
        self.init_pos_d_w = np.clip(init_pos_d_w, -(MAX_COORDINATE - self.margin), MAX_COORDINATE - self.margin)
        self.init_pos_d_w = [self.make_feasible(pos) for pos in self.init_pos_d_w]

        if not render:
            pass
            # os.environ["SDL_VIDEODRIVER"] = "dummy"
            os.environ['SDL_AUDIODRIVER'] = 'dsp'
        pygame.init()

        self.viewer = pygame.Surface((SCREEN_SIZE, SCREEN_SIZE))
        self.viewer_started = False
        self.background = None

        # self.size_gripper_pixels = int(55*(SCREEN_SIZE/800))
        # self.size_gripper_closed_pixels = int(45*(SCREEN_SIZE/800))
        # self.size_gripper_pixels = int(110*(SCREEN_SIZE/800))
        # self.size_gripper_closed_pixels = int(90*(SCREEN_SIZE/800))
        self.size_gripper_pixels = int(self.gripper_ratio*SCREEN_SIZE)
        self.size_gripper_closed_pixels = int(self.closed_gripper_ratio*SCREEN_SIZE)
        print("open gripper  p:", self.size_gripper_pixels)
        print("closed gripper p:", self.size_gripper_closed_pixels)

        gripper_icon = pygame.image.load(IMAGE_PATH + 'hand_open.png')
        closed_gripper_icon = pygame.image.load(IMAGE_PATH + 'hand_closed.png')

        def _color_surface(surface, rgb):
            arr = pygame.surfarray.pixels3d(surface)
            arr[:, :, 0] = rgb[0]
            arr[:, :, 1] = rgb[1]
            arr[:, :, 2] = rgb[2]

        if self.red_gripper:
            if not self.smooth_colors:
                _color_surface(closed_gripper_icon, [255, 0, 0])
                _color_surface(gripper_icon, [255, 0, 0])
            else:
                _color_surface(closed_gripper_icon, [126, 0, 0])
                _color_surface(gripper_icon, [126, 0, 0])

        self.gripper_icon = pygame.transform.scale(gripper_icon, (self.size_gripper_pixels, self.size_gripper_pixels)) #.convert_alpha()
        self.closed_gripper_icon = pygame.transform.scale(closed_gripper_icon,
                                                          (self.size_gripper_closed_pixels, self.size_gripper_pixels)) #.convert_alpha()
        tv_icon = pygame.image.load(IMAGE_PATH + 'tv.png')
        self.tv_icon_size = self.size_gripper_pixels
        self.tv_icon = pygame.transform.scale(tv_icon, (self.tv_icon_size, self.tv_icon_size)) #.convert_alpha()

        disco_icon = pygame.image.load(IMAGE_PATH + 'disco.png')
        self.disco_icon_size = self.size_gripper_pixels
        self.ball_location = self.sample_disco_ball_location()
        self.disco_icon = pygame.transform.scale(disco_icon, (self.disco_icon_size, self.disco_icon_size)) #.convert_alpha()

        if white_obj_room:
            self.objects_room_init_color = np.array([255, 255, 255])
        elif gray_obj_room:
            self.objects_room_init_color = np.array([120, 120, 100])
        else:
            self.objects_room_init_color = np.array([0, 0, 0])

        if self.rooms_mode:
            self.current_room = self.get_start_room()
            print("Agent is starting from the ", self.current_room)

        elif self.debug_mode:
            self.current_room = self.available_room
        else:
            self.current_room = self.Room.OBJECTS

        # self.random_room_colors = random_colors[:5000]
        # self.TV_room_colors = random_colors[5000:]

        self.TV_location_OFF = np.array([-(MAX_COORDINATE - self.margin), MAX_COORDINATE - self.margin])
        self.TV_location_ON = self.sample_TV_location(1)
        self.TV_init_color = TV_init_color

        if colored_start_room:
            self.start_room_init_color = [190, 175, 45]
        else:
            self.start_room_init_color = [200, 200, 200]

        self.rooms_colors = {
            self.Room.TV: {
                -1: self.TV_init_color,
                1: self.sample_TV_room_color(1)
            },
            self.Room.RANDOM: self.sample_random_room_color(),
            self.Room.START: self.start_room_init_color,
            self.Room.OBJECTS: self.objects_room_init_color,
        }

        # self.goal_info_vector = self.state_dict_to_vector(self.sample_goal_dict())

        self.reset()
        # self.goal_info_d = self.sample_goal_dict()

        # We set to None to rush error if reset not called
        self.reward = None
        self.observation = None
        self.initial_observation = None
        self.done = None

        self.info = dict(is_success=0)

        self.static_eval_set_counter = 0
        self.static_eval_set = [self.state_dict_to_vector(d) for d in self.static_eval_goal_dicts()]
        self.static_eval_set_size = static_eval_set_size
        assert len(self.static_eval_set) == self.static_eval_set_size

    def get_start_room(self):
        assert self.rooms_mode
        if self.random_start_room:
            return np.random.choice(self.available_rooms)
        else:
            return self.available_rooms[0]

    def set_room_color(self, color, room, gripper_state=None):
        if room == self.Room.TV:
            assert gripper_state == 1
            self.rooms_colors[self.Room.TV][gripper_state] = color

        else:
            self.rooms_colors[room]=color

    def get_room_color(self, room, gripper_state=None):
        if room == self.Room.TV:
            assert gripper_state in [-1, 1]
            return self.rooms_colors[self.Room.TV][gripper_state]

        return self.rooms_colors[room]

    def sample_room_color(self, room, gripper_state=None):
        # use for sampling a goal
        if self.debug_mode:
            assert room == self.available_room

        assert room in self.available_rooms

        if room in [self.Room.OBJECTS, self.Room.START]:
            return self.get_room_color(room)

        elif room == self.Room.RANDOM:
            return self.sample_random_room_color()
        elif room == self.Room.TV:
            return self.sample_TV_room_color(gripper_state)
        else:
            raise ValueError("Unknown room: ", room)

    def initialize_camera(self, bla):
        pass

    def get_image(self, width, height):
        obs = self.get_image_obs()
        # import cv2
        # cv2.imshow("get_im", obs)
        # cv2.waitKey(10)
        assert obs.shape == (width, height, 3)
        return obs

    def get_goal(self):
        return {
            'desired_goal': self.goal_info_vector,
            'state_desired_goal': self.goal_info_vector,
        }

    def get_next_static_eval_goals(self, batch_size):
        # number_of_eval_steps_per_epoch should be rollout lenght * 25
        assert batch_size == 1
        index = self.static_eval_set_counter % self.static_eval_set_size
        goals = [self.static_eval_set[index]]
        self.static_eval_set_counter += 1
        assert self.goal_generation_rooms == [self.Room.OBJECTS]

        return {
            'desired_goal': goals,
            'state_desired_goal': goals,
        }

    def set_to_goal(self, goal):
        goal_info_vector = goal['state_desired_goal']
        goal_info_dict = self.vector_to_state_dict(goal_info_vector)
        self.set_context(goal_info_dict)

    def compute_rewards(self, action, obs, info=None):
        return np.array([-13.0])

    def get_env_state(self):
        return copy.deepcopy(self.state_dict_to_vector(self.create_info_d()))

    def create_init_context(self):
        raise DeprecationWarning()
        assert False
        # objs_info = [{
        #     "id": o.id, "type": o.type, "color": o.color, "size": o.size, "shade": o.shade, "position": o.position
        # } for o in self.objects]
        #
        # info_d = {"objects": objs_info, "gripper_pos": self.pos, "gripper_state": self.gripper_state}
        # self.init_context = info_d

    def get_init_context(self):
        raise DeprecationWarning()
        assert False
        return self.init_context

    def set_env_state(self, state):
        self.set_context(self.vector_to_state_dict(state))

    def set_context(self, context):

        for obj in self.objects:
            obj.update_all_attributes()

        objects, objects_ids, objects_types = [], [], []
        for obj in context["objects"]:
            type = obj["type"]
            color = obj["color"]
            size = obj["size"]
            shade = obj["shade"]
            position = obj["position"]
            obj_id = get_obj_identifier(type, color, shade, size)
            objects.append(build_object(type, color, shade, size, len(objects), objects, True, self, position))
            objects_ids.append(obj_id)
            objects_types.append(type)

        self.reset_scene(objects, objects_ids, objects_types, keep_goal=True)

        self.gripper_state = context["gripper_state"]
        self.pos = context["gripper_pos"]

        self.current_room = context["current_room"]
        self.set_room_color(color=context['current_room_color'], room=context['current_room'], gripper_state=context['gripper_state'])
        self.set_room_color(color=context['random_room_color'], room=self.Room.RANDOM)
        self.set_room_color(color=context['TV_ON_room_color'], room=self.Room.RANDOM, gripper_state=1)

        self.ball_location = context['ball_location']
        self.TV_location_ON = context['TV_location_ON']
        self.TV_location_OFF = context['TV_location_OFF']

        if self.debug_mode:
            assert self.current_room == self.available_room
        elif self.rooms_mode:
            assert self.current_room in self.available_rooms

        elif not self.rooms_mode:
            assert self.current_room == self.Room.OBJECTS

        if self.image_obs:
            self.observation = self.get_image_obs()

        info_vec = self.state_dict_to_vector(self.create_info_d())

        return self.observation.copy(), 0, False, info_vec

    def set_goal(self, goal):
        self.goal_info_vector = goal['state_desired_goal']

    def sample_goals(self, batch_size):
        goals = [self.state_dict_to_vector(self.sample_goal_dict()) for _ in range(batch_size)]
        return {
            'desired_goal': goals,
            'state_desired_goal': goals,
        }

    @staticmethod
    def state_dict_is_TV_ON(state_dict):
        return state_dict['gripper_state']

    def vector_to_state_dict(self, vec):

        pos_d, st_d, pos_w, gr_pos, gr_st, room_color, room_id, ball_loc, TV_loc_ON, TV_loc_OFF, random_room_color, TV_ON_room_color = vec[:2], vec[2],vec[3:5],vec[5:7],vec[7],vec[8:11],vec[11], vec[12:14], vec[14:16], vec[16:18], vec[18:21], vec[21:24]

        locs = [pos_d, pos_w]
        sizes = ["big" if bool(st_d) else "small", "small"]

        objs_info = [{
            "id": o.id, "type": o.type, "color": o.color, "size": s, "shade": o.shade, "position": l
        } for l, s, o in zip(locs, sizes, self.objects)]

        if self.debug_mode:
            assert self.Room(room_id) == self.available_room
        if self.rooms_mode:
            assert self.Room(room_id) in self.available_rooms

        info_d = {
            "objects": objs_info,
            "gripper_pos": gr_pos,
            "gripper_state": gr_st,
            "current_room": self.Room(room_id),
            "current_room_color": room_color,
            "ball_location": ball_loc,
            "TV_location_ON": TV_loc_ON,
            "TV_location_OFF": TV_loc_OFF,
            "random_room_color": random_room_color,
            "TV_ON_room_color": TV_ON_room_color,
        }

        return info_d

    def state_dict_to_vector(self, dict):
        vector = np.concatenate([
                dict['objects'][0]['position'],
                [dict['objects'][0]['size'] == 'big'],
                dict['objects'][1]['position'],
                dict['gripper_pos'],
                [dict['gripper_state']],
                dict['current_room_color'],
                [dict['current_room'].value],
                dict['ball_location'],
                dict['TV_location_ON'],
                dict['TV_location_OFF'],
                dict['random_room_color'],
                dict['TV_ON_room_color']
        ])

        return vector

    def is_inside_square(self, pos):
        return all((np.array([-self.sq_border, -self.sq_border]) < pos) & (pos < np.array([self.sq_border, self.sq_border])))

    def is_inside_cross(self, pos):
        return any((np.array([-self.cross_border, -self.cross_border]) < pos) & (pos < np.array([self.cross_border, self.cross_border])))

    def make_feasible(self, pos):
        if self.square_objects_room:
            if self.is_inside_square(pos):
                if abs(pos[0]) > abs(pos[1]):
                    pos = np.array([np.sign(pos[0] + 1e-6) * self.sq_border, pos[1]])
                else:
                    pos = np.array([pos[0], np.sign(pos[1] + 1e-6) * self.sq_border])

        elif self.cross_objects_room:
            if not self.is_inside_cross(pos):
                if abs(pos[0]) < abs(pos[1]):
                    pos = np.array([np.sign(pos[0] + 1e-6) * self.cross_border, pos[1]])
                else:
                    pos = np.array([pos[0], np.sign(pos[1] + 1e-6) * self.cross_border])

        return pos

    def get_TV_location(self, gripper_state):
        if self.random_TV_location and gripper_state == 1:
            pos = self.TV_location_ON
        else:
            pos = self.TV_location_OFF
        return pos

    def sample_TV_location(self, gripper_state):
        assert gripper_state == 1
        if self.random_TV_location:
            pos = np.random.uniform(-(MAX_COORDINATE - self.margin), MAX_COORDINATE - self.margin, 2)
        else:
            pos = np.array([-(MAX_COORDINATE - self.margin), MAX_COORDINATE - self.margin])
        return pos

    def sample_TV_room_color(self, gripper_state):
        assert gripper_state == 1
        if gripper_state == 1:

            if self.number_of_TV_room_colors is not None:
                return random.sample(self.TV_room_possible_colors, 1)[0]

            if self.noise_TV_color:
                # this is not the acctual color to be used, each pixel will be different actually
                return [-1, -1, -1]

            else:
                return np.random.randint(0, 255, 3)

        elif gripper_state == -1:
            return self.TV_init_color

        else:
            print('gs:', gripper_state)
            raise ValueError("Gripper state undefined.")

    def sample_disco_ball_location(self):
        if self.random_ball_location:
            pos = np.random.uniform(-(MAX_COORDINATE - self.margin), MAX_COORDINATE - self.margin, 2)
        else:
            pos = np.array([-(MAX_COORDINATE - self.margin), MAX_COORDINATE - self.margin])
        return pos

    def sample_location(self, room):
        pos = np.random.uniform(-(MAX_COORDINATE - self.margin), MAX_COORDINATE - self.margin, 2)
        if room == self.Room.OBJECTS:
            if self.square_objects_room:
                while self.is_inside_square(pos):
                    pos = np.random.uniform(-(MAX_COORDINATE - self.margin), MAX_COORDINATE - self.margin, 2)

            elif self.cross_objects_room:
                while not self.is_inside_cross(pos):
                    pos = np.random.uniform(-(MAX_COORDINATE - self.margin), MAX_COORDINATE - self.margin, 2)

        return pos

    def static_eval_goal_dicts(self):
        assert self.rooms_mode
        assert self.Room.OBJECTS in self.goal_generation_rooms

        assert self.block_dog
        assert not self.grow

        room = self.Room.OBJECTS
        all_gr_pos = []
        all_water_pos = []

        coors = [(MAX_COORDINATE-self.margin)*np.sqrt(2)/3, -(MAX_COORDINATE-self.margin)*np.sqrt(2)/3]
        all_gr_pos = list(product(coors, coors)) + [(0.0, 0.0)]
        all_water_pos = list(product(coors, coors)) + [(0.0, 0.0)]

        dicts = []
        for gr_pos, wat_pos in product(all_gr_pos, all_water_pos):
            objs_info = [{
                "id": o.id, "type": o.type, "color": o.color, "size": o.size, "shade": o.shade, "position": wat_pos
            } for o in self.objects]

            objs_info[0]['size'] = "small"
            objs_info[0]['position'] = self.init_pos_d_w[0]

            assert room == self.Room.OBJECTS

            if self.block_water or room != self.Room.OBJECTS:
                objs_info[1]['position'] = self.init_pos_d_w[1]

            ball_loc = self.sample_disco_ball_location()
            gr_st = 1

            TV_loc_ON = self.sample_TV_location(gr_st)
            info_d = {
                "objects": objs_info,
                "gripper_pos": gr_pos,
                "gripper_state": gr_st,
                "current_room": room,
                "current_room_color": self.sample_room_color(room),  # we asserted that we are in the OBJ room
                "ball_location": ball_loc,
                "TV_location_ON": TV_loc_ON,
                "TV_location_OFF": self.TV_location_OFF,
                "random_room_color": [-1000, -1000, -1000], # we asserted that we are in the OBJ room
                "TV_ON_room_color": [-1000, -1000, -1000],  # we asserted that we are in the OBJ room
            }
            dicts.append(info_d)

        return dicts

    def sample_goal_dict(self):
        if self.rooms_mode:
            room = np.random.choice(self.goal_generation_rooms)
        elif self.debug_mode:
            room = self.available_room
        else:
            room = self.Room.OBJECTS

        gr_pos = self.sample_location(room)
        objs_info = [{
            "id": o.id, "type": o.type, "color": o.color, "size": o.size, "shade": o.shade, "position": self.sample_location(room)
        } for o in self.objects]

        if self.grow and room == self.Room.OBJECTS:
            objs_info[0]['size'] = np.random.choice(["small", "big"])
        else:
            objs_info[0]['size'] = "small"

        if self.block_dog or room != self.Room.OBJECTS:
            objs_info[0]['position'] = self.init_pos_d_w[0]

        if self.block_water or room != self.Room.OBJECTS:
            objs_info[1]['position'] = self.init_pos_d_w[1]

        ball_loc = self.sample_disco_ball_location()

        gr_st = np.random.choice([-1, 1])

        TV_loc_ON = self.sample_TV_location(1)
        info_d = {
            "objects": objs_info,
            "gripper_pos": gr_pos,
            "gripper_state": gr_st,
            "current_room": room,
            "current_room_color": self.sample_room_color(room, gr_st),
            "ball_location": ball_loc,
            "TV_location_ON": TV_loc_ON,
            "TV_location_OFF": self.TV_location_OFF,
            "random_room_color": self.sample_random_room_color(),
            "TV_ON_room_color": self.sample_TV_room_color(1)
        }

        return info_d

    def oracle_reward_fn(
            self,
            goal_context_vector,
            achieved_goal_context_vector,
            gripper_state_insensitive=False
    ):

        goal_context = self.vector_to_state_dict(goal_context_vector)
        achieved_goal_context = self.vector_to_state_dict(achieved_goal_context_vector)

        g_grs, g_grl = goal_context["gripper_state"], goal_context["gripper_pos"]
        o_grs, o_grl = achieved_goal_context["gripper_state"], achieved_goal_context["gripper_pos"]

        if gripper_state_insensitive:
            gr_r = (np.abs(o_grl - g_grl) < self.oracle_reward_allowed_gap).prod(-1)
        else:
            gr_r = (np.abs(o_grl - g_grl) < self.oracle_reward_allowed_gap).prod(-1) * (g_grs == o_grs)

        g_pos = np.array([o["position"] for o in goal_context["objects"]])
        g_s = np.array([o["size"] != "small" for o in goal_context["objects"]])

        ob_pos = np.array([o["position"] for o in achieved_goal_context["objects"]])
        ob_s = np.array([o["size"] != "small" for o in achieved_goal_context["objects"]])

        ob_r = (np.abs(g_pos - ob_pos) < self.oracle_reward_allowed_gap).prod(-1) * (ob_s == g_s)

        if goal_context['current_room'] != achieved_goal_context['current_room']:
            # all rewards are 0, (gr, d, w)
            total_rew = [0]

        elif (
                goal_context['current_room'] != self.Room.OBJECTS
        ):
            # goal is not in the objects room
            # no objects just scale gripper_reward
            total_rew = [gr_r]

        else:
            assert goal_context['current_room'] == self.Room.OBJECTS
            assert achieved_goal_context['current_room'] == self.Room.OBJECTS

            total_rew = [gr_r]
            if not self.block_dog:
                total_rew.append(ob_r[0])
            if not self.block_water:
                total_rew.append(ob_r[1])

        per_obj_final_obj_rewards = np.mean(total_rew, axis=-1)

        # only give reward if all objects are correctly placed
        final_obj_rewards = np.min(total_rew, axis=-1)

        return final_obj_rewards, per_obj_final_obj_rewards


    def reward_fn(
            self,
            goals_contexts_vector,
            achieved_goals_contexts_vector,
            gripper_state_insensitive=False
    ):
        raise DeprecationWarning()
        assert False

        goals_contexts = [self.vector_to_state_dict(vec) for vec in goals_contexts_vector]
        achieved_goals_contexts = [self.vector_to_state_dict(vec) for vec in achieved_goals_contexts_vector]
        # todo: revisit this and check everything

        if goals_contexts[0] is None:
            print("goal contexts None")
            return [0]

        g_grs = np.array([g["gripper_state"] > 0 for g in goals_contexts])
        g_grl = np.array([g["gripper_pos"] for g in goals_contexts])
        o_grs = np.array([g["gripper_state"] > 0 for g in achieved_goals_contexts])
        o_grl = np.array([g["gripper_pos"] for g in achieved_goals_contexts])

        # gap = self.pos_step_size
        gap = 0.20  # self.pos_step_size

        if gripper_state_insensitive:
            gr_r = (np.abs(o_grl - g_grl) < gap).prod(-1)
        else:
            gr_r = (np.abs(o_grl - g_grl) < gap).prod(-1) * (g_grs == o_grs)

        rews = []
        for i, (g, ob) in enumerate(zip(goals_contexts, achieved_goals_contexts)):

            g_pos = np.array([o["position"] for o in g["objects"]])
            g_s = np.array([o["size"] != "small" for o in g["objects"]])

            ob_pos = np.array([o["position"] for o in ob["objects"]])
            ob_s = np.array([o["size"] != "small" for o in ob["objects"]])
            ob_r = (np.abs(g_pos - ob_pos) < gap).prod(-1) * (ob_s == g_s)

            if g['current_room'] != ob['current_room']:
                # all rewards are 0
                total_rew = (gr_r[i]*0, *ob_r*0)

            elif (
                    g['current_room'] != self.Room.OBJECTS
            ):
                # goal is not in the objects room
                # no objects just scale gripper_reward
                ob_r = ob_r * 0 + gr_r[i]
                total_rew = (gr_r[i], *ob_r)

            else:
                assert g['current_room'] == self.Room.OBJECTS
                assert ob['current_room'] == self.Room.OBJECTS

                total_rew = (gr_r[i], *ob_r)

            rews.append(total_rew)

        # rew = np.array([(g_r, *ob_r) for ob_r, g_r in zip(obj_rews, gr_r)])
        rew = np.array(rews)

        per_obj_final_obj_rewards = rew.mean(axis=-1, keepdims=True)
        # only give reward if all objects are correctly placed
        final_obj_rewards = rew.min(axis=-1, keepdims=True)

        g_room = np.array([g["current_room"] for g in goals_contexts])
        o_room = np.array([g["current_room"] for g in achieved_goals_contexts])
        correct_room = (g_room == o_room).astype(int).reshape(final_obj_rewards.shape)

        g_room_col = np.array([g["current_room_color"] for g in goals_contexts])
        o_room_col = np.array([g["current_room_color"] for g in achieved_goals_contexts])
        correct_col = (g_room_col == o_room_col).prod(axis=-1).astype(int).reshape(final_obj_rewards.shape)

        assert (final_obj_rewards*correct_room).mean() == final_obj_rewards.mean()

        final_obj_rewards = final_obj_rewards * correct_room * correct_col
        per_obj_final_obj_rewards = per_obj_final_obj_rewards * correct_room * correct_col

        assert not(self.debug_mode and self.available_room != self.Room.OBJECTS and not final_obj_rewards == per_obj_final_obj_rewards)

        return final_obj_rewards, per_obj_final_obj_rewards

    def create_info_d(self):
        objs_info = [{
            "id": o.id, "type": o.type, "color": o.color, "size": o.size, "shade": o.shade, "position": o.position
        } for o in self.objects]

        info_d = {
            "objects": objs_info,
            "gripper_pos": self.pos,
            "gripper_state": self.gripper_state,
            "current_room": self.current_room,
            "current_room_color": self.get_room_color(self.current_room, self.gripper_state),
            "ball_location": self.ball_location,
            "TV_location_ON": self.TV_location_ON,
            "TV_location_OFF": self.TV_location_OFF,
            "random_room_color": self.get_room_color(self.Room.RANDOM),
            "TV_ON_room_color": self.get_room_color(self.Room.TV, 1),
        }

        success = float(self.oracle_reward_fn(
            self.goal_info_vector,
            self.state_dict_to_vector(info_d)
        )[0])

        info_d = {
            **info_d, **{"success": success}
        }

        return info_d

    def change_n_objs(self, n_objs):
        self.nb_obj = n_objs
        self.inds_objs = [np.arange(n_inds_before_obj_inds + self.dim_obj * i_obj,
                                    n_inds_before_obj_inds + self.dim_obj * (i_obj + 1)) for i_obj in
                          range(self.nb_obj)]
        self.n_half_obs = self.nb_obj * self.dim_obj + n_inds_before_obj_inds
        self.n_obs = self.n_half_obs * 2

    def set_state(self, state):
        raise ValueError("DEPRECATED")

        assert state.size == self.n_half_obs, 'N_OBJECTS_IN_SCENE is not right'
        current_state = state[:state.shape[0] // 2]
        self.pos = current_state[:2]
        self.gripper_state = current_state[2]

        self.initial_observation = current_state - state[state.shape[0] // 2:]
        obj_features = []
        for i_obj in range(self.nb_obj):
            obj_features.append(current_state[self.inds_objs[i_obj]])
        self.set_objects(obj_features)
        self.object_grasped = np.any(np.array(obj_features)[:, -1] == 1)

    def reset(self):
        if self.static:
            return self.reset_scene(*self.get_static_objects())
        else:
            return self.reset_scene(*self.get_dynamic_objects())

    def get_diagnostics(self, paths, prefix=""):
        statistics = OrderedDict()
        for stat_name in [
            # 'rewards',
            # 'puck_distance',
            'hand_and_water_distance',
            'success',
        ]:
            stat_name = stat_name
            stat = get_stat_in_paths(paths, 'env_infos', stat_name)
            statistics.update(create_stats_ordered_dict(
                '%s%s' % (prefix, stat_name),
                stat,
                always_show_all_stats=True,
                ))
            statistics.update(create_stats_ordered_dict(
                'Final %s%s' % (prefix, stat_name),
                [s[-1] for s in stat],
                always_show_all_stats=True,
                ))
        return statistics

    def reset_scene(self, objects=None, objects_ids=None, objects_types=None, keep_goal=False):

        if self.rooms_mode:
            self.current_room = self.get_start_room()

            self.set_room_color(self.sample_TV_room_color(1), self.Room.TV, 1)
            assert self.rooms_colors[self.Room.TV][-1] == self.TV_init_color
            self.set_room_color(self.sample_random_room_color(), self.Room.RANDOM)
            self.ball_location = self.sample_disco_ball_location()
            self.TV_location_ON = self.sample_TV_location(1)

        elif self.debug_mode:
            self.current_room = self.available_room
        else:
            self.current_room = self.Room.OBJECTS

        if self.random_init:
            self.pos = self.sample_location(self.current_room)
        else:
            self.pos = self.pos_init

        self.gripper_state = -1

        if objects is None or objects_ids is None or objects_types is None:
            self.objects, self.objects_ids, self.objects_types = self.sample_objects(objects)
        else:
            self.objects, self.objects_ids, self.objects_types = objects, objects_ids, objects_types

        self.object_grasped = False

        # update attributes
        for obj in self.objects:
            obj.give_ref_to_obj_list(self.objects)
            obj.update_all_attributes()

        # Print objects
        for obj in self.objects:
            self.object_grasped = obj.update_state(self.pos,
                                                   self.gripper_state > 0,
                                                   self.objects,
                                                   self.object_grasped,
                                                   np.zeros([10]))

        # construct vector of observations
        assert self.image_obs
        self.observation = self.get_image_obs()

        self.steps = 0
        self.done = False

        if not keep_goal:
            self.goal_info_vector = self.state_dict_to_vector(self.sample_goal_dict())

        observation = self._get_obs()

        return observation

    def get_static_objects(self):
        self.types = ['dog', 'water']
        self.colors = ['green', 'blue']
        self.shades = ['light', 'light']
        self.sizes = ['small', 'small']
        self.positions = self.init_pos_d_w

        objects, objects_ids, objects_types = [], [], []
        for type, color, shade, size, position in zip(self.types, self.colors, self.shades, self.sizes, self.positions):
            obj_id = get_obj_identifier(type, color, shade, size)
            objects.append(build_object(type, color, shade, size, len(objects), objects, True, self, position))
            objects_ids.append(obj_id)
            objects_types.append(type)

        assert len(objects_ids) == len(set(objects_ids))

        return objects, objects_ids, objects_types

    def get_dynamic_objects(self):
        self.types = ['dog', 'water']
        self.colors = ['green', 'blue']
        self.shades = ['light', 'light']
        self.sizes = ['small', 'small']

        if self.block_dog:
            d_pos = self.init_pos_d_w[0]
        else:
            d_pos = self.sample_location(self.Room.OBJECTS)

        if self.grow:
            raise not NotImplementedError("This not implemented")

        if self.block_water:
            w_pos = self.init_pos_d_w[1]
        else:
            w_pos = self.sample_location(self.Room.OBJECTS)

        self.positions = [d_pos, w_pos]
        # self.positions = self.init_pos_d_w

        objects, objects_ids, objects_types = [], [], []
        for type, color, shade, size, position in zip(self.types, self.colors, self.shades, self.sizes, self.positions):
            obj_id = get_obj_identifier(type, color, shade, size)
            objects.append(build_object(type, color, shade, size, len(objects), objects, True, self, position))
            objects_ids.append(obj_id)
            objects_types.append(type)

        assert len(objects_ids) == len(set(objects_ids))

        return objects, objects_ids, objects_types

    def sample_objects(self, objects_to_add):
        objects = []
        objects_ids = []
        objects_types = []
        if objects_to_add is not None:
            for object in objects_to_add:
                if object['type'] is not None:
                    type = object['type']
                elif object['category'] is not None:
                    type = np.random.choice(groups[group_names.index(object['category'])])
                else:
                    type = np.random.choice(things)
                if object['size'] is not None:
                    size = object['size']
                else:
                    size = np.random.choice(thing_sizes)
                if object['color'] is not None:
                    color = object['color']
                else:
                    color = np.random.choice(thing_colors)
                if object['shade'] is not None:
                    shade = object['shade']
                else:
                    shade = np.random.choice(thing_shades)
                obj_id = get_obj_identifier(type, color, shade, size)
                if obj_id not in objects_ids:
                    objects.append(build_object(type, color, shade, size, len(objects), objects, True, self))
                    objects_ids.append(obj_id)
                    objects_types.append(type)

        while len(objects) < self.nb_obj:
            type = np.random.choice(things)
            color = np.random.choice(thing_colors)
            shade = np.random.choice(thing_shades)
            size = np.random.choice(thing_sizes)
            obj_id = get_obj_identifier(type, color, shade, size)
            if obj_id not in objects_ids:
                objects.append(build_object(type, color, shade, size, len(objects), objects, True, self))
                objects_ids.append(obj_id)
                objects_types.append(type)

        return objects, objects_ids, objects_types

    def enter_door(self, pos):
        if not self.doors:
            return True

        enter_door = False
        if self.shifted_door:
            if -self.door_size < pos < 0:
                if not self.doors_handle or self.gripper_state == 1:
                    enter_door = True

        elif -self.door_size / 2 < pos < self.door_size / 2:
            if not self.doors_handle or self.gripper_state == 1:
                enter_door = True

        return enter_door

    def step(self, action):
        # actions
        # 0 = u
        # 1 = d
        # 2 = l
        # 3 = r
        # 4 = gripper

        """
        Run one timestep of the environment's dynamics.
        """
        if not self.continuous:
            to_continous = np.array([[1, 0, 0], [-1, 0, 0], [0, 1, 0], [0, -1, 0], [0, 0, 1]])
            action = to_continous[action]

        if self.current_room == self.Room.OBJECTS:
            if self.obj_room_flipped_actions:
                action *= -1
            if self.obj_room_rotated_actions:
                action = action @ np.array([
                    [0.5, 0.5, 0],
                    [0.5, -0.5, 0],
                    [0, 0, 1]
                ])

        if np.sum(action) != 0:
            self.first_action = True

        fake_action = np.zeros([10])
        fake_action[:3] = action[:3].copy()  # give gripper actions

        # Update the arm position
        self.pos = np.clip(self.pos + action[:2] * self.pos_step_size, -(MAX_COORDINATE-self.margin), (MAX_COORDINATE-self.margin))

        if self.current_room == self.Room.OBJECTS:
            self.pos = self.make_feasible(self.pos)

        # Update the gripper state
        if self.human:
            assert False
            if action[2] > 0:
                self.gripper_state = 1 if self.gripper_state == -1 else -1
        else:
            if self.continuous:
                if action[2] > 0.:
                    new_gripper = 1
                else:
                    new_gripper = -1
            else:
                # actions changes the state
                if action[2] > 0.:
                    new_gripper = 1 if self.gripper_state == -1 else -1
                else:
                    new_gripper = self.gripper_state

            self.gripper_change = new_gripper == self.gripper_state
            self.gripper_state = new_gripper

        # update rooms
        assert not self.debug_mode
        assert self.rooms_mode

        set_coord = MAX_COORDINATE-self.margin-(self.pos_step_size/2)
        limit = MAX_COORDINATE-self.margin-0.01
        if self.current_room == self.Room.START:
            if self.pos[0] <= -limit:
                if self.enter_door(self.pos[1]):
                    if self.Room.TV in self.available_rooms:
                        self.current_room = self.Room.TV

                        if not self.random_TV_location_per_rollout:
                            self.TV_location_ON = self.sample_TV_location(1)

                        if not self.TV_color_per_rollout:
                            self.set_room_color(self.sample_TV_room_color(1), self.Room.TV, 1)

                        self.pos[0] = set_coord

            elif self.pos[0] >= limit:
                if self.enter_door(self.pos[1]):
                    if self.Room.RANDOM in self.available_rooms:
                        self.current_room = self.Room.RANDOM

                        if not self.random_color_per_rollout:
                            self.set_room_color(self.sample_random_room_color(), self.Room.RANDOM)

                        if not self.random_ball_per_rollout:
                            self.ball_location = self.sample_disco_ball_location()

                        self.pos[0] = -set_coord

            elif self.pos[1] >= limit:
                if self.enter_door(self.pos[0]):
                    if self.Room.OBJECTS in self.available_rooms:
                        # corridor random room means no access to obj from start room
                        if not self.corridor_random_room:
                            self.current_room = self.Room.OBJECTS
                            self.pos[1] = -set_coord

        elif self.current_room == self.Room.TV:

            if self.pos[0] >= limit:
                if self.enter_door(self.pos[1]):
                    if self.Room.START in self.available_rooms:
                        self.current_room = self.Room.START
                        self.pos[0] = -set_coord

            if not self.TV_color_per_rollout:
                self.set_room_color(self.sample_TV_room_color(1), self.Room.TV, 1)
            if not self.random_TV_location_per_rollout:
                self.TV_location_ON = self.sample_TV_location(1)

        elif self.current_room == self.Room.RANDOM:
            if self.pos[0] <= -limit:
                if self.enter_door(self.pos[1]):
                    if self.Room.START in self.available_rooms:
                        self.current_room = self.Room.START
                        self.pos[0] = set_coord

            elif self.pos[0] >= limit and self.corridor_random_room:
                # corridor random room means we can access obj from rand room
                if self.enter_door(self.pos[1]):
                    if self.Room.OBJECTS in self.available_rooms:
                        self.current_room = self.Room.OBJECTS
                        self.pos[0] = -set_coord

            elif self.teleport_random_room:
                self.pos = self.sample_location(self.Room.RANDOM)

            if not self.random_color_per_rollout:
                self.set_room_color(self.sample_random_room_color(), self.Room.RANDOM)
            if not self.random_ball_per_rollout:
                self.ball_location = self.sample_disco_ball_location()

        elif self.current_room == self.Room.OBJECTS:
            if self.pos[1] <= -limit:
                if self.enter_door(self.pos[0]):
                    if self.Room.START in self.available_rooms:
                        if not self.corridor_random_room:
                            self.current_room = self.Room.START
                            self.pos[1] = set_coord

            elif self.pos[0] <= -limit:
                if self.enter_door(self.pos[1]):
                    if self.Room.RANDOM in self.available_rooms:
                        if self.corridor_random_room:
                            self.current_room = self.Room.RANDOM
                            self.pos[0] = set_coord

        else:
            raise ValueError("Undefined current room.")

        assert self.current_room in self.available_rooms

        # update objects
        if self.current_room == self.Room.OBJECTS:
            for obj in self.objects:
                self.object_grasped = obj.update_state(self.pos,
                                                       self.gripper_state > 0,
                                                       self.objects,
                                                       self.object_grasped,
                                                       fake_action)

            for obj in self.objects:
                obj.update_all_attributes()

        assert self.image_obs
        self.observation = self.get_image_obs()

        self.steps += 1
        if self.steps == self.n_timesteps:
            self.done = True

        reward = 0

        # obs = np.reshape(np.moveaxis(self.observation.copy(), -1, 0), -1)
        observation = self._get_obs()

        # here additional data
        oracle_reward, per_obj_oracle_reward = self.oracle_reward_fn(
            observation['state_desired_goal'], observation['state_achieved_goal'])
        gripper_state_insensitive_oracle_reward, gripper_state_insensitive_per_obj_oracle_reward = self.oracle_reward_fn(
            observation['state_desired_goal'], observation['state_achieved_goal'], gripper_state_insensitive=True)

        hand_distance, water_distance, dog_distance, dog_correct, room_correct, ball_distance = self.calculate_metrics(
            vector_goal=observation['state_desired_goal'],
            vector_state=observation['state_achieved_goal']
        )

        info = dict(
            hand_distance=hand_distance,
            water_distance=water_distance,
            dog_distance=dog_distance,
            ball_distance=ball_distance,
            hand_and_water_distance=hand_distance+water_distance,
            hand_and_water_and_dog_distance=hand_distance+water_distance+dog_distance,
            dog_correct=dog_correct,
            room_correct=room_correct,
            goal_room=self.vector_to_state_dict(self.goal_info_vector)['current_room'],
            # touch_distance=0.0,
            oracle_reward=oracle_reward,
            per_obj_oracle_reward=per_obj_oracle_reward,
            gripper_state_insensitive_oracle_reward=gripper_state_insensitive_oracle_reward,
            gripper_state_insensitive_per_obj_oracle_reward=gripper_state_insensitive_per_obj_oracle_reward,
            success=oracle_reward,
        )

        return observation, reward, False, info

    def calculate_metrics(self, vector_state, vector_goal):
        desired_goal_dict = self.vector_to_state_dict(vector_goal)
        achieved_goal_dict = self.vector_to_state_dict(vector_state)

        hand_distance = (((desired_goal_dict['gripper_pos'] - achieved_goal_dict['gripper_pos']) ** 2).sum()) ** (1/2)
        water_distance = (
                             ((desired_goal_dict['objects'][1]['position'] - achieved_goal_dict['objects'][1]['position']) ** 2).sum()
                         ) ** (1/2)
        dog_distance = (
                           ((desired_goal_dict['objects'][0]['position'] - achieved_goal_dict['objects'][0]['position']) ** 2).sum()
                       ) ** (1/2)
        ball_distance = (
                           ((desired_goal_dict['ball_location'] - achieved_goal_dict['ball_location']) ** 2).sum()
                       ) ** (1/2)

        dog_correct = desired_goal_dict['objects'][0]['size'] == achieved_goal_dict['objects'][0]['size']
        room_correct = desired_goal_dict['current_room'] == achieved_goal_dict['current_room']

        if not room_correct:
            assert not self.debug_mode
            assert self.rooms_mode
            # if achieved_goal_dict['current_room'] != self.Room.OBJECTS:
            #     # we assert that if we are not in the objects room the water was not moved
            #     # probably true unless the agent moved back to a different room
            #     assert all(achieved_goal_dict['objects'][1]['position'] == self.init_pos_d_w[1])

            # biggest possible distance in the same room is 2.82*(MAX_COORDINATE-self.margin)
            if self.air_wrong_room_distance:
                relative_center = {
                    self.Room.START: np.array([0.0, 0.0]),
                    self.Room.OBJECTS: np.array([0.0, 2 * (MAX_COORDINATE - self.margin)]),
                    self.Room.RANDOM: np.array([2 * (MAX_COORDINATE - self.margin), 0.0]),
                    self.Room.TV: np.array([-2 * (MAX_COORDINATE - self.margin), 0.0]),
                }
                # move locations to absolute locations
                gripper_last_state = achieved_goal_dict['gripper_pos']+relative_center[achieved_goal_dict['current_room']]
                gripper_goal = desired_goal_dict['gripper_pos']+relative_center[desired_goal_dict['current_room']]
                hand_distance = ((gripper_last_state - gripper_goal)**2).sum()

            else:
                water_distance = self.wrong_room_distance
                dog_distance = self.wrong_room_distance
                hand_distance = self.wrong_room_distance
                ball_distance = self.wrong_room_distance

        # todo: use this?
        # metrics = {
        #     "hand_distance": hand_distance,
        #     "water_distance": water_distance,
        #     "dog_distance": dog_distance,
        #     "dog_correct": dog_correct,
        #     "room_correct": room_correct,
        #     "ball_distance": ball_distance
        # }
        # return metrics

        return hand_distance, water_distance, dog_distance, dog_correct, room_correct, ball_distance

    def _get_obs(self):
        info_vec = self.state_dict_to_vector(self.create_info_d())
        # maybe better to transform state_dict to vector?
        observation = {
            "observation": info_vec,
            "state_observation": info_vec,
            "desired_goal": self.goal_info_vector,
            "state_desired_goal": self.goal_info_vector,
            "achieved_goal": info_vec,
            "state_achieved_goal": info_vec,
        }

        return observation

    def sample_random_room_color(self):
        if self.number_of_random_room_colors is not None:
            return random.sample(self.random_room_possible_colors, 1)[0]

        if self.noise_random_color:
            # this is not the acctual color to be used, each pixel will be different actually
            return [-1, -1, -1]
        else:
            return np.random.randint(0, 255, 3)
            # return random.sample(self.random_room_colors, 1)[0]

    def get_image_obs(self):

        background_color = self.get_room_color(self.current_room, self.gripper_state)
        if np.mean(background_color) == -1:
            assert self.noise_random_color or self.noise_TV_color
            assert self.current_room in [self.Room.RANDOM, self.Room.TV]
            background_color_surf = pygame.surfarray.make_surface(np.random.randint(0, 255, (SCREEN_SIZE, SCREEN_SIZE, 3)))
            self.viewer.blit(background_color_surf, (0, 0))

        elif self.square_objects_room and self.current_room == self.Room.OBJECTS:
            background = np.zeros((SCREEN_SIZE, SCREEN_SIZE, 3))+background_color
            sq_size_pix = int((SCREEN_SIZE/(2*MAX_COORDINATE))*self.square_size)
            x1, y1 = SCREEN_SIZE//2-sq_size_pix//2, SCREEN_SIZE//2-sq_size_pix//2
            x2, y2 = SCREEN_SIZE//2+sq_size_pix//2, SCREEN_SIZE//2+sq_size_pix//2

            cv2.rectangle(background, (x1, y1), (x2, y2), (0, 0, 0), -1)
            background_color_surf = pygame.surfarray.make_surface(background)
            self.viewer.blit(background_color_surf, (0, 0))

        elif self.cross_objects_room and self.current_room == self.Room.OBJECTS:
            background = np.zeros((SCREEN_SIZE, SCREEN_SIZE, 3))+background_color
            cross_width_pix = int((SCREEN_SIZE/(2*MAX_COORDINATE))*self.cross_width)

            mi = SCREEN_SIZE//2-cross_width_pix//2
            ma = SCREEN_SIZE//2+cross_width_pix//2

            cv2.rectangle(background, (0, 0), (mi, mi), (0, 0, 0), -1)
            cv2.rectangle(background, (ma, 0), (SCREEN_SIZE, mi), (0, 0, 0), -1)
            cv2.rectangle(background, (ma, ma), (SCREEN_SIZE, SCREEN_SIZE), (0, 0, 0), -1)
            cv2.rectangle(background, (0, ma), (mi, SCREEN_SIZE), (0, 0, 0), -1)

            background_color_surf = pygame.surfarray.make_surface(background)
            self.viewer.blit(background_color_surf, (0, 0))

        else:
            self.viewer.fill(background_color)

        self.shapes = {}
        self.anchors = {}
        self.patches = {}

        # OBJECTS
        if self.current_room == self.Room.OBJECTS:
            for object in self.objects:
                object.update_rendering(self.viewer)

        if self.current_room == self.Room.TV:
            # self.viewer.blit(self.tv_icon, (self.tv_icon_size//2, self.tv_icon_size//2))
            TV_location = self.get_TV_location(self.gripper_state)
            bx, by = get_pixel_coordinates(TV_location[0], TV_location[1])
            left = int(bx - self.tv_icon_size // 2)
            top = int(by - self.tv_icon_size // 2)
            self.viewer.blit(self.tv_icon, (left, top))

        if self.current_room == self.Room.RANDOM:
            bx, by = get_pixel_coordinates(self.ball_location[0], self.ball_location[1])
            left = int(bx - self.disco_icon_size // 2)
            top = int(by - self.disco_icon_size // 2)
            self.viewer.blit(self.disco_icon, (left, top))

        # GRIPPER
        x, y = get_pixel_coordinates(self.pos[0], self.pos[1])
        if self.gripper_state == 1:
            left = int(x - self.size_gripper_closed_pixels // 2)
            top = int(y - self.size_gripper_closed_pixels // 2)
            self.viewer.blit(self.closed_gripper_icon, (left, top))
        else:
            left = int(x - self.size_gripper_pixels // 2)
            top = int(y - self.size_gripper_pixels // 2)
            self.viewer.blit(self.gripper_icon, (left, top))

        if self.render_mode:
            pygame.display.update()
            pygame.time.wait(50)

        image = pygame.surfarray.array3d(self.viewer)

        obs = image.copy().T
        obs = np.array(obs).transpose(1, 2, 0)

        return obs

    def extract_metrics_from_path(
            self,
            path,
            goal_sampling_mode,
            decoded_achieved_goal_key='image_achieved_goal',
            decoded_desired_goal_key='image_desired_goal'
    ):
        if goal_sampling_mode == "custom_goal_sampler":
            # training
            last_next_observation = path['next_observations'][-1]

            assert decoded_achieved_goal_key == 'image_achieved_goal'
            last_image = last_next_observation[decoded_achieved_goal_key]
            last_image_expert_vector = last_next_observation["expert_knowledge_state_achieved_goal"]

            # goal
            assert decoded_desired_goal_key == 'image_desired_goal'
            goal = last_next_observation[decoded_desired_goal_key]
            goal_expert_vector = last_next_observation["expert_knowledge_state_desired_goal"]
            goal_room = self.vector_to_state_dict(goal_expert_vector)['current_room']

            hand_distance, water_distance, dog_distance, dog_correct, room_correct, ball_distance = self.calculate_metrics(
                vector_goal=goal_expert_vector,
                vector_state=last_image_expert_vector
            )

        else:
            # evaluation
            assert goal_sampling_mode in ["reset_of_env", "static_eval"]
            last_infos = path['env_infos'][-1]
            last_next_observation = path['next_observations'][-1]
            # goal = last_next_observation[decoded_desired_goal_key]
            goal = last_next_observation["real_image_desired_goal"]
            last_image = last_next_observation[decoded_achieved_goal_key]

            goal_room = last_infos["goal_room"]
            hand_distance = last_infos["hand_distance"]
            water_distance = last_infos["water_distance"]
            dog_distance = last_infos["dog_distance"]
            room_correct = last_infos["room_correct"]

            # imsize = 48
            # img = last_image.reshape(3, imsize, imsize).transpose()
            # img = img[::, :, ::-1]
            # cv2.imshow('img', img)
            # cv2.waitKey(100)
            #
            # img = goal.reshape(3, imsize, imsize).transpose()
            # img = img[::, :, ::-1]
            # cv2.imshow('goal', img)
            # cv2.waitKey(100)


        perf_metrics = {
            "goal": goal.copy(),
            "last_state": last_image.copy()
        }

        expert_perf_metrics = {
            "goal_room": goal_room,
            "hand_distance": hand_distance,
            "water_distance": water_distance,
            "hand_and_water_distance": hand_distance+water_distance,
            "hand_and_water_and_dog_distance": hand_distance+water_distance+dog_distance,
            "room_correct": room_correct
        }

        return perf_metrics, expert_perf_metrics, {}


    def create_metrics_statistics_dict(
        self,
        eval_expert_cluster_perf_dict,
        eval_expert_cluster_lp_dict
    ):

        # expert_perf
        statistics_dict = {}

        statistics_dict.update({
            "eval_expert_perf_" + str(r): eval_expert_cluster_perf_dict[
                "hand_and_water_distance" if r == self.Room.OBJECTS else "hand_distance"
            ].get(r.value, [2 * self.wrong_room_distance] if r == self.Room.OBJECTS else [self.wrong_room_distance])[-1]
            for r in self.goal_generation_rooms
        })

        # expert lp
        statistics_dict.update({
            "eval_expert_lp_" + str(r): eval_expert_cluster_lp_dict[
                "hand_and_water_distance" if r == self.Room.OBJECTS else "hand_distance"
            ].get(r.value, 0.0) for r in self.goal_generation_rooms
        })

        # expert performance
        statistics_dict.update({"eval_expert_hand_and_water_and_dog_distance_" + str(r):
                               eval_expert_cluster_perf_dict["hand_and_water_and_dog_distance"].get(r.value, [
                                   3 * self.wrong_room_distance])[-1] for r in self.goal_generation_rooms})
        statistics_dict.update({"eval_expert_hand_and_water_distance_" + str(r):
                               eval_expert_cluster_perf_dict["hand_and_water_distance"].get(r.value, [
                                   2 * self.wrong_room_distance])[-1] for r in self.goal_generation_rooms})
        statistics_dict.update({"eval_expert_hand_distance_" + str(r):
                               eval_expert_cluster_perf_dict["hand_distance"].get(r.value,
                                                                                       [self.wrong_room_distance])[-1]
                           for r in self.goal_generation_rooms})
        statistics_dict.update({"eval_expert_correct_room_" + str(r):
                               eval_expert_cluster_perf_dict["room_correct"].get(r.value, [0.0])[-1] for r in
                           self.goal_generation_rooms})

        #  LPs
        statistics_dict.update({"eval_expert_lp_hand_and_water_and_dog_" + str(r): eval_expert_cluster_lp_dict[
            "hand_and_water_and_dog_distance"].get(r.value, 0.0) for r in self.goal_generation_rooms})
        statistics_dict.update({"eval_expert_lp_hand_and_water_" + str(r): eval_expert_cluster_lp_dict[
            "hand_and_water_distance"].get(r.value, 0.0) for r in self.goal_generation_rooms})
        statistics_dict.update(
            {"eval_expert_lp_hand_" + str(r): eval_expert_cluster_lp_dict["hand_distance"].get(r.value, 0.0) for r
             in self.goal_generation_rooms})
        statistics_dict.update(
            {"eval_expert_lp_correct_room_" + str(r): eval_expert_cluster_lp_dict["room_correct"].get(r.value, 0.0)
             for r in self.goal_generation_rooms})

        return statistics_dict

    def render(self):
        pass

    def seed(self, seed):
        np.random.seed(seed)

    def close(self):
        if self.viewer is not None:
            pygame.quit()
            self.viewer = None
