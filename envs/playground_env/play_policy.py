import click
import numpy as np
import pickle
import os

os.environ['LD_LIBRARY_PATH'] = os.environ['HOME'] + '/.mujoco/mjpro150/bin:'
import json
import sys

sys.path.append('../../../')
from src.utils.util import set_global_seeds
import src.architecture_le2.experiment.config as config
from src.architecture_le2.rollout import RolloutWorker
from src.architecture_le2.goal_sampler import GoalSampler, EvalGoalSampler
from src.playground_env.reward_function import get_reward_from_state, water_on_furniture, food_on_furniture, \
    supply_on_furniture
from src.goal_generator.descriptions import get_descriptions
from src.playground_env.env_params import ENV_ID

PATH = 'PATH TO MODEL FOLDER'
n_epochs = 160
POLICY_FILE = PATH + 'policy_checkpoints/policy_{}.pkl'.format(n_epochs)
PARAMS_FILE = PATH + 'params.json'


@click.command()
@click.argument('policy_file', type=str, default=POLICY_FILE)
@click.option('--seed', type=int, default=int(np.random.randint(1e6)))
@click.option('--n_test_rollouts', type=int, default=2000)
@click.option('--render', type=int, default=1)
def main(policy_file, seed, n_test_rollouts, render):
    render = True
    set_global_seeds(seed)

    # Load params
    with open(PARAMS_FILE) as json_file:
        params = json.load(json_file)
    # # Prepare params.

    nb_instr = len(params['train_descriptions'])
    rank = 0
    if not render:
        env = 'PlaygroundNavigation-v1'
    else:
        env = 'PlaygroundNavigationRender-v1'
    params, rank_seed = config.configure_everything(rank=rank,
                                                    seed=seed,
                                                    num_cpu=params['experiment_params']['n_cpus'],
                                                    env=env,
                                                    trial_id=0,
                                                    n_epochs=10,
                                                    reward_function=params['conditions']['reward_function'],
                                                    curriculum_replay_target=params['conditions'][
                                                        'curriculum_replay_target'],
                                                    curriculum_target=params['conditions']['curriculum_target'],
                                                    policy_encoding=params['conditions']['policy_encoding'],
                                                    bias_buffer=params['conditions']['bias_buffer'],
                                                    feedback_strategy=params['conditions']['feedback_strategy'],
                                                    goal_sampling_policy=params['conditions']['goal_sampling_policy'],
                                                    policy_architecture=params['conditions']['policy_architecture'],
                                                    goal_invention=params['conditions']['goal_invention'],
                                                    reward_checkpoint=params['conditions']['reward_checkpoint'],
                                                    rl_positive_ratio=params['conditions']['rl_positive_ratio'],
                                                    p_partner_availability=params['conditions'][
                                                        'p_social_partner_availability'],
                                                    git_commit='',
                                                    power_rarity=5)

    policy_language_model, reward_language_model = config.get_language_models(params)

    onehot_encoder = config.get_one_hot_encoder()
    goal_sampler = GoalSampler(policy_language_model=policy_language_model,
                               reward_language_model=reward_language_model,
                               goal_dim=policy_language_model.goal_dim,
                               one_hot_encoder=onehot_encoder,
                               **params['goal_sampler'],
                               params=params)
    reward_function = config.get_reward_function(goal_sampler, params)
    if params['conditions']['reward_function'] == 'learned_lstm':
        reward_function.restore_from_checkpoint(PATH + 'reward_checkpoints/reward_func_checkpoint_{}'.format(n_epochs))
    # reward_function.load_params(PATH + 'params_reward_{}'.format(n_epochs))

    # reward_function.reward_function.set_or_function(n_obj=4)
    policy_language_model.set_reward_function(reward_function)
    if reward_language_model is not None:
        reward_language_model.set_reward_function(reward_function)

    goal_sampler.update_discovered_goals(params['all_descriptions'], episode_count=0, epoch=0)

    # Load policy.
    with open(policy_file, 'rb') as f:
        policy = pickle.load(f)

    # eval goal sampler samples from set of possible goals to track progress
    eval_goal_sampler = EvalGoalSampler(policy_language_model=policy_language_model,
                                        one_hot_encoder=onehot_encoder,
                                        params=params)

    evaluation_worker = RolloutWorker(make_env=params['make_env'],
                                      policy=policy,
                                      reward_function=reward_function,
                                      params=params,
                                      render=render,
                                      **params['evaluation_rollout_params'])
    # evaluation_worker.env.unwrapped.change_n_objs(n_objs=4)
    evaluation_worker.seed(seed)

    # Run evaluation.
    evaluation_worker.clear_history()
    all_obs = []

    _, test_descriptions, _ = get_descriptions(ENV_ID)
    valid = []
    for d in test_descriptions:
        if 'flower' not in d:
            valid.append(d)
    test_descriptions = valid

    exploit = True
    n_test_rollouts = 1
    frame_count = 0
    descriptions = ['Go bottom right', 'Grasp green cat', 'Grasp red bush', 'Grasp blue door', 'Grow red lion', 'Grow green parrot', 'Grow blue dog' ]
    for d in descriptions:

        goal_str = [d]
        goal_encoding = [policy_language_model.encode(goal_str[0])]
        goal_id = [0]
        ep, frame_count = evaluation_worker.generate_rollouts(exploit,frame_count=frame_count, save_to_png=True, output_dir='gauge',
                                                 imagined=False,
                                                 goals_str=goal_str,
                                                 goals_encodings=goal_encoding,
                                                 goals_ids=goal_id)


if __name__ == '__main__':
    main()
