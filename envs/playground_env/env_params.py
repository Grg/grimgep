import numpy as np
from enum import Enum

N_OBJECTS_IN_SCENE = 1
ENV_ID = 'big'
ADD_HARD_GOALS = False
ADD_GRASPED_OBJ = ADD_HARD_GOALS
admissible_actions = ['Gripper', 'Grasp', 'Grow']
n_inds_before_obj_inds = 3  # use 6 for Playground-v1
MODES = [1, 2]
if ADD_HARD_GOALS:
    magical_creatures = ['unicorn', 'phoenix', 'dragon']
else:
    magical_creatures = []
controllers = ['animal_switch', 'light_controller', 'tv_button']
furnitures = ['door', 'chair', 'desk', 'lamp', 'table']#
if 'big' in ENV_ID:
    furnitures += ['cupboard', 'sink', 'window', 'sofa', 'carpet']
plants = ['cactus', 'carnivorous', 'flower', 'tree', 'bush']#
if 'big' in ENV_ID:
    plants += ['grass', 'algae', 'tea', 'rose', 'bonsai']
animals = ['dog', 'cat', 'cameleon', 'human', 'fly']#
if 'big' in ENV_ID:
    animals += ['parrot', 'mouse', 'lion', 'pig', 'cow']#, 'parrot', 'mouse']
living_things = animals + plants
supply = ['food', 'water']
things = living_things + furnitures + supply
if ADD_HARD_GOALS:
    things += magical_creatures #+ controllers #+ gripper
group_names = ['animal', 'plant', 'living_thing', 'furniture', 'supply']#, 'controller']
groups = [animals, plants, living_things, furnitures, supply]#, controllers, things]
all_non_attributes = ['animal', 'plant', 'living_thing', 'furniture', 'supply', 'controller', 'thing'] + living_things + supply + controllers
thing_colors = ['red', 'blue', 'green']
thing_shades = ['light', 'dark']
thing_sizes = ['big', 'small']

if 'plant' not in ENV_ID:
    words_to_remove_from_train = ['flower']+['red tree', 'green dog', 'blue door'] + \
                                 ['Grasp {} animal'.format(c) for c in thing_colors + ['any']] +  \
                                 ['Grow {} {}'.format(c, p) for c in thing_colors + ['any'] for p in plants + ['plant', 'living_thing']] + \
                                 ['Grasp {} fly'.format(c) for c in thing_colors + ['any']] + \
                                 ['Grow any above {} thing'.format(c) for c in things + thing_colors] + \
                                 ['Grasp any left_of blue thing', 'Grasp any right_of dog thing']
else:
    if 'big' in ENV_ID:
        plants_to_ban = ['algae', 'bonsai', 'tree', 'bush', 'plant', 'living_thing']
    else:
        plants_to_ban = ['tree', 'bush', 'plant', 'living_thing']
    words_to_remove_from_train = ['flower']+['red tree', 'green dog', 'blue door'] + \
                                 ['Grasp {} animal'.format(c) for c in thing_colors + ['any']] +  \
                                 ['Grow {} {}'.format(c, p) for c in thing_colors + ['any'] for p in plants_to_ban] + \
                                 ['Grasp {} fly'.format(c) for c in thing_colors + ['any']] + \
                                 ['Grow any above {} thing'.format(c) for c in things + thing_colors] + \
                                 ['Grasp any left_of blue thing', 'Grasp any right_of dog thing']

    # investigate generalization of :
    # new combinations of attribute + obj type (three first)
    # meaning of attribute projected on new objects (flower)
    # category meaning (Grasp ** animal)
    # generalization of predicate onto known attribute + obj type


n_things = len(things)
n_things_combinatorial = things * len(thing_colors) * len(thing_shades) * len(thing_sizes)

DIM_OBJ = n_things + 7
# get indices of attributes in object feature vector
color_inds = np.arange(n_things + 3, n_things + 6)
size_inds = np.array(n_things + 2)
position_inds = np.arange(n_things, n_things + 2)
type_inds = np.arange(0, n_things)

# init_pos_d_w = [np.array((0.5, 0.5)), np.array((-0.5, -0.5))]

# min_max_sizes = [[0.2, 0.25], [0.25, 0.3]]
min_max_sizes = [[0.18, 0.2], [0.30, 0.32]]
# GRIPPER_SIZE = 0.05

HD = False
if HD:
    raise DeprecationWarning("HD deprecated. Move to constructor")
    MAX_COORDINATE = 1.5
    GRIPPER_RATIO = 0.1375
    CLOSED_GRIPPER_RATIO = 0.1125
    GRIPPER_SIZE = GRIPPER_RATIO*2
    EPSILON = 0  # epsilon for initial positions
    SCREEN_SIZE = 84
    # SCREEN_SIZE = 48

    # RATIO_SIZE = int(SCREEN_SIZE * 2/3 / 2)
    RATIO_SIZE = int(SCREEN_SIZE * 3/4)
    NEXT_TO_EPSILON = 0.3
else:
    MAX_COORDINATE = 1.5
    # GRIPPER_RATIO = 0.25
    # CLOSED_GRIPPER_RATIO = 0.22

    # small_objs = False
    # huge_objs = True
    # if small_objs:
    #     GRIPPER_RATIO = 0.15
    #     CLOSED_GRIPPER_RATIO = 0.13
    #
    # elif huge_objs:
    #     GRIPPER_RATIO = 0.4
    #     CLOSED_GRIPPER_RATIO = 0.35


    # GRIPPER_SIZE = GRIPPER_RATIO*2
    EPSILON = 0  # epsilon for initial positions
    SCREEN_SIZE = 48

    # RATIO_SIZE = int(SCREEN_SIZE * 1.4)  # this is multiplied by min_max sizes

    # if small_objs:
    #     RATIO_SIZE = int(SCREEN_SIZE * 3/5)  # this is multiplied by min_max sizes
    # elif huge_objs:
    #     RATIO_SIZE = int(SCREEN_SIZE * 20/9)  # this is multiplied by min_max sizes

    NEXT_TO_EPSILON = 0.3


ATTRIBUTE_LIST = ['color', 'category', 'type']  #, 'relative_location']#, 'update_relative_size_attribute'] # 'size', 'relative_position', 'relative_shade', 'relative_size', 'shade',
# 'absolute position'

TV_init_color = [50, 150, 50]


# print("OBJ ROOM IS BLACK")
# objects_room_init_color = np.array([0, 0, 0])
# print("OBJ ROOM IS WHITE")
# objects_room_init_color = np.array([255, 255, 255])

# random_room_init_color = [200, 0, 0]

# random_colors = list(np.random.randint(0, 255, (10000, 3)))

# this was moved to variant
# rooms_mode = False
# debug_mode = True
# print("ROOMS MODE: ", rooms_mode)
# if debug_mode:
#     print("DEBUG MODE ONLY ONE ROOM WILL EXIST, ROOM_MODE False")
#     rooms_mode = False
#     available_room = [Room.START, Room.RANDOM, Room.TV, Room.OBJECTS][2]
#
# block_dog = True
# grow = True

random_gripper_location = False
if random_gripper_location:
    print("RANDOM GRIPPER LOCATION")

EPSILON = 0
MAX_COORDINATE = 1.5



