# from src.playground_env.extract_from_state import *
# from src.playground_env.env_params import *
# from src.playground_env.descriptions import train_descriptions, test_descriptions, extra_descriptions
from extract_from_state import *
from env_params import *
from descriptions import train_descriptions, test_descriptions, extra_descriptions


actions = [['Touch'], ['Grasp'], ['Shift'], ['Grow']]
actions_synonyms = [['Touch', 'Go_over', 'Get_close_to', 'Go over', 'Bring_gripper_over', 'Pass_hand_over'], ['Grasp'], ['Shift'], ['Grow']]


def sample_descriptions_from_state(state, modes=MODES, method='one'):
    # modes:
    # 1 use maximum one attribute per object
    # 2 also use attribute + object type
    # 3 also use random order of attributes
    # 4 also use synonyms for action verbs

    descriptions = []

    current_state = state[:len(state) // 2]
    initial_state = current_state - state[len(state) // 2:]
    assert len(current_state) == len(initial_state)

    # extract attributes of objects
    initial_absolute_attributes, initial_relative_attributes = get_attributes_from_state(initial_state)
    current_absolute_attributes, current_relative_attributes = get_attributes_from_state(current_state)

    if 4 in modes:
        act = actions_synonyms
    else:
        act = actions

    if 'Gripper' in admissible_actions:
        # descriptions about the gripper
        hand_pos = current_state[:2]
        if hand_pos[0] < -0.05:
            descriptions.append('Go left')
        elif hand_pos[0] > 0.05:
            descriptions.append('Go right')
        if hand_pos[1] < -0.05:
            descriptions.append('Go bottom')
        elif hand_pos[1] > 0.05:
            descriptions.append('Go top')
        if hand_pos[0] < - 0.25:
            if hand_pos[1] < -0.25:
                descriptions.append('Go bottom left')
            elif hand_pos[1] > 0.25:
                descriptions.append('Go top left')
        elif hand_pos[0] > 0.25:
            if hand_pos[1] < -0.25:
                descriptions.append('Go bottom right')
            elif hand_pos[1] > 0.25:
                descriptions.append('Go top right')
        else:
            if hand_pos[1] < 0.25 and hand_pos[1] > -0.25 and hand_pos[0] < 0.25 and hand_pos[0] > -0.25:
                descriptions.append('Go center')


    # deal with Touch
    # get touched objects
    if 'Touch' in admissible_actions:
        obj_id_touched_current = get_touched_obj_ids(current_state)
        for verb in act[0]:
            for obj_id in obj_id_touched_current:
                if 1 in modes:
                    for attr in current_absolute_attributes[obj_id] + initial_relative_attributes[obj_id]:
                        if attr not in all_non_attributes: #thing_sizes + thing_shades + thing_colors:
                            descriptions.append('{} any {} thing'.format(verb, attr))
                        else:
                            descriptions.append('{} any {}'.format(verb, attr))

                # add combination of attribute and object type
                if 2 in modes:
                    object_types = []
                    for attr in current_absolute_attributes[obj_id]:
                        if attr in things + group_names:
                            object_types.append(attr)
                    for obj_type in object_types:
                        for attr in current_absolute_attributes[obj_id]:
                            if attr not in all_non_attributes:
                                descriptions.append('{} {} {}'.format(verb, attr, obj_type))

    # deal with Grasp
    # get grasped objects
    if 'Grasp' in admissible_actions:
        obj_id_grasped_current = get_grasped_obj_ids(current_state)
        for verb in act[1]:
            for obj_id in obj_id_grasped_current:
                if 1 in modes:
                    for attr in current_absolute_attributes[obj_id] + initial_relative_attributes[obj_id]:
                        if attr not in all_non_attributes: #in thing_sizes + thing_shades + thing_colors:
                            descriptions.append('{} any {} thing'.format(verb, attr))
                        else:
                            descriptions.append('{} any {}'.format(verb, attr))

                # add combination of attribute and object type
                if 2 in modes:
                    # add combination of attribute and object type
                    object_types = []
                    for attr in current_absolute_attributes[obj_id]:
                        if attr in things + group_names:
                            object_types.append(attr)
                    for obj_type in object_types:
                        for attr in current_absolute_attributes[obj_id] + initial_relative_attributes[obj_id]:
                            if attr not in all_non_attributes:
                                descriptions.append('{} {} {}'.format(verb, attr, obj_type))

    # get grasped objects
    if 'Grow' in admissible_actions:
        obj_grown = get_grown_obj_ids(initial_state, current_state)
        for verb in act[3]:
            for obj_id in obj_grown:
                if 1 in modes:
                    for attr in current_absolute_attributes[obj_id] + initial_relative_attributes[obj_id]:
                        if attr not in all_non_attributes:#thing_sizes + thing_shades + thing_colors:
                            descriptions.append('{} any {} thing'.format(verb, attr))
                        else:
                            descriptions.append('{} any {}'.format(verb, attr))

                # add combination of attribute and object type
                if 2 in modes:
                    # add combination of attribute and object type
                    object_types = []
                    for attr in current_absolute_attributes[obj_id]:
                        if attr in things + group_names:
                            object_types.append(attr)
                    for obj_type in object_types:
                        for attr in current_absolute_attributes[obj_id] + initial_relative_attributes[obj_id]:
                            if attr not in all_non_attributes:
                                descriptions.append('{} {} {}'.format(verb, attr, obj_type))

        for obj_id in get_transformed_obj_ids(initial_state, current_state):
            if 'unicorn' in current_absolute_attributes[obj_id]:
                descriptions.append('Transform dark unicorn')
            elif 'phoenix' in current_absolute_attributes[obj_id]:
                descriptions.append('Transform dark phoenix')
            elif 'dragon' in current_absolute_attributes[obj_id]:
                descriptions.append('Transform dark dragon')

    # deal with furniture stuff
    if 'Grow' in admissible_actions:
        obj_on_supply = get_obj_ids_on_supply(initial_state, current_state)
        verb = 'Attempt grow'
        for obj_id in obj_on_supply:
            if 1 in modes:
                for attr in current_absolute_attributes[obj_id] + initial_relative_attributes[obj_id]:
                    if attr in plants + furnitures + ['plant', 'furniture']:
                        descriptions.append('{} any {}'.format(verb, attr))

            # add combination of attribute and object type
            if 2 in modes:
                # add combination of attribute and object type
                object_types = []
                for attr in current_absolute_attributes[obj_id]:
                    if attr in things + group_names:
                        object_types.append(attr)
                for obj_type in object_types:
                    for attr in current_absolute_attributes[obj_id]:
                        if attr not in all_non_attributes:
                            if obj_type in plants + furnitures + ['plant', 'furniture']:
                                descriptions.append('{} {} {}'.format(verb, attr, obj_type))


    # deal with Shift
    if 'Shift' in admissible_actions:
        left, right, higher, lower = get_shifted_obs_ids(initial_state, state)
        for verb in act[2]:
            for obj_id in range(N_OBJECTS_IN_SCENE):
                dir = []
                if obj_id in left:
                    dir.append('left')
                elif obj_id in right:
                    dir.append('right')
                if obj_id in higher:
                    dir.append('top')
                elif obj_id in lower:
                    dir.append('bottom')

                object_types = []
                for attr in current_absolute_attributes[obj_id]:
                    if attr in things + group_names:
                        object_types.append(attr)

                for d in dir:
                    if 1 in modes:
                        for attr in current_absolute_attributes[obj_id] + initial_relative_attributes[obj_id]:
                            if attr in thing_sizes + thing_shades + thing_colors:
                                descriptions.append('{} any {} thing {}'.format(verb, attr, d))
                            else:
                                descriptions.append('{} any {} {}'.format(verb, attr, d))

                    # add combination of attribute and object type
                    if 2 in modes:
                        # add combination of attribute and object type
                        for obj_type in object_types:
                            for attr in current_absolute_attributes[obj_id]:
                                if attr not in all_non_attributes:
                                    descriptions.append('{} {} {} {}'.format(verb, attr, obj_type, d))

    train_descr = []
    test_descr = []
    extra_descr = []
    for descr in descriptions:
        if descr in train_descriptions:
            train_descr.append(descr)
        if descr in test_descriptions:
            test_descr.append(descr)
        if descr in extra_descriptions:
            extra_descr.append(descr)
    return sorted(list(set(train_descr.copy()))), sorted(list(set(test_descr.copy()))), sorted(list(set(extra_descr.copy())))

def get_reward_from_state(state, goal):
    current_state = state[:len(state) // 2]
    initial_state = current_state - state[len(state) // 2:]
    assert len(current_state) == len(initial_state)

    # extract attributes of objects
    initial_absolute_attributes, initial_relative_attributes = get_attributes_from_state(initial_state)
    current_absolute_attributes, current_relative_attributes = get_attributes_from_state(current_state)

    words = goal.split(' ')
    reward = False

    # Deal with gripper goals
    if words[0] == 'Go':
        # descriptions about the gripper
        hand_pos = current_state[:2]
        if words[1] == 'top':
            if len(words) > 2:
                if hand_pos[1] > 0.25:
                    if hand_pos[0] < -0.25 and words[2] == 'left':
                        return True
                    elif hand_pos[0] > 0.25 and words[2] == 'right':
                        return True
                    else:
                        return False
                else:
                    return False
            elif hand_pos[1] > 0.05:
                return True
            else:
                return False
        elif words[1] == 'bottom':
            if len(words) > 2:
                if hand_pos[1] < -0.25:
                    if hand_pos[0] < -0.25 and words[2] == 'left':
                        return True
                    elif hand_pos[0] > 0.25 and words[2] == 'right':
                        return True
                    else:
                        return False
                else:
                    return False
            elif hand_pos[1] < -0.05:
                return True
            else:
                return False
        elif words[1] == 'left':
            if len(words) > 2:
                if hand_pos[0] < -0.25:
                    if hand_pos[1] < -0.25 and words[2] == 'bottom':
                        return True
                    elif hand_pos[1] > 0.25 and words[2] == 'top':
                        return True
                    else:
                        return False
                else:
                    return False
            elif hand_pos[0] < -0.05:
                return True
            else:
                return False
        elif words[1] == 'right':
            if len(words) > 2:
                if hand_pos[0] > 0.25:
                    if hand_pos[1] < -0.25 and words[2] == 'bottom':
                        return True
                    elif hand_pos[1] > 0.25 and words[2] == 'top':
                        return True
                    else:
                        return False
                else:
                    return False
            elif hand_pos[0] > 0.05:
                return True
            else:
                return False
        elif words[1] == 'center':
            if hand_pos[1] < 0.25 and hand_pos[1] > -0.25 and hand_pos[0] < 0.25 and hand_pos[0] > -0.25:
                return True
            else:
                return False


    # Deal with grasped
    if words[0] in actions_synonyms[1]:
        # get grasped objects
        obj_id_grasped_current = get_grasped_obj_ids(current_state)
        # check whether the goal refers to any of the grasped objects
        for id in obj_id_grasped_current:
            # in that case only the attribute matters
            if words[1] == 'any':
                if words[2] in current_absolute_attributes[id] + initial_relative_attributes[id]:
                    return True
                elif len(words) > 3:
                    if words[2] + ' ' + words[3] in initial_relative_attributes[id]:
                        return True
            # in other cases, check whether the last word is an object type
            elif words[-1] in current_absolute_attributes[id] and (words[-1] in things or words[-1] in group_names):
                # check that the attributes corresponds to that object
                if words[1] in current_absolute_attributes[id]:
                    return True
                elif len(words) > 2:
                    if words[1] + ' ' + words[2] in initial_relative_attributes[id]:
                        return True


    # Deal with grow
    if words[0] in actions_synonyms[3]:
        # get grasped objects
        obj_id_grown = get_grown_obj_ids(initial_state, current_state)
        # check whether the goal refers to any of the grasped objects
        for id in obj_id_grown:
            # in that case only the attribute matters
            if words[1] == 'any':
                if words[2] in current_absolute_attributes[id] + initial_relative_attributes[id]:
                        return True
                elif len(words) > 3:
                    if words[2] + ' ' + words[3] in initial_relative_attributes[id]:
                        return True
            # in other cases, check whether the last word is an object type
            elif words[-1] in current_absolute_attributes[id] and (words[-1] in things or words[-1] in group_names):
                # check that the attributes corresponds to that object
                if words[1] in current_absolute_attributes[id]:
                    return True
                elif len(words) > 2:
                    if words[1] + ' ' + words[2] in initial_relative_attributes[id]:
                        return True
    if words[0] == 'Transform':
        transformed_obj_id = get_transformed_obj_ids(initial_state, current_state)
        for id in transformed_obj_id:
            if words[2] in current_absolute_attributes[id]:
                return True


    return reward





def supply_on_furniture(state, goal):
    current_state = state[:len(state) // 2]
    initial_state = current_state - state[len(state) // 2:]
    assert len(current_state) == len(initial_state)

    # extract attributes of objects
    initial_absolute_attributes, initial_relative_attributes = get_attributes_from_state(initial_state)
    current_absolute_attributes, current_relative_attributes = get_attributes_from_state(current_state)

    words = goal.split(' ')
    reward = False


    # Deal with grow
    if words[0] in actions_synonyms[3]:
        # get grasped objects
        obj_id_water = get_obj_ids_on_water(initial_state, current_state).tolist()
        obj_id_food = get_obj_ids_on_food(initial_state, current_state).tolist()
        # check whether the goal refers to any of the grasped objects
        for id in obj_id_water + obj_id_food:
            # in that case only the attribute matters
            if words[1] == 'any':
                if words[2] in current_absolute_attributes[id] + current_relative_attributes[id]:
                        return True
            # in other cases, check whether the last word is an object type
            elif words[2] in current_absolute_attributes[id] and (words[2] in things or words[2] in group_names):
                # check that the attributes corresponds to that object
                if words[1] in current_absolute_attributes[id]:
                    return True

    return reward

def food_on_furniture(state, goal):
    current_state = state[:len(state) // 2]
    initial_state = current_state - state[len(state) // 2:]
    assert len(current_state) == len(initial_state)

    # extract attributes of objects
    initial_absolute_attributes, initial_relative_attributes = get_attributes_from_state(initial_state)
    current_absolute_attributes, current_relative_attributes = get_attributes_from_state(current_state)

    words = goal.split(' ')
    reward = False


    # Deal with grow
    if words[0] in actions_synonyms[3]:
        # get grasped objects
        obj_id_food = get_obj_ids_on_food(initial_state, current_state).tolist()
        # check whether the goal refers to any of the grasped objects
        for id in obj_id_food:
            # in that case only the attribute matters
            if words[1] == 'any':
                if words[2] in current_absolute_attributes[id] + current_relative_attributes[id]:
                        return True
            # in other cases, check whether the last word is an object type
            elif words[2] in current_absolute_attributes[id] and (words[2] in things or words[2] in group_names):
                # check that the attributes corresponds to that object
                if words[1] in current_absolute_attributes[id]:
                    return True

    return reward


def water_on_furniture(state, goal):
    current_state = state[:len(state) // 2]
    initial_state = current_state - state[len(state) // 2:]
    assert len(current_state) == len(initial_state)

    # extract attributes of objects
    initial_absolute_attributes, initial_relative_attributes = get_attributes_from_state(initial_state)
    current_absolute_attributes, current_relative_attributes = get_attributes_from_state(current_state)

    words = goal.split(' ')
    reward = False


    # Deal with grow
    if words[0] in actions_synonyms[3]:
        # get grasped objects
        obj_id_water = get_obj_ids_on_water(initial_state, current_state).tolist()
        # check whether the goal refers to any of the grasped objects
        for id in obj_id_water:
            # in that case only the attribute matters
            if words[1] == 'any':
                if words[2] in current_absolute_attributes[id] + current_relative_attributes[id]:
                        return True
            # in other cases, check whether the last word is an object type
            elif words[2] in current_absolute_attributes[id] and (words[2] in things or words[2] in group_names):
                # check that the attributes corresponds to that object
                if words[1] in current_absolute_attributes[id]:
                    return True

    return reward




