import numpy as np
import glob
import copy
import math

import traceback
import termcolor
import random

from enum import IntEnum, Enum
from gym import spaces
from ..miniworld import MiniWorldEnv, Room
from ..entity import Entity, ImageFrame, TVFrame, MeshEnt, Box, Key, Ball, COLOR_NAMES

from collections import OrderedDict
from multiworld.envs.env_util import create_stats_ordered_dict, get_stat_in_paths


class ThreeRoomsTVRicher(MiniWorldEnv):
    """
    Two small rooms connected to one large room
    """

    class Room(Enum):
        GALLERY = 0
        OFFICE = 1
        TV = 2
        CORRIDOR_GALLERY_OFFICE = 3
        CORRIDOR_GALLERY_TV = 4

    # Enumeration of possible actions
    class Actions(IntEnum):
        # Turn left or right by a small amount
        turn_left = 0
        turn_right = 1

        # Move forward or back by a small amount
        move_forward = 2
        move_back = 3

        # Move left or right by a small amount
        move_left = 4
        move_right = 5

        # # Pick up or drop an object being carried
        # pickup = 6
        # drop = 7

        # Toggle/activate an object
        toggle = 6

    def __init__(self,
                 obs_width=320,
                 obs_height=320,
                 max_episode_steps=400,
                 static_eval_set_size=20,
                 goal_generation_rooms=None,
                 TV_active=True,
                 TV_type="imagenet_very_small",
                 auto_change_image_prob=0.2,
                 start_room="TV",
                 static_initial_position=True,
                 **kwargs
        ):
        self.obs_height = obs_height
        self.obs_width = obs_width
        self.max_episode_steps = max_episode_steps
        self.TV_active = TV_active
        self.TV_type = TV_type
        self.auto_change_image_prob = auto_change_image_prob
        self.static_initial_position = static_initial_position
        self.start_room = start_room
        self.entities_dict = {}

        # true state size
        # this must be before super init because super init calls reset
        # self.state_vector_size = 6
        self.state_vector_size = 20

        # this calls gen_world
        super().__init__(
            obs_width=obs_width,
            obs_height=obs_height,
            max_episode_steps=max_episode_steps,
            **kwargs
        )

        # # Allow only the movement actions
        # self.action_space = spaces.Discrete(self.actions.toggle+1)

        # we set the actions space to be continuous

        # left/right, forward/backward, right/left, toggle
        # self.n_act = 5
        # self.n_act = 6
        self.n_act = 4
        self.action_space = spaces.Box(low=-np.ones(self.n_act),
                                       high=np.ones(self.n_act),
                                       dtype=np.float32)

        self.obs_box = spaces.Box(
            low=0, high=255, shape=(self.obs_height, self.obs_width, 3), dtype=np.uint8
        )


        self.goal_box = spaces.Box(
            np.array([-1] * self.state_vector_size),
            np.array([1] * self.state_vector_size),
        )

        self.observation_space = spaces.Dict([
            ("observation", self.obs_box),
            ("state_observation", self.obs_box),

            # achieved and desired goal have the same shape
            ("desired_goal", self.goal_box),
            ("state_desired_goal", self.goal_box),
            ("achieved_goal", self.goal_box),
            ("state_achieved_goal", self.goal_box),
        ])

        self.available_rooms = [
            self.Room.OFFICE,
            self.Room.GALLERY,
            self.Room.TV,
            self.Room.CORRIDOR_GALLERY_OFFICE,
            self.Room.CORRIDOR_GALLERY_TV
        ]

        if goal_generation_rooms is None:
            self.goal_generation_rooms = self.available_rooms
        else:
            self.goal_generation_rooms = goal_generation_rooms

        print("aval:", self.available_rooms)
        print("gen:", self.goal_generation_rooms)
        for g in self.goal_generation_rooms: assert g in self.available_rooms

        # tv textures
        self.static_eval_set_size = static_eval_set_size
        assert self.static_eval_set_size == 20
        self.static_eval_set_counter = 0
        self.static_eval_set = [self.state_dict_to_vector(d) for d in self.static_eval_goal_dicts()]
        assert len(self.static_eval_set) == self.static_eval_set_size
        # assert conversions
        assert all(
            self.state_dict_to_vector(self.vector_to_state_dict(self.static_eval_set[0])) == self.static_eval_set[0]
        )

        self.reset()

        # for visualization -> ent_names_to_visualize
        assert len(self.entities_dict.keys()) == 14
        self.ent_names_to_visualize = list(sorted(self.entities_dict.keys()))
        pos = self.ent_names_to_visualize.index("tv")
        self.ent_names_to_visualize[pos:pos + 1] = ("tv_OFF", "tv_ON")  # replace tv with ON/OFF

    def _gen_world(self):
        # GALLERY
        gallery_room = self.add_rect_room(
            min_x=-6.5, max_x=-0.5,
            min_z=0.5, max_z=6.5,
            wall_tex='cardboard',
            ceil_tex='ceiling_tiles',
            # floor_tex='floor_tiles_bw',
            floor_tex='parket',
            # floor_tex='wood',
        )
        assert self.Room.GALLERY.value == 0

        # OFFICE
        office_room = self.add_rect_room(
            min_x=-6.5, max_x=-0.5,
            min_z=-6.5, max_z=-0.5,
            # wall_tex='cinder_blocks',
            wall_tex='stucco',
            ceil_tex='stucco',
            # ceil_tex='concrete_tiles',
            # floor_tex='floor_tiles_bw',
            floor_tex='wood',
            # floor_tex='marble',
        )
        assert self.Room.OFFICE.value == 1

        # TV
        tv_room = self.add_rect_room(
            min_x=0.5, max_x=6.5,
            min_z=0.5, max_z=6.5,
            # wall_tex='concrete',
            wall_tex='brick_wall',
            ceil_tex='concrete_tiles',
            # floor_tex='floor_tiles_bw',
            # floor_tex='parket',
            floor_tex='carpet',
        )
        assert self.Room.TV.value == 2

        # Connect the rooms with portals/openings
        self.connect_rooms(gallery_room, office_room, min_x=-4.5, max_x=-2.5)

        assert self.Room.CORRIDOR_GALLERY_OFFICE.value == 3

        self.connect_rooms(gallery_room, tv_room, min_z=2.5, max_z=4.5)

        assert self.Room.CORRIDOR_GALLERY_TV.value == 4

        # Painting on the walls in the lobby

        # gallery objects
        self.entities_dict["wooden_chair"] = MeshEnt(
            height=1.3,
            static=True,
            mesh_name='wooden_chair'
        )
        self.place_entity(
            self.entities_dict["wooden_chair"],
            pos=[-1., 0, 6.],
            dir=np.pi,
        )

        self.entities_dict['bench'] = MeshEnt(
            height=1.0,
            static=True,
            mesh_name='bench'
        )
        self.place_entity(
            self.entities_dict["bench"],
            pos=[-1., 0, 1.5],
            dir=0,
        )

        self.entities_dict["impression"] = ImageFrame(
            pos=[-6.5, 1.35, 3.5],
            dir=0,
            width=2.0,
            tex_name="impression"
        )
        self.entities.append(self.entities_dict["impression"])

        # self.entities_dict["portraits/alice_pike_barney"] = ImageFrame(
        #     pos=[-6.5, 1.35, 3.5],
        #     dir=0,
        #     width=1.8,
        #     tex_name='portraits/alice_pike_barney'
        # )
        # self.entities.append(self.entities_dict["portraits/alice_pike_barney"])

        self.entities_dict["starry_night"] = ImageFrame(
            pos=[-3.5, 1.35, 6.5],
            dir=math.pi/2,
            width=2.0,
            tex_name="starry_night"
        )
        self.entities.append(self.entities_dict["starry_night"])
        # self.entities_dict["portraits/aman_theodor"] = ImageFrame(
        #     pos=[-3.5, 1.35, 6.5],
        #     dir=math.pi/2,
        #     width=1.8,
        #     tex_name='portraits/aman_theodor'
        # )
        # self.entities.append(self.entities_dict["portraits/aman_theodor"])

        # # office objects

        # plant
        # https://www.cgtrader.com/free-3d-models/plant/pot-plant/pot-plant-0a4b77e0-e073-4183-b92f-d12a53eecce4
        self.entities_dict["potted_plant"] = MeshEnt(
            height=1.,
            static=True,
            mesh_name='potted_plant'
        )
        self.place_entity(
            self.entities_dict["potted_plant"],
            # pos=[-3, 0, -5.5],
            pos=[-1.5, 0, -5.5],
            dir=0,
        )

        # # plant
        # # https://sketchfab.com/3d-models/indoor-plant-bpaOec5a2WUaiKuF0ATci1cjCK7 licence
        # self.place_entity(MeshEnt(
        #     height=1.5,
        #     static=True,
        #     mesh_name='palm_fan'
        # ),
        #     pos=[-3, 0, -5.5],
        #     dir=0,
        # )

        self.entities_dict["office_desk"] = MeshEnt(
            height=2.,
            static=True,
            mesh_name='office_desk'
        )
        self.place_entity(
            self.entities_dict["office_desk"],
            pos=[-5.5, 0, -6.],
            dir=0,
        )

        self.entities_dict["office_chair"] = MeshEnt(
            height=1.5,
            static=True,
            mesh_name='office_chair'
        )
        self.place_entity(
            self.entities_dict["office_chair"],
            pos=[-5.5, 0, -5.5],
            dir=0,
        )

        # Box
        self.entities_dict["box"] = Box(color="red", size=1.0)
        self.place_entity(self.entities_dict["box"], room=office_room, pos=[-1.5, 0, -1.5], dir=0.0)

        # Grate
        self.entities_dict["airduct_grate"] = ImageFrame(
            pos=[-0.5, .7, -5.5],
            dir=-math.pi,
            width=1.,
            tex_name='airduct_grate'
        )
        self.entities.append(self.entities_dict["airduct_grate"])

        # Whiteboard
        self.entities_dict["whiteboard"] = ImageFrame(
            pos=[-6.5, 1.5, -3],
            dir=0,
            width=3.,
            tex_name='whiteboard'
        )
        self.entities.append(self.entities_dict["whiteboard"])

        self.entities_dict["agent"] = self.agent

        if self.static_initial_position:
            if self.start_room == "GALLERY":
                self.place_agent(pos=[-3.5, 0.0, 3.5], dir=np.pi*1/4)   # Gallery

            elif self.start_room == "TV":
                self.place_agent(pos=[3.5, 0.0, 3.5], dir=0)  # TV
            else:
                raise NotImplementedError("Undefined room")
        else:
            self.place_agent()

        # # TV room objects
        # self.entities_dict["tv"] = ImageFrame(
        #     pos=[3.5, 1.35, 6.5],
        #     dir=math.pi/2,
        #     # width=1.8,
        #     width=3.0,
        #     tex_name='tv/tv'
        # )
        self.tv_tex_name_off = "tv/tv"

        # self.tv_tex_names_on = [
        #     "tv/tv_cat1",
        #     "tv/tv_cat2",
        #     "tv/tv_cat3",
        #     "tv/tv_cat4",
        #     "tv/tv_cat5",
        #     "tv/tv_cat6",
        #     "tv/tv_cat7",
        #     "tv/tv_cat8",
        #     "tv/tv_cat9",
        #     "tv/tv_cat10",
        #     "tv/tv_dog1",
        #     "tv/tv_dog2",
        #     "tv/tv_dog3",
        #     "tv/tv_dog4",
        #     "tv/tv_dog5",
        #     "tv/tv_dog6",
        #     "tv/tv_dog7",
        #     "tv/tv_dog8",
        #     "tv/tv_dog9",
        #     "tv/tv_dog10",
        #     "tv/tv_car1",
        #     "tv/tv_car2",
        #     "tv/tv_car3",
        #     "tv/tv_car4",
        #     "tv/tv_car5",
        #     "tv/tv_car6",
        #     "tv/tv_car7",
        #     "tv/tv_car8",
        #     "tv/tv_car9",
        #     "tv/tv_car10",
        # ]

        if self.TV_type == "colors":
            # colors tv
            self.tv_tex_names_on = [
                p.replace("./envs/gym_miniworld/textures/", "").replace("_1.png", "") for p in
                     glob.glob("./envs/gym_miniworld/textures/tv_colors/*")
            ]
            # assert len(self.tv_tex_names_on) == 728
        elif self.TV_type == "imagenet":
            # imagenet tv
            self.tv_tex_names_on = [
                p.replace("./envs/gym_miniworld/textures/", "").replace("_1.png", "") for p in
                glob.glob("./envs/gym_miniworld/textures/tv_imagenet/*")
            ]

        elif self.TV_type == "imagenet_very_small":
            # imagenet tv
            self.tv_tex_names_on = [
                p.replace("./envs/gym_miniworld/textures/", "").replace("_1.png", "") for p in
                glob.glob("./envs/gym_miniworld/textures/tv_imagenet_very_small/*")
            ]
        else:
            raise ValueError("Unknown TV type")

        self.entities_dict["tv"] = TVFrame(
            pos=[3.5, 1.35, 6.5],
            dir=math.pi/2,
            width=3.0,
            tex_name_off=self.tv_tex_name_off,
            tex_names_on=self.tv_tex_names_on,
            # auto_change_image_prob=0.2,
            auto_change_image_prob=self.auto_change_image_prob,
        )
        self.entities.append(self.entities_dict["tv"])

        # Couch
        self.entities_dict["couch"] = MeshEnt(
            height=1.2,
            static=True,
            mesh_name='couch'
        )
        self.place_entity(
            self.entities_dict["couch"],
            pos=[3.5, 0, 1.0],
            dir=-math.pi/2,
        )

        # https://www.cgtrader.com/items/1012579/download-page
        self.entities_dict["clock"] = MeshEnt(
            height=1.2,
            static=True,
            mesh_name='clock'
        )
        self.place_entity(
            self.entities_dict["clock"],
            pos=[6.4, 1.35, 3.5],
            dir=-math.pi/2,
        )

        assert len(self.entities_dict) == len(self.entities)

    def initialize_camera(self, bla):
        pass

    def get_image(self, width, height):
        obs = self.get_image_obs()

        assert obs.shape == (width, height, 3)
        return obs

    def get_goal(self):
        return {
            "desired_goal": self.goal_info_vector,
            "state_desired_goal": self.goal_info_vector,
        }

    def get_next_static_eval_goals(self, batch_size):
        # number_of_eval_steps_per_epoch should be rollout length * 25
        assert batch_size == 1
        index = self.static_eval_set_counter % self.static_eval_set_size
        goals = [self.static_eval_set[index]]
        self.static_eval_set_counter += 1

        return {
            "desired_goal": goals,
            "state_desired_goal": goals,
        }

    def set_to_goal(self, goal):
        goal_info_vector = goal["state_desired_goal"]
        goal_info_dict = self.vector_to_state_dict(goal_info_vector)
        self.set_context(goal_info_dict)


    def compute_rewards(self, action, obs, info=None):
        return np.array([-13.0])

    def get_env_state(self):
        return copy.deepcopy(self.state_dict_to_vector(self.create_info_d()))

    def create_init_context(self):
        raise DeprecationWarning()

    def get_init_context(self):
        raise DeprecationWarning()

    def set_env_state(self, state):
        self.set_context(self.vector_to_state_dict(state))

    def set_context(self, context):

        self.agent.pos[0] = context["agent_xya"][0]
        self.agent.pos[2] = context["agent_xya"][1]
        self.agent.dir = context["agent_xya"][2]

        # non static: agent, box, tv
        assert len([e for e in self.entities if not e.is_static]) == 3

        # current room is detected online based on agent.pos
        assert self.current_room() == context["current_room"]

        self.entities_dict["tv"].set_tv_context(context["tv_state"], context["tv_tex_name"])

        # todo: figure out why is it not identical when loading the replay buffer?
        # assert set(self.create_info_d()["visible_entities"]) == set(context["visible_entities"])
        if not set(self.create_info_d()["visible_entities"]) == set(context["visible_entities"]):
            print(
                "set context does not match: \n",
                str(set(self.create_info_d()["visible_entities"])) + " vs " + str(set(context["visible_entities"]))
            )

            # import matplotlib.pyplot as plt
            # plt.imshow(self.get_image_obs())
            # plt.show()

    def set_goal(self, goal):
        self.goal_info_vector = goal["state_desired_goal"]

    def sample_goals(self, batch_size):
        goals = [self.state_dict_to_vector(self.sample_goal_dict()) for _ in range(batch_size)]
        return {
            "desired_goal": goals,
            "state_desired_goal": goals,
        }

    def state_dict_is_TV_ON(self, state_dict):
        # todo: make static
        return state_dict["tv_state"]

    def static_eval_goal_dicts(self):
        # generating evaluation set
        dicts = [{
            "agent_xya": [

                # GALLERY
                np.array([-3.28786797, 3.28786797, 0.78539816]),  # bench
                np.array([-3.8, 1.2, 0.0]),  # bench
                np.array([-3.5, 3.5, 3.1]),  # portr 1
                np.array([-5.2944153, 1.6320693, 3.92699082]),  # portr 1
                np.array([-5.52699183, 4.47439943, 5.23598776]),  # portr 2
                np.array([-5.82699183, 3.95478419, 5.49778714]),  # char portr 2
                np.array([-3.5, 3.5, 5.5]),  # wooden chair
                np.array([-1.00191251, 3.31054427, 4.71238898]),  # wooden chair

                # OFFICE
                np.array([-3.00366493, -1.82968068, 2.87979327]),  # whiteboard

                np.array([-6.07196423, -2.31078535, 1.30899694]),  # desktop back
                np.array([-3.14372596, -3.51150656, 2.35619449]),  # side desk

                np.array([-2.72820971, -2.03590415, 8.90117919]),  # desk, whiteboard

                np.array([-3.45, -2.67, np.pi / 3.0]),  # plant, grate
                np.array([-1.35, -2.67, np.pi/2.0]),  # plant, grade

                np.array([-3.89330876, -3.34359731, -0.52359878]),  # box
                np.array([-5.74291742, -3.37426407, 6.02138592]),  # box

                # TV
                np.array([1.3, 3.5, 0]),  # clock
                np.array([3.89930466, 5.38586557, 0.52359878]),  # clock
                np.array([1.75, 3.75980762, 1.04719755]),  # couch
                np.array([4.19697896, 4.33626347, 1.83259571]),  # couch

                # np.array([4.89579038, 2.88737473, -1.83259571])  # TV
                # np.array([1.6023257, 2.99075638, -1.04719755])  # TV

            ][i % self.static_eval_set_size],
            "current_room": [
                self.Room.GALLERY,
                self.Room.GALLERY,
                self.Room.GALLERY,
                self.Room.GALLERY,
                self.Room.GALLERY,
                self.Room.GALLERY,
                self.Room.GALLERY,
                self.Room.GALLERY,

                self.Room.OFFICE,
                self.Room.OFFICE,
                self.Room.OFFICE,
                self.Room.OFFICE,
                self.Room.OFFICE,
                self.Room.OFFICE,
                self.Room.OFFICE,
                self.Room.OFFICE,

                self.Room.TV,
                self.Room.TV,
                self.Room.TV,
                self.Room.TV,
            ][i % self.static_eval_set_size],
            "tv_state": False,
            "tv_tex_name": self.tv_tex_name_off,
        } for i in range(self.static_eval_set_size)]

        for d in dicts:
            d["visible_entities"] = self.calculate_visible_ents_for_state_dict(d)

        # check consistency
        for d in dicts:
            assert d["current_room"] == self.get_room_for_xy(d["agent_xya"][:2])
            assert d["current_room"] in self.goal_generation_rooms

        return dicts

    def sample_goal_dict(self):
        # uniformly sampling goals
        # we don"t want to use this, so it"s a dummy goal
        goal = {
            "agent_xya": np.array([-3.05, 3.55, 2.85]),
            "current_room": self.Room.GALLERY,
            "tv_state": False,
            "tv_tex_name": self.tv_tex_name_off
        }
        goal["visible_entities"] = self.calculate_visible_ents_for_state_dict(goal)

        # check consistency
        assert self.Room.GALLERY == self.get_room_for_xy(goal["agent_xya"][:2])

        return goal

    def get_room_for_xy(self, pos_xy):
        x, y = pos_xy
        agent_pos = (x, 0, y)
        rooms = [i for (i, r) in enumerate(self.rooms) if r.point_inside(agent_pos)]

        if len(rooms) < 1:
            raise ValueError("Position {} is not in any room.".format(pos_xy))
        if len(rooms) > 1:
            raise ValueError("Position {} is not in many rooms.".format(pos_xy))

        room = rooms[0]
        return self.Room(room)

    def oracle_reward_fn(
            self,
            goal_context_vector,
            achieved_goal_context_vector,
    ):
        # goal_context = self.vector_to_state_dict(goal_context_vector)
        # achieved_goal_context = self.vector_to_state_dict(achieved_goal_context_vector)

        visible_wall_iou = 0
        same_location = 0

        return visible_wall_iou, same_location

    def reward_fn(
            self,
            goals_contexts_vector,
            achieved_goals_contexts_vector,
            gripper_state_insensitive=False
    ):
        raise DeprecationWarning()

    def create_info_d(self):

        static_entities_ = [e for e in self.entities if not e.is_static]
        assert len(static_entities_) == 3
        # agent, box, tv -> pickup doesn't exist so only tv and agent can be changed

        agent_xya = np.array([self.agent.pos[0], self.agent.pos[2], self.agent.dir])

        info_d = {
            "agent_xya": agent_xya,
            "current_room": self.current_room(),
            "tv_state": self.entities_dict["tv"].is_tv_on(),
            "tv_tex_name": self.entities_dict["tv"].current_tex_name(),
        }

        # assert consistency
        if info_d["tv_state"]:
            assert info_d["tv_tex_name"] in self.tv_tex_names_on
        else:
            assert info_d["tv_tex_name"] == self.tv_tex_name_off

        assert not bool(self.agent.carrying)

        visible_entities = self.calculate_visible_ents_for_state_dict(info_d)
        info_d["visible_entities"] = visible_entities

        success = float(self.oracle_reward_fn(
            self.goal_info_vector,
            self.state_dict_to_vector(info_d)
        )[0])

        info_d = {
            **info_d, **{"success": success}
        }

        return info_d

    def state_dict_to_vector(self, s_dict):
        vector = np.concatenate([
            s_dict["agent_xya"],
            [s_dict["current_room"].value],
            [int(s_dict["tv_state"])],
            [self.tv_tex_names_on.index(s_dict["tv_tex_name"]) if s_dict["tv_state"] else -1]
        ])
        bag_of_ents = np.array([int(e in s_dict["visible_entities"]) for e in sorted(self.entities_dict.keys())])
        vector = np.concatenate([vector, bag_of_ents])

        if s_dict["tv_state"]:
            assert self.tv_tex_names_on.index(s_dict["tv_tex_name"]) >= 0

        assert vector.shape == (self.state_vector_size,)

        return vector

    def vector_to_state_dict(self, vec):
        agent_xya = vec[:3]
        current_room_id = vec[3]
        tv_state = bool(vec[4])
        tv_tex_id = int(vec[5])  # np.float to int
        bag_of_ents = vec[6:20]

        assert len(bag_of_ents) == len(self.entities_dict.keys())
        visible_entities = [e for e, inside in zip(sorted(self.entities_dict.keys()), bag_of_ents) if inside]

        if tv_state:
            assert tv_tex_id >= 0
            tv_tex_name = self.tv_tex_names_on[tv_tex_id]
        else:
            assert tv_tex_id == -1
            tv_tex_name = self.tv_tex_name_off

        info_d = {
            "agent_xya": agent_xya,
            "current_room": self.Room(current_room_id),
            "tv_state": tv_state,
            "tv_tex_name": tv_tex_name,
            "visible_entities": visible_entities
        }

        return info_d

    def set_state(self, state):
        raise ValueError("DEPRECATED")

    def reset(self):
        obs = super(ThreeRoomsTVRicher, self).reset()

        # this is set as dummy, on this goal it is never trained or evaluated
        self.goal_info_vector = self.state_dict_to_vector(self.sample_goal_dict())

        observation = self._get_obs()

        return observation

    def get_diagnostics(self, paths, prefix=""):
        statistics = OrderedDict()
        for stat_name in [
            # "rewards",
            # "puck_distance",
            "success",
        ]:
            stat_name = stat_name
            stat = get_stat_in_paths(paths, "env_infos", stat_name)
            statistics.update(create_stats_ordered_dict(
                "%s%s" % (prefix, stat_name),
                stat,
                always_show_all_stats=True,
                ))
            statistics.update(create_stats_ordered_dict(
                "Final %s%s" % (prefix, stat_name),
                [s[-1] for s in stat],
                always_show_all_stats=True,
                ))
        return statistics

    def reset_scene(self, objects=None, objects_ids=None, objects_types=None, keep_goal=False):
        raise DeprecationWarning

    def step(self, action):
        """
        Perform one action and update the simulation
        """
        self.step_count += 1

        rand = self.rand if self.domain_rand else None
        fwd_step = self.params.sample(rand, "forward_step")
        lateral_step = self.params.sample(rand, "forward_step")
        fwd_drift = self.params.sample(rand, "forward_drift")
        turn_step = self.params.sample(rand, "turn_step")

        # cam left/right, forward/backward, right/left, toggle
        assert len(action) == 4

        # turn
        turn_angle = turn_step*(2*action[0])
        self.turn_agent(turn_angle)

        # straight movement
        move_step = fwd_step*(2*action[1])
        self.move_agent(move_step, fwd_drift)

        # lateral movement
        lat_move_step = lateral_step*(2*action[2])
        self.lat_move_agent(lat_move_step, fwd_drift)

        # toggle
        if action[3] > 0:
            # turns the tv on/off and updates the texture
            if self.TV_active:
                self.entities_dict["tv"].toggle_tv()

        # If we are carrying an object, something is wrong
        assert not bool(self.agent.carrying)

        # updates the texture
        self.entities_dict["tv"].step_tv()

        if not self.TV_active:
            assert self.entities_dict["tv"].current_tex_name() == self.tv_tex_name_off

        reward = 0

        # calculate oracle rewards and metrics
        observation = self._get_obs()

        visible_wall_iou, same_location = self.oracle_reward_fn(
            observation["state_desired_goal"], observation["state_achieved_goal"])

        metrics = self.calculate_metrics(
            vector_goal=observation["state_desired_goal"],
            vector_state=observation["state_achieved_goal"]
        )

        goal_state_dict = self.vector_to_state_dict(observation["state_desired_goal"])
        state_state_dict = self.vector_to_state_dict(observation["state_achieved_goal"])

        info = dict(
            goal_room=goal_state_dict["current_room"],
            goal_tv_state=goal_state_dict["tv_state"],
            state_room=state_state_dict["current_room"],
            state_tv_state=state_state_dict["tv_state"],
            **metrics,
            visible_wall_iou=visible_wall_iou,
            same_location=same_location,
            success=visible_wall_iou,
        )

        done = False
        return observation, reward, done, info

    def goal_dict_to_ent_names(self, g_d):
        ents = g_d["visible_entities"]
        if "tv" in ents:
            if self.state_dict_is_TV_ON(g_d):
                ents = ["tv_ON" if e == "tv" else e for e in ents]
            else:
                ents = ["tv_OFF" if e == "tv" else e for e in ents]

        # todo: remove this when it's no longer needed
        if "starry_sky" in ents:
            ents = ["starry_night" if e == "starry_sky" else e for e in ents]

        assert all([e in self.ent_names_to_visualize for e in ents])
        return ents

    def calculate_metrics(self, vector_state, vector_goal):
        desired_goal_dict = self.vector_to_state_dict(vector_goal)
        achieved_goal_dict = self.vector_to_state_dict(vector_state)

        assert len(self.entities_dict) == len(self.entities)

        room_correct = float(desired_goal_dict["current_room"] == achieved_goal_dict["current_room"])

        state_visible_ent_names_ = self.calculate_visible_ents_for_state_dict(achieved_goal_dict)
        state_visible_ent_names = achieved_goal_dict['visible_entities']

        goal_visible_ent_names_ = self.calculate_visible_ents_for_state_dict(desired_goal_dict)
        goal_visible_ent_names = desired_goal_dict['visible_entities']

        assert set(state_visible_ent_names_) == set(state_visible_ent_names)
        assert set(goal_visible_ent_names_) == set(goal_visible_ent_names)

        # todo: think about how tv on/off should be evaluated
        # todo: currently it's considered to be the same, we would probably want to change this

        if len(state_visible_ent_names) == 0 and len(goal_visible_ent_names) == 0:
            rec, pr = 1.0, 1.0

        else:

            # how many objects from the goal are visible
            rec = len([g for g in goal_visible_ent_names if g in state_visible_ent_names]) / len(goal_visible_ent_names)\
                if goal_visible_ent_names else 0.0

            # how many object from the state should be visible
            pr = len([s for s in state_visible_ent_names if s in goal_visible_ent_names]) / len(state_visible_ent_names)\
                if state_visible_ent_names else 0.0

        f1 = 2*pr*rec/(pr+rec) if pr+rec else 0.0
        metrics = {
            "state_agent_x": achieved_goal_dict['agent_xya'][0],
            "state_agent_y": achieved_goal_dict['agent_xya'][1],
            "state_agent_a": achieved_goal_dict['agent_xya'][2],
            "dummy_metric": 0.0,
            "room_correct": room_correct,
            "visible_ent_precision": pr,
            "visible_ent_recall": rec,
            "visible_ent_f1": f1
        }

        return metrics

    def calculate_visible_ents_for_state_dict(self, state_dict):
        # todo: can't be called because calcualte vis envs is called inside create_info_d
        # save_state_for_test = copy.deepcopy(self.state_dict_to_vector(self.create_info_d()))

        # box and agent
        assert len([e for e in self.entities if not e.is_static]) == 3
        assert not self.entities_dict["box"].is_static
        assert not self.entities_dict["tv"].is_static

        # save backups
        backup_agent_pos, backup_agent_dir = self.agent.pos.copy(), copy.deepcopy(self.agent.dir)

        a_x, a_y, a_a = state_dict["agent_xya"]

        self.agent.pos[0] = a_x
        self.agent.pos[2] = a_y
        self.agent.dir = a_a


        visible_ents = self.get_visible_ents()

        def ks_for_vs(D, vs):
            inv_D = {v_: k_ for k_, v_ in D.items()}

            # assert dict is 1to1
            assert len(inv_D) == len(D)

            return [inv_D[v] for v in vs]

        ent_names = ks_for_vs(self.entities_dict, visible_ents)

        ent_names = [e for e in ent_names if e != "agent"]

        # return to previous
        self.agent.pos = backup_agent_pos
        self.agent.dir = backup_agent_dir

        # assert state unchanged
        # assert np.array_equal(save_state_for_test, self.state_dict_to_vector(self.create_info_d()))

        return ent_names

    def _get_obs(self):
        info_vec = self.state_dict_to_vector(self.create_info_d()).copy()
        observation = {
            "observation": info_vec,
            "state_observation": info_vec,
            "desired_goal": self.goal_info_vector,
            "state_desired_goal": self.goal_info_vector,
            "achieved_goal": info_vec,
            "state_achieved_goal": info_vec,
        }
        return observation

    def get_image_obs(self):
        obs = self.render_obs()
        return obs

    def extract_metrics_from_path(
            self,
            path,
            goal_sampling_mode,
            decoded_achieved_goal_key="image_achieved_goal",
            decoded_desired_goal_key="image_desired_goal"
    ):
        """
        This method should be called ONLY from the following method:
        vae_wrapped_env.extract_metrics_from_path
        """
        if goal_sampling_mode == "custom_goal_sampler":
            # training
            last_next_observation = path["next_observations"][-1]

            assert decoded_achieved_goal_key == "image_achieved_goal"
            last_image = last_next_observation[decoded_achieved_goal_key]
            last_image_expert_vector = last_next_observation["expert_knowledge_state_achieved_goal"]

            # goal
            assert decoded_desired_goal_key == "image_desired_goal"
            goal = last_next_observation[decoded_desired_goal_key]
            goal_pretrained_features = last_next_observation["pretrained_features_desired_goal"]
            goal_expert_vector = last_next_observation["expert_knowledge_state_desired_goal"]
            goal_room = self.vector_to_state_dict(goal_expert_vector)["current_room"]

            metrics = self.calculate_metrics(
                vector_goal=goal_expert_vector,
                vector_state=last_image_expert_vector
            )


            state_agent_x = metrics["state_agent_x"]
            state_agent_y = metrics["state_agent_y"]
            state_agent_a = metrics["state_agent_a"]
            room_correct = metrics["room_correct"]
            dummy_metric = metrics["dummy_metric"]
            visible_ent_precision = metrics["visible_ent_precision"]
            visible_ent_recall = metrics["visible_ent_recall"]
            visible_ent_f1 = metrics["visible_ent_f1"]

        else:
            # evaluation
            assert goal_sampling_mode in ["reset_of_env", "static_eval"]
            last_infos = path["env_infos"][-1]
            last_next_observation = path["next_observations"][-1]
            last_image_expert_vector = last_next_observation["expert_knowledge_state_achieved_goal"]

            goal = last_next_observation[decoded_desired_goal_key]
            goal_pretrained_features = last_next_observation["pretrained_features_desired_goal"]
            goal_expert_vector = last_next_observation["expert_knowledge_state_desired_goal"]
            assert decoded_desired_goal_key == "image_desired_goal"

            # goal_ = last_next_observation["real_image_desired_goal"]
            # assert all(goal_ == goal)

            last_image = last_next_observation[decoded_achieved_goal_key]

            state_agent_x = last_infos["state_agent_x"]
            state_agent_y = last_infos["state_agent_y"]
            state_agent_a = last_infos["state_agent_a"]
            goal_room = last_infos["goal_room"]
            dummy_metric = last_infos["dummy_metric"]
            room_correct = last_infos["room_correct"]
            visible_ent_precision = last_infos["visible_ent_precision"]
            visible_ent_recall = last_infos["visible_ent_recall"]
            visible_ent_f1 = last_infos["visible_ent_f1"]



        perf_metrics = {
            "goal": goal.copy(),
            "goal_pretrained_features": goal_pretrained_features.copy(),
            "last_state": last_image.copy()
        }

        metadata = {
            "goal_expert_state_dict": self.vector_to_state_dict(goal_expert_vector),
            "last_image_expert_state_dict": self.vector_to_state_dict(last_image_expert_vector),
        }

        expert_perf_metrics = {
            "state_agent_x": state_agent_x,
            "state_agent_y": state_agent_y,
            "state_agent_a": state_agent_a,
            "goal_room": goal_room,
            "dummy_metric": dummy_metric,
            "room_correct": room_correct,
            "visible_ent_precision": visible_ent_precision,
            "visible_ent_recall": visible_ent_recall,
            "visible_ent_f1": visible_ent_f1,
        }

        return perf_metrics, expert_perf_metrics, metadata

    def create_metrics_statistics_dict(
            self,
            eval_expert_cluster_perf_dict,
            eval_expert_cluster_lp_dict
    ):

        # expert_perf
        statistics_dict = {}

        statistics_dict.update({"eval_expert_perf_" + str(r):
                                    eval_expert_cluster_perf_dict["visible_ent_f1"].get(r.value, [0.0])[-1]
                                for r in self.goal_generation_rooms})

        # expert lp
        statistics_dict.update({"eval_expert_lp_" + str(r):
                                    eval_expert_cluster_lp_dict["visible_ent_f1"].get(r.value, 0.0)
                                for r in self.goal_generation_rooms})

        for m in [
            "room_correct",
            "dummy_metric",
            "visible_ent_precision",
            "visible_ent_recall",
            "visible_ent_f1",
        ]:
            # expert performance all metrics
            statistics_dict.update({
                "eval_expert_"+str(m) + "_" + str(r): eval_expert_cluster_perf_dict[m].get(r.value, [0.0])[-1]
                 for r in self.goal_generation_rooms})

            #  LPs all metrics
            statistics_dict.update({
                "eval_expert_lp_"+str(m) + "_" + str(r): eval_expert_cluster_lp_dict[m].get(r.value, 0.0)
                 for r in self.goal_generation_rooms})

        return statistics_dict

    def seed(self, seed=None):
        np.random.seed(seed)
        super(ThreeRoomsTVRicher, self).seed(seed)

    def current_room(self):
        for i, r in enumerate(self.rooms):
            # Make sure the position is within the room"s outline
            if r.point_inside(self.agent.pos):
                return self.Room(i)
        raise ValueError("Agent is not in any room: {}".format(self.agent.pos))

