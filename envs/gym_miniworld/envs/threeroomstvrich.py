import numpy as np
import copy
import math

import traceback
import termcolor
import random

from enum import IntEnum, Enum
from gym import spaces
from ..miniworld import MiniWorldEnv, Room
from ..entity import Entity, ImageFrame, MeshEnt, Box, Key, Ball, COLOR_NAMES

from collections import OrderedDict
from multiworld.envs.env_util import create_stats_ordered_dict, get_stat_in_paths

class ThreeRoomsTVRich(MiniWorldEnv):
    """
    Two small rooms connected to one large room
    """

    class Room(Enum):
        GALLERY = 0
        OFFICE = 1
        TV = 2
        CORRIDOR_GALLERY_OFFICE = 3
        CORRIDOR_GALLERY_TV = 4

    # Enumeration of possible actions
    class Actions(IntEnum):
        # Turn left or right by a small amount
        turn_left = 0
        turn_right = 1

        # Move forward or back by a small amount
        move_forward = 2
        move_back = 3

        # Move left or right by a small amount
        move_left = 4
        move_right = 5

        # Pick up or drop an object being carried
        pickup = 6
        drop = 7

        # Toggle/activate an object
        toggle = 8


    def __init__(self, obs_width=320, obs_height=320, max_episode_steps=400, static_eval_set_size=25, goal_generation_rooms=None, **kwargs):
        self.obs_height = obs_height
        self.obs_width = obs_width
        self.max_episode_steps = max_episode_steps

        # true state size
        # this must be before super init because super init calls reset
        self.state_vector_size = 9

        super().__init__(
            obs_width=obs_width,
            obs_height=obs_height,
            max_episode_steps=max_episode_steps,
            **kwargs
        )

        # # Allow only the movement actions
        # self.action_space = spaces.Discrete(self.actions.toggle+1)

        # we set the actions space to be continuous

        # left/right, forward/backward, right/left, pickup/drop, toggle
        # self.n_act = 5
        self.n_act = 6
        self.action_space = spaces.Box(low=-np.ones(self.n_act),
                                       high=np.ones(self.n_act),
                                       dtype=np.float32)

        self.obs_box = spaces.Box(
            low=0, high=255, shape=(self.obs_height, self.obs_width, 3), dtype=np.uint8
        )


        self.goal_box = spaces.Box(
            np.array([-1] * self.state_vector_size),
            np.array([1] * self.state_vector_size),
        )

        self.observation_space = spaces.Dict([
            ("observation", self.obs_box),
            ("state_observation", self.obs_box),

            # achieved and desired goal have the same shape
            ("desired_goal", self.goal_box),
            ("state_desired_goal", self.goal_box),
            ("achieved_goal", self.goal_box),
            ("state_achieved_goal", self.goal_box),
        ])


        self.available_rooms = [
            self.Room.OFFICE, self.Room.GALLERY, self.Room.TV, self.Room.CORRIDOR_GALLERY_OFFICE, self.Room.CORRIDOR_GALLERY_TV
        ]

        if goal_generation_rooms is None:
            self.goal_generation_rooms = self.available_rooms
        else:
            self.goal_generation_rooms = goal_generation_rooms

        print("aval:", self.available_rooms)
        print("gen:", self.goal_generation_rooms)
        for g in self.goal_generation_rooms: assert g in self.available_rooms

        self.static_eval_set_size = static_eval_set_size
        self.static_eval_set_counter = 0
        self.static_eval_set = [self.state_dict_to_vector(d) for d in self.static_eval_goal_dicts()]
        assert len(self.static_eval_set) == self.static_eval_set_size

        # Define TV channels
        self.tv_channels = ["tv_cat", "tv_dog", "tv_car", "tv_person"]

        self.reset()

    def _gen_world(self):
        # GALLERY
        room0 = self.add_rect_room(
            min_x=-7, max_x=7,
            min_z=0.5 , max_z=7,
            wall_tex="cardboard",
            ceil_tex="ceiling_tiles",
            floor_tex="parket",
        )
        assert self.Room.GALLERY.value == 0

        # OFFICE
        room1 = self.add_rect_room(
            min_x=-7, max_x=-1,
            min_z=-7, max_z=-0.5,
            wall_tex="stucco",
            ceil_tex="stucco",
            floor_tex="wood",
        )
        assert self.Room.OFFICE.value == 1

        # TV
        room2 = self.add_rect_room(
            min_x=1 , max_x=7,
            min_z=-7, max_z=-0.5,
            wall_tex="brick_wall",
            ceil_tex="concrete_tiles",
            floor_tex="carpet",
        )
        assert self.Room.TV.value == 2

        # Connect the rooms with portals/openings
        self.connect_rooms(room0, room1, min_x=-5., max_x=-3.)
        assert self.Room.CORRIDOR_GALLERY_OFFICE.value == 3

        self.connect_rooms(room0, room2, min_x=3., max_x=5.)
        assert self.Room.CORRIDOR_GALLERY_TV.value == 4

        # Painting on the walls in the lobby

        # gallery objects
        self.place_entity(MeshEnt(
            height=1.5,
            static=True,
            mesh_name='wooden_chair'
            ),
            pos=[-6.5, 0, 6.5],
            dir=np.pi,
        )

        self.entities.append(ImageFrame(
            pos=[0, 1.35, 7],
            dir=math.pi/2,
            width=1.8,
            tex_name="portraits/adelaide_hanscom"
        ))

        self.entities.append(ImageFrame(
            pos=[4, 1.35, 7],
            dir=math.pi/2,
            width=1.8,
            tex_name="portraits/alexei_harlamov"
        ))

        self.entities.append(ImageFrame(
            pos=[-4, 1.35, 7],
            dir=math.pi/2,
            width=1.8,
            tex_name="portraits/alexey_petrovich_antropov"
        ))

        self.entities.append(ImageFrame(
            pos=[-7, 1.35, 4],
            dir=0,
            width=1.8,
            tex_name="portraits/alice_pike_barney"
        ))

        self.entities.append(ImageFrame(
            pos=[7, 1.35, 4],
            dir=math.pi,
            width=1.8,
            tex_name="portraits/aman_theodor"
        ))


        # OFFICE room objects

        # # plant
        # # https://sketchfab.com/3d-models/indoor-plant-bpaOec5a2WUaiKuF0ATci1cjCK7 licence
        # self.place_entity(MeshEnt(
        #     height=2.,
        #     static=True,
        #     mesh_name='potted_plant'
        # ),
        #     pos=[-3, 0, -6],
        #     dir=0,
        # )

        # plant
        self.place_entity(MeshEnt(
            height=2.,
            static=True,
            mesh_name='palm_fan'
        ),
            pos=[-3, 0, -6],
            dir=0,
        )



        self.place_entity(MeshEnt(
            height=2.,
            static=True,
            mesh_name="office_desk"
        ),
            pos=[-6, 0, -6.5],
            dir=0,
        )

        self.place_entity(MeshEnt(
            height=1.5,
            static=True,
            mesh_name="office_chair"
        ),
            pos=[-5.5, 0, -5.5],
            dir=0,
        )

        box = Box(color="red", size=1.0)
        self.place_entity(
            box, room=room1, pos=[-2.0, 0, -1.5], dir=0
        )

        # Grate
        self.entities.append(ImageFrame(
            pos=[-1., .7, -5],
            dir=-math.pi,
            width=1.,
            tex_name='airduct_grate'
        ))

        # Whiteboard
        self.entities.append(ImageFrame(
            pos=[-7., 1.5, -3],
            dir=0,
            width=3.,
            tex_name='whiteboard'
        ))

        # self.place_agent(room=room0)
        self.place_agent()

        # TV room objects
        self.tv_ent = ImageFrame(
            pos=[7, 1.35, -4.0],
            dir=math.pi,
            width=1.8,
            tex_name="tv"
        )

        self.entities.append(self.tv_ent)

        self.place_entity(MeshEnt(
            height=1.2,
            static=True,
            mesh_name='couch'
        ),
            pos=[1.5, 0, -4.0],
            dir=0,
        )

    def initialize_camera(self, bla):
        print("initialize camera: ", bla)
        pass

    def get_image(self, width, height):
        obs = self.get_image_obs()

        # import cv2
        # cv2.imshow("get_im", obs)
        # cv2.waitKey(10)

        assert obs.shape == (width, height, 3)
        return obs

    def get_goal(self):
        return {
            "desired_goal": self.goal_info_vector,
            "state_desired_goal": self.goal_info_vector,
        }

    def get_next_static_eval_goals(self, batch_size):
        # number_of_eval_steps_per_epoch should be rollout length * 25
        assert batch_size == 1
        index = self.static_eval_set_counter % self.static_eval_set_size
        goals = [self.static_eval_set[index]]
        self.static_eval_set_counter += 1

        return {
            "desired_goal": goals,
            "state_desired_goal": goals,
        }

    def set_to_goal(self, goal):
        goal_info_vector = goal["state_desired_goal"]
        goal_info_dict = self.vector_to_state_dict(goal_info_vector)
        self.set_context(goal_info_dict)


    def compute_rewards(self, action, obs, info=None):
        return np.array([-13.0])

    def get_env_state(self):
        return copy.deepcopy(self.state_dict_to_vector(self.create_info_d()))

    def create_init_context(self):
        raise DeprecationWarning()

    def get_init_context(self):
        raise DeprecationWarning()

    def set_env_state(self, state):
        self.set_context(self.vector_to_state_dict(state))

    def set_context(self, context):

        self.agent.pos[0] = context["agent_xya"][0]
        self.agent.pos[2] = context["agent_xya"][1]
        self.agent.dir = context["agent_xya"][2]

        non_static_non_agent = [e for e in self.entities if not e.is_static and e != self.agent]

        # todo: we assume only box
        assert len(non_static_non_agent) == 1

        box = non_static_non_agent[0]
        box.pos[0] = context["box_xya"][0]
        box.pos[2] = context["box_xya"][1]
        box.dir = context["box_xya"][2]

        if context["is_carrying"]:
            self.agent.carrying = box
        else:
            self.agent.carrying = None

        # current room is detected online based on agent.pos
        assert self.current_room() == context["current_room"]

        self.current_channel = int(context["tv_channel"])

        # todo: are there other things that change?
        # todo: when exaclty is this function used and is it relevant?


    def set_goal(self, goal):
        self.goal_info_vector = goal["state_desired_goal"]

    def sample_goals(self, batch_size):
        goals = [self.state_dict_to_vector(self.sample_goal_dict()) for _ in range(batch_size)]
        return {
            "desired_goal": goals,
            "state_desired_goal": goals,
        }

    def state_dict_is_TV_ON(self, state_dict):
        return bool(state_dict["tv_channel"] > 0)

    def vector_to_state_dict(self, vec):
        agent_xya = vec[:3]
        box_xya = vec[3:6]
        carrying = vec[6]
        current_room_id = vec[7]
        tv_channel = vec[8]

        info_d = {
            "agent_xya": agent_xya,
            "box_xya": box_xya,
            "is_carrying": carrying,
            "current_room": self.Room(current_room_id),
            "tv_channel": tv_channel
        }

        return info_d

    def state_dict_to_vector(self, dict):
        vector = np.concatenate([
            dict["agent_xya"],
            dict["box_xya"],
            [dict["is_carrying"]],
            [dict["current_room"].value],
            [dict["tv_channel"]],
        ])
        assert vector.shape == (self.state_vector_size,)

        return vector

    def static_eval_goal_dicts(self):
        # generating evaluation set

        # # rotating red box
        # dicts = [{
        #     "agent_xya": np.array([-5.0, -2.4, 0]),
        #     "box_xya": np.array([-2.0, -2.4, (i/25)*2*np.pi]),
        #     "is_carrying": bool(False),  # is this needed?
        #     "current_room": self.Room.OFFICE,
        #     "tv_channel": 0
        # } for i in range(self.static_eval_set_size)]

        # todo: currently it's dummy
        dicts = [{
            "agent_xya": [
                np.array([3.8, -4.6, 1.2]),  # tv
                np.array([-5.85, -3.3, 1.49]),  # desk
                np.array([-3.87, -5.24, 0.18]),  # airduct
                np.array([1.05, 3.55, 0.2]),  # gallery
                np.array([-3.7, 4.05, 3.38]),  # portrait woman
                np.array([-1.2, -2.4, np.pi]),  # box
            ][i % 6],
            "box_xya": np.array([-4.2, -2.4, -0.25]),
            "is_carrying": bool(False),  # is this needed?
            "current_room": [
                self.Room.TV,
                self.Room.OFFICE,
                self.Room.OFFICE,
                self.Room.GALLERY,
                self.Room.GALLERY,
                self.Room.OFFICE,
            ][i % 6],
            "tv_channel": 0
        } for i in range(self.static_eval_set_size)]

        # check consistency
        for d in dicts:
            assert d["current_room"] == self.get_room_for_xy(d["agent_xya"][:2])
            assert d["current_room"] in self.goal_generation_rooms

        return dicts

    def sample_goal_dict(self):
        # uniformly sampling goals
        # we don"t want to use this, so it"s a dummy goal
        goal = {
            "agent_xya": np.array([1.05, 3.55, 2.85]),
            "box_xya": np.array([-4.2, -2.4, -0.25]),
            "is_carrying": bool(False),  # is this needed?
            "current_room": self.Room.GALLERY,
            "tv_channel": 0
        }

        # check consistency
        assert self.Room.GALLERY == self.get_room_for_xy(goal["agent_xya"][:2])

        return goal

    def get_room_for_xy(self, pos_xy):
        x, y = pos_xy
        agent_pos = (x, 0, y)
        rooms = [i for (i, r) in enumerate(self.rooms) if r.point_inside(agent_pos)]

        if len(rooms) < 1:
            raise ValueError("Position {} is not in any room.".format(pos_xy))
        if len(rooms) > 1:
            raise ValueError("Position {} is not in many rooms.".format(pos_xy))

        room = rooms[0]
        return self.Room(room)


    def oracle_reward_fn(
            self,
            goal_context_vector,
            achieved_goal_context_vector,
    ):
        goal_context = self.vector_to_state_dict(goal_context_vector)
        achieved_goal_context = self.vector_to_state_dict(achieved_goal_context_vector)

        visible_wall_iou = 0
        same_location = 0

        return visible_wall_iou, same_location

    def reward_fn(
            self,
            goals_contexts_vector,
            achieved_goals_contexts_vector,
            gripper_state_insensitive=False
    ):
        raise DeprecationWarning()

    def create_info_d(self):
        static_entities = [e for e in self.entities if not e.is_static]
        entity_locations = [np.array([e.pos[0], e.pos[2], e.dir]) for e in static_entities]

        agent_idx = static_entities.index(self.agent)
        agent_xya = entity_locations[agent_idx]

        # for now we assume only one box is non static
        assert len(static_entities) == 2
        box_idx = 0 if agent_idx else 0  # the only static object that is not the agent

        box_xya = entity_locations[box_idx]
        assert static_entities[box_idx] != self.agent

        info_d = {
            "agent_xya": agent_xya,
            "box_xya": box_xya,
            "is_carrying": bool(self.agent.carrying),  # is this needed?
            "current_room": self.current_room(),
            "tv_channel": self.current_channel
        }

        success = float(self.oracle_reward_fn(
            self.goal_info_vector,
            self.state_dict_to_vector(info_d)
        )[0])

        info_d = {
            **info_d, **{"success": success}
        }

        return info_d

    def set_state(self, state):
        raise ValueError("DEPRECATED")

    def reset(self):
        obs = super(ThreeRoomsTVRich, self).reset()

        self.goal_info_vector = self.state_dict_to_vector(self.sample_goal_dict())

        self.current_channel = -1

        # this sets self.observation to be an image i don't think it"s needed
        # used here and in step

        # self.observation = self.get_image_obs()
        # assert (obs = self.observation).all()

        observation = self._get_obs()

        return observation

    def get_diagnostics(self, paths, prefix=""):
        statistics = OrderedDict()
        for stat_name in [
            # "rewards",
            # "puck_distance",
            "success",
        ]:
            stat_name = stat_name
            stat = get_stat_in_paths(paths, "env_infos", stat_name)
            statistics.update(create_stats_ordered_dict(
                "%s%s" % (prefix, stat_name),
                stat,
                always_show_all_stats=True,
                ))
            statistics.update(create_stats_ordered_dict(
                "Final %s%s" % (prefix, stat_name),
                [s[-1] for s in stat],
                always_show_all_stats=True,
                ))
        return statistics

    def reset_scene(self, objects=None, objects_ids=None, objects_types=None, keep_goal=False):
        raise DeprecationWarning

    def discretize_action(self, action):

        # 0           , 1         , 2          , 3     , 4
        # left/right, forward/backward, pickup/drop, toggle
        assert len(action) == 5

        # we decide which action to take
        action_idx = np.argmax(
            np.concatenate([
                np.abs(action[:3]), # for two sided we look at abs
                np.maximum(action[3:], 0)  # for one sided we look > 0
            ])
        )

        if action_idx <= 2:
            # two sided action
            action_value = action[action_idx]

            # we deside which direction
            if action_value > 0.0:
                discr_action = {
                    0: self.Actions.turn_right,
                    1: self.Actions.move_back,
                    2: self.Actions.drop
                }[action_idx]

            else:
                discr_action = {
                    0: self.Actions.turn_left,
                    1: self.Actions.move_forward,
                    2: self.Actions.pickup
                }[action_idx]

        else:
            # onesided action
            discr_action = {
                3: self.Actions.toggle,
            }[action_idx]

        return discr_action

    def step(self, action):
        """
        Perform one action and update the simulation
        """
        self.step_count += 1

        rand = self.rand if self.domain_rand else None
        fwd_step = self.params.sample(rand, "forward_step")
        lateral_step = self.params.sample(rand, "forward_step")
        fwd_drift = self.params.sample(rand, "forward_drift")
        turn_step = self.params.sample(rand, "turn_step")

        # cam left/right, forward/backward, right/left, pickup/drop, toggle

        # turn
        turn_angle = turn_step*(2*action[0])
        self.turn_agent(turn_angle)

        # straight movement
        move_step = fwd_step*(2*action[1])
        self.move_agent(move_step, fwd_drift)

        # lateral movement
        lat_move_step = lateral_step*(2*action[2])
        self.lat_move_agent(lat_move_step, fwd_drift)

        # pickup / drop
        if action[3] > 0:
            # Pick up an object

            # Position at which we will test for an intersection
            test_pos = self.agent.pos + self.agent.dir_vec * 1.5 * self.agent.radius
            ent = self.intersect(self.agent, test_pos, 1.2 * self.agent.radius)
            if not self.agent.carrying:
                if isinstance(ent, Entity):
                    if not ent.is_static:
                        self.agent.carrying = ent

        elif action[3] <= 0:
            # Drop an object being carried
            if self.agent.carrying:
                self.agent.carrying.pos[1] = 0
                self.agent.carrying = None

        # toggle
        if action[4] > 0:
            self.change_tv_channel()

        # If we are carrying an object, update its position as we move
        if self.agent.carrying:
            ent_pos = self._get_carry_pos(self.agent.pos, self.agent.carrying)
            self.agent.carrying.pos = ent_pos
            self.agent.carrying.dir = self.agent.dir

        reward = 0


        # pretty sure this is not needed, here and in reset
        # self.observation = self.get_image_obs()

        # calculate oracle rewards and metrics
        observation = self._get_obs()


        visible_wall_iou, same_location = self.oracle_reward_fn(
            observation["state_desired_goal"], observation["state_achieved_goal"])

        metrics = self.calculate_metrics(
            vector_goal=observation["state_desired_goal"],
            vector_state=observation["state_achieved_goal"]
        )
        dummy_metric, room_correct = metrics['dummy_metric'], metrics["room_correct"]

        info = dict(
            dummy_metric=dummy_metric,
            room_correct=room_correct,
            goal_room=self.vector_to_state_dict(self.goal_info_vector)["current_room"],
            goal_tv_channel=self.vector_to_state_dict(self.goal_info_vector)["tv_channel"],
            state_room=self.current_room(),
            state_tv_channel=self.current_channel,
            # agent_xy_distance=agent_xy_distance,
            # agent_xya_distance=agent_xya_distance,
            # angle_distance=angle_distance,
            visible_wall_iou=visible_wall_iou,
            same_location=same_location,
            success=visible_wall_iou,
        )

        done = False
        return observation, reward, done, info

    def calculate_metrics(self, vector_state, vector_goal):
        desired_goal_dict = self.vector_to_state_dict(vector_goal)
        achieved_goal_dict = self.vector_to_state_dict(vector_state)

        room_correct = float(desired_goal_dict["current_room"] == achieved_goal_dict["current_room"])

        metrics = {
            "dummy_metric": 0.0,
            "room_correct": room_correct
        }

        return metrics

    def _get_obs(self):
        info_vec = self.state_dict_to_vector(self.create_info_d())
        observation = {
            "observation": info_vec,
            "state_observation": info_vec,
            "desired_goal": self.goal_info_vector,
            "state_desired_goal": self.goal_info_vector,
            "achieved_goal": info_vec,
            "state_achieved_goal": info_vec,
        }
        return observation

    def get_image_obs(self):
        obs = self.render_obs()
        return obs

    def extract_metrics_from_path(
            self,
            path,
            goal_sampling_mode,
            decoded_achieved_goal_key="image_achieved_goal",
            decoded_desired_goal_key="image_desired_goal"
    ):
        """
        This method should be called ONLY from the following method:
        vae_wrapped_env.extract_metrics_from_path
        """
        # todo: reimplement these are the playground metrics
        if goal_sampling_mode == "custom_goal_sampler":
            # training
            last_next_observation = path["next_observations"][-1]

            assert decoded_achieved_goal_key == "image_achieved_goal"
            last_image = last_next_observation[decoded_achieved_goal_key]
            last_image_expert_vector = last_next_observation["expert_knowledge_state_achieved_goal"]

            # goal
            assert decoded_desired_goal_key == "image_desired_goal"
            goal = last_next_observation[decoded_desired_goal_key]
            goal_expert_vector = last_next_observation["expert_knowledge_state_desired_goal"]
            goal_room = self.vector_to_state_dict(goal_expert_vector)["current_room"]

            metrics = self.calculate_metrics(
                vector_goal=goal_expert_vector,
                vector_state=last_image_expert_vector
            )
            dummy_metric, room_correct = metrics["dummy_metric"], metrics["room_correct"]


        else:
            # evaluation
            assert goal_sampling_mode in ["reset_of_env", "static_eval"]
            last_infos = path["env_infos"][-1]
            last_next_observation = path["next_observations"][-1]
            # goal = last_next_observation[decoded_desired_goal_key]
            goal = last_next_observation["real_image_desired_goal"]
            last_image = last_next_observation[decoded_achieved_goal_key]

            goal_room = last_infos["goal_room"]
            dummy_metric = last_infos["dummy_metric"]
            room_correct = last_infos["room_correct"]

            # imsize = 48
            # img = last_image.reshape(3, imsize, imsize).transpose()
            # img = img[::, :, ::-1]
            # cv2.imshow("img", img)
            # cv2.waitKey(100)
            #
            # img = goal.reshape(3, imsize, imsize).transpose()
            # img = img[::, :, ::-1]
            # cv2.imshow("goal", img)
            # cv2.waitKey(100)

        perf_metrics = {
            "goal": goal.copy(),
            "last_state": last_image.copy()
        }

        expert_perf_metrics = {
            "goal_room": goal_room,
            "dummy_metric": dummy_metric,
            "room_correct": room_correct
        }

        return perf_metrics, expert_perf_metrics, {}

    def create_metrics_statistics_dict(
            self,
            eval_expert_cluster_perf_dict,
            eval_expert_cluster_lp_dict
    ):

        # expert_perf
        statistics_dict = {}

        statistics_dict.update({
            "eval_expert_perf_" + str(r): eval_expert_cluster_perf_dict[
                "dummy_metric" if r == self.Room.GALLERY else "dummy_metric"
            ].get(r.value, [2 * 1.4] if r == self.Room.GALLERY else [1.4])[-1]
            for r in self.goal_generation_rooms
        })

        # expert lp
        statistics_dict.update({
            "eval_expert_lp_" + str(r): eval_expert_cluster_lp_dict[
                "dummy_metric" if r == self.Room.GALLERY else "dummy_metric"
            ].get(r.value, 0.0) for r in self.goal_generation_rooms
        })

        # expert performance
        statistics_dict.update({"eval_expert_dummy_metric_" + str(r):
                                    eval_expert_cluster_perf_dict["dummy_metric"].get(r.value, [1.4])[-1]
                                for r in self.goal_generation_rooms})
        statistics_dict.update({"eval_expert_correct_room_" + str(r):
                                    eval_expert_cluster_perf_dict["room_correct"].get(r.value, [0.0])[-1] for r in
                                self.goal_generation_rooms})

        #  LPs
        statistics_dict.update(
            {"eval_expert_lp_hand_" + str(r): eval_expert_cluster_lp_dict["dummy_metric"].get(r.value, 0.0) for r
             in self.goal_generation_rooms})
        statistics_dict.update(
            {"eval_expert_lp_correct_room_" + str(r): eval_expert_cluster_lp_dict["room_correct"].get(r.value, 0.0)
             for r in self.goal_generation_rooms})

        return statistics_dict

    def render(self):
        pass

    def seed(self, seed=None):
        np.random.seed(seed)
        super(ThreeRoomsTVRich, self).seed(seed)

    def current_room(self):
        for i, r in enumerate(self.rooms):
            # Make sure the position is within the room"s outline
            if r.point_inside(self.agent.pos):
                return self.Room(i)
        raise ValueError("Agent is not in any room: {}".format(self.agent.pos))

    def change_tv_channel(self):
        # Chose TV channel TODO: change modality
        self.current_channel = (self.current_channel + 1) % len(self.tv_channels)

        # Draw image
        self.tv_ent.change_tex(self.tv_channels[self.current_channel])
        self._render_static()
