import numpy as np
import glob
from pathlib import Path
from envs.playground_env.env_params import PlaygroundRoom
import matplotlib.pyplot as plt
from collections import defaultdict
import sys
import matplotlib
matplotlib.use('Agg')

prefixes = ["", ""]
# prefixes = ["", ""]
# prefixes = ["", "", "", ""]
plt.rcParams["figure.figsize"] = (len(prefixes)*5, 10)

if len(sys.argv) > 1:
    path = Path(sys.argv[1])
else:
    path = Path("./")


max_iter = 100
step = 10
wrong_distance_margins = 2.25
room = PlaygroundRoom.OBJECTS
clusters = [0, 1, 3]
# clusters = [0, 3]

max_iter = min([
    max([
        int(Path(p).stem.split("_")[-1]) for p in glob.glob(str(path) + "*"+"/{}cluster_perf_dict_*".format(p))
    ]) for p in prefixes
])
max_iter = max_iter - max_iter % step

steps = list(range(0, max_iter, step))
print(steps)
for I, prefix in enumerate(prefixes):

    exp_dirs = [Path(p).parent for p in glob.glob(str(path) + "*" + "/{}cluster_perf_dict_{}.npy".format(prefix, max_iter))]

    data = {
        c: {
            st: {
                exp_dir: [] for exp_dir in exp_dirs
            } for st in steps
        } for c in clusters
    }
    data_LP = {
        c: {
            exp_dir: [] for exp_dir in exp_dirs
        } for c in clusters
    }
    normalized_data_LP = {
        c: {
            exp_dir: [] for exp_dir in exp_dirs
        } for c in clusters
    }
    print('seeds:', len(exp_dirs))
    if len(exp_dirs) == 0: exit()

    for exp_dir in exp_dirs:

        x, lp_S, lp_R, lp_O = [], [], [], []
        lps = defaultdict(list)
        nums = defaultdict(lambda: [0])
        limit_p_u = dict()
        limit_p_d = dict()

        all_smooth = []
        for st in steps:
            all_smooth = []
            x.append(st)
            # D = np.load(exp_dir / "eval_cluster_perf_dict_{}.npy".format(st), allow_pickle=True).item()
            D = np.load(exp_dir / "{}cluster_perf_dict_{}.npy".format(prefix, st), allow_pickle=True).item()

            for cluster in clusters:
                if "expert" in prefix:
                    metric = "hand_and_water_distance" if cluster == PlaygroundRoom.OBJECTS.value else "hand_distance"
                    # metric = "room_correct"
                    m_D = D[metric]
                    perfs = m_D.get(cluster, [2 * wrong_distance_margins] if cluster == PlaygroundRoom.OBJECTS.value else [
                        wrong_distance_margins])
                else:
                    m_D = D
                    perfs = m_D.get(cluster, [-1.0])


                smooth_p = []
                # smooth_v = []
                # for i, n in enumerate(nums[cluster]):
                #     if i == len(nums[cluster])-1:
                #         if n == len(perfs):
                #             smooth_p.append(smooth_p[-1])
                #             smooth_v.append(smooth_v[-1])
                #         else:
                #             smooth_p.append(np.array(perfs[n:]).mean())
                #             smooth_v.append(np.array(perfs[n:]).var())
                #     else:
                #         start = n
                #         end = nums[cluster][i+1]
                #         if start == end:
                #             smooth_p.append(smooth_p[-1])
                #             smooth_v.append(smooth_v[-1])
                #         else:
                #             smooth_p.append(np.array(perfs[start:end]).mean())
                #             smooth_v.append(np.array(perfs[start:end]).var())
                smooth_p = perfs

                nums[cluster].append(len(perfs))

                smooth_p = np.array(smooth_p)
                # smooth_v = np.array(smooth_v)

                if cluster == PlaygroundRoom.OBJECTS.value and "expert" in prefix:
                    smooth_p /= 2

                smooth_p = np.clip(smooth_p, -np.inf, smooth_p[0])

                data[cluster][st][exp_dir] = smooth_p.copy()

                history_size = 1000
                if history_size is not None:
                    smooth_p = smooth_p[-history_size:]
                # smooth_v = smooth_v[-hist:]
                # smooth_p /= np.mean(np.sqrt(smooth_v))

                colls = len(prefixes)*10
                plt.subplot(201 + colls + I)
                # plt.title(title)
                # plt.title(prefix)

                if I < 2:
                    D_lp = np.load(exp_dir / (str(prefix)+"cluster_lp_dict_{}.npy".format(st)), allow_pickle=True).item()
                    lps[cluster].append(D_lp.get(cluster, 0))
                else:
                    if len(smooth_p) > 1:
                        l = max(len(smooth_p)//2, 1)

                        lps[cluster].append(np.abs(
                            np.mean(smooth_p[:l])
                            -
                            np.mean(smooth_p[l:])
                        ))
                    else:
                        lps[cluster].append(0)

        sum_LP = []

        for i in range(len(list(lps.values())[0])):
            sum_LP.append(sum([lps[c][i] for c in clusters]))
        sum_LP = np.array(sum_LP)

        for cluster in clusters:
            data_LP[cluster][exp_dir] = lps[cluster].copy()
            normalized_data_LP[cluster][exp_dir] = data_LP[cluster][exp_dir] / sum_LP

    for c in clusters:
        for st in steps:
            if st % steps[-1] != 0: continue
            data[c][st]["mean"] = np.array(list(data[c][st].values())).mean(0)
            data[c][st]["var"] = np.sqrt(np.array(list(data[c][st].values())).var(0))
            L = len(data[c][st]["mean"])
            x = np.around(np.linspace(0, st, L))
            label = PlaygroundRoom(c) if "eval" in prefix else c
            if history_size is not None:
                if st == steps[-1]:
                    plt.plot(x[-history_size:], data[c][st]["mean"][-history_size:], color=["red", "green", "blue", "black"][c], label=label)
                else:
                    plt.plot(x[-history_size:], data[c][st]["mean"][-history_size:], color=["red", "green", "blue", "black"][c])
            else:
                # no hist clamp
                if st == steps[-1]:
                    plt.plot(x, data[c][st]["mean"], color=["red", "green", "blue", "black"][c], label=label)
                    plt.fill_between(x, data[c][st]["mean"]+data[c][st]["var"], data[c][st]["mean"]-data[c][st]["var"], color=["red", "green", "blue", "black"][c], alpha=0.1)
                else:
                    plt.plot(x, data[c][st]["mean"], color=["red", "green", "blue", "black"][c])
                    plt.fill_between(x, data[c][st]["mean"]+data[c][st]["var"], data[c][st]["mean"]-data[c][st]["var"], color=["red", "green", "blue", "black"][c], alpha=0.1)

            # for exp_dir in exp_dirs:
            #     x = steps[:len(data[c][st][exp_dir])]
            #     if st == steps[-1]:
            #         # plt.plot(x, data[c][st][exp_dir], color=["red", "green", "blue", "black"][c], label=PlaygroundRoom(c))
            #         plt.plot(x, data[c][st][exp_dir], color=["red", "green", "blue", "black"][c], label=PlaygroundRoom(c))
            #     else:
            #         plt.plot(x, data[c][st][exp_dir], color=["red", "green", "blue", "black"][c])

    plt.legend()
    # plt.figure(100+I)
    colls = len(prefixes)

    for c in clusters:

        plt.subplot(201 + colls + colls*10 + I)
        # plt.subplot(245 + I)
        # plt.title(prefix)
        m = np.array(list(data_LP[c].values())).mean(0)
        v = np.sqrt(np.array(list(data_LP[c].values())).var(0))
        plt.plot(steps, m, label="LP_{}".format(PlaygroundRoom(c)), color=["red", "green", "blue", "black"][c])
        plt.fill_between(steps, m+v, m-v, color=["red", "green", "blue", "black"][c], alpha=0.1)

        # plt.subplot(205 + colls + I)
        # # plt.title(prefix)
        # # normalized
        # m_n = np.array(list(normalized_data_LP[c].values())).mean(0)
        # v_n = np.sqrt(np.array(list(normalized_data_LP[c].values())).var(0))
        # plt.plot(steps, m_n, label="lp_{}_norm".format(PlaygroundRoom(c)), color=["red", "green", "blue", "black"][c]) #, linestyle=":")
        # plt.fill_between(steps, m_n+v_n, m_n-v_n, linestyle=":", color=["red", "green", "blue", "black"][c], alpha=0.1)


    plt.legend()

plt.tight_layout()
plt.savefig("LP.png")
