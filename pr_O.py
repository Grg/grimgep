import matplotlib.pyplot as plt
import numpy as np


lp_o_i = 4
lp_r_i = 1
lp_s_i = 2
temp = 2

lp_s_is = [0.72, 0.45, 0.36, 0.33, 0.31, 0.27, 0.24, 0.19]
lp_r_is = [0.15, 0.24, 0.16, 0.18, 0.13, 0.10, 0.11, 0.09]
lp_o_is = [0.3,  0.35, 0.46, 0.58, 0.64, 0.64, 0.72, 0.72]

lp_s_is = [0.3] * 10
lp_r_is = [0.2] * 10
lp_o_is = [0.4] * 10

n_clust = 19

sigm = lambda x: (1/(1 + np.exp(-x))) * 2 - 1

# random
# n_r = n_clust-2
n_o = 5
# n_r = (n_clust-n_o)//2
# n_s = n_clust-n_o-n_r
n_s = 5
n_r = n_clust - n_o - n_s

fig, axs = plt.subplots(nrows=1, ncols=4, sharex=True, sharey=True)
fig.set_size_inches(9, 3)
for i in range(4):
    x = []
    yolp = []
    youni = []
    yrlp = []
    yruni = []
    yslp = []
    ysuni = []

    for iter, (lp_o_i, lp_r_i, lp_s_i) in enumerate(zip(lp_o_is, lp_r_is, lp_s_is)):

        x.append(iter*50)
        if i == 0:
            # title = "lp"
            # lp_o = lp_o_i
            # lp_r = lp_r_i
            # lp_s = lp_s_i
            temp=2
            title = "lp^{}".format(temp)
            lp_o = lp_o_i ** temp
            lp_r = lp_r_i ** temp
            lp_s = lp_s_i ** temp
        elif i == 1:
            temp=4
            title = "lp^{}".format(temp)
            lp_o = lp_o_i ** temp
            lp_r = lp_r_i ** temp
            lp_s = lp_s_i ** temp
        elif i == 2:
            temp=6
            title = "lp^{}".format(temp)
            lp_o = lp_o_i ** temp
            lp_r = lp_r_i ** temp
            lp_s = lp_s_i ** temp
        elif i == 3:
            temp=8
            title = "lp^{}".format(temp)
            lp_o = lp_o_i ** temp
            lp_r = lp_r_i ** temp
            lp_s = lp_s_i ** temp
        # elif i == 2:
        #     title = "sigm(lp)"
        #     lp_o = sigm(lp_o_i)
        #     lp_r = sigm(lp_r_i)
        #     lp_s = sigm(lp_s_i)
        # elif i == 3:
        #     title = "sigm({}*lp)".format(temp)
        #     lp_o = sigm(temp*lp_o_i)
        #     lp_r = sigm(temp*lp_r_i)
        #     lp_s = sigm(temp*lp_s_i)

        # objects
        polp=n_o*(0.8*(lp_o/(lp_o*n_o+lp_r*n_r+lp_s*n_s)) + 0.2*(1/n_clust))
        prlp=n_r*(0.8*(lp_r/(lp_o*n_o+lp_r*n_r+lp_s*n_s)) + 0.2*(1/n_clust))
        pslp=n_s*(0.8*(lp_s/(lp_o*n_o+lp_r*n_r+lp_s*n_s)) + 0.2*(1/n_clust))

        yolp.append(polp)
        yrlp.append(prlp)
        yslp.append(pslp)

        pouni=(n_o/n_clust)
        pruni=(n_r/n_clust)
        psuni=(n_s/n_clust)

        youni.append(pouni)
        yruni.append(pruni)
        ysuni.append(psuni)

    # for n_clust in range(3, 20):
        #     x.append(n_clust)
        #     n_o = 1
        #     n_s = 1
        #     n_r = n_clust - n_o - n_s
        #
        #     # x.append(iter*50)
        #     # lp_o = lp_o_i ** temp
        #     # lp_r = lp_r_i ** temp
        #     # lp_s = lp_s_i ** temp
        #     # lp_o = sigm(lp_o_i)
        #     # lp_r = sigm(lp_r_i)
        #     # lp_s = sigm(lp_s_i)
        #
        #     lp_o = lp_o_i
        #     lp_r = lp_r_i
        #     lp_s = lp_s_i
        #
        #     # objects
        #     polp=n_o*(0.8*(lp_o/(lp_o*n_o+lp_r*n_r+lp_s*n_s)) + 0.2*(1/n_clust))
        #     prlp=n_r*(0.8*(lp_r/(lp_o*n_o+lp_r*n_r+lp_s*n_s)) + 0.2*(1/n_clust))
        #     pslp=n_s*(0.8*(lp_s/(lp_o*n_o+lp_r*n_r+lp_s*n_s)) + 0.2*(1/n_clust))
        #
        #     yolp.append(polp)
        #     yrlp.append(prlp)
        #     yslp.append(pslp)
        #
        #     pouni=(n_o/n_clust)
        #     pruni=(n_r/n_clust)
        #     psuni=(n_s/n_clust)
        #
        #     youni.append(pouni)
        #     yruni.append(pruni)
        #     ysuni.append(psuni)

    yrlp = np.array(yrlp)
    yolp = np.array(yolp)
    yslp = np.array(yslp)

    yruni = np.array(yruni)
    youni = np.array(youni)
    ysuni = np.array(ysuni)

    axs[i].plot(x, yolp, label="O_lp")
    # axs[i].plot(x,youni, label="O_uni", linestyle=":")
    # axs[i].plot(x,yolp-youni, label="O_better")

    axs[i].plot(x, yrlp, label="R_lp")
    # axs[i].plot(x,yruni, label="R_uni", linestyle=":")
    # axs[i].plot(x,yruni-yrlp, label="R_better")

    axs[i].plot(x, yslp, label="S_lp")
    # axs[i].plot(x,ysuni, label="S_uni", linestyle=":")

    # axs[i].plot(x,yolp-youni, label="more_O_lp")
    # axs[i].plot(x,(yruni-yrlp)+(ysuni-yslp)+(yolp-youni), label="better")

    axs[i].title.set_text(title)

plt.legend()
plt.show()
