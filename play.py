#!/usr/bin/env python3
import time

"""
This script allows you to manually control the simulator
using the keyboard arrows.
"""

from pathlib import Path
from PIL import Image
import pickle
import sys
import argparse
import pyglet
import math
from pyglet.window import key
from pyglet import clock
import numpy as np
import gym
import envs.gym_miniworld

parser = argparse.ArgumentParser()
parser.add_argument('--env-name', default='MiniWorld-ThreeRoomsTVRicher-v0')
parser.add_argument('--domain-rand', action='store_true', help='enable domain randomization')
parser.add_argument('--no-time-limit', action='store_true', help='ignore time step limits')
parser.add_argument('--top_view', action='store_true', help='show the top view instead of the agent view')
parser.add_argument('--save_obs', action='store_true', help='save obs collected by the agent')
parser.add_argument('--save_state', action='store_true', help='save obs collected by the agent')
parser.add_argument('--save_actions', action='store_true', help='save actions executed')
args = parser.parse_args()

H, W = 320, 320
env = gym.make(
    args.env_name,
    # is this parameter needed? used for generating DONE
    # intentionally set to > m_st so that no DONEs appear
    max_episode_steps=100,
    TV_active=True,
    TV_type="imagenet_very_small",
    obs_width=W,
    obs_height=H,
    # window_width=W,
    # window_height=H,
    domain_rand=False,
    static_eval_set_size=20,
    static_initial_position=True,
    start_room="TV"
)

if args.no_time_limit:
    env.max_episode_steps = math.inf
if args.domain_rand:
    env.domain_rand = True

view_mode = 'top' if args.top_view else 'agent'

env.reset()

if args.save_state:
    states = []

# Create the display window
env.render('pyglet', view=view_mode)


def step(action):
    print('step {}/{}'.format(env.step_count + 1, env.max_episode_steps))
    obs, reward, done, info = env.step(action)

    if args.save_obs:
        # Save image
        im_obs = env.render_obs()
        im = Image.fromarray(im_obs)
        dir_path = Path("./observations/{}_H_{}_colors".format(args.env_name, H))
        dir_path.mkdir(exist_ok=True, parents=True)
        n_files = len(list(dir_path.iterdir()))
        im_path = dir_path / "obs_{}.jpeg".format(n_files)
        im.save(im_path)

    if args.save_state:
        states.append((obs, info))

    if reward > 0:
        print('reward={:.2f}'.format(reward))

    if done:
        print('done!')
        env.reset()
    s=time.time()
    env.render('pyglet', view=view_mode)
    e=time.time()
    print(e-s)

actions=[]

@env.unwrapped.window.event
def on_key_press(symbol, modifiers):
    """
    This handler processes keyboard commands that
    control the simulation
    """

    if symbol == key.BACKSPACE or symbol == key.SLASH:
        print('RESET')
        env.reset()
        env.render('pyglet', view=view_mode)
        return

    if symbol == key.ESCAPE:
        env.close()
        sys.exit(0)

    move_forward = np.array([0, 1., 0, 0])
    move_back = np.array([0, -1., 0, 0])

    turn_left = np.array([1., 0, 0, 0])
    turn_right = np.array([-1., 0, 0, 0])

    lat_left = np.array([0, 0, -1., 0])
    lat_right = np.array([0, 0, 1., 0])

    toggle = np.array([0, 0, 0, 1])

    # move_forward = np.array([0, 1., 0, 0, 0, 0])
    # move_back = np.array([0, -1., 0, 0, 0, 0])
    # turn_left = np.array([1., 0, 0, 0, 0, 0])
    # turn_right = np.array([-1., 0, 0, 0, 0, 0])
    # lat_left = np.array([0, 0, -1., 0, 0, 0])
    # lat_right = np.array([0, 0, 1., 0, 0, 0])
    # pickup = np.array([0, 0, 0, 1., 0, 0])
    # drop = np.array([0, 0, 0, -1., 0, 0])
    # toggle = np.array([0, 0, 0, 0, 1., 0])
    # done = np.array([0, 0, 0, 0, 0, 1.])


    if symbol in [key.UP, key.W]:
        step(move_forward)
    elif symbol in [key.DOWN, key.S]:
        step(move_back)

    elif symbol in [key.LEFT, key.A]:
        step(turn_left)

    elif symbol in [key.RIGHT, key.D]:
        step(turn_right)

    elif symbol in [key.Q]:
        step(lat_left)

    elif symbol in [key.E]:
        step(lat_right)

    # elif symbol == key.PAGEUP or symbol == key.P:
    #     step(pickup)
    # elif symbol == key.PAGEDOWN or symbol == key.O:
    #     step(drop)
    #
    # elif symbol == key.ENTER:
    #     step(done)

    elif symbol == key.I:
        step(toggle)

    if args.save_actions:
        actions.append(symbol)
        with open('actions.pkl', 'wb') as f:
            pickle.dump(actions, f)


@env.unwrapped.window.event
def on_key_release(symbol, modifiers):
    pass


@env.unwrapped.window.event
def on_draw():
    env.render('pyglet', view=view_mode)


@env.unwrapped.window.event
def on_close():
    pyglet.app.exit()


# Enter main event loop
pyglet.app.run()

if args.save_state:
    dir_path = Path("./states/{}" % args.env_name)
    dir_path.mkdir(exist_ok=True, parents=True)
    with open(Path("./states/{}" % args.env_name) / 'states.pickle', 'wb') as handle:
        pickle.dump(states, handle)

env.close()