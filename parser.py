from pathlib import Path
import pandas as pd
import sys


if len(sys.argv) > 1:
	regex = sys.argv[-1]
else:
	regex = ""

for pr_csv in list(Path("./data").rglob("*"+regex+"*/progress.csv")):
	if pr_csv.stat().st_size == 0: continue
	f = pd.read_csv(str(pr_csv))
	# new_f = f.filter(regex="(.*reward.*Mean)|(.*distance.*Mean)|(.*vae_dist.*)")
	new_f = f.filter(
		regex="(.*reward.*Mean)|(.*distance.*Mean)|(.*vae_dist.*)|(.*Sample.*)|(.*room_correct.*)|(.*dog_correct.*)|(.*exp_perc_of.*)|(.*buffer_num_of_.*)|(.*time/epoch.*)|(.*lp_.*)|(.*perf_.*)|(.*expert.*)|(.*replay_buffer.*)")
	# new_f = new_f.clip(0, 1)
	new_f_path = pr_csv.parent / "progress_filtered.csv"
	print("dumping to: ", str(new_f_path))
	new_f.to_csv(str(new_f_path), index=False)
	print("new size (kb):", new_f_path.stat().st_size / 1024, " from (kb): ", pr_csv.stat().st_size / 1024)
