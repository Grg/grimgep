#!/bin/bash
# Our custom function

# run_experiments_3D.sh <novelty_type> <num_procs>

if [ $# -lt 2 ];
  then
    echo "You must provide the experiment type AND node id."
    exit
fi

case "$1" in
  "skf")
    echo "Underlying imgep is Skewfit"
    exp_name="3D_Skewfit_"
    script_name="Skewfit_experiment.py"
    num_confs=6
    ;;
  "cb")
    echo "Underlying imgep is CountBased"
    exp_name="3D_CountBased_"
    script_name="CountBased_experiment.py"
    num_confs=3
    ;;
  "rig")
    echo "Underlying imgep is RIG"
    exp_name="3D_OnlineRIG_"
    script_name="OnlineRIG_experiment.py"
    num_confs=3
    ;;
  "skf_inactive")
    echo "Underlying imgep is Skewfit"
    exp_name="3D_Skewfit_inactive_"
    script_name="Skewfit_experiment_inactive.py"
    num_confs=6
    ;;
  "cb_inactive")
    echo "Underlying imgep is CountBased"
    exp_name="3D_CountBased_inactive_"
    script_name="CountBased_experiment_inactive.py"
    num_confs=3
    ;;
esac

PY_PATH="/home/gkovac/anaconda3/envs/grimenv36/bin/python"
#PY_PATH="/home/flowers/miniconda3/envs/grimenv36/bin/python"

# parallels
start_run () {
  ngpus=`nvidia-smi -q | grep GPU\ UUID | wc -l`
  ngpus=$(($ngpus>1?$ngpus:1)) # in case there are no gpus
  gpu=$(($1 % $ngpus))

  touch ./outs/error_$2_$1
  $PY_PATH -u examples/grimgep3d/$script_name $1 $2 1 2>&1 | tee -a ./outs/error_$2_$1
}

# id
# exp_name (to be added to folder name)
start_run $2 $exp_name