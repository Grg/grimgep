import json
import cv2
from sklearn.cluster import SpectralClustering
from sklearn.cluster import DBSCAN, OPTICS
import glob
from sklearn.svm import SVC
from envs.playground_env.env_params import Room
from collections import defaultdict, Counter
from pyclustering.cluster.optics import optics
from pyclustering.cluster.cure import cure
from pyclustering.cluster.kmeans import kmeans
from pyclustering.cluster.kmedians import kmedians
from pyclustering.cluster.center_initializer import kmeans_plusplus_initializer

import torch
from rlkit.launchers.skewfit_experiments import train_vae
from rlkit.launchers.launcher_util import setup_logger
from rlkit.torch.vae.conv_vae import imsize48_default_architecture, imsize48_tiny_architecture


setup_logger(
    exp_prefix="cluster_vae",
)

import time
from sklearn.neighbors import NearestNeighbors, KNeighborsClassifier
from sklearn.cluster import KMeans, AgglomerativeClustering
from sklearn.mixture import GaussianMixture
import numpy as np
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
from pathlib import Path

N_max = 100000  # this is maximum
import sys

path = Path(sys.argv[1])
epochs = int(sys.argv[2])
al = str(sys.argv[3])


filenames = []
step = 20

prefix = "{}_{}_ep_step_{}".format(al, epochs, step)
print("prefix:", prefix)

max_iter = min(max([int(Path(p).stem.split("_")[-1]) for p in glob.glob(str(path) + "/replay_buffer*")]), 300)
for i in range(0, max_iter, step):
        filenames.append(
        str(path/"replay_buffer_embeddings_{}.npy".format(i))
)

# grids = {
#     "beta": [1, 30],
#     "n_epochs": [5, 40],
#     "repr": [3, 10]
# }

grids = {
    "beta": [1],
    "n_epochs": [epochs],
    "repr": [3]
}

for b in grids["beta"]:
    for n_ep in grids["n_epochs"]:
        for repr_size in grids["repr"]:
            exp_name = "{}_b_{}_n_ep_{}_repr_{}".format(prefix, b, n_ep, repr_size)

            exp_variant = {
                "beta": b,
                "n_epochs": n_ep,
                "repr": repr_size
            }

            path = './clustering/{}/variant.json'.format(exp_name)
            Path(path).parent.mkdir(parents=True, exist_ok=True)

            with open(path, 'w+') as json_file:
                json.dump(exp_variant, json_file)

            n_clus = [20, 20, 20]
            # algos = ["dbscan", "dbscan", "dbscan"]
            # algos = ["optics", "optics", "optics"]
            # algos = ["aaic", "aaic", "aaic"]
            algos = [al, al, al]

            inputs = ["avg_col", "avg_col", "avg_col"]
            inputs = ["small_vae_online", "small_vae_online", "small_vae_online"]

            num_alogs = min(len(algos), len(inputs))
            algos = algos[:num_alogs]
            inputs = inputs[:num_alogs]
            n_clus = n_clus[:num_alogs]

            vae = None

            draw_p = 3
            fig, axs = plt.subplots(nrows=len(filenames), ncols=num_alogs+1)
            plt.rcParams["figure.figsize"] = (5*len(filenames), 5*(num_alogs+1))

            # n_cluters=5
            from matplotlib.lines import Line2D
            recs = defaultdict(lambda : defaultdict(list))
            precs = defaultdict(lambda : defaultdict(list))
            m_title = defaultdict(str)
            colors = ["red", "green", "black", "blue", "purple", "yellow", "pink", "gray", "crimson", "cyan", "red", "green", "black", "blue", "purple", "yellow", "pink", "gray", "crimson", "cyan"]

            vaes = {}

            for iter, filename in enumerate(filenames):
                print("\step:", iter*step)
                data = np.load(filename, allow_pickle=True)

                data_dict = data[()]

                X = data_dict['embeddings']
                rooms = np.array(data_dict["rooms"])
                rooms_ids = np.array([r.value for r in rooms])
                id_name = {r.value: r for r in rooms}

                imgs = data_dict["images"]/256
                # empty = np.where(imgs.mean(axis=1) == 0)[0]

                N = len(X)

                X = X[:N]
                rooms = rooms[:N]
                rooms_ids = rooms_ids[:N]
                imgs = imgs[:N]

                print("num of images:", N)

                imgs = np.array(imgs)
                imsize = int(np.sqrt(imgs.shape[-1] // 3))
                avg_colors = imgs.reshape(-1, 3, imsize, imsize).mean(axis=(2, 3))

                # repl = [200, 96, 31]
                # with_replace = [200, 12, 31]
                # to_repl = np.mean(np.abs(np.array(repl) / 255 - avg_colors), axis=1) < 0.1
                # imgs[to_repl] = np.tile(with_replace, sum(to_repl) * 48 * 48).reshape(sum(to_repl), 48 * 48 * 3) / 255

                # repl = [19, 222, 23],
                # with_replace = [70, 222, 23],
                # to_repl = np.mean(np.abs(np.array(repl) / 255 - avg_colors), axis=1) < 0.1
                # imgs[to_repl] = np.tile(with_replace, sum(to_repl) * 48 * 48).reshape(sum(to_repl), 48 * 48 * 3) / 255


                # nbrs = NearestNeighbors(n_neighbors=n_cluters).fit(X[:N])
                # distances, indices = nbrs.kneighbors(X[:N])
                #
                # same = rooms_ids[indices].var(axis=1) == 0
                #
                # print("NN acc: ", same.mean())

                # pca
                for p in range(num_alogs):
                    n_clusters = n_clus[p]
                    algo = algos[p]
                    ins = inputs[p]
                    print("n_clus:", n_clusters)
                    tol = 1e-4
                    clus_max_iter = 1000

                    if ins == 'lat':
                        X_train = X[:N]

                    elif ins == 'pca':
                        X_train = np.array(list(zip(pca_x, pca_y)))[:N]

                    elif ins == "col":

                        def get_color(id):
                            if id == 0:
                                cl = [200, 200, 200]
                            elif id == 1:
                                cl = [9, 73, 241]
                            elif id == 3:
                                cl = [255, 255, 255]
                            else:
                                raise NotImplementedError

                            # return cl
                            if id == 0:
                                stddev = np.array([0.19860198, 0.71674945, 0.71674945])
                            elif id == 3:
                                stddev = np.array([1.2966844, 1.43012021, 1.65580101])
                            else:
                                stddev = np.array([0.19860198, 0.71674945, 0.71674945])
                            return cl + np.random.random(3) * stddev

                        X_train = np.array([get_color(id) for id in rooms_ids])

                    elif ins == "5cols":
                        cls = np.random.randint(0, 255, (5, 3))
                        cl = [
                            [9, 73, 241],
                            [19, 222, 23],
                            [200, 96, 31],
                            [11, 208, 219],
                            [212, 13, 254],
                        ]


                        def get_color(id):
                            if id == 0:
                                cl = [200, 200, 200]
                            elif id == 1:
                                cl = cls[np.random.randint(len(cls))]
                            elif id == 3:
                                cl = [255, 255, 255]
                                # cl = np.random.randint(0, 255, (48, 48, 3)).mean()
                            else:
                                raise NotImplementedError

                            # return cl
                            if id == 0:
                                stddev = np.array([0.19860198, 0.71674945, 0.71674945])
                            elif id == 3:
                                stddev = np.array([1.2966844, 1.43012021, 1.65580101])
                            else:
                                stddev = np.array([0.19860198, 0.71674945, 0.71674945])
                            # return cl
                            return cl + np.random.random(3) * stddev

                        X_train = np.array([get_color(id) for id in rooms_ids])

                    elif ins == "10cols":
                        cls = np.random.randint(0, 255, (10, 3))
                        def get_color(id):
                            if id == 0:
                                cl = [200, 200, 200]
                            elif id == 1:
                                cl = cls[np.random.randint(len(cls))]
                            elif id == 3:
                                cl = [255, 255, 255]
                            else:
                                raise NotImplementedError

                            # return cl
                            if id == 0:
                                stddev = np.array([0.19860198, 0.71674945, 0.71674945])
                            elif id == 3:
                                stddev = np.array([1.2966844, 1.43012021, 1.65580101])
                            else:
                                stddev = np.array([0.19860198, 0.71674945, 0.71674945])
                            return cl+np.random.random(3)*stddev

                        X_train = np.array([get_color(id) for id in rooms_ids])

                    elif ins == "infcols":
                        def get_color(id):
                            if id == 0:
                                cl = [200, 200, 200]
                            elif id == 1:
                                cl = np.random.randint(0, 255, 3)
                            elif id == 3:
                                cl = [255, 255, 255]
                            else:
                                raise NotImplementedError

                            # return cl
                            if id == 0:
                                stddev = np.array([0.19860198, 0.71674945, 0.71674945])
                            elif id == 3:
                                stddev = np.array([1.2966844, 1.43012021, 1.65580101])
                            else:
                                stddev = np.array([0.19860198, 0.71674945, 0.71674945])
                            return cl + np.random.random(3) * stddev

                        X_train = np.array([get_color(id) for id in rooms_ids])

                    elif ins in ["small_vae", "small_vae_online"]:

                        # def image_for_room(id):
                        #     if id == 3:
                        #         col = [
                        #             [9, 73, 241],
                        #             [200, 96, 31],
                        #             [19, 222, 23],
                        #             [11, 208, 219],
                        #             [212, 13, 254]
                        #         ][np.random.randint(5)]
                        #
                        #     elif id == 0:
                        #         col = [190, 175, 45]
                        #
                        #     elif id == 1:
                        #         col = [120, 120, 100]
                        #
                        #     col = np.array(col) / 255.0
                        #     img = np.tile(col, 48 * 48)
                        #
                        #     return img
                        #
                        # imgs = np.array([image_for_room(r) for r in rooms_ids])
                        # avg_colors = imgs[:, :3]

                        def generate_dataset(variant):

                            imsize = variant.get("imsize", 48)
                            imgs = (variant["imgs"]*256).astype(np.uint8)

                            dataset = np.array(imgs)

                            test_p = variant.get('test_p', 0.95)

                            n = int(len(dataset) * test_p)
                            train_dataset = dataset[:n, :]
                            test_dataset = dataset[n:, :]

                            return train_dataset, test_dataset, {}

                        train_vae_variant = dict(
                            representation_size=repr_size,
                            beta=b,
                            num_epochs=n_ep,
                            imsize=48,
                            dump_skew_debug_plots=False,
                            decoder_activation='gaussian',
                            # decoder_activation='sigmoid',
                            generate_vae_data_fctn=generate_dataset,
                            generate_vae_dataset_kwargs={"imgs": imgs},
                            vae_kwargs=dict(
                                decoder_distribution='gaussian_identity_variance',
                                input_channels=3,
                                architecture=imsize48_tiny_architecture,
                            ),
                            algo_kwargs=dict(
                                lr=1e-3,
                            ),
                            save_period=50,
                        )
                        vae = vaes.get(p, None)
                        if vae is None or "online" not in ins:
                            vae, vae_train_data, vae_test_data = train_vae(
                                train_vae_variant, return_data=True)
                            vaes[p] = vae
                        else:
                            assert "online" in ins
                            vae, vae_train_data, vae_test_data = train_vae(
                                train_vae_variant, return_data=True, init_model=vae)
                            vaes[p] = vae

                        print("trained")
                        vae.eval()
                        with torch.no_grad():
                            latents_tr = vae.encode(torch.Tensor(vae_train_data))[0]
                            latents_te = vae.encode(torch.Tensor(vae_test_data))[0]
                            X_train = torch.cat([latents_tr, latents_te], 0).detach().cpu().numpy()

                        # imgs = np.array(imgs)
                        # imsize = int(np.sqrt(imgs.shape[-1] // 3))
                        # X_train = imgs.reshape(-1, 3, imsize, imsize).mean(axis=(2, 3))
                        #
                        # n = sum(rooms_ids == 3)
                        # X_train[rooms_ids == 3] = np.random.random(size=(n, 48, 48, 3)).reshape((n, -1, 3)).mean(1)

                    elif ins == "avg_col":
                        imgs = np.array(imgs)
                        imsize = int(np.sqrt(imgs.shape[-1] // 3))
                        X_train = imgs.reshape(-1, 3, imsize, imsize).mean(axis=(2, 3))

                        n = sum(rooms_ids == 3)
                        X_train[rooms_ids == 3] = np.random.random(size=(n, 48, 48, 3)).reshape((n, -1, 3)).mean(1)

                    elif ins == "reshaped_img":
                        def embed_image(im, size):
                            imsize = int(np.sqrt(im.shape[-1] // 3))
                            im = im.reshape(3, imsize, imsize).transpose()
                            im = cv2.resize(im, dsize=(size, size), interpolation=cv2.INTER_AREA)
                            im = im.reshape(-1)
                            # im = im.mean(axis=(0, 1))
                            return im

                    s = time.time()
                    pca = PCA(n_components=2)
                    pca_data = pca.fit_transform(X_train[:N])
                    pca_x, pca_y = zip(*pca_data)
                    print("pca: ", time.time() - s)

                    title = algo + "_" + ins
                    if algo in ["bic", "aic", "aaic"]:
                        # possible_ns = [3, 4, 5, 6, 7, 8, 9, 10]
                        possible_ns = list(range(1, n_clusters, 2))
                        gmms = [GaussianMixture(
                            n_components=n, max_iter=clus_max_iter, tol=tol, verbose=0, # covariance_type='diag',
                        ).fit(X_train) for n in possible_ns]

                        if algo == "bic":
                            fitnesses = [g.bic(X_train) for g in gmms]
                        elif algo == "aic":
                            fitnesses = [g.aic(X_train) for g in gmms]
                        elif algo == "aaic":
                            fitnesses = []
                            d=3
                            params_per_gmm = (d * d - d) / 2 + 2 * d + 1
                            n=20*params_per_gmm - 1

                            def get_nb_gmm_params(gmm):
                                # assumes full covariance
                                # see https://stats.stackexchange.com/questions/229293/the-number-of-parameters-in-gaussian-mixture-model
                                nb_gmms = gmm.get_params()['n_components']
                                params_per_gmm = (d * d - d) / 2 + 2 * d + 1
                                return nb_gmms * params_per_gmm - 1

                            for l, m in enumerate(gmms):
                                k = get_nb_gmm_params(m)
                                penalty = (2 * k * (k + 1)) / (n - k - 1)
                                # fitnesses.append(0.1*m.aic(X_train) + 0.9*penalty)
                                fitnesses.append(m.aic(X_train) + penalty)
                        gmm = gmms[np.argmin(fitnesses)]

                        n_clusters = gmm.n_components
                        preds_gmm = gmm.predict(X_train[:N])
                        title = str(gmm.n_components) + title

                    elif algo == "km":
                        km = KMeans(n_clusters=n_clusters).fit(X_train)
                        preds_gmm = km.predict(X_train[:N])
                    elif algo == "auto_km":
                        possible_ns = list(range(1, 10, 2))
                        gmms = [KMeans(n_clusters=n).fit(X_train) for n in possible_ns]
                        fitnesses = [g.score(X_train) for g in gmms]
                        km = gmms[np.argmax(fitnesses)]
                        print(fitnesses)
                        preds_gmm = km.predict(X_train[:N])
                    elif algo == "km++":
                        # Prepare initial centers using K-Means++ method.
                        initial_centers = kmeans_plusplus_initializer(X_train, n_clusters).initialize()
                        kmeans_instance = kmeans(X_train, initial_centers)
                        # Run cluster analysis and obtain results.
                        kmeans_instance.process()
                        clusters = kmeans_instance.get_clusters()

                        preds_gmm = np.zeros(len(X_train))
                        for c, inds in enumerate(clusters):
                            preds_gmm[inds] = c

                    elif algo == "optics":
                        inds = np.random.randint(len(X_train), size=(min(51000, len(X_train))))
                        train_set = X_train[inds]
                        preds_gmm = OPTICS(min_samples=int(len(train_set)/20)).fit_predict(train_set[:-1000])
                        preds_gmm -= preds_gmm.min()

                        cluster_algo = KNeighborsClassifier(n_neighbors=1)
                        cluster_algo.fit(train_set[:-1000], preds_gmm)
                        preds_gmm = cluster_algo.predict(X_train)

                        n_clusters = len(np.unique(preds_gmm))
                        title = str(n_clusters) + title

                    elif algo == "dbscan":
                        inds = np.random.randint(len(X_train), size=(min(51000, len(X_train))))
                        train_set = X_train[inds]
                        # todo: try optics, because of density
                        # preds_gmm = DBSCAN(eps=(abs(train_set).max()*30)/255, min_samples=20).fit_predict(

                        if ins in ["small_vae", "small_vae_online"]:
                            eps = 10
                        else:
                            eps = 20/255

                        preds_gmm = DBSCAN(eps=eps, min_samples=20).fit_predict(
                            train_set[:-1000])
                        preds_gmm -= preds_gmm.min()

                        cluster_algo = KNeighborsClassifier(n_neighbors=1)
                        cluster_algo.fit(train_set[:-1000], preds_gmm)
                        preds_gmm = cluster_algo.predict(X_train)

                        # old_nbrs = NearestNeighbors(n_neighbors=1, algorithm='ball_tree').fit(X_train[:-1000])
                        # inds = old_nbrs.kneighbors(X_train, return_distance=False).reshape(-1)
                        # preds_gmm = preds_gmm[inds]
                        n_clusters = len(np.unique(preds_gmm))
                        title = str(n_clusters) + title

                    elif algo == "kmed":
                        # Prepare initial centers using K-Means++ method.
                        initial_centers = kmeans_plusplus_initializer(X_train, n_clusters).initialize()
                        kmeans_instance = kmedians(X_train, initial_centers)
                        # Run cluster analysis and obtain results.
                        kmeans_instance.process()
                        clusters = kmeans_instance.get_clusters()

                        preds_gmm = np.zeros(len(X_train))
                        for c, inds in enumerate(clusters):
                            preds_gmm[inds] = c

                    elif algo == 'dc':
                        km = KMeans(n_clusters=n_clusters).fit(X_train)
                        # km = KMeans(n_clusters=n_clusters).fit(X_train)
                        preds_train = np.random.randint(n_clusters, size=len(X_train))
                        for i in range(10):
                            svm = SVC(kernel='linear', max_iter=10).fit(X_train, preds_train)
                            preds_train = svm.predict(X_train)

                        preds_gmm = svm.predict(X_train[:N])

                    elif algo == 'gmm':
                        gmm = GaussianMixture(
                            n_components=n_clusters, max_iter=clus_max_iter, tol=tol, verbose=0, covariance_type='diag',
                        ).fit(X_train)
                        preds_gmm = gmm.predict(X_train[:N])

                    elif algo == 'agglo':
                        preds_gmm = AgglomerativeClustering(n_clusters=n_clusters, linkage="single").fit_predict(X_train[:N])

                    elif algo == "spec":
                        preds_gmm = SpectralClustering(n_clusters=n_clusters, assign_labels="discretize", random_state=0).fit_predict(X_train)

                    elif algo == "cure":
                        # Allocate three clusters.
                        cure_instance = cure(X_train, n_clusters)
                        cure_instance.process()
                        clusters = cure_instance.get_clusters()
                        preds_gmm = np.zeros(len(X_train))
                        for c, inds in enumerate(clusters):
                            preds_gmm[inds] = c

                    else:
                        raise NotImplementedError(algo)

                    print("time {}:".format(n_clusters), time.time() - s)

                    raw_separation = defaultdict(list)

                    for c in range(n_clusters):
                        raw_separation[c] = [i for i, pred in enumerate(preds_gmm) if pred == c]
                    # this is just for mapping cluster names so we have better visualizations
                    expert_separation = defaultdict(list)
                    for i, r in enumerate(rooms):
                        expert_separation[r.value].append(i)
                    expert_separation = dict(expert_separation)

                    mapping = {}
                    for room in [1, 3, 0]:
                        indices = expert_separation[room]
                        aliases = [a[0] for a in Counter(preds_gmm[indices]).most_common(3)]

                        for alias in aliases:
                            if alias not in mapping:
                                mapping[alias] = room
                                break

                    for cl in set(preds_gmm):
                        if cl not in mapping:
                            # if 013 not in preds, room snot in clsuters rooms should go first
                            if n_clusters < 4:
                                mapping[cl] = list(set([0, 1, 3]) - set(mapping.values()))[0]
                            else:
                                mapping[cl] = list(set(range(n_clusters))-set(mapping.values()))[0]

                    preds_gmm = np.array([mapping[i] for i in preds_gmm])

                    #metrics
                    m_title[p] = title
                    for ro in [0, 3, 1]:
                        inds = [i for i, r in enumerate(rooms) if r.value == ro]
                        cls = preds_gmm[inds]
                        most_c, n_c = Counter(cls).most_common(1)[0]
                        recl = n_c / len(cls)
                        recs[p][ro].append(recl)

                    # for cl in range(max(n_clusters, 4)):
                    for cl in np.unique(preds_gmm):
                        inds = [i for i, c in enumerate(preds_gmm) if c == cl]
                        rms = np.array(rooms)[inds]
                        if len(rms) == 0:
                            prec = 0.0
                        else:
                            print(str(cl) + ":", Counter(rms))
                            most_r, n_r = Counter(rms).most_common(1)[0]
                            prec = n_r / len(rms)
                        precs[p][cl].append(prec)

                    # draw
                    # cols = [colors[mapping[i]] for i in preds_gmm]
                    # def color_for_room(id):
                    #     if id == 1:
                    #         col = [
                    #             [9, 73, 241],
                    #             [200, 96, 31],
                    #             [19, 222, 23],
                    #             [11, 208, 219],
                    #             [212, 13, 254]][np.random.randint(5)]
                    #
                    #     elif id == 0:
                    #         col = [190, 175, 45]
                    #
                    #     elif id == 3:
                    #         col = [120, 120, 100]
                    #     else:
                    #         print(id)
                    #
                    #     return col

                    avg_colors_d = {}
                    for c in np.unique(preds_gmm):
                        avg_colors_d[c] = avg_colors[c == preds_gmm].mean(0)

                    cols = [avg_colors_d[i] for i in preds_gmm]
                    axs[iter][p].scatter(pca_x, pca_y, c=cols, s=1)
                    axs[iter][p].title.set_text(title)

                # gt
                cols = [colors[r.value] for r in rooms]
                axs[iter][num_alogs].scatter(pca_x, pca_y, c=cols, s=1)
                axs[iter][num_alogs].title.set_text("GT")

                fig.set_size_inches(10, 10)

                legend_elements = [Line2D([0], [0], marker='o', color=colors[i], label=id_name[i]) for i in id_name.keys()]
                plt.legend(handles=legend_elements)
                plt.savefig("./clustering/{}/cluster_{}.png".format(exp_name, exp_name))

            plt.clf()

            fig, axs = plt.subplots(nrows=2, ncols=num_alogs, sharex='all', sharey='all')
            fig.set_size_inches(10, 10)
            # plt.rcParams["figure.figsize"] = (10, 10)
            for a in range(num_alogs):
                for ro in [1, 3, 0]:
                    r_recs = recs[a][ro]
                    axs[0][a].plot([i*step for i in range(len(r_recs))], r_recs, color=colors[ro], label=Room(ro))
                    axs[0][a].title.set_text(m_title[a])

                axs[0][a].legend()

                for cl in range(max(n_clusters, 4)):
                    ps = precs[a][cl]
                    axs[1][a].plot([i*step for i in range(len(ps))], ps, color=colors[cl], label=cl)
                    axs[1][a].title.set_text(m_title[a])
                axs[1][a].legend()

            plt.tight_layout()
            plt.savefig("./clustering/{}/cluster_metrics_{}".format(exp_name, exp_name))
            print("saved for:", exp_name)
            # np.mean([precs[a][3] for a in num_alogs])
            # np.mean([[precs[a][c] for a in num_alogs] for c in clusters])

