import numpy as np
import copy
import time
from collections import Counter
from itertools import chain
from itertools import chain
from rlkit.core import logger
from collections import defaultdict
import cv2

from sklearn.decomposition import PCA
from sklearn.mixture import GaussianMixture
from sklearn.cluster import DBSCAN, KMeans
from sklearn.neighbors import NearestNeighbors, KNeighborsClassifier

from pathlib import Path

import rlkit.torch.pytorch_util as ptu
from multiworld.core.image_env import normalize_image
from rlkit.core.eval_util import create_stats_ordered_dict
from rlkit.data_management.obs_dict_replay_buffer import flatten_dict
from rlkit.data_management.shared_obs_dict_replay_buffer import \
    SharedObsDictRelabelingBuffer
from rlkit.envs.vae_wrapper import VAEWrappedEnv
from rlkit.torch.vae.vae_trainer import (
    compute_p_x_np_to_np,
    relative_probs_from_log_probs,
)
from rlkit.torch.vae.conv_vae import ConvVAE
from rlkit.torch.vae.vae_trainer import ConvVAETrainer
from rlkit.torch.vae.conv_vae import imsize48_default_architecture, imsize48_tiny_architecture
from rlkit.pythonplusplus import identity
from scipy.sparse import csr_matrix


def dff(): return defaultdict(float)
def dfl(): return defaultdict(list)

class OnlineVaeRelabelingBuffer(SharedObsDictRelabelingBuffer):

    def __init__(
            self,
            vae,
            *args,
            decoded_obs_key='image_observation',
            decoded_achieved_goal_key='image_achieved_goal',
            decoded_desired_goal_key='image_desired_goal',
            exploration_rewards_type='None',
            exploration_rewards_scale=1.0,
            vae_priority_type='None',
            start_skew_epoch=0,
            power=1.0,
            internal_keys=None,
            priority_function_kwargs=None,
            relabeling_goal_sampling_mode='vae_prior',
            use_log_probs=False,
            dump_reprs=False,
            replacement=True,
            regions=False,
            hard_prior=False,
            uni_prior_scalar=0.2,
            n_clusters=5,
            aaic_possible_ns=None,
            expert_LP=False,
            uniform_LP=False,
            custom_expert_LP=False,
            custom_expert_LP_dict=False,
            alp_gmm=False,
            alp_gmm_k=1,
            quantize_size=3,
            quantize_n_colors=4,
            cluster_dqn_train=False,
            lp_history_size=None,
            clamp_by_initial=False,
            normalize_by_mean_of_vars=False,
            cluster_space="pretrained_features",
            reshaped_img_size=2,
            clustering='gmm',
            pca_n=50,
            hard_cluster_sample=False,
            lp_temp=1.0,
            policy=None,
            **kwargs
    ):
        if internal_keys is None:
            internal_keys = []

        for key in [
            decoded_obs_key,
            decoded_achieved_goal_key,
            decoded_desired_goal_key,
            "expert_knowledge_state_observation",
            "expert_knowledge_state_desired_goal",
            "expert_knowledge_state_achieved_goal",
            "image_observation_for_pretrained_features",
            "pretrained_features_observation",
        ]:
            if key not in internal_keys:
                internal_keys.append(key)
        super().__init__(internal_keys=internal_keys, *args, **kwargs)
        assert isinstance(self.env, VAEWrappedEnv)
        self.clustering = clustering
        assert self.clustering in ["gmm", "km", "dbscan", "aaic", "pca_gmm"]

        self.pca_n = pca_n

        self.n_clusters = n_clusters
        self.aaic_possible_ns = aaic_possible_ns
        self.policy = policy
        self.cluster_space = cluster_space
        assert self.cluster_space in [
            "vae", "policy", "average_color", "reshaped_image",
            "clustering_vae", "pretrained_features"
        ]

        if self.cluster_space == "clustering_vae":
            # unhardcode this things, create a variant?
            self.clustering_vae = ConvVAE(
                representation_size=3,
                decoder_output_activation=identity,
                decoder_distribution='gaussian_identity_variance',
                input_channels=3,
                imsize=48,
                architecture=imsize48_tiny_architecture,
            )
            self.clustering_vae_trainer = ConvVAETrainer(
                np.zeros(shape=(2, 48 * 48 * 3)).astype(np.uint8),  # dummy train dataset
                np.zeros(shape=(2, 48 * 48 * 3)).astype(np.uint8),  # dummy test dataset
                self.clustering_vae,
                batch_size=128,
                beta=1,
                beta_schedule=None,
                lr=1e-3,
            )

        self.reshaped_img_size = reshaped_img_size
        self.hard_cluster_sample = hard_cluster_sample
        self.lp_temp = lp_temp

        self.clusters = list(range(self.n_clusters))

        self.uni_prior_scalar = uni_prior_scalar
        self.hard_prior = hard_prior
        self.clustering_vae_train_time = 0.0
        self.clustering_time = 0.0
        self.refresh_lats_time = 0.0
        self.compute_pretrained_features_time = 0.0
        self.create_separation_time = 0.0

        self.expert_LP = expert_LP
        self.uniform_LP = uniform_LP
        self.custom_expert_LP = custom_expert_LP
        self.alp_gmm = alp_gmm
        self.alp_gmm_k = alp_gmm_k

        self.quantize_size = quantize_size
        self.quantize_n_colors = quantize_n_colors

        assert not (uniform_LP and custom_expert_LP)

        if self.custom_expert_LP:
            assert custom_expert_LP_dict is not None
            self.custom_expert_LP_dict = {
                {
                    "START": self.env.Room.START,
                    "RANDOM": self.env.Room.RANDOM,
                    "TV": self.env.Room.TV,
                    "OBJECTS": self.env.Room.OBJECTS
                }[k]: v for k, v in custom_expert_LP_dict.items()
            }

        self.skew = False
        self.dump_reprs = dump_reprs
        self.vae = vae
        self.regions = regions
        self.lp_history_size = lp_history_size
        self.clamp_by_initial = clamp_by_initial
        self.normalize_by_mean_of_vars = normalize_by_mean_of_vars

        self.decoded_obs_key = decoded_obs_key
        self.decoded_desired_goal_key = decoded_desired_goal_key
        self.decoded_achieved_goal_key = decoded_achieved_goal_key
        self.exploration_rewards_type = exploration_rewards_type
        self.exploration_rewards_scale = exploration_rewards_scale
        self.start_skew_epoch = start_skew_epoch
        self.cluster_dqn_train = cluster_dqn_train
        self.vae_priority_type = vae_priority_type
        self.power = power
        if self.power > 0.0:
            raise ValueError("power must be negative or zero")
        self.use_log_probs = use_log_probs
        self.replacement = replacement
        self._relabeling_goal_sampling_mode = relabeling_goal_sampling_mode
        self.performance_history = []
        self.performance_history_expert = []
        self.history_metadata = []

        self.perf_history_sizes = [0]
        self.cluster_perf_steps_dicts_history = []
        self.expert_cluster_perf_steps_dicts = []
        self.expert_cluster_perf_dict = defaultdict(dfl)

        self.vae_training_sampled_rooms = Counter()
        self.vae_training_sampled_rooms_last = Counter()
        self.goals_sampled_rooms = Counter()
        self.goals_sampled_rooms_last = Counter()
        self.goals_sampled_clusters = Counter()
        self.goals_sampled_clusters_last = Counter()

        # visible entities
        self.goals_sampled_entities = Counter()
        self.goals_sampled_entities_last = Counter()

        self.vae_training_sampled_TV_states = Counter()
        self.vae_training_sampled_TV_states_last = Counter()
        self.goals_sampled_TV_states = Counter()
        self.goals_sampled_TV_states_last = Counter()

        self.sample_indices_sampled_clusters = Counter()
        self.sample_indices_sampled_clusters_last = Counter()

        self._give_explr_reward_bonus = (
                exploration_rewards_type != 'None'
                and exploration_rewards_scale != 0.
        )
        self._exploration_rewards = np.zeros((self.max_size, 1))
        self._prioritize_vae_samples = (
                vae_priority_type != 'None'
                and power != 0.
        )
        self._vae_sample_priorities = np.zeros((self.max_size, 1))
        self._vae_sample_probs = None

        type_to_function = {
            'vae_prob': self.vae_prob,
            'count_based': self.state_visitation,
            'None': self.no_reward,
        }

        if self.exploration_rewards_type == "count_based":
            assert self.vae_priority_type == self.exploration_rewards_type
            self.state_counts = defaultdict(int)

        self.exploration_reward_func = (
            type_to_function[self.exploration_rewards_type]
        )
        self.vae_prioritization_func = (
            type_to_function[self.vae_priority_type]
        )

        if priority_function_kwargs is None:
            self.priority_function_kwargs = dict()
        else:
            self.priority_function_kwargs = priority_function_kwargs

        self.epoch = 0
        self._register_mp_array("_exploration_rewards")
        self._register_mp_array("_vae_sample_priorities")

    def add_paths(self, paths):
        super().add_paths(paths)

        self.compute_pretrained_features()


    def add_path(self, path):
        # must be called before decoded vae_goal_to_path
        self.save_goal_last_state_pair(path)

        if self.exploration_rewards_type == "count_based":
            self.update_counts(path)

        self.add_decoded_vae_goals_to_path(path)
        super().add_path(path)

    def save_goal_last_state_pair(self, path):
        # this "if" is important because before the buffer is filled vae_prior is used and then we dont have images
        # also, if we dont use the buffer the whole thing doesn't make sense
        if self.env.goal_sampling_mode == "custom_goal_sampler":
            perf_metrics, expert_perf_metrics, metadata = self.env.extract_metrics_from_path(
                path,
                decoded_achieved_goal_key='image_achieved_goal',
                decoded_desired_goal_key='image_desired_goal'
            )

            # not sure if deepcopy is needed here but it cant hurt
            self.performance_history.append(copy.deepcopy(perf_metrics))
            self.performance_history_expert.append(copy.deepcopy(expert_perf_metrics))
            self.history_metadata.append(copy.deepcopy(metadata))


    def add_decoded_vae_goals_to_path(self, path):
        # decoding the self-sampled vae images should be done in batch (here)
        # rather than in the env for efficiency
        desired_goals = flatten_dict(
            path["observations"],
            [self.desired_goal_key]
        )[self.desired_goal_key]
        desired_decoded_goals = self.env._decode(desired_goals)
        desired_decoded_goals = desired_decoded_goals.reshape(
            len(desired_decoded_goals),
            -1
        )
        for idx, next_obs in enumerate(path["observations"]):
            path["observations"][idx][self.decoded_desired_goal_key] = \
                desired_decoded_goals[idx]
            path["next_observations"][idx][self.decoded_desired_goal_key] = \
                desired_decoded_goals[idx]

    def get_diagnostics(self):
        if self._vae_sample_probs is None or self._vae_sample_priorities is None:
            stats = create_stats_ordered_dict(
                'VAE Sample Weights',
                np.zeros(self._size),
            )
            stats.update(create_stats_ordered_dict(
                'VAE Sample Probs',
                np.zeros(self._size),
            ))
        else:
            vae_sample_priorities = self._vae_sample_priorities[:self._size]
            vae_sample_probs = self._vae_sample_probs[:self._size]
            stats = create_stats_ordered_dict(
                'VAE Sample Weights',
                vae_sample_priorities,
            )
            stats.update(create_stats_ordered_dict(
                'VAE Sample Probs',
                vae_sample_probs,
            ))

        def fallback_division(n, d):
            return n / d if d else 0

        # vae perf and LP
        stats.update({"perf_vae_"+str(r): self.cluster_perf_dict.get(r, [0.0])[-1] for r in self.clusters})
        stats.update({"lp_vae_"+str(r): self.cluster_lp_dict.get(r, 0.0) for r in self.clusters})

        sum_lp = sum([self.cluster_lp_dict.get(c, 0.0) for c in self.clusters])

        stats.update({"norm_lp_vae_"+str(c): fallback_division(self.cluster_lp_dict.get(c, 0.0), sum_lp) for c in self.clusters})

        # vae train sampling statistics
        vae_training_sampled_rooms_current = self.vae_training_sampled_rooms - self.vae_training_sampled_rooms_last
        self.vae_training_sampled_rooms_last = self.vae_training_sampled_rooms.copy()

        vae_training_sampled_TV_states_current = self.vae_training_sampled_TV_states - self.vae_training_sampled_TV_states_last
        self.vae_training_sampled_TV_states_last = self.vae_training_sampled_TV_states.copy()
        if len(vae_training_sampled_TV_states_current.keys()): assert type(list(vae_training_sampled_TV_states_current.keys())[0]) in [np.bool_, bool]  # assert counter keys are bool

        # vae: room / all rooms
        stats.update({"vae_tr_exp_perc_of_"+str(self.env.Room(r)): fallback_division(
            vae_training_sampled_rooms_current[r], sum(vae_training_sampled_rooms_current.values())) for r in self.env.available_rooms})

        # vae: TV state / TV_room
        stats.update({"vae_tr_exp_perc_of_TV_room_with_TV_states_"+str("ON" if s else "OFF"): fallback_division(
            vae_training_sampled_TV_states_current[s],
            sum(vae_training_sampled_TV_states_current.values())
        ) for s in [True, False]})

        # vae: TV state / all rooms
        stats.update({
            "vae_tr_exp_perc_of_TV_"+str("ON" if s else "OFF"): fallback_division(
                vae_training_sampled_TV_states_current[s],
                sum(vae_training_sampled_rooms_current.values())
            ) for s in [True, False]
        })

        # goal sampling statistics
        goals_sampled_rooms_current = self.goals_sampled_rooms - self.goals_sampled_rooms_last
        self.goals_sampled_rooms_last = self.goals_sampled_rooms.copy()

        goals_sampled_TV_states_current = self.goals_sampled_TV_states - self.goals_sampled_TV_states_last
        self.goals_sampled_TV_states_last = self.goals_sampled_TV_states.copy()
        if len(goals_sampled_TV_states_current.keys()): assert type(list(goals_sampled_TV_states_current.keys())[0]) in [np.bool_, bool]  # assert counter keys are bool

        print("current sum:", sum(goals_sampled_TV_states_current.values()))

        goals_sampled_entities_current = self.goals_sampled_entities - self.goals_sampled_entities_last
        self.goals_sampled_entities_last = self.goals_sampled_entities.copy()

        # goals: entity / all goals sampled
        assert len(self.env.ent_names_to_visualize) == 14 + 1  # tv on off
        stats.update({
            "goals_exp_perc_of_{}".format(e): fallback_division(
                goals_sampled_entities_current[e],
                sum(goals_sampled_rooms_current.values())  # all goals
            ) for e in self.env.ent_names_to_visualize
        })

        # all goals: entity / all goals sampled
        stats.update({
            "all_goals_exp_perc_of_{}".format(e): fallback_division(
                self.goals_sampled_entities[e],
                sum(self.goals_sampled_entities.values())  # all goals
            ) for e in self.env.ent_names_to_visualize
        })

        # all goals: room / all rooms
        stats.update({
            "all_goals_exp_perc_of_"+str(self.env.Room(r)): fallback_division(
                self.goals_sampled_rooms[r],
                sum(self.goals_sampled_rooms.values())
            ) for r in self.env.available_rooms
        })

        # goals: room / all rooms
        stats.update({
            "goals_exp_perc_of_"+str(self.env.Room(r)): fallback_division(
                goals_sampled_rooms_current[r],
                sum(goals_sampled_rooms_current.values())
            ) for r in self.env.available_rooms
        })


        # goals: TV state / TV_room
        stats.update({"goals_exp_perc_of_TV_room_with_TV_states_"+str("ON" if s else "OFF"): fallback_division(
            goals_sampled_TV_states_current[s],
            sum(goals_sampled_TV_states_current.values())
        ) for s in [True, False]})

        # goals: tv state / all rooms
        stats.update({
            "goals_exp_state_perc_of_TV_"+str("ON" if s else "OFF"): fallback_division(
            goals_sampled_TV_states_current[s],
            sum(goals_sampled_rooms_current.values())
            ) for s in [True, False]
        })

        assert max({
            "goals_exp_perc_of_TV_"+str("ON" if s else "OFF"): fallback_division(
                goals_sampled_TV_states_current[s],
                sum(goals_sampled_rooms_current.values())
            ) for s in [True, False]
        }.values()) <= 1.0


        # cluster sampling statistics
        goals_sampled_clusters_current = self.goals_sampled_clusters - self.goals_sampled_clusters_last
        self.goals_sampled_clusters_last = self.goals_sampled_clusters.copy()

        stats.update({"goals_exp_perc_of_cl_"+str(c): fallback_division(goals_sampled_clusters_current.get(c, 0), sum(goals_sampled_clusters_current.values())) for c in self.clusters})

        sample_indices_sampled_clusters_current = self.sample_indices_sampled_clusters - self.sample_indices_sampled_clusters_last
        self.sample_indices_sampled_clusters_last = self.sample_indices_sampled_clusters.copy()
        stats.update({"sample_indices_perc_of_cl_"+str(c): fallback_division(sample_indices_sampled_clusters_current.get(c, 0), sum(sample_indices_sampled_clusters_current.values())) for c in self.clusters})

        buffer_rooms = [self.env.vector_to_state_dict(g)["current_room"] for g in self._next_obs["expert_knowledge_state_achieved_goal"][:self._size]]
        TV_on_off_states = [self.env.state_dict_is_TV_ON(self.env.vector_to_state_dict(g)) for g in self._next_obs["expert_knowledge_state_achieved_goal"][:self._size]]

        buffer_visible_entities = [self.env.goal_dict_to_ent_names(self.env.vector_to_state_dict(g)) for g in self._next_obs["expert_knowledge_state_achieved_goal"][:self._size]]

        num_separated = len(list(chain(*self.separation.values())))
        separated_rooms = np.array(buffer_rooms)[:num_separated]
        separated_visible_ent_names = np.array(buffer_visible_entities, dtype=object)[:num_separated]

        TV_on_off_states_separated = np.array(TV_on_off_states)[:num_separated]

        cl_distr = {}
        cl_size = {}
        cl_distr_norm = {}

        cl_ent_distr = {}
        cl_n_ents = {}
        cl_ent_distr_norm = {}

        for cl in self.clusters:
            counter = Counter(separated_rooms[self.separation.get(cl, [])])
            cl_distr[cl] = {r: counter[r] for r in self.env.available_rooms}
            cl_size[cl] = len(self.separation.get(cl, []))
            cl_distr_norm[cl] = {r: fallback_division(cl_distr[cl][r], cl_size[cl]) for r in self.env.available_rooms}

            # per cluster entity statistics
            entities_in_cluster = list(chain(*separated_visible_ent_names[self.separation.get(cl, [])]))
            counter = Counter(entities_in_cluster)
            cl_ent_distr[cl] = {e: counter[e] for e in self.env.ent_names_to_visualize}
            cl_n_ents[cl] = len(entities_in_cluster)
            cl_ent_distr_norm[cl] = {e: fallback_division(cl_ent_distr[cl][e], cl_n_ents[cl]) for e in self.env.ent_names_to_visualize}

        stats.update({"cl_size_"+str(c): cl_size[c] for c in self.clusters})
        stats.update({"cl_n_ents_"+str(c): cl_n_ents[c] for c in self.clusters})

        for cl in self.clusters:
            # stats.update({"cl_{}_pc_of_r_{}".format(str(cl), r): cl_distr_norm[cl][r] for r in self.env.available_rooms})
            # stats.update({"cl_{}_pc_of_ent_{}".format(str(cl), e): cl_ent_distr_norm[cl][e] for e in self.env.ent_names_to_visualize})
            stats.update({"cl_{}_num_of_ent_{}".format(str(cl), e): cl_ent_distr[cl][e] for e in self.env.ent_names_to_visualize})

        if self.cluster_space == "clustering_vae":
            stats.update({"clustering_vae_train_time": self.clustering_vae_train_time})

        stats.update({"clustering_time": self.clustering_time})
        stats.update({"refresh_latents_time": self.refresh_lats_time})
        stats.update({"compute_pretrained_features_time": self.compute_pretrained_features_time})
        stats.update({"create_separation_time": self.create_separation_time})

        # clustering per room distribution
        # room -> n_cls that this room is the most common in
        rc = Counter([max(cl_distr_norm[cl], key=cl_distr_norm[cl].get) for cl in self.separation.keys()])
        stats.update({"{}_num_of_cls".format(r): rc[r] for r in self.env.available_rooms})
        stats.update({"{}_pc_of_cls".format(r): rc[r]/len(self.separation.keys()) for r in self.env.available_rooms})
        stats.update({"num_of_clusters": len(self.separation.keys())})

        # ent -> n clusters that this ent is the most common in
        ent_cl = Counter([max(cl_ent_distr_norm[cl], key=cl_ent_distr_norm[cl].get) for cl in self.separation.keys()])
        stats.update({"{}_num_of_cls".format(e): ent_cl[e] for e in self.env.ent_names_to_visualize})
        stats.update({"{}_pc_of_cls".format(e): ent_cl[e]/self.n_clusters for e in self.env.ent_names_to_visualize})

        # clustering quality
        # perc of the most common room in cluster
        stats.update({"cl_{}_precision".format(str(cl)): max(cl_distr_norm[cl].values()) for cl in self.clusters})
        # the most common room
        stats.update({"cl_{}_room".format(str(cl)): max(cl_distr_norm[cl], key=cl_distr_norm[cl].get) for cl in self.clusters})
        stats.update({"cl_avg_precision": np.mean([max(cl_distr_norm[cl].values()) for cl in self.separation.keys()])})


        # this is not ideal since there can be many entities in one goal
        # perc of the most common ent in cluster
        stats.update({"cl_ent_{}_precision".format(str(cl)): max(cl_ent_distr_norm[cl].values()) for cl in self.clusters})
        # the most common ent
        stats.update({"cl_ent_{}_ent".format(str(cl)): max(cl_ent_distr_norm[cl], key=cl_ent_distr_norm[cl].get) for cl in self.clusters})
        stats.update({"cl_ent_avg_precision": np.mean([max(cl_ent_distr_norm[cl].values()) for cl in self.separation.keys()])})

        assert len(self.separation.keys()) == self.n_clusters

        # estimated LP for many clusters
        room_est_lp = defaultdict(list)
        for cl in self.clusters:
            # room is the one most common in the cluster
            room = max(cl_distr_norm[cl], key=cl_distr_norm[cl].get)
            if cl_distr_norm[cl][room] > 0:
                # todo: should add that the cluster with the most samples from the room is selected
                room_est_lp[room].append(self.cluster_lp_dict[cl])

            # todo: do this properly for entities


        room_est_lp_averaged = defaultdict(float)
        for room in room_est_lp.keys():
            room_est_lp_averaged[room] = np.mean(room_est_lp[room])

        stats.update({"estimated_LP_{}".format(r): room_est_lp_averaged[r] for r in self.env.available_rooms})

        sum_est_lp = sum([room_est_lp_averaged[r] for r in self.env.available_rooms])
        room_est_lp_averaged_norm = defaultdict(float)
        for room in self.env.available_rooms:
            room_est_lp_averaged_norm[room] = fallback_division(room_est_lp_averaged[room], sum_est_lp)

        stats.update({"norm_estimated_LP_{}".format(r): room_est_lp_averaged_norm[r] for r in self.env.available_rooms})

        # for TV room
        tv_st_LP = defaultdict(list)
        tv_st_prec = defaultdict(list)
        for cl in self.clusters:
            room = max(cl_distr_norm[cl], key=cl_distr_norm[cl].get)
            if room == self.env.Room.TV:
                indices = self.separation.get(cl, [])
                if len(indices) > 0:
                    assert self.env.Room.TV in set(separated_rooms[indices])
                    tv_indices = [i for i in indices if separated_rooms[i] == self.env.Room.TV]

                    TV_on_off_states_counter = Counter(TV_on_off_states_separated[tv_indices])
                    max_gr_st = max(TV_on_off_states_counter, key=TV_on_off_states_counter.get)
                    tv_st_LP[max_gr_st].append(self.cluster_lp_dict[cl])
                    tv_st_prec[max_gr_st].append(TV_on_off_states_counter[max_gr_st]/len(indices))

            # todo: do this for new env

        for s in tv_st_LP.keys():
            tv_st_LP[s] = np.mean(tv_st_LP[s])
            tv_st_prec[s] = np.mean(tv_st_prec[s])

        if len(tv_st_LP.keys()):
            assert type(list(tv_st_LP.keys())[0]) in [np.bool_, bool]  # assert counter keys are bool
        stats.update({"estimated_LP_{}".format("TV_ON" if s > 0 else "TV_OFF"): tv_st_LP.get(s, 0) for s in [True, False]})
        stats.update({"norm_estimated_LP_{}".format("TV_ON" if s > 0 else "TV_OFF"): fallback_division(tv_st_LP.get(s, 0), sum_est_lp) for s in [True, False]})
        stats.update({"avg_prec_{}".format("TV_ON" if s > 0 else "TV_OFF"): tv_st_prec.get(s, 0) for s in [True, False]})

        # buffer distribution
        rooms_distr = Counter(buffer_rooms)
        stats.update({"buffer_num_of_"+str(r): rooms_distr[r] for r in self.env.available_rooms})
        stats.update({"buffer_pc_of_"+str(r): rooms_distr[r]/self._size for r in self.env.available_rooms})
        # todo: add intr rew per room/object
        stats.update({"buffer_size": self._size})

        # number of different images on tv
        # todo: move to env
        buffer_goal_dicts = [self.env.vector_to_state_dict(g) for g in self._next_obs["expert_knowledge_state_achieved_goal"][:self._size]]
        n_tex_names = len(set([gd['tv_tex_name'] for gd in buffer_goal_dicts if "tv_ON" in self.env.goal_dict_to_ent_names(gd)]))
        stats.update({"num_of_tex_names_in_buffer": n_tex_names})
        # print("n_tex_names:", n_tex_names)


        if hasattr(self, "stats_len"):
            assert len(stats.keys()) == self.stats_len
        else:
            self.sgoals_sampled_TV_states_currenttats_len = len(stats.keys())

        return stats

    def compute_pretrained_features(self):
        if self.cluster_space == "pretrained_features" and self.regions:
            compute_pretrained_features_start = time.time()

            uncomputed_indxs = (self._next_obs["pretrained_features_observation"][:self._size].sum(axis=1) == 0.0).nonzero()[0]

            size = len(uncomputed_indxs)

            cur_idx = 0
            batch_size = 512
            next_idx = min(batch_size, size)

            while cur_idx < size:

                uncomp_idxs = uncomputed_indxs[cur_idx:next_idx]
                # todo: vrati i pokreni, malo dulje da se vrti za svaki slucaj
                pretrained_features = self.env.extract_pretrained_features(
                    normalize_image(self._next_obs["image_observation_for_pretrained_features"][uncomp_idxs]).reshape(
                        len(uncomp_idxs),
                        3,
                        self.env.pre_feat_imsize,
                        self.env.pre_feat_imsize,
                    )
                )
                #
                # assert np.array_equal(
                #     pretrained_features,
                #     self._next_obs["pretrained_features_observation"][uncomp_idxs].astype(np.float32)
                # )
                self._next_obs["pretrained_features_observation"][uncomp_idxs] = pretrained_features.copy()

                cur_idx = next_idx
                next_idx += batch_size
                next_idx = min(next_idx, size)
            compute_pretrained_features_end = time.time()
            self.compute_pretrained_features_time = compute_pretrained_features_end-compute_pretrained_features_start
            print("compute_pretrained_features_time:", self.compute_pretrained_features_time)

    def refresh_latents(self, epoch):
        ref_lats_start = time.time()
        self.epoch = epoch
        self.skew = (self.epoch > self.start_skew_epoch)
        batch_size = 512
        next_idx = min(batch_size, self._size)

        if self.exploration_rewards_type == 'hash_count':
            # you have to count everything then compute exploration rewards
            cur_idx = 0
            next_idx = min(batch_size, self._size)
            while cur_idx < self._size:
                idxs = np.arange(cur_idx, next_idx)
                normalized_imgs = (
                    normalize_image(self._next_obs[self.decoded_obs_key][idxs])
                )
                cur_idx = next_idx
                next_idx += batch_size
                next_idx = min(next_idx, self._size)

        cur_idx = 0
        obs_sum = np.zeros(self.vae.representation_size)
        obs_square_sum = np.zeros(self.vae.representation_size)


        # use sparse matrix? todo: test for speed improvements/degradations
        # # maye this makes everything to slow?
        # if type(self._obs[self.decoded_desired_goal_key]) == np.ndarray:
        #     sh = self._obs[self.decoded_desired_goal_key].shape
        #     self._obs[self.decoded_desired_goal_key] = csr_matrix(sh, dtype=np.int8) * 0
        #
        #     sh = self._obs[self.decoded_achieved_goal_key].shape
        #     self._obs[self.decoded_achieved_goal_key] = csr_matrix(sh, dtype=np.int8) * 0
        # else:
        #     self._obs[self.decoded_desired_goal_key] *= 0
        #     self._obs[self.decoded_achieved_goal_key] *= 0
        #
        # self._obs[self.decoded_desired_goal_key].eliminate_zeros()
        # self._obs[self.decoded_achieved_goal_key].eliminate_zeros()


        while cur_idx < self._size:
            idxs = np.arange(cur_idx, next_idx)
            self._obs[self.observation_key][idxs] = \
                self.env._encode(
                    normalize_image(self._obs[self.decoded_obs_key][idxs])
                )
            self._next_obs[self.observation_key][idxs] = \
                self.env._encode(
                    normalize_image(self._next_obs[self.decoded_obs_key][idxs])
                )
            # WARNING: we only refresh the desired/achieved latents for
            # "next_obs". This means that obs[desired/achieve] will be invalid,
            # so make sure there's no code that references this.
            # TODO: enforce this with code and not a comment
            self._next_obs[self.desired_goal_key][idxs] = \
                self.env._encode(
                    normalize_image(self._next_obs[self.decoded_desired_goal_key][idxs])
                )

            # we do not need this because goal and state are the same, so we just copy
            # self._next_obs[self.achieved_goal_key][idxs] = \
            #     self.env._encode(
            #         normalize_image(self._next_obs[self.decoded_achieved_goal_key][idxs])
            #     )

            # assert achieved goal and obs is equal
            # assert np.array_equal(
            #     self._next_obs[self.decoded_achieved_goal_key][idxs],
            #     self._next_obs[self.decoded_obs_key][idxs]
            # )
            # copy
            self._next_obs[self.achieved_goal_key][idxs] = self._next_obs[self.observation_key][idxs].copy()


            normalized_imgs = (
                normalize_image(self._next_obs[self.decoded_obs_key][idxs])
            )
            if self._give_explr_reward_bonus:
                rewards = self.exploration_reward_func(
                    normalized_imgs,
                    idxs,
                    **self.priority_function_kwargs
                )
                self._exploration_rewards[idxs] = rewards.reshape(-1, 1)
            if self._prioritize_vae_samples:
                if (
                        self.exploration_rewards_type == self.vae_priority_type
                        and self._give_explr_reward_bonus
                ):
                    self._vae_sample_priorities[idxs] = (
                        self._exploration_rewards[idxs]
                    )
                else:
                    self._vae_sample_priorities[idxs] = (
                        self.vae_prioritization_func(
                            normalized_imgs,
                            idxs,
                            **self.priority_function_kwargs
                        ).reshape(-1, 1)
                    )
            obs_sum+= self._obs[self.observation_key][idxs].sum(axis=0)
            obs_square_sum+= np.power(self._obs[self.observation_key][idxs], 2).sum(axis=0)

            cur_idx = next_idx
            next_idx += batch_size
            next_idx = min(next_idx, self._size)
        self.vae.dist_mu = obs_sum/self._size
        self.vae.dist_std = np.sqrt(obs_square_sum/self._size - np.power(self.vae.dist_mu, 2))

        if self._prioritize_vae_samples:
            """
            priority^power is calculated in the priority function
            for image_bernoulli_prob or image_gaussian_inv_prob and
            directly here if not.
            """
            if self.vae_priority_type == 'vae_prob':
                self._vae_sample_priorities[:self._size] = relative_probs_from_log_probs(
                    self._vae_sample_priorities[:self._size], use_log_probs=self.use_log_probs
                )
                self._vae_sample_probs = self._vae_sample_priorities[:self._size]
            else:
                self._vae_sample_probs = self._vae_sample_priorities[:self._size] ** self.power

            self._vae_sample_probs = self._vae_sample_probs.copy()
            p_sum = np.sum(self._vae_sample_probs)
            if np.isnan(p_sum):
                self._vae_sample_probs = np.nan_to_num(self._vae_sample_probs)

            if not self.regions:
                # for regions we will normalize later
                self._vae_sample_probs /= np.sum(self._vae_sample_probs)

            self._vae_sample_probs = self._vae_sample_probs.flatten()

        if len(self.performance_history) > 0:

            # calc performances with new vae
            assert len(self.performance_history) == len(self.performance_history_expert)

            # cluster
            if self.cluster_space == 'clustering_vae' and self.n_clusters > 1:
                # train clustering VAE
                print("Training clustering vae")
                s = time.time()
                self.clustering_vae_trainer.train_epoch(
                    self.epoch,
                    sample_batch=self.clustering_vae_training_data,
                    batches=self._size//self.clustering_vae_trainer.batch_size,  # todo: param how many epochs
                    from_rl=True,
                )
                e = time.time()
                self.clustering_vae_train_time = e - s
                print('cluster_train_time:', self.clustering_vae_train_time)

            s = time.time()
            self.separation, self.rollout_separation = self.create_separation()
            e = time.time()
            self.create_separation_time = e - s

            # refresh LPs
            goal_rooms = [h["goal_room"] for h in self.performance_history_expert]
            goal_images = np.array([h["goal"] for h in self.performance_history])
            goal_pretrained_features = np.array([h["goal_pretrained_features"] for h in self.performance_history])

            # todo: remove this assert, and also the code from to_cluster_space?
            # goal_embeddings = self.to_cluster_space(normalized_images=goal_images)
            # assert (goal_pretrained_features.astype(np.float32) == goal_embeddings).all()

            # just clustering inference
            if self.cluster_space == "pretrained_features":
                goal_embeddings = self.to_cluster_space(pretrained_features=goal_pretrained_features)
            else:
                raise NotImplementedError("to cluster space should be called with relevant arguments")

            clusters = self.cluster_embeddings(goal_embeddings)

            _, self.cluster_perf_dict, self.cluster_var_dict, goals_lat, last_states_lat, perfs = self.env.calculate_cluster_perfs(
                self.performance_history,
                clusters,
                self.perf_history_sizes
            )

            # self.cluster_perf_steps_dicts_history.append(cluster_perf_steps_dicts)
            # calculate lp based on current VAE distances
            if self.alp_gmm:
                raise DeprecationWarning("Do not use this")
                self.cluster_lp_dict = self.env.calculate_NN_LP(
                    goals_lat=goals_lat,
                    clusters=clusters,
                    perfs=perfs,
                    k=self.alp_gmm_k,
                    history_size=self.lp_history_size,
                )

            else:
                self.cluster_lp_dict = self.env.calculate_lp_for_perf_dict(
                    perf_dict=self.cluster_perf_dict,
                    history_size=self.lp_history_size,
                    clamp_by_initial=self.clamp_by_initial,
                    normalize_by_mean_of_vars=self.normalize_by_mean_of_vars,
                    vars_dict=self.cluster_var_dict,
                )

            current_cluster_expert_perf_dict_ = self.env.calculate_expert_performances(
                self.performance_history_expert,
                clusters,
                self.perf_history_sizes
            )

            self.env.add_expert_performances_to_perf_dict(
                self.expert_cluster_perf_dict,
                current_cluster_expert_perf_dict_
            )

            self.expert_cluster_perf_steps_dicts.append(current_cluster_expert_perf_dict_)

            # EXPERT LP
            self.expert_cluster_lp_dict = defaultdict(dff)
            for metric, m_dict in self.expert_cluster_perf_dict.items():
                self.expert_cluster_lp_dict[metric] = self.env.calculate_lp_for_perf_dict(m_dict)

            # rooms_lp_dict, just for diagnostic purposes
            _, self.room_perf_dict, room_var_dict, r_goals_lat, _, r_perfs = self.env.calculate_cluster_perfs(
                self.performance_history,
                [r.value for r in goal_rooms],
                self.perf_history_sizes
            )
            if self.alp_gmm:
                raise DeprecationWarning("Do not use this.")
                self.room_lp_dict = self.env.calculate_NN_LP(
                    goals_lat=r_goals_lat,
                    clusters=[r.value for r in goal_rooms],
                    perfs=r_perfs,
                    k=self.alp_gmm_k,
                    history_size=self.lp_history_size,
                )

            else:
                self.room_lp_dict = self.env.calculate_lp_for_perf_dict(
                    perf_dict=self.room_perf_dict,
                    history_size=self.lp_history_size,
                    clamp_by_initial=self.clamp_by_initial,
                    normalize_by_mean_of_vars=self.normalize_by_mean_of_vars,
                    vars_dict=room_var_dict,
                )

            if self.epoch % 10 == 0:
                perf_dict_path = Path(logger.get_snapshot_dir()) / ("cluster_perf_dict_" + str(self.epoch) + ".npy")
                np.save(str(perf_dict_path), dict(self.cluster_perf_dict))
                lp_dict_path = Path(logger.get_snapshot_dir()) / ("cluster_lp_dict_" + str(self.epoch) + ".npy")
                np.save(str(lp_dict_path), dict(self.cluster_lp_dict))

                perf_dict_path = Path(logger.get_snapshot_dir()) / ("room_perf_dict_" + str(self.epoch) + ".npy")
                np.save(str(perf_dict_path), dict(self.room_perf_dict))
                lp_dict_path = Path(logger.get_snapshot_dir()) / ("room_lp_dict_" + str(self.epoch) + ".npy")
                np.save(str(lp_dict_path), dict(self.room_lp_dict))

                lats_path = Path(logger.get_snapshot_dir()) / ("vae_lats_" + str(self.epoch) + ".npy")
                np.save(str(lats_path), {"goals_lat": goals_lat, "last_states_lat": last_states_lat, "clusters": clusters, "goal_rooms": goal_rooms})


            self.perf_history_sizes.append(len(self.performance_history))

        if self.regions:
            # recalculate probs
            if self._prioritize_vae_samples:
                for cluster, inds in self.separation.items():
                    probs = self._vae_sample_probs[inds]
                    probs_ = probs / probs.sum()
                    self._vae_sample_probs[inds] = probs_.copy()

            available_clusters = list(self.separation.keys())
            if self.uniform_LP:
                assert not self.expert_LP
                self.region_LP = {r: 1.0 for r in self.clusters}

            elif self.expert_LP:
                assert not self.uniform_LP
                # expert_LP = {c: 1.0 for c in available_clusters}

                if self.custom_expert_LP:
                    expert_LP = self.custom_expert_LP_dict

                else:
                    raise NotImplementedError("There is no mapping going on so expert clusters have to be reimplementd")

                self.region_LP = {r: expert_LP[r] for r in available_clusters}

            else:
                assert not self.expert_LP
                assert not self.uniform_LP
                self.region_LP = {r: self.cluster_lp_dict.get(r, 0) for r in self.clusters}

        # dump the replay buffer
        if self.dump_reprs and epoch % 20 == 0:

            goal_expert_dicts = [m["goal_expert_state_dict"] for m in self.history_metadata]
            last_image_expert_state_dicts = [m["last_image_expert_state_dict"] for m in self.history_metadata]

            history = {
                "goals_vae_lat": goals_lat,
                "goal_expert_dicts": goal_expert_dicts,
                "last_states_vae_lat": last_states_lat,
                "last_states_expert_dicts": last_image_expert_state_dicts,
                "vae_perfs": perfs
            }

            # dump embeddings
            expert_states = self._next_obs["expert_knowledge_state_observation"][:self._size]
            dump_dict = {
                # "images": self._next_obs[self.decoded_obs_key][:self._size],
                # "embeddings": self._next_obs[self.achieved_goal_key][:self._size],
                "history": history,
                "expert_states": expert_states[:self._size],
                "rooms": [self.env.vector_to_state_dict(v)["current_room"] for v in expert_states],
                "separation": self.separation,
                "intrinsic_rewards": self._vae_sample_probs,
            }
            filename = str(Path(logger.get_snapshot_dir()) / ("replay_buffer_embeddings_" + str(self.epoch) + ".npy"))

            np.save(filename, dump_dict)

        ref_lats_end = time.time()
        self.refresh_lats_time = ref_lats_end-ref_lats_start

    def to_cluster_space(self, normalized_images=None, latents=None, pretrained_features=None):

        if latents is None and self.cluster_space in ["policy", "vae"]:
            latents = self.env._encode(normalized_images)

        if self.cluster_space == "policy":
            embeddings = self.vae_lat_to_dqn_lat(latents)

        elif self.cluster_space == "vae":
            embeddings = latents

        elif self.cluster_space == "clustering_vae":
            self.clustering_vae.eval()
            batch_size, embeddings = 512, []
            for i in range(0, len(normalized_images), batch_size):
                embeddings.append(
                    ptu.get_numpy(self.clustering_vae.encode(ptu.from_numpy(normalized_images[i:i+512]))[0])
                )
            embeddings = np.vstack(embeddings)
            assert embeddings.shape[-1] == 3

        elif self.cluster_space == "average_color":
            imsize = int(np.sqrt(normalized_images.shape[-1] // 3))
            embeddings = normalized_images.reshape(-1, 3, imsize, imsize).mean(axis=(2, 3))

        elif self.cluster_space == "reshaped_image":
            def embed_image(img, size):
                imsize = int(np.sqrt(img.shape[-1] // 3))
                img = img.reshape(3, imsize, imsize).transpose()
                emb = cv2.resize(img, dsize=(size, size), interpolation=cv2.INTER_AREA).reshape(-1)
                return emb

            embeddings = np.array([embed_image(img, self.reshaped_img_size) for img in normalized_images])

        elif self.cluster_space == "pretrained_features":
            if pretrained_features is not None:
                return pretrained_features

            else:
                raise ValueError("This should not be used.")

                assert normalized_images is not None
                assert normalized_images.max() <= 1.0
                assert normalized_images.min() >= 0.0

                normalized_images = normalized_images.reshape(
                    normalized_images.shape[0],
                    3,
                    self.env.imsize,
                    self.env.imsize,
                )

                return self.env.extract_pretrained_features(normalized_images)

        else:
            raise NotImplementedError("unknown cluster space: ", self.cluster_space)

        return embeddings

    def vae_lat_to_dqn_lat(self, embs):
        padded_embs = np.hstack([embs, np.zeros_like(embs)])
        embs = self.policy.embed_inputs(padded_embs)
        return embs

    def construct_PCA(self, x):
        pca = PCA(n_components=self.pca_n)
        pca_x = pca.fit_transform(x)
        return pca, pca_x

    @staticmethod
    def get_nb_gmm_params(gmm, d):
        # assumes full covariance
        # see https://stats.stackexchange.com/questions/229293/the-number-of-parameters-in-gaussian-mixture-model
        nb_gmms = gmm.get_params()["n_components"]
        params_per_gmm = (d * d - d) / 2 + 2 * d + 1
        return nb_gmms * params_per_gmm - 1

    def create_separation(self):
        # separation is a dict
        # k=cluster v=indices_for_cluster
        normalized_images = normalize_image(self._next_obs[self.decoded_obs_key][:self._size])
        imgep_vae_embeddings = self._next_obs[self.achieved_goal_key][:self._size]
        pretrained_features = self._next_obs["pretrained_features_observation"][:self._size]

        cluster_start = time.time()
        embeddings = self.to_cluster_space(normalized_images, imgep_vae_embeddings, pretrained_features)

        if self.clustering == "gmm":
            self.cluster_algo = GaussianMixture(
                n_components=self.n_clusters, max_iter=100, tol=1e-2, verbose=0,
            ).fit(embeddings)

        elif self.clustering == "pca_gmm":
            pca, pca_embeddings = self.construct_PCA(embeddings)
            gmm = GaussianMixture(
                n_components=self.n_clusters, max_iter=100, tol=1e-2, verbose=0,
            ).fit(pca_embeddings)

            self.cluster_algo = lambda x: gmm.predict(pca.transform(x))
            # todo: add drop_ratio

        elif self.clustering == "km":
            self.cluster_algo = KMeans(n_clusters=self.n_clusters).fit(embeddings).predict

        elif self.clustering == "dbscan":
            preds_gmm = DBSCAN(eps=30/255, min_samples=20).fit_predict(embeddings)
            preds_gmm -= preds_gmm.min()
            if len(set(preds_gmm)) > self.n_clusters:
                print("too many dbscan clusters puting all in the same one")
                preds_gmm *= 0

            knn = KNeighborsClassifier(n_neighbors=3)
            knn.fit(embeddings, preds_gmm)
            self.cluster_algo = knn.predict

        elif self.clustering == "aaic":
            assert self.n_clusters > 10 or self.n_clusters == 1  # doesn't really make sense otherwise

            if self.n_clusters != 1:
                if self.aaic_possible_ns is None:
                    possible_ns = list(range(2, self.n_clusters, 2))
                else:
                    possible_ns = self.aaic_possible_ns
                gmms = [GaussianMixture(
                    n_components=n, max_iter=100, tol=1e-2, verbose=0,  # covariance_type='diag',
                ).fit(embeddings) for n in possible_ns]

            else:
                gmms = [GaussianMixture(
                    n_components=1, max_iter=100, tol=1e-2, verbose=0,  # covariance_type='diag',
                ).fit(embeddings)]

            d = embeddings.shape[-1]
            assert d == 3
            params_per_gmm = (d * d - d) / 2 + 2 * d + 1
            n = self.n_clusters * params_per_gmm - 1


            fitnesses = []
            for l, m in enumerate(gmms):
                k = self.get_nb_gmm_params(m, d)
                penalty = (2 * k * (k + 1)) / (n - k - 1)
                # fitnesses.append(0.1*m.aic(embeddings) + 0.9*penalty)
                fitnesses.append(m.aic(embeddings) + penalty)

            self.cluster_algo = gmms[np.argmin(fitnesses)].predict
        else:
            raise NotImplementedError("{} undefined algorithm".format(self.clustering))

        preds_gmm = self.cluster_embeddings(embeddings)

        cluster_end = time.time()
        self.clustering_time = cluster_end - cluster_start
        print("clustering time:", self.clustering_time)

        raw_separation = defaultdict(list)
        for c in np.unique(preds_gmm):
            raw_separation[c] = list(np.where(preds_gmm == c)[0])

        separation = raw_separation.copy()

        idx_to_cluster = dict()
        # dqn agent is trained between adding the new data to the buffer and refreshing the latents and separtaion
        # so at this point we have more data in the buffer than in the separation

        # rollout separation
        separation_size = max(list(chain(*separation.values())))
        for c, inds in separation.items():
            for ind in inds:
                idx_to_cluster[ind] = c

        rollout_separtaion = defaultdict(list)
        for i in range(separation_size):
            last_idx = self._idx_to_future_obs_idx[i][-1]
            rollout_separtaion[idx_to_cluster[last_idx]].append(i)

        return separation, rollout_separtaion

    def cluster_embeddings(self, embeddings):
        preds = self.cluster_algo(embeddings)
        return preds

    def _sample_indices(self, batch_size):
        if self.regions and self.skew and self.cluster_dqn_train:
            # dqn agent is trained between adding the new data to the buffer and refreshing the latents and separation
            # so at this point we have more data in the buffer than in the separation

            sampled_clusters = self.sample_clusters(batch_size)
            self.sample_indices_sampled_clusters += Counter(sampled_clusters)
            sampled_indices = list(chain(*[
                np.random.choice(self.rollout_separation[c], size=n) for c, n in Counter(sampled_clusters).items()
            ]))

            return sampled_indices

        else:
            return super()._sample_indices(batch_size)

    def sample_clusters(self, batch_size):
        clusters = list(self.separation.keys())
        clusters_LP = [self.region_LP[c] for c in clusters]
        regions_probs = np.array(clusters_LP) ** self.lp_temp

        # add prior
        if self.hard_prior:
            # we must sample because goals are sampled with batch_size = 1
            n_uni = np.sum(np.random.random(batch_size) < self.uni_prior_scalar)
            n_lp = batch_size - n_uni

            if np.sum(regions_probs) == 0:
                # happens when all are clipped by the first one
                regions_probs += 1

            regions_probs /= np.sum(regions_probs)

            assert not np.isnan(np.sum(regions_probs))  # clamped by first to same values? so all zeros?
            lp_sampled_clusters = np.random.choice(clusters, n_lp, p=regions_probs)
            uni_sampled_clusters = np.random.choice(clusters, n_uni)

            sampled_clusters = np.hstack((lp_sampled_clusters, uni_sampled_clusters))

        else:
            prior = np.mean(regions_probs) * self.uni_prior_scalar
            regions_probs += prior

            regions_probs /= np.sum(regions_probs)

            sampled_clusters = np.random.choice(clusters, batch_size, p=regions_probs)

        return sampled_clusters

    def sample_weighted_indices(self, batch_size):
        if self.regions:
            if self.skew:
                sampled_clusters = self.sample_clusters(batch_size)

                clusters_n = Counter(sampled_clusters)
                self.goals_sampled_clusters += clusters_n

                sampled_indices = []
                for cluster, n in clusters_n.items():
                    cluster_inds = self.separation[cluster]

                    if self._prioritize_vae_samples and self._vae_sample_probs is not None:
                        cluster_probs = self._vae_sample_probs[cluster_inds]
                        cluster_sampled_inds = np.random.choice(
                            cluster_inds,
                            size=min(n, len(cluster_inds)),
                            replace=False,
                            p=cluster_probs
                        )
                    else:
                        cluster_sampled_inds = np.random.choice(
                            cluster_inds,
                            size=min(n, len(cluster_inds)),
                            replace=False,
                        )

                    sampled_indices.extend(cluster_sampled_inds)

                sampled_indices = np.array(sampled_indices)
                if len(sampled_indices) == batch_size:
                    indices = sampled_indices.copy()

                else:
                    # fill with uniform sampling
                    uni_indices = self._sample_indices(batch_size - len(sampled_indices))
                    indices = np.hstack([sampled_indices, uni_indices])
            else:
                indices = self._sample_indices(batch_size)

        else:
            if (
                self._prioritize_vae_samples and
                self._vae_sample_probs is not None and
                self.skew
            ):

                if not self.replacement and np.count_nonzero(self._vae_sample_probs) < batch_size:
                    probs = self._vae_sample_probs + 1e-2
                    probs /= probs.sum()
                else:
                    probs = self._vae_sample_probs

                indices = np.random.choice(
                    len(self._vae_sample_probs),
                    batch_size,
                    # p=self._vae_sample_probs,
                    p=probs,
                    replace=self.replacement or batch_size > len(self._vae_sample_probs),
                )

                assert (
                    np.max(self._vae_sample_probs) <= 1 and
                    np.min(self._vae_sample_probs) >= 0
                )
            else:
                indices = self._sample_indices(batch_size)

        return indices

    def _sample_goals_from_env(self, batch_size):
        self.env.goal_sampling_mode = self._relabeling_goal_sampling_mode
        return self.env.sample_goals(batch_size)

    def sample_buffer_goals(self, batch_size):
        """
        Samples goals from weighted replay buffer for relabeling or exploration.
        Returns None if replay buffer is empty.

        Example of what might be returned:
        dict(
            image_desired_goals: image_achieved_goals[weighted_indices],
            latent_desired_goals: latent_desired_goals[weighted_indices],
        )
        """
        if self._size == 0:
            return None
        weighted_idxs = self.sample_weighted_indices(
            batch_size,
        )
        next_image_obs = normalize_image(
            self._next_obs[self.decoded_obs_key][weighted_idxs]
        )
        next_latent_obs = self._next_obs[self.achieved_goal_key][weighted_idxs]

        next_expert_knowledge_state = self._next_obs["expert_knowledge_state_achieved_goal"][weighted_idxs]
        next_pretrained_features = self._next_obs["pretrained_features_observation"][weighted_idxs]

        # observation is the same as achieved goal
        sampled_rooms = [self.env.vector_to_state_dict(g)["current_room"] for g in self._next_obs["expert_knowledge_state_achieved_goal"][:self._size][weighted_idxs]]
        self.goals_sampled_rooms += Counter(sampled_rooms)

        sampled_TV_states = [
            self.env.state_dict_is_TV_ON(self.env.vector_to_state_dict(g)) \
            for g in self._next_obs["expert_knowledge_state_achieved_goal"][:self._size][weighted_idxs] \
            if self.env.vector_to_state_dict(g)["current_room"] == self.env.Room.TV
        ]
        self.goals_sampled_TV_states += Counter(sampled_TV_states)

        goals_dicts = [self.env.vector_to_state_dict(g) for g in self._next_obs["expert_knowledge_state_achieved_goal"][:self._size][weighted_idxs]]

        goal_visible_entities = [self.env.goal_dict_to_ent_names(g_d) for g_d in goals_dicts]

        sampled_entities = list(chain(*goal_visible_entities))
        self.goals_sampled_entities += Counter(sampled_entities)

        return {
            self.decoded_desired_goal_key:  next_image_obs,
            self.desired_goal_key:          next_latent_obs,
            "expert_knowledge_state_desired_goal": next_expert_knowledge_state,
            "pretrained_features_desired_goal": next_pretrained_features,
        }

    def clustering_vae_training_data(self, batch_size, epoch):
        idxs = np.random.randint(0, self._size, batch_size)
        # todo: balanced batches?

        next_image_obs = normalize_image(
            self._next_obs[self.decoded_obs_key][idxs]
        )

        return dict(
            next_obs=ptu.from_numpy(next_image_obs)
        )

    def random_vae_training_data(self, batch_size, epoch):
        # epoch no longer needed. Using self.skew in sample_weighted_indices
        # instead.
        weighted_idxs = self.sample_weighted_indices(batch_size)

        next_image_obs = normalize_image(
            self._next_obs[self.decoded_obs_key][weighted_idxs]
        )

        sampled_rooms = [self.env.vector_to_state_dict(g)["current_room"] for g in self._next_obs["expert_knowledge_state_achieved_goal"][:self._size][weighted_idxs]]
        self.vae_training_sampled_rooms += Counter(sampled_rooms)

        sampled_TV_states = [
            self.env.state_dict_is_TV_ON(self.env.vector_to_state_dict(g)) \
            for g in self._next_obs["expert_knowledge_state_achieved_goal"][:self._size][weighted_idxs] \
            if self.env.vector_to_state_dict(g)["current_room"] == self.env.Room.TV
        ]
        self.vae_training_sampled_TV_states += Counter(sampled_TV_states)

        return dict(
            next_obs=ptu.from_numpy(next_image_obs)
        )

    def vae_prob(self, next_vae_obs, indices, **kwargs):
        return compute_p_x_np_to_np(
            self.vae,
            next_vae_obs,
            power=self.power,
            **kwargs
        )

    def update_counts(self, path):
        for next_obs in path["next_observations"]:
            image = next_obs[self.decoded_achieved_goal_key]
            q_img = self.quantize(image)
            self.state_counts[q_img.tobytes()] += 1

    def quantize(self, img):
        imsize = int(np.sqrt(img.shape[-1]//3))
        img = img.reshape(3, imsize, imsize).transpose()
        img = cv2.resize(img, dsize=(self.quantize_size, self.quantize_size), interpolation=cv2.INTER_AREA)

        delta = 1.0 / self.quantize_n_colors

        # quantize colors
        img *= 0.9999  # to make sure 1.0 does not appear and have a specific value
        img = np.floor(img / delta) * delta

        assert len(set(img.reshape(-1))) <= self.quantize_n_colors

        return img

    def state_visitation(self, next_vae_obs, indices):
        # quantize
        quantized_imgs = [self.quantize(img) for img in next_vae_obs]
        counts = np.array([self.state_counts[q_img.tobytes()] for q_img in quantized_imgs])
        return counts

    def no_reward(self, next_vae_obs, indices):
        return np.zeros((len(next_vae_obs), 1))

    def _get_sorted_idx_and_train_weights(self):
        idx_and_weights = zip(range(len(self._vae_sample_probs)),
                              self._vae_sample_probs)
        return sorted(idx_and_weights, key=lambda x: x[1])
