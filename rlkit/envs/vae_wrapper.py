import copy
from rlkit.core import logger
from pathlib import Path
import random
import warnings

import torch

from collections import defaultdict
import cv2
import numpy as np
from gym.spaces import Box, Dict
import rlkit.torch.pytorch_util as ptu
from multiworld.core.multitask_env import MultitaskEnv
from multiworld.envs.env_util import get_stat_in_paths, create_stats_ordered_dict
from rlkit.envs.wrappers import ProxyEnv
from sklearn.neighbors import NearestNeighbors

def dff(): return defaultdict(float)
def dfl(): return defaultdict(list)

class VAEWrappedEnv(ProxyEnv, MultitaskEnv):
    """This class wraps an image-based environment with a VAE.
    Assumes you get flattened (channels,84,84) observations from wrapped_env.
    This class adheres to the "Silent Multitask Env" semantics: on reset,
    it resamples a goal.
    """
    def __init__(
        self,
        wrapped_env,
        vae,
        vae_input_key_prefix='image',
        sample_from_true_prior=False,
        decode_goals=False,
        render_goals=False,
        render_rollouts=False,
        reward_params=None,
        goal_sampling_mode="vae_prior",
        imsize=84,
        obs_size=None,
        norm_order=2,
        epsilon=20,
        presampled_goals=None,
    ):
        if reward_params is None:
            reward_params = dict()
        super().__init__(wrapped_env)
        self.vae = vae
        self.representation_size = self.vae.representation_size
        self.input_channels = self.vae.input_channels
        self.sample_from_true_prior = sample_from_true_prior
        self.eval_performance_history = []
        self.eval_perf_history_sizes = [0]
        self.eval_performance_history_expert = []
        self.eval_cluster_perf_steps_dicts_history = []
        self.eval_expert_cluster_perf_dict = defaultdict(dfl)
        self.eval_expert_cluster_perf_steps_dicts = []
        self._decode_goals = decode_goals
        self.render_goals = render_goals
        self.render_rollouts = render_rollouts
        self.default_kwargs=dict(
            decode_goals=decode_goals,
            render_goals=render_goals,
            render_rollouts=render_rollouts,
        )
        self.imsize = imsize
        self.epoch = -1
        self.reward_params = reward_params
        self.reward_type = self.reward_params.get("type", 'latent_distance')
        self.norm_order = self.reward_params.get("norm_order", norm_order)
        self.epsilon = self.reward_params.get("epsilon", epsilon)
        self.reward_min_variance = self.reward_params.get("min_variance", 0)
        latent_space = Box(
            -10 * np.ones(obs_size or self.representation_size),
            10 * np.ones(obs_size or self.representation_size),
            dtype=np.float32,
        )
        spaces = self.wrapped_env.observation_space.spaces
        expert_knowledge_space = copy.deepcopy(spaces['state_achieved_goal'])
        spaces['observation'] = latent_space
        spaces['desired_goal'] = latent_space
        spaces['achieved_goal'] = latent_space
        spaces['latent_observation'] = latent_space
        spaces['latent_desired_goal'] = latent_space
        spaces['latent_achieved_goal'] = latent_space
        spaces["expert_knowledge_state_observation"] = expert_knowledge_space
        spaces["expert_knowledge_state_desired_goal"] = expert_knowledge_space
        spaces["expert_knowledge_state_achieved_goal"] = expert_knowledge_space
        self.observation_space = Dict(spaces)
        self._presampled_goals = presampled_goals
        if self._presampled_goals is None:
            self.num_goals_presampled = 0
        else:
            self.num_goals_presampled = presampled_goals[random.choice(list(presampled_goals))].shape[0]

        self.vae_input_key_prefix = vae_input_key_prefix
        assert vae_input_key_prefix in {'image', 'image_proprio'}
        self.vae_input_observation_key = vae_input_key_prefix + '_observation'
        self.vae_input_achieved_goal_key = vae_input_key_prefix + '_achieved_goal'
        self.vae_input_desired_goal_key = vae_input_key_prefix + '_desired_goal'
        self._mode_map = {}
        self.desired_goal = {'latent_desired_goal': latent_space.sample()}
        self._initial_obs = None
        self._custom_goal_sampler = None
        self._goal_sampling_mode = goal_sampling_mode

    def reset(self):
        obs = self.wrapped_env.reset()
        goal = self.sample_goal()

        # if self._goal_sampling_mode == "static_eval":
        #     imsize = 48
        #     last_image = goal['image_desired_goal']
        #     img = last_image.reshape(3, imsize, imsize).transpose()
        #     img = img[::, :, ::-1]
        #     cv2.imshow('img', img)
        #     cv2.waitKey(100)

        self.set_goal(goal)
        self._initial_obs = obs
        return self._update_obs(obs)

    def step(self, action):
        obs, reward, done, info = self.wrapped_env.step(action)
        new_obs = self._update_obs(obs)
        self._update_info(info, new_obs)
        reward = self.compute_reward(
            action,
            {'latent_achieved_goal': new_obs['latent_achieved_goal'],
             'latent_desired_goal': new_obs['latent_desired_goal']}
        )
        self.try_render(new_obs)
        return new_obs, reward, done, info

    def _update_obs(self, obs):
        # save expert_knowledge for visualizations and similar things
        obs["expert_knowledge_state_observation"] = copy.deepcopy(obs["state_observation"])
        obs["expert_knowledge_state_achieved_goal"] = copy.deepcopy(obs["state_achieved_goal"])

        # this is needed because of "vae_prior" phase where data is collected
        obs["expert_knowledge_state_desired_goal"] = copy.deepcopy(obs["state_desired_goal"])

        # this is needed in case we want to cluster on different imsize
        # obs['image_observation_for_pretrained_features'] = copy.deepcopy(obs['image_observation_for_pretrained_features'])

        latent_obs = self._encode_one(obs[self.vae_input_observation_key])
        obs['latent_observation'] = latent_obs
        obs['latent_achieved_goal'] = latent_obs
        obs['observation'] = latent_obs
        obs['achieved_goal'] = latent_obs

        # only used for asserting
        # obs['real_image_desired_goal'] = copy.deepcopy(obs['image_desired_goal'])

        # if self._goal_sampling_mode != "vae_prior":
        #     # the goal from obs is the correct one during static_eval
        #     # it is the wrong one during custom_goal_sampler -> replay_buffer
        #
        #     # expert_knowledge is filled in custom_goal_sampler ?
        #     # and it should be filled in static_eval ?
        #     if self._goal_sampling_mode == "custom_goal_sampler":
        #
        #     # self.show_image(obs['image_desired_goal'], "{}_obs_wrapped_goal".format(self._goal_sampling_mode))
        #     # self.show_image(self.desired_goal['image_desired_goal'], "{}_obs_desired_goal".format(self._goal_sampling_mode))

        assert self._goal_sampling_mode in ["vae_prior", "static_eval", "custom_goal_sampler"]

        if self._goal_sampling_mode is not "vae_prior":
            assert "expert_knowledge_state_desired_goal" in self.desired_goal
            assert "image_desired_goal" in self.desired_goal
            assert "latent_desired_goal" in self.desired_goal

        # keys from obs are overwritten by those from desired_goal
        obs = {**obs, **self.desired_goal}
        return obs

    def _update_info(self, info, obs):
        latent_distribution_params = self.vae.encode(
            ptu.from_numpy(obs[self.vae_input_observation_key].reshape(1,-1))
        )
        latent_obs, logvar = ptu.get_numpy(latent_distribution_params[0])[0], ptu.get_numpy(latent_distribution_params[1])[0]
        # assert (latent_obs == obs['latent_observation']).all()
        latent_goal = self.desired_goal['latent_desired_goal']
        dist = latent_goal - latent_obs
        var = np.exp(logvar.flatten())
        var = np.maximum(var, self.reward_min_variance)
        err = dist * dist / 2 / var
        mdist = np.sum(err)  # mahalanobis distance
        info["vae_image_obs"] = obs[self.vae_input_observation_key].mean()
        info["vae_image_goal"] = obs[self.vae_input_desired_goal_key].mean()
        info["vae_latent_goal"] = latent_goal.mean()
        info["vae_latent_obs"] = latent_obs.mean()
        info["vae_mdist"] = mdist
        info["vae_success"] = 1 if mdist < self.epsilon else 0
        info["vae_dist"] = np.linalg.norm(dist, ord=self.norm_order)
        info["vae_dist_l1"] = np.linalg.norm(dist, ord=1)
        info["vae_dist_l2"] = np.linalg.norm(dist, ord=2)

    """
    Multitask functions
    """
    def sample_goals(self, batch_size):
        # TODO: make mode a parameter you pass in
        if self._goal_sampling_mode == 'custom_goal_sampler':
            return self.custom_goal_sampler(batch_size)
        elif self._goal_sampling_mode == 'presampled':
            idx = np.random.randint(0, self.num_goals_presampled, batch_size)
            sampled_goals = {
                k: v[idx] for k, v in self._presampled_goals.items()
            }
            # ensures goals are encoded using latest vae
            if 'image_desired_goal' in sampled_goals:
                sampled_goals['latent_desired_goal'] = self._encode(sampled_goals['image_desired_goal'])
            return sampled_goals
        elif self._goal_sampling_mode == "static_eval":
            assert batch_size == 1
            goals = self._wrapped_env.get_next_static_eval_goals(batch_size)
            latent_goals = self._encode(
                goals[self.vae_input_desired_goal_key]
            )
            return {
                "image_desired_goal":  goals['image_desired_goal'],
                "latent_desired_goal": latent_goals,
                "expert_knowledge_state_desired_goal": np.array(goals['state_desired_goal']),
                "state_desired_goal": np.array(goals['state_desired_goal']),
                "pretrained_features_desired_goal": goals["pretrained_features_desired_goal"],
            }

        elif self._goal_sampling_mode == 'env':
            goals = self.wrapped_env.sample_goals(batch_size)
            latent_goals = self._encode(goals[self.vae_input_desired_goal_key])
        elif self._goal_sampling_mode == 'reset_of_env':
            assert batch_size == 1
            goal = self.wrapped_env.get_goal()
            goals = {k: v[None] for k, v in goal.items()}
            latent_goals = self._encode(
                goals[self.vae_input_desired_goal_key]
            )
        elif self._goal_sampling_mode == 'vae_prior':
            goals = {}
            latent_goals = self._sample_vae_prior(batch_size)
        else:
            raise RuntimeError("Invalid: {}".format(self._goal_sampling_mode))

        if self._decode_goals:
            decoded_goals = self._decode(latent_goals)
        else:
            decoded_goals = None
        image_goals, proprio_goals = self._image_and_proprio_from_decoded(
            decoded_goals
        )

        goals['desired_goal'] = latent_goals
        goals['latent_desired_goal'] = latent_goals
        if proprio_goals is not None:
            goals['proprio_desired_goal'] = proprio_goals
        if image_goals is not None:
            goals['image_desired_goal'] = image_goals
        if decoded_goals is not None:
            goals[self.vae_input_desired_goal_key] = decoded_goals
        return goals

    def get_goal(self):
        return self.desired_goal

    def compute_reward(self, action, obs):
        if type(action) is int:
            actions = [action]
            raise DeprecationWarning("this should not happen")
        else:
            actions = action[None]

        next_obs = {
            k: v[None] for k, v in obs.items()
        }
        return self.compute_rewards(actions, next_obs)[0]

    def compute_rewards(self, actions, obs):
        # TODO: implement log_prob/mdist
        if self.reward_type == 'latent_distance':
            achieved_goals = obs['latent_achieved_goal']
            desired_goals = obs['latent_desired_goal']
            dist = np.linalg.norm(desired_goals - achieved_goals, ord=self.norm_order, axis=1)
            return -dist
        elif self.reward_type == 'vectorized_latent_distance':
            assert False
            achieved_goals = obs['latent_achieved_goal']
            desired_goals = obs['latent_desired_goal']
            return -np.abs(desired_goals - achieved_goals)
        elif self.reward_type == 'latent_sparse':
            assert False
            achieved_goals = obs['latent_achieved_goal']
            desired_goals = obs['latent_desired_goal']
            dist = np.linalg.norm(desired_goals - achieved_goals, ord=self.norm_order, axis=1)
            reward = 0 if dist < self.epsilon else -1
            return reward
        elif self.reward_type == 'state_distance':
            assert False
            achieved_goals = obs['state_achieved_goal']
            desired_goals = obs['state_desired_goal']
            return - np.linalg.norm(desired_goals - achieved_goals, ord=self.norm_order, axis=1)
        elif self.reward_type == 'wrapped_env':
            assert False
            return self.wrapped_env.compute_rewards(actions, obs)
        else:
            raise NotImplementedError

    @property
    def goal_dim(self):
        return self.representation_size

    def set_goal(self, goal):
        """
        Assume goal contains both image_desired_goal and any goals required for wrapped envs

        :param goal:
        :return:
        """
        self.desired_goal = goal
        # TODO: fix this hack / document this
        if self._goal_sampling_mode in {'presampled', 'env', 'static_eval'}:
            self.wrapped_env.set_goal(goal)

    def calculate_NN_LP(
            self,
            goals_lat,
            clusters,
            perfs,
            k=1,
            history_size=None,
    ):
        raise DeprecationWarning("This method was ot used in the paper")
        assert len(clusters) == len(perfs) == len(goals_lat)

        if history_size is not None:
            assert history_size > 1
            perfs = perfs[-history_size:]
            clusters = clusters[-history_size:]
            goals_lat = goals_lat[-history_size:]

        l = len(clusters) // 2
        old_goals = goals_lat[:l]
        old_perfs = perfs[:l]

        new_goals = goals_lat[l:]
        new_perfs = perfs[l:]

        new_clusters = clusters[l:]

        k = min(k, l)

        old_nbrs = NearestNeighbors(n_neighbors=k, algorithm='ball_tree').fit(old_goals)
        old_indices = old_nbrs.kneighbors(new_goals, return_distance=False)
        nn_old_perfs = old_perfs[old_indices].mean(axis=1)

        new_nbrs = NearestNeighbors(n_neighbors=k, algorithm='ball_tree').fit(new_goals)
        new_indices = new_nbrs.kneighbors(new_goals, return_distance=False)
        nn_new_perfs = new_perfs[new_indices].mean(axis=1)

        lps = abs(nn_new_perfs - nn_old_perfs)

        available_clusters = np.unique(new_clusters)
        cluster_lp_dict = defaultdict(float)
        for cl in available_clusters:
            cl_indices = np.where(new_clusters == cl)[0]
            cluster_lp_dict[cl] = lps[cl_indices].mean()

        return cluster_lp_dict

    def calculate_cluster_perfs(self, performance_history, clusters, perf_history_sizes):
        """
        This function calculates per step performances for each cluster given a history, cluster_ids, and history sizes

        Primarily used for refreshing the performance estimates for each attempted goal,
        that is needed when the reward function or the goal space is trained

        It is also used for evaluation and tracking metrics per room then clusters are room ids.

        :param performance_history: a history of {goal: ... , last_state: ...} dicts
        :param clusters: a list of integers indicating the cluster id for each rollout
        :param perf_history_sizes:
            -   integers indicating how many goals were sampled each epoch
            -   used to separate the history to steps
            example: [0, 10, 10, 10, 10]

        :returns
            - cluster_perf_steps_dicts : a nested structure with the following format
                cluster_perf_steps_dicts - a list where index is the epoch, element of the list is a dict
                                         - every epoch the VAE is trained and hece this has to refreshed
                                         - depending on the hyperparmaeters but one epoch is usualy 10 rollouts
                cluster_perf_steps_dicts[t] - a dict with:
                                                 - key: cluster_id
                                                 - value: list of performances
                cluster_perf_step_dicts[t][cluster_id] - a list of performances


                example:

                epoch = 100
                available_clusters = {1, 2, 3, 4]

                cluster_perf_step_dicts[epoch] = {
                    1: [0.1, 0.5, 0.4]
                    2: [0.4, 0.1]
                    3: [0.8, 0.8, 0.9, 0.84]
                    4: [0.4]
                }


            - cluster_perf_dict : a dict containing per cluster per step average performances
                example:
                # there were three epochs where at least one goal was sampled from the cluster_id cluster
                clsuter_perf_dict[cluster_id] = [0.4, 0.1, 0.7]
                cluster_perf_dict[cluster_id][t] - the average of the rollouts
                                                 - np.mean(cluster_perf_step_dicts[t][cluster_id])

            - cluster_var_dict : a dict containing per cluster per step performance variances
                example:
                # there were three epochs where at least one goal was sampled from the cluster_id cluster
                clsuter_perf_dict[cluster_id] = [0.04, 0.05, 0.2]
                cluster_perf_dict[cluster_id][t] - the varianve of the rollouts
                                                 - np.var(cluster_perf_step_dicts[t][cluster_id])
            - goals_lat : np.array of new latent representation of goals
            - last_states_lat : np.array of new last_state representatins
            - vae_perfs : np.array of recalculated performaces
        """
        goals = [h['goal'] for h in performance_history]
        last_states = [h['last_state'] for h in performance_history]

        # recalculate distances using the new VAE
        assert self.reward_type == "latent_distance"
        goals = np.array(goals)
        last_states = np.array(last_states)
        goals_lat = self._encode(goals)
        last_states_lat = self._encode(last_states)
        vae_perfs = np.linalg.norm(goals_lat - last_states_lat, ord=self.norm_order, axis=1)

        # VAE DISTANCES
        cluster_perf_steps_dicts = []
        per_step_performances = np.split(list(zip(clusters, vae_perfs)), perf_history_sizes[1:])
        for i, (step_cs_ps) in enumerate(per_step_performances):
            # k: cluster_id p: performances from this step
            step_cluster_perf_dict = defaultdict(list)
            for c, p in step_cs_ps:
                step_cluster_perf_dict[c].append(p)

            # whole performance history for the current vae
            cluster_perf_steps_dicts.append(step_cluster_perf_dict)

        cluster_perf_dict = defaultdict(list)
        cluster_var_dict = defaultdict(list)
        for step_perf_dict in cluster_perf_steps_dicts:
            for c, p in step_perf_dict.items():
                assert type(p) == list
                cluster_perf_dict[c].append(np.mean(p))
                cluster_var_dict[c].append(np.var(p))

        # clp=defaultdict(list)
        # for i, (step_cs_ps) in enumerate(per_step_performances):
        #     cs, ps = zip(*step_cs_ps)
        #     for cl in np.unique(cs):
        #         clp[cl].append(np.array(ps)[np.where(cs==cl)[0]].mean())
        # assert clp == cluster_perf_dict

        return cluster_perf_steps_dicts.copy(), cluster_perf_dict.copy(), cluster_var_dict.copy(), goals_lat, last_states_lat, vae_perfs

    def calculate_expert_performances(self, performance_history_expert, clusters, perf_history_sizes):
        """
        reorganizes the performance history in terms of clusters, without averaging
        :param performance_history_expert: a list of dicts containing the history of rollout performances for each metric

            example:
                [h['hand_distance'] for h in performance_history_expert] - history of "hand_distance" containing all the goal ever sampled (list)

        :param clusters: a list of integers indicating the cluster id for each rollout
        :param perf_history_sizes:
            -   integers indicating how many goals were sampled each epoch
            -   used to separate the history to steps
            example: [0, 10, 10, 10, 10]

        :return
            current_cluster_expert_perf_dict["hand_distance"] = {
                2 : [0.4, 0.1, 0.6],  # all goals sampled from cluster 2 in the last epoch
                3 : [....]
                 ....
            }

            current_cluster_expert_perf_dict["hand_distance"][2][4] = [0.4, 0.1, 0.6]
            In epoch=4 there were there were three goals sacmpled from cluster 2.
            The hand_distance for these three goal in the last state were 0.4, 0.1 and 0.6


        """
        # we only take the performances of the last epoch
        num_of_goals_sampled_before_the_current_epoch = perf_history_sizes[-1]

        current_cluster_expert_perf_dict = defaultdict(dfl)

        for c, h in list(zip(clusters, performance_history_expert))[num_of_goals_sampled_before_the_current_epoch:]:

            for metric, value in h.items():
                if type(value) == self.wrapped_env.wrapped_env.Room:
                    # this metrics will be averaged so Room objects do not make sense
                    assert metric == "goal_room"
                    continue

                current_cluster_expert_perf_dict[metric][c].append(value)

        # k: metric v: (k:cluster: v: [p1,p2,p3] )
        # self.expert_cluster_perf_steps_dicts.append(current_cluster_expert_perf_dict.copy())

        # if self.wrapped_env.wrapped_env.__class__.__name__ == "PlayGroundRGB":
        #     # testing
        #     hand_distances = [h["hand_distance"] for h in performance_history_expert]
        #     water_distances = [h["water_distance"] for h in performance_history_expert]
        #     hand_and_water_distances = [h["hand_and_water_distance"] for h in performance_history_expert]
        #     hand_and_water_and_dog_distances = [h["hand_and_water_and_dog_distance"] for h in performance_history_expert]
        #     rooms_correct = [h["room_correct"] for h in performance_history_expert]
        #
        #     # we only take the performances of the last epoch
        #     num_of_goals_sampled_before_the_current_epoch = perf_history_sizes[-1]
        #
        #     current_cluster_expert_perf_dict_ = defaultdict(dfl)
        #
        #     for c, h_d, w_d, h_w_d, h_w_d_d, r_c in list(zip(
        #             clusters, hand_distances, water_distances, hand_and_water_distances, hand_and_water_and_dog_distances, rooms_correct
        #     ))[num_of_goals_sampled_before_the_current_epoch:]:
        #         current_cluster_expert_perf_dict_["hand_distance"][c].append(h_d)
        #         current_cluster_expert_perf_dict_["water_distance"][c].append(w_d)
        #         current_cluster_expert_perf_dict_["hand_and_water_distance"][c].append(h_w_d)
        #         current_cluster_expert_perf_dict_["hand_and_water_and_dog_distance"][c].append(h_w_d_d)
        #         current_cluster_expert_perf_dict_["room_correct"][c].append(r_c)
        #
        #     for k in current_cluster_expert_perf_dict_.keys():
        #         assert current_cluster_expert_perf_dict_[k] == current_cluster_expert_perf_dict[k]


        return current_cluster_expert_perf_dict.copy()

    def add_expert_performances_to_perf_dict(self, expert_cluster_perf_dict, current_cluster_expert_perf_dict):
        """
        Averages and add new averaperformances to full history.
        :param expert_cluster_perf_dict:
            {
            "hand_distance": {  # hand_distance metric
                "1": [           # cluster_id = 1
                    0.2,            # average performance at the first epoch that a goal from cluster 1 was sampled in
                    0.6,
                    0.2
                ],
                "2" : [          # cluster_id = 2
                    ...
                ]
            },
            "water_distance": ...
            }
        :param current_cluster_expert_perf_dict:
            output of self.calculate_expert_performances function
        :return: None
        """
        for metric, cluster_dict in current_cluster_expert_perf_dict.items():
            for c, perfs in cluster_dict.items():
                # k: metric v: { k:cluster v: [ mean(p_step1), mean(p_step2), ... ] }
                expert_cluster_perf_dict[metric][c].append(np.mean(perfs))

    def extract_metrics_from_path(
            self,
            path,
            decoded_achieved_goal_key='image_achieved_goal',
            decoded_desired_goal_key='image_desired_goal'
    ):
        """
        This solution is not ideal.
        In ProxyEnv (parent of this class) the __getattr__ method is overriden so as to give this class access
        to all the attributes from self.wrapped_env
        i.e. self.wrapped.env.func is equal to self.func

        (It was like this in the original implementation of skewfit so i don't think we should change it.
        But a cleaner solution would probably not include this __getattr__ override)

        In short: self sees all from self.wrapped env (as if it were in self),
         but self.wrapped_env doesn't see anything from self


        The problem is that extract_metrics_from_path both:
            - needs to be in the wrapped env class because of different metrics that are extracted in different envs (ex. hand distance)
            - requires the goal_sampling_mode attribute that is in this class and NOT in the self.wrapped_env class


        Therefore, the workaround is to override the method here and give it the goal_sampling_mode
        """
        return self.wrapped_env.extract_metrics_from_path(
            path,
            goal_sampling_mode=self._goal_sampling_mode,
            decoded_achieved_goal_key=decoded_achieved_goal_key,
            decoded_desired_goal_key=decoded_desired_goal_key,
        )

    def get_diagnostics(self, paths, **kwargs):
        statistics = self.wrapped_env.get_diagnostics(paths, **kwargs)
        if self._goal_sampling_mode != "custom_goal_sampler":
            # this is during training
            track_eval_lp = len(self.goal_generation_rooms) > 1
            assert self._goal_sampling_mode in ["reset_of_env", "static_eval"]
            self.epoch += 1
            for path in paths:
                perf_metrics, expert_perf_metrics, metadata = self.extract_metrics_from_path(
                    path,
                    decoded_achieved_goal_key='image_achieved_goal',
                    decoded_desired_goal_key='image_desired_goal'
                )
                # not sure if deepcopy is needed here but it cant hurt
                self.eval_performance_history.append(copy.deepcopy(perf_metrics))
                self.eval_performance_history_expert.append(copy.deepcopy(expert_perf_metrics))

            # goal_rooms, *_ = zip(
            #     *self.eval_performance_history_expert
            # )
            goal_rooms = [h['goal_room'] for h in self.eval_performance_history_expert]

            # cluster
            room_ids = [r.value for r in goal_rooms]

            if track_eval_lp:
                # we calculate LP and performace estimates for each room using VAE distances
                cluster_perf_steps_dicts_, cluster_perf_dict_, cluster_vae_dict_, goals_lat, last_states_lat, _ = self.calculate_cluster_perfs(
                    self.eval_performance_history, room_ids, self.eval_perf_history_sizes)
                self.eval_cluster_perf_steps_dicts_history.append(cluster_perf_steps_dicts_)
                self.eval_cluster_perf_dict = cluster_perf_dict_
                self.eval_cluster_vae_dict = cluster_vae_dict_

                # calculate lp based on current VAE distances
                self.eval_cluster_lp_dict = self.calculate_lp_for_perf_dict(self.eval_cluster_perf_dict)

                # vae perf and LP
                statistics.update({"eval_perf_vae_" + str(r): self.eval_cluster_perf_dict.get(r.value, [0.0])[-1] for r in self.available_rooms})
                statistics.update({"eval_lp_vae_" + str(r): self.eval_cluster_lp_dict.get(r.value, 0.0) for r in self.available_rooms})

            # we calculate performace estimates for each room using expert distances
            current_cluster_expert_perf_dict_ = self.calculate_expert_performances(
                self.eval_performance_history_expert,
                room_ids,
                self.eval_perf_history_sizes
            )

            self.add_expert_performances_to_perf_dict(
                self.eval_expert_cluster_perf_dict,
                current_cluster_expert_perf_dict_
            )

            self.eval_expert_cluster_perf_steps_dicts.append(current_cluster_expert_perf_dict_)

            # EXPERT LP
            self.eval_expert_cluster_lp_dict = defaultdict(dff)
            for metric, m_dict in self.eval_expert_cluster_perf_dict.items():
                self.eval_expert_cluster_lp_dict[metric] = self.calculate_lp_for_perf_dict(m_dict)

            metric_statistics = self.wrapped_env.create_metrics_statistics_dict(
                eval_expert_cluster_perf_dict=self.eval_expert_cluster_perf_dict,
                eval_expert_cluster_lp_dict=self.eval_expert_cluster_lp_dict,
            )
            statistics.update(metric_statistics)

            if self.epoch % 10 == 0 and track_eval_lp:
                perf_dict_path = Path(logger.get_snapshot_dir()) / ("eval_cluster_perf_dict_" + str(self.epoch) + ".npy")
                np.save(str(perf_dict_path), dict(self.eval_cluster_perf_dict))
                perf_dict_path = Path(logger.get_snapshot_dir()) / ("eval_expert_cluster_perf_dict_" + str(self.epoch) + ".npy")
                np.save(str(perf_dict_path), dict(self.eval_expert_cluster_perf_dict))
                lp_dict_path = Path(logger.get_snapshot_dir()) / ("eval_cluster_lp_dict_" + str(self.epoch) + ".npy")
                np.save(str(lp_dict_path), dict(self.eval_cluster_lp_dict))
                lp_dict_path = Path(logger.get_snapshot_dir()) / ("eval_expert_cluster_lp_dict_" + str(self.epoch) + ".npy")
                np.save(str(lp_dict_path), dict(self.eval_expert_cluster_lp_dict))
                lats_path = Path(logger.get_snapshot_dir()) / ("eval_vae_lats_" + str(self.epoch) + ".npy")
                np.save(str(lats_path), {"goals_lat": goals_lat, "last_states_lat": last_states_lat, "clusters": room_ids})

            self.eval_perf_history_sizes.append(len(self.eval_performance_history))

        for stat_name_in_paths in ["vae_mdist", "vae_success", "vae_dist"]:
            stats = get_stat_in_paths(paths, 'env_infos', stat_name_in_paths)
            statistics.update(create_stats_ordered_dict(
                stat_name_in_paths,
                stats,
                always_show_all_stats=True,
            ))
            final_stats = [s[-1] for s in stats]
            statistics.update(create_stats_ordered_dict(
                "Final " + stat_name_in_paths,
                final_stats,
                always_show_all_stats=True,
            ))
        return statistics

    def calculate_lp_for_perf_dict(
            self,
            perf_dict,
            history_size=None,
            clamp_by_initial=False,
            normalize_by_mean_of_vars=False,
            vars_dict=None
    ):
        """

        :param perf_dict: dictionary of per cluster perfromances
            {
                cluster_id: [p1,p2,p3...pn],
                ...
            }

        :param history_size: hyperparameter
        :param clamp_by_initial: hyperparameter
                - if True the performances will be clamped by the first performance
                - usefull for handling learning when performance is worse at the beginning
        :param normalize_by_mean_of_vars: hypermarameter
        :param vars_dict: vars to normalize by, needed if normalize_by_mean_of_vars is True
        :return: a dictionary {
            {
                cluster_id: lp_value,
                ...
            }
        """
        assert type(perf_dict) in [dict, defaultdict]
        # perf_dict {k: cluster v: [p1,p2,p3...pn]}
        # lp_dict {k: lp}

        lp_dict = defaultdict(float)

        for c, perfs in perf_dict.items():
            if clamp_by_initial:
                perfs = np.clip(perfs, -np.inf, perfs[0])

            if history_size is not None:
                assert history_size > 1
                perfs = perfs[-history_size:]

            if normalize_by_mean_of_vars:
                vars = vars_dict[c]

                if history_size is not None:
                    vars = vars[-history_size:]

                perfs /= np.mean(vars)

            half = max(len(perfs) // 2, 1)
            lp_dict[c] = np.abs(np.mean(perfs[:half]) - np.mean(perfs[-half:]))

        return lp_dict.copy()

    """
    Other functions
    """
    @property
    def goal_sampling_mode(self):
        return self._goal_sampling_mode

    @goal_sampling_mode.setter
    def goal_sampling_mode(self, mode):
        assert mode in [
            'custom_goal_sampler',
            # 'presampled',
            'vae_prior',
            # 'env',
            'reset_of_env',
            'static_eval'
        ], "Invalid env mode"
        self._goal_sampling_mode = mode
        if mode == 'custom_goal_sampler':
            test_goals = self.custom_goal_sampler(1)
            if test_goals is None:
                self._goal_sampling_mode = 'vae_prior'
                warnings.warn(
                    "self.goal_sampler returned None. " + \
                    "Defaulting to vae_prior goal sampling mode"
                )

    @property
    def custom_goal_sampler(self):
        return self._custom_goal_sampler

    @custom_goal_sampler.setter
    def custom_goal_sampler(self, new_custom_goal_sampler):
        assert self.custom_goal_sampler is None, (
            "Cannot override custom goal setter"
        )
        self._custom_goal_sampler = new_custom_goal_sampler

    @property
    def decode_goals(self):
        return self._decode_goals

    @decode_goals.setter
    def decode_goals(self, _decode_goals):
        self._decode_goals = _decode_goals

    def get_env_update(self):
        """
        For online-parallel. Gets updates to the environment since the last time
        the env was serialized.

        subprocess_env.update_env(**env.get_env_update())
        """
        return dict(
            mode_map=self._mode_map,
            gpu_info=dict(
                use_gpu=ptu._use_gpu,
                gpu_id=ptu._gpu_id,
            ),
            vae_state=self.vae.__getstate__(),
        )

    def update_env(self, mode_map, vae_state, gpu_info):
        self._mode_map = mode_map
        self.vae.__setstate__(vae_state)
        gpu_id = gpu_info['gpu_id']
        use_gpu = gpu_info['use_gpu']
        ptu.device = torch.device("cuda:" + str(gpu_id) if use_gpu else "cpu")
        self.vae.to(ptu.device)

    def enable_render(self, save_dir):
        self._decode_goals = True
        self.render_goals = True
        if not self.render_rollouts:
            self.rollout_videowriter_w_recs = cv2.VideoWriter(
                str(save_dir / "testing_run_w_recs.mp4"), cv2.VideoWriter_fourcc(*'mp4v'), 60, (48*4+3, 48)
            )
            self.rollout_videowriter = cv2.VideoWriter(
                str(save_dir / "testing_run.mp4"), cv2.VideoWriter_fourcc(*'mp4v'), 60, (48 * 2 + 1, 48)
            )

            self.save_viz_dir = save_dir
            self.viz_ims = []
            self.viz_w_recs_ims = []

        self.render_enabled = True
        self.render_rollouts = True

    def disable_render(self):
        self._decode_goals = False
        self.render_goals = False
        self.render_rollouts = False
        print("RELEASE")
        self.rollout_videowriter_w_recs.release()
        self.rollout_videowriter.release()

        import os
        from moviepy.editor import ImageSequenceClip

        def gif(filename, array, fps=10, scale=1.0):

            # ensure that the file has the .gif extension
            fname, _ = os.path.splitext(filename)
            filename = fname + '.gif'

            # copy into the color dimension if the images are black and white
            if array.ndim == 3:
                array = array[..., np.newaxis] * np.ones(3)

            # make the moviepy clip
            clip = ImageSequenceClip(list(array), fps=fps).resize(scale)
            clip.write_gif(filename, fps=fps)
            return clip

        gif(str(self.save_viz_dir / "run.gif"), np.array(self.viz_ims), fps=10)
        gif(str(self.save_viz_dir / "run_w_recs.gif"), np.array(self.viz_w_recs_ims), fps=10)

    def show_image(self, im, s="testing"):
        img = im.reshape(
            self.input_channels,
            self.imsize,
            self.imsize,
        ).transpose()
        img_RGB = cv2.cvtColor(img.astype(np.float32), cv2.COLOR_BGR2RGB)
        cv2.imshow(s, img_RGB)
        cv2.waitKey(10)

    def try_render(self, obs):
        # cv2.destroyAllWindows()
        if not hasattr(self, "render_enabled") or not self.render_enabled:
            # this is inside the training loop
            if self.render_goals:
                assert False
                assert self.render_rollouts

                # state
                img = obs['image_observation'].reshape(
                    self.input_channels,
                    self.imsize,
                    self.imsize,
                ).transpose()
                img_RGB = cv2.cvtColor(img.astype(np.float32), cv2.COLOR_BGR2RGB)
                cv2.imshow('{}_state'.format(self._goal_sampling_mode), img_RGB)
                cv2.waitKey(10)

                # # state reconstruction
                # reconstruction = self._reconstruct_img(obs['image_observation']).transpose()
                # img_RGB = cv2.cvtColor(reconstruction.astype(np.float32), cv2.COLOR_BGR2RGB)
                # cv2.imshow('{}_state_reconstruction'.format(self._goal_sampling_mode), img_RGB)
                # cv2.waitKey(10)

                # goal
                goal = obs['image_desired_goal'].reshape(
                    self.input_channels,
                    self.imsize,
                    self.imsize
                ).transpose()
                img_RGB = cv2.cvtColor(goal.astype(np.float32), cv2.COLOR_BGR2RGB)
                cv2.imshow('{}_goal'.format(self._goal_sampling_mode), img_RGB)
                cv2.waitKey(10)

                # # goal reconstruction
                # goal_rec = self._reconstruct_img(obs['image_desired_goal']).transpose()
                # img_RGB = cv2.cvtColor(goal_rec.astype(np.float32), cv2.COLOR_BGR2RGB)
                # cv2.imshow('{}_goal_reconstruction'.format(self._goal_sampling_mode), img_RGB)
                # cv2.waitKey(10)

        else:

            if self.render_goals:
                assert self.render_rollouts

                # image
                img = obs['image_observation'].reshape(
                    self.input_channels,
                    self.imsize,
                    self.imsize,
                ).transpose()

                # reconstruction
                reconstruction = self._reconstruct_img(obs['image_observation']).transpose()

                # real goal
                real_goal=obs['image_desired_goal'].reshape(
                    self.input_channels,
                    self.imsize,
                    self.imsize,
                ).transpose()

                # rec goal
                rec_goal = self._reconstruct_img(obs['image_desired_goal']).transpose()


                line = np.ones((48, 1, 3))

                def resize(img):
                    if img.shape[:1] != (48, 48):
                        return cv2.resize(img, (48, 48), interpolation=cv2.INTER_AREA)
                    else:
                        return img

                images = [resize(img), line, resize(reconstruction), line, resize(real_goal), line, resize(rec_goal)]
                joint_image = (np.concatenate(images, axis=1)*255).astype(np.uint8)

                self.viz_w_recs_ims.append(joint_image)

                joint_image = cv2.cvtColor(joint_image, cv2.COLOR_BGR2RGB)
                self.rollout_videowriter_w_recs.write(joint_image)


                images = [resize(img), line, resize(real_goal)]
                joint_image = (np.concatenate(images, axis=1)*255).astype(np.uint8)
                self.viz_ims.append(joint_image)

                joint_image = cv2.cvtColor(joint_image, cv2.COLOR_BGR2RGB)
                self.rollout_videowriter.write(joint_image)

    def _sample_vae_prior(self, batch_size):
        if self.sample_from_true_prior:
            mu, sigma = 0, 1  # sample from prior
        else:
            mu, sigma = self.vae.dist_mu, self.vae.dist_std
        n = np.random.randn(batch_size, self.representation_size)
        return sigma * n + mu

    def _decode(self, latents):
        reconstructions, _ = self.vae.decode(ptu.from_numpy(latents))
        decoded = ptu.get_numpy(reconstructions)
        return decoded

    def _encode_one(self, img):
        return self._encode(img[None])[0]

    def _encode(self, imgs):
        latent_distribution_params = self.vae.encode(ptu.from_numpy(imgs))
        return ptu.get_numpy(latent_distribution_params[0])

    def _reconstruct_img(self, flat_img):
        latent_distribution_params = self.vae.encode(ptu.from_numpy(flat_img.reshape(1, -1)))
        reconstructions, _ = self.vae.decode(latent_distribution_params[0])
        imgs = ptu.get_numpy(reconstructions)
        imgs = imgs.reshape(
            1, self.input_channels, self.imsize, self.imsize
        )
        return imgs[0]

    def _image_and_proprio_from_decoded(self, decoded):
        if decoded is None:
            return None, None
        if self.vae_input_key_prefix == 'image_proprio':
            images = decoded[:, :self.image_length]
            proprio = decoded[:, self.image_length:]
            return images, proprio
        elif self.vae_input_key_prefix == 'image':
            return decoded, None
        else:
            raise AssertionError("Bad prefix for the vae input key.")

    def __getstate__(self):
        state = super().__getstate__()
        state = copy.copy(state)
        state['_custom_goal_sampler'] = None
        warnings.warn('VAEWrapperEnv.custom_goal_sampler is not saved.')
        return state

    def __setstate__(self, state):
        warnings.warn('VAEWrapperEnv.custom_goal_sampler was not loaded.')
        super().__setstate__(state)


def temporary_mode(env, mode, func, args=None, kwargs=None):
    if args is None:
        args = []
    if kwargs is None:
        kwargs = {}
    cur_mode = env.cur_mode
    env.mode(env._mode_map[mode])
    return_val = func(*args, **kwargs)
    env.mode(cur_mode)
    return return_val
