import csv
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid
import numpy as np
from pathlib import Path

import argparse

parser = argparse.ArgumentParser()
# parser.add_argument('--dataset-dir', type=str, default='./observations/MiniWorld-ThreeRoomsTVRicher-v0_H_320_overlay')
parser.add_argument('--model-path', type=str, default="/home/flowers/Documents/projects/grimgep/grimgep3D/grimgep/data/03-30-3D-CountBased-torch-120-single-seed-cl-UNI-pca-n-25-n-clus-25-tv-imagenet-very-small-start-in-TV-pow-1.0-tv-act-True-quant-s-3-n-4/03-30-3D_CountBased_torch_120_single_seed_cl_UNI_pca_n_25_n_clus_25_tv_imagenet_very_small_start_in_TV_pow_1.0_tv_act_True_quant_s_3_n_4_2021_03_30_21_07_18_0000--s-2864/")
parser.add_argument('--step', type=int, default=50)
args = parser.parse_args()

# csv_path="/home/flowers/Documents/projects/grimgep/grimgep3D/grimgep/data/03-30-3D-CountBased-torch-120-single-seed-cl-UNI-pca-n-25-n-clus-25-tv-imagenet-very-small-start-in-TV-pow-1.0-tv-act-True-quant-s-3-n-4/03-30-3D_CountBased_torch_120_single_seed_cl_UNI_pca_n_25_n_clus_25_tv_imagenet_very_small_start_in_TV_pow_1.0_tv_act_True_quant_s_3_n_4_2021_03_30_21_07_18_0000--s-2864/progress.csv"
step = args.step
csv_path = Path(args.model_path) / "progress.csv"



# init figure grid
fig = plt.figure(figsize=(14., 7.))
grid = ImageGrid(fig, 111,  # similar to subplot(111)
                 nrows_ncols=(1, 4),  # creates 2x2 grid of axes
                 axes_pad=0.1,  # pad between axes in inch.
)

# parse csv
with open(csv_path, mode='r') as csv_file:
    csv_reader = csv.DictReader(csv_file)
    line_count = 0
    for row in csv_reader:
        line_count += 1

        if line_count == 0:
            print(f'Column names are {", ".join(row)}')

        else:
            epoch = int(row["Epoch"])
            ent_names_num_of_clus = [k for k in row.keys() if "num_of_cls" in k and ("Room" not in k  and "agent" not in k)]
            labels = [e.replace("_num_of_cls", "").replace("replay_buffer/", "") for e in ent_names_num_of_clus]
            # for k in ent_names_num_of_clus:
            #     print("\t", row[k])

            ids=list(range(len(ent_names_num_of_clus)))
            print(epoch)
            if epoch % step == 0 and epoch != 0:

                ax = grid[(epoch // step) - 1]
                ax.set_title("epoch: {}".format(epoch))
                ax.bar(
                    ids,
                    [float(row[k]) for k in ent_names_num_of_clus],
                    align='center'
                )

                labels = [e.replace("_num_of_cls", "").replace("replay_buffer/", "") for e in ent_names_num_of_clus]
                plt.sca(ax)
                plt.xticks(ids, labels, rotation=90)
                # plt.tight_layout()
                if (epoch // step) - 1 == 3:
                    break


fig.tight_layout()
plt.show()



























