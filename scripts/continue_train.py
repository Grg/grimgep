import argparse
import time
import numpy as np
import pickle
import torch
import json
from pathlib import Path

from rlkit.core import logger
from rlkit.samplers.rollout_functions import multitask_rollout
from rlkit.torch import pytorch_util as ptu
from rlkit.envs.vae_wrapper import VAEWrappedEnv

import gym
from rlkit.util.my_image_env import ImageEnv
import json
import torch
from pathlib import Path
import ctypes
import torch.multiprocessing as mp

from rlkit.torch.data import InfiniteRandomSampler
from torch.utils.data import DataLoader


def reconstruct_image_env_from_variant(variant):
    assert variant.get('presample_goals', False) == False

    env = gym.make(variant["env_id"], **variant.get('base_env_kwargs', {}))

    init_camera = variant.get("init_camera", None)
    image_env = ImageEnv(
        env,
        variant.get('imsize'),
        init_camera=init_camera,
        transpose=True,
        normalize=True,
    )
    return image_env


def reconstruct_algo(exp_dir):
    variant = json.load((Path(exp_dir) / "variant.json").open())

    # can be reconstructed manually
    load_path = str((Path(exp_dir) / "algorithm_dump.pt"))

    print("loading algo from :", load_path)
    algo = torch.load(load_path, map_location=torch.device("cuda"))

    image_env = reconstruct_image_env_from_variant(variant)

    algo.eval_env._wrapped_env = image_env
    algo.expl_env._wrapped_env = image_env
    algo.expl_env.custom_goal_sampler = algo.replay_buffer.sample_buffer_goals

    algo.replay_buffer._shared_size = mp.Value(ctypes.c_long, algo.replay_buffer.shared_size_raw)

    print("replay buffer perf history size", len(algo.replay_buffer.performance_history))

    algo.vae_trainer.test_dataloader = DataLoader(
        algo.vae_trainer.test_dataset_pt,
        sampler=InfiniteRandomSampler(algo.vae_trainer.test_dataset),
        batch_size=algo.vae_trainer.batch_size,
        drop_last=False,
        num_workers=0,
        pin_memory=True,
    )
    algo.vae_trainer.test_dataloader = iter(algo.vae_trainer.test_dataloader)

    return algo


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('--exp-dir', type=str,
                        help='path to the snapshot file')
    args = parser.parse_args()
    print("loading algorithm")
    algo = reconstruct_algo(args.exp_dir)

    print("reconstructing algorithm")
    ptu.set_gpu_mode(True, None)

    logger.set_snapshot_dir(algo.logger_dict['log_dir'])
    logger.set_snapshot_mode(algo.logger_dict['snapshot_mode'])
    logger.set_snapshot_gap(algo.logger_dict['snapshot_gap'])
    logger.set_log_tabular_only(algo.logger_dict['log_tabular_only'])

    default_log_path = str(Path(algo.logger_dict["log_dir"]) / "debug.log")
    logger.add_text_output(algo.logger_dict.get('text_log_path', default_log_path))

    default_tab_log_path = str(Path(algo.logger_dict["log_dir"]) / "progress.csv")

    logger._add_output(
        algo.logger_dict.get('tabular_log_path', default_tab_log_path),
        logger._tabular_outputs,
        logger._tabular_fds, mode='a')

    logger._no_headers = True
    print("continuing training")
    algo._continue_train()
