import argparse
import time
import numpy as np
import pickle
import torch
import json
from pathlib import Path

from rlkit.core import logger
from rlkit.samplers.rollout_functions import multitask_rollout
from rlkit.torch import pytorch_util as ptu
from rlkit.envs.vae_wrapper import VAEWrappedEnv

def simulate_policy(args):
    # data = pickle.load(open(args.file, "rb"))
    data = torch.load(args.file, map_location="cuda" if args.gpu else "cpu")
    policy = data['evaluation/policy']
    print("Policy and environment loaded")
    exp_dir = Path(args.file).parent
    variant = json.load((exp_dir / "variant.json").open())

    mode ="static_eval"
    # mode="reset_of_env"

    # variant['base_env_kwargs']['goal_generation_rooms'] = ["OBJECTS", "TV", "START"]
    # variant['base_env_kwargs']['goal_generation_rooms'] = ["OBJECTS"]
    variant['base_env_kwargs']['static_eval_set_size'] = 20
    # variant['base_env_kwargs']['static_eval_set_size'] = 6

    if "Sawyer" not in variant['env_id']:
        variant['skewfit_variant']['env_id'] = variant['env_id']
        variant['skewfit_variant']['base_env_kwargs'] = variant.get('base_env_kwargs', {})
        variant['skewfit_variant']['imsize'] = variant['imsize']
        variant['skewfit_variant']['vae_path'] = data['vae']

        variant['skewfit_variant']['evaluation_goal_sampling_mode'] = mode
        # variant['skewfit_variant']['evaluation_goal_sampling_mode'] = mode

        from rlkit.launchers.skewfit_experiments import get_envs

        env = get_envs(variant['skewfit_variant'])
        env.goal_sampling_mode = variant['skewfit_variant']['evaluation_goal_sampling_mode']
        env.decode_goals = True

    if args.gpu:
        ptu.set_gpu_mode(True)
        policy.to(ptu.device)
    if isinstance(env, VAEWrappedEnv) and hasattr(env, 'mode'):
        env.mode(args.mode)
    if args.enable_render or hasattr(env, 'enable_render'):
        # some environments need to be reconfigured for visualization
        env.enable_render(save_dir=exp_dir)
        # env.enable_render(save_dir="./")
    paths = []
    for i in range(variant['base_env_kwargs']['static_eval_set_size']):
        print("i: ", i)
        s = time.time()
        paths.append(multitask_rollout(
            env,
            policy,
            max_path_length=args.H,
            render=not args.hide,
            observation_key='latent_observation',
            desired_goal_key='latent_desired_goal',
        ))
        print("rollout: ", time.time() - s)

        # if hasattr(env, "log_diagnostics"):
        #     env.log_diagnostics(paths)
        # if hasattr(env, "get_diagnostics"):
        #     for k, v in env.get_diagnostics(paths).items():
        #         logger.record_tabular(k, v)
        # logger.dump_tabular()
    print("perf:", np.mean([p['env_infos'][-1]['visible_ent_f1'] for p in paths]))

    env.disable_render()

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('file', type=str,
                        help='path to the snapshot file')
    parser.add_argument('--H', type=int, default=50,
                        help='Max length of rollout')
    parser.add_argument('--speedup', type=float, default=10,
                        help='Speedup')
    parser.add_argument('--mode', default='video_env', type=str,
                        help='env mode')
    parser.add_argument('--gpu', action='store_true')
    parser.add_argument('--enable_render', action='store_true')
    parser.add_argument('--hide', action='store_true')
    args = parser.parse_args()

    simulate_policy(args)
