from pathlib import Path
import matplotlib
import copy
import numpy as np
import json
import pandas as pd
import scipy.stats as ss
import sys
from itertools import chain
from termcolor import cprint

import matplotlib.pyplot as plt
np.random.seed(1)

#

comparison = True
p_allowed = 0.05
print("P: ", p_allowed)

max_epochs = 2000
min_epochs = 1005
sort_longest = False
k_seeds = 15
smooth_n = 1

per_appr_figures = False
save = True

if comparison:
    titles, metrics = zip(*[
        ("Performance", 'evaluation/env_infos/final/visible_ent_f1 Mean'),
        ("TV ON", 'replay_buffer/all_goals_exp_perc_of_tv_ON'),
    ])

    to_load = [
        # ("CountBased", Path("./data/CountBased_1.0/")),
        # ("GRIM-CountBased", Path("./data/GRIM-LP-CountBased_1.0_50/")),
        # ("Skewfit", Path("./data/Skewfit_0.25/")),
        # ("GRIM-Skewfit", Path("./data/GRIM-LP-Skewfit_0.75_50/")),

        ("Skewfit (\u03B1=-0.25)", Path("./data/Skewfit_0.25/")),
        ("GRIM-Skewfit (\u03B1=-0.25)", Path("./data/GRIM-LP-Skewfit_0.25_50/")),
        ("Skewfit (\u03B1=-0.75)", Path("./data/Skewfit_0.75/")),
        ("GRIM-Skewfit (\u03B1=-0.75)", Path("./data/GRIM-LP-Skewfit_0.75_50/")),

        #("Skewfit_1.0", Path("./data/Skewfit_1.0/")),
        #("GRIM-Skewfit_1.0_50", Path("./data/GRIM-LP-Skewfit_1.0_50/")),
    ]
    prefix = "compare_"
    fignames = ["./plots/"+prefix + s for s in ["SKF_25_perf", "SKF_25_TV_ON", "SKF_75_perf", "SKF_75_TV_ON"]]
    colors = ["red", "black", "brown", "purple"]

    # fignames = ["./plots/"+prefix + s for s in ["CB_perf", "CB_TV_ON", "SKF_25_perf", "SKF_25_TV_ON", "SKF_75_perf", "SKF_75_TV_ON"]]
    # colors = ["blue", "green", "red", "black", "brown", "purple"]

    # fignames = ["./plots/"+prefix + s for s in ["CB_perf", "CB_TV_ON", "SKF_perf", "SKF_TV_ON"]]
    # colors = ["blue", "green", "red", "black"]
    expand_margin=1.7

else:
    # ablation
    titles, metrics = zip(*[
        # ("Performance", 'evaluation/env_infos/final/visible_ent_f1 Mean'),
        # ("TV ON", 'replay_buffer/all_goals_exp_perc_of_tv_ON'),
        #
        # ("clock", 'replay_buffer/all_goals_exp_perc_of_clock'),
        # ("impressionn", 'replay_buffer/all_goals_exp_perc_of_impression'),
        #
        # ("office desk", 'replay_buffer/all_goals_exp_perc_of_office_desk'),
        # ("office desk", 'replay_buffer/all_goals_exp_perc_of_whiteboard'),
        #
        # ("TV OFF", 'replay_buffer/all_goals_exp_perc_of_tv_OFF'),
        # ("couch", 'replay_buffer/all_goals_exp_perc_of_couch'),

        ("bench", 'replay_buffer/all_goals_exp_perc_of_bench'),
        ("starry night", 'replay_buffer/all_goals_exp_perc_of_starry_night'),

        # ("wooden chair", 'replay_buffer/all_goals_exp_perc_of_wooden_chair'),
        # ("office chair", 'replay_buffer/all_goals_exp_perc_of_office_chair'),
        #
        # ("box", 'replay_buffer/all_goals_exp_perc_of_box'),
        # ("airduct grate", 'replay_buffer/all_goals_exp_perc_of_airduct_grate'),
        #
        # ("potted plant", 'replay_buffer/all_goals_exp_perc_of_potted_plant'),
        # ("whiteboard", 'replay_buffer/all_goals_exp_perc_of_potted_plant'),
    ])

    to_load = [
        ("GRIM-UNI-CountBased", Path("./data/GRIM-UNI-CountBased_1.0_50/")),
        ("GRIM-LP-CountBased", Path("./data/GRIM-LP-CountBased_1.0_50/")),

        ("GRIM-UNI-Skewfit", Path("./data/GRIM-UNI-Skewfit_0.75_50/")),
        ("GRIM-LP-Skewfit", Path("./data/GRIM-LP-Skewfit_0.75_50/")),
    ]
    prefix = "ablation_"
    # fignames = ["./plots/"+prefix + s for s in ["CB_perf", "CB_TV_ON", "SKF_perf", "SKF_TV_ON"]]
    fignames = ["./plots/"+prefix + s for s in [
        # "CB_perf", "CB_TV_ON",
        # "CB_clock", "CB_impression",
        # "CB_office_desk", "CB_whiteboard",
        # "CB_TV_OFF", "CB_couch",
        "CB_bench", "CB_starry_night",
        # "CB_wooden_chair", "CB_office_chair",
        # "CB_box", "CB_airduct_grate",
        # "CB_potted_plant", "CB_plant",

        # "SKF_perf", "SKF_TV_ON",
        # "SKF_clock", "SKF_impression",
        # "SKF_office_desk", "SKF_whiteboard",
        # "SKF_TV_OFF", "SKF_couch",
        "SKF_bench", "SKF_starry_night",
        # "SKF_wooden_chair", "SKF_office_chair",
        # "SKF_box", "SKF_airduct_grate",
        # "SKF_potted_plant", "SKF_plant",
    ]]

    colors = ["blue", "green", "blue", "green"]
    expand_margin=1.7

labels, approaches = zip(*to_load)


titles = {k: c for k, c in zip(metrics, titles)}
labels = {k: c for k, c in zip(approaches, labels)}
colors = {k: c for k, c in zip(approaches, colors)}

# titles, metrics = zip(*[
#     ("Performance", 'evaluation/env_infos/final/visible_ent_f1 Mean'),
#     #("clock", 'replay_buffer/goals_exp_perc_of_clock'),
#     #("TV OFF", 'replay_buffer/goals_exp_perc_of_tv_OFF'),
#     #("couch", 'replay_buffer/goals_exp_perc_of_couch'),
#     #("bench", 'replay_buffer/goals_exp_perc_of_bench'),
#     #("impressionn", 'replay_buffer/goals_exp_perc_of_impression'),
#     #("starry night", 'replay_buffer/goals_exp_perc_of_starry_night'),
#     #("wooden chair", 'replay_buffer/goals_exp_perc_of_wooden_chair'),
#     #("office chair", 'replay_buffer/goals_exp_perc_of_office_chair'),
#     #("office desk", 'replay_buffer/goals_exp_perc_of_office_desk'),
#     #("box", 'replay_buffer/goals_exp_perc_of_box'),
#     #("airduct grate", 'replay_buffer/goals_exp_perc_of_airduct_grate'),
#     #("potted plant", 'replay_buffer/goals_exp_perc_of_potted_plant'),
#     #("whiteboard", 'replay_buffer/goals_exp_perc_of_whiteboard'),
#     # all objs
#     ("TV ON", 'replay_buffer/all_goals_exp_perc_of_tv_ON'),
#     ("clock", 'replay_buffer/all_goals_exp_perc_of_clock'),
#     ("TV OFF", 'replay_buffer/all_goals_exp_perc_of_tv_OFF'),
#     ("couch", 'replay_buffer/all_goals_exp_perc_of_couch'),
#     ("bench", 'replay_buffer/all_goals_exp_perc_of_bench'),
#     ("impressionn", 'replay_buffer/all_goals_exp_perc_of_impression'),
#     ("starry night", 'replay_buffer/all_goals_exp_perc_of_starry_night'),
#     ("wooden chair", 'replay_buffer/all_goals_exp_perc_of_wooden_chair'),
#     ("office chair", 'replay_buffer/all_goals_exp_perc_of_office_chair'),
#     ("office desk", 'replay_buffer/all_goals_exp_perc_of_office_desk'),
#     ("box", 'replay_buffer/all_goals_exp_perc_of_box'),
#     ("airduct grate", 'replay_buffer/all_goals_exp_perc_of_airduct_grate'),
#     ("potted plant", 'replay_buffer/all_goals_exp_perc_of_potted_plant'),
#     ("whiteboard", 'replay_buffer/all_goals_exp_perc_of_whiteboard'),
#     # all rooms
#     #("% of TV room", 'replay_buffer/all_goals_exp_perc_of_Room.TV'),
#     #("% of GALLERY room", 'replay_buffer/all_goals_exp_perc_of_Room.GALLERY'),
#     #("% of OFFICE room", 'replay_buffer/all_goals_exp_perc_of_Room.OFFICE'),
#     # current
#     #("% of TV room", 'replay_buffer/goals_exp_perc_of_Room.TV'),
#     #("% of GALLERY room", 'replay_buffer/goals_exp_perc_of_Room.GALLERY'),
#     #("% of OFFICE room", 'replay_buffer/goals_exp_perc_of_Room.OFFICE'),
# ])

def save_subplot(ax, fig, filename):
    extent = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
    fig.savefig(filename, bbox_inches=extent.expanded(expand_margin, expand_margin), dpi=400)

def load_json(json_path):
    with open(str(json_path)) as json_file:
        return json.load(json_file)

def d_diff(d1, d2, path=""):
    for k in d1:
        if (k not in d2):
            print (path, ":")
            print (k + " as key not in d2", "\n")
        else:
            if type(d1[k]) is dict:
                if path == "":
                    path = k
                else:
                    path = path + "->" + k
                d_diff(d1[k],d2[k], path)
            else:
                if d1[k] != d2[k]:
                    print (path, ":")
                    print (" - ", k," : ", d1[k])
                    print (" + ", k," : ", d2[k])

def delete_keys_from_dict(dict_del, lst_keys):
    for k in lst_keys:
        try:
            del dict_del[k]
        except KeyError:
            pass
    for v in dict_del.values():
        if isinstance(v, dict):
            delete_keys_from_dict(v, lst_keys)

    return dict_del

def load_performances(directory, metric, ignore_keys=[]):
    # approach
    if type(directory) == str:
        directory = Path(directory)
    hyper = copy.deepcopy([load_json(p) for p in directory.glob("*/variant.json")])


    seeds = [h['seed'] for h in hyper]
    assert len(set(seeds)) == len(seeds)

    hyper = [delete_keys_from_dict(h, ignore_keys) for h in hyper]


    if not all(x == hyper[0] for x in hyper):
        print("Variants differrent!")
        different_dicts = [i for i in range(len(hyper)) if hyper[0] != hyper[i]]
        for different_d in different_dicts:
            d_diff(hyper[0], hyper[different_d])
        exit()

    data = [pd.read_csv(p) for p in directory.glob("*/progress.csv") if p.stat().st_size > 0]  # ignore empty progress files

    #perf = [np.array(d.get(metric, [0.0]*max_epochs)) for d in data]
    perf = [np.array(d[metric]) for d in data]
    if min_epochs is not None:
        perf = [p for p in perf if len(p) > min_epochs]

    if max_epochs is not None:
        perf = [p[:max_epochs] for p in perf]

    if sort_longest:
        k = np.array([len(p) for p in perf]).argsort()[-k_seeds:][::-1]
        perf = np.array(perf)[k]
    else:
        perf = np.array(perf)[:k_seeds]

    print("dir:", directory)
    print("k:", [len(p) for p in perf])
    print("n_seeds:", len(perf))

    m = min([len(p) for p in perf])

    m = min(m, max_epochs)
    perf = np.array([p[:m] for p in perf])

    return perf

fig_1, axs_1 = plt.subplots(nrows=len(to_load)//2, ncols=len(metrics))
plt.subplots_adjust(hspace=0.9, wspace=0.9)

for m_i, metric in enumerate(metrics):
    performances = {}
    for a_i, approach in enumerate(approaches):
        m_y, m_x = a_i // 2 , m_i
        axs_1[m_y][m_x].set_box_aspect(1)
        axs_1[m_y][m_x].tick_params(axis="x", labelsize=5)
        axs_1[m_y][m_x].tick_params(axis="y", labelsize=5)

        ignore_keys = ['seed', 'exp_prefix', 'num_epochs', "dump_reprs"]
        if "GRIM" not in labels[approach]:
            ignore_keys.append("lp_history_size")

        approach_perfs = load_performances(approach, metric, ignore_keys=ignore_keys)
        #assert len(approach_perfs) == longest_k

        performances[approach] = copy.deepcopy(approach_perfs)
        app_m = approach_perfs.mean(0)


        def smooth(ps, smooth_n):
            return [np.mean(ps[max(i - smooth_n, 0):i]) for i in range(len(ps))]

        if smooth_n > 1:
            app_m_sm = smooth(app_m, smooth_n)
        else:
            app_m_sm = app_m


        # app_m_sm_per_seed = np.array([smooth(approach_perfs[i, :], smooth_n) for i in range(8)])
        # app_m_sm = np.mean(app_m_sm_per_seed, axis=0)

        app_m_sm = np.minimum(app_m_sm, 1.0)
        app_margin = approach_perfs.std(0)/np.sqrt(approach_perfs.shape[0])

        est = 1000
        cprint("{} step {} m {} ---> {} +- {}".format(approach, est, metric, approach_perfs.mean(0)[est], approach_perfs.std(0)[est]), "red")

        if smooth_n > 0:
            app_margin = smooth(app_margin, smooth_n)
        # app_margin = app_m_sm_per_seed.std(0)/np.sqrt(approach_perfs.shape[0])

        if not save:
            axs_1[m_y][m_x].title.set_text(titles[metric])

        axs_1[m_y][m_x].plot(range(len(app_m_sm)), app_m_sm, label=str(labels[approach]), color=colors[approach], linewidth=1)
        axs_1[m_y][m_x].scatter(0, max_epochs, s=1)

        # axs_1[m_y][m_x].set_ylim(0, float(np.clip((app_m_sm+app_m).max() + 0.1, 0, 1)))
        axs_1[m_y][m_x].set_ylim(0, 1.0)

        axs_1[m_y][m_x].fill_between(
            range(len(app_m)),
            np.clip(app_m_sm+app_margin, 0, 1), np.clip(app_m_sm-app_margin, 0, 1), color=colors[approach], alpha=0.1)

        # if "visible_ent_f1" in str(approaches[m_i]):
        if "visible_ent_f1" in str(metrics[m_i]):
                axs_1[m_y][m_x].legend(loc="upper center", fontsize=6)
        
        axs_1[m_y][m_x].set_ylabel(titles[metric], fontsize=5)

        axs_1[m_y][m_x].set_xlabel("Epoch", fontsize=5)


    if not per_appr_figures and "visible_ent_f1" in str(approaches[m_i]):
        to_compare = [(i*2, i*2+1) for i in range(len(to_load)//2)]
        for comp_id, (ap_id, base_id) in enumerate(to_compare):
            m_y, m_x = ap_id // 2, m_i
            ap = performances[approaches[ap_id]]
            baseline = approaches[base_id]
            bs = performances[baseline]
            xs, good = [], []
            for i in range(min(ap.shape[1], bs.shape[1])):
                st, p = ss.ttest_ind(ap[:, i], bs[:, i], equal_var=False)
                if p <= p_allowed and p is not None:
                    xs.append(i)
                    if m_i == 0:
                        # good.append(max(p0.max(), p1.max()))
                        # good.append(0.6 - 0.1 * comp_id)
                        good.append(0.6)
                    else:
                        good.append(0.0 + 0.1 * comp_id)

            axs_1[m_y][m_x].scatter(xs, good, s=1, color=colors[baseline])  # , label="p<={}".format(p_allowed))

        # axs_1[m_y][m_x].legend(loc="upper center", fontsize=6)
        if m_i == 0:
            axs_1[m_y][m_x].set_ylim(0.0, 1.0)
        else:
            axs_1[m_y][m_x].set_ylim(1.0, 1.0)

    if not per_appr_figures and save:
        for i, ax in enumerate(list(chain(*axs_1))):
            save_subplot(ax, fig_1, fignames[i])

# matplotlib.rcParams.update({
#     'font.size': 1,
#     'xtick.labelsize': 1,
#     'ytick.labelsize': 1,
#     'axes.titlesize': 1,
#     'axes.labelsize': 1,
#     'legend.fontsize': 6,
# })
# fig_1.set_size_inches(8, 8)
plt.show()


