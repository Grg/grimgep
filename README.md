# GRIMGEP

This is the official implementation of the GRIMGEP paper.
The website of the project is [here](https://sites.google.com/view/grimgep).

## Installation

Build and activate conda env
```
conda env create -f ./environment/neurips_grimenv36.yml
conda activate neurips_grimenv
```


To setup gluoncv, if you want to use gpu make sure to set the right version of mxnet in ``requirements.txt``. 
Alternatively, remove `mxnet` and `gluoncv` from requirements.txt and install `gluoncv` following the instructions on [gluoncv website](https://cv.gluon.ai/install.html).


If you don't want to use gpu no modification to the `reauirements.txt` is needed.

Install requirements
```
pip install -r requirements.txt
pip install -e .
```

Clone and Install multiworld
```
cd .. # cd to where you want multiworld cloned
git clone https://github.com/vitchyr/multiworld.git
cd  multiworld
pip install .

# go back to grimgep root
cd - 
```

Unpack mini imagenet. 
Download miniimgaenet from [google drive](https://drive.google.com/file/d/19exNPJ1reF8dgYU_gEh1jb86gQKS_mVa/view?usp=sharing)

```
mv <path_to_download_dir>/tv_imagenet_very_small.tar.gz envs/gym_miniworld/textures/
cd envs/gym_miniworld/textures
tar -xvf tv_imagenet_very_small.tar.gz

# go back to grimgep root
cd -
```

## Running experiments

### Minimal example
To run a minimal example, run the following command for Skewfit:
```
python examples/grimgep3d/Skewfit_experiment.py <exp_id> <experiment_name> <n_seeds>
```
, and the following for CountBased:
```
python examples/grimgep3d/CountBased_experiment.py <exp_id> <experiment_name> <n_seeds>
```

The <exp_id> should be set according to the following tables:

**Skewfit**

[comment]: <> ( approach | Skf &#40;-0.25&#41; | GRIM-UNI-Skf &#40;-0.25&#41; | GRIM-LP-Skf &#40;-0.25&#41; | Skf &#40;-0.75&#41; | GRIM-UNI-Skf &#40;-0.75&#41; | GRIM-LP-Skf &#40;-0.75&#41;)
 approach | Skf (-0.25) | Skf (-0.75) | GRIM-UNI-Skf (-0.25) | GRIM-UNI-Skf (-0.75) | GRIM-LP-Skf (-0.25) | GRIM-LP-Skf (-0.75) 
 --- | --- | --- | --- | --- | ---  | ---
 exp_id | 0 | 1 | 2 | 3 | 4 | 5

**CountBased**

approach | CB | GRIM-UNI-CB | GRIM-LP-CB
--- | --- | --- | ---
 exp_id | 0 | 1 | 2


### Full experiments
Check that `PY_PATH` is correctly set for your conda installation in `run_experiments3D.sh`.
By default, its `PY_PATH="$HOME/anaconda3/envs/neurips_grimenv36/bin/python" `


**Slurm-based server**

To start the Skewfit experiments on slurm based server run:
for Skewfit run:
```
for i in {0..89}; do sbatch sb_run.sh skf $i; done
```
for CountBased run:
```
for i in {0..45}; do sbatch sb_run.sh cb $i; done
```

**Regular server**

To start the Skewfit experiments on a regular server run:
for Skewfit run:
```
for i in {0..89}; do bash run_experiments3D.sh skf $i; done
```
for CountBased run:
```
for i in {0..45}; do bash run_experiments3D.sh cb $i; done
```

### Resources used
This source code is build upon the following resources:
[rlkit](https://github.com/vitchyr/rlkit/),
[miniworld](https://github.com/maximecb/gym-miniworld)
[gluon-cv](https://github.com/dmlc/gluon-cv)
[sklearn](https://scikit-learn.org/stable/)
[imagenet](https://scikit-learn.org/stable/)

The meshes for the Explore3D environment were taken from [open games art](https://opengameart.org/), [cgtrader](https://www.cgtrader.com/), and modified using [Blender](https://www.blender.org/)
The icons for the PlaygroundRGB environment were taken from [the noun project](https://thenounproject.com/.)
