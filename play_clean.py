import gym
import os
from moviepy.editor import ImageSequenceClip
import numpy as np
import copy
import cv2
import time
import numpy as np
import pygame
from pygame.locals import *
import time
import sys
import random
import os

import envs.playground_env
import envs.gym_miniworld

from matplotlib import pyplot as plt
import matplotlib
matplotlib.use("Agg")
import torch
normalize_obs = True

def save_img(img, path="fig"):
    if type(img) == torch.Tensor:
        img = img.cpu().numpy()

    if img.dtype != np.float:
        img = img.astype(np.float)
    if img.max() > 1.0:
        img /= 255.

    if len(img.shape) == 4:
        # first from batch
        img = img[0]

    if len(img.shape) == 4:
        img = np.squeeze(img)

    if img.shape[0] == 3 and img.shape[-1] != 3:
        # we want H, W, C
        img = np.array(img).transpose(1, 2, 0)


    fig = plt.figure(figsize=(20, 20))

    # plt.clf()
    fig.add_subplot(2, 2, 1)
    plt.imshow(img)


    def draw_imgs(imgs):
        h = np.sqrt(len(imgs))
        w = np.ceil(len(imgs)/h)+1
        fig = plt.figure(figsize=(20, 20))

        for i, im in enumerate(imgs):
            fig.add_subplot(h, w, i+1)
            plt.imshow(im)

    # # qunatize 10
    # img_q = cv2.resize(img, dsize=(10, 10), interpolation=cv2.INTER_AREA)
    # img_q = (img_q//0.34)*0.34
    # fig.add_subplot(2, 2, 2)
    # plt.imshow(img_q)

    nbins=5

    # qunatize 4
    img_q_4 = cv2.resize(img, dsize=(4, 4), interpolation=cv2.INTER_AREA)
    img_q_4 = (img_q_4//0.34)*0.34
    delta = 1.0 / nbins
    # quantize colors
    img_q_4 *= 0.9999  # to make sure 1.0 does not appear and have a specific value
    img_q_4 = np.floor(img_q_4 / delta) * delta


    # fig.add_subplot(2, 2, 3)
    # plt.imshow(img_q_4)

    # qunatize 3
    img_q_3 = cv2.resize(img, dsize=(3, 3), interpolation=cv2.INTER_AREA)
    img_q_3 = (img_q_3//0.34)*0.34
    # fig.add_subplot(2, 2, 2)
    # plt.imshow(img_q)
    delta = 1.0 / nbins
    # quantize colors
    img_q_3 *= 0.9999  # to make sure 1.0 does not appear and have a specific value
    img_q_3 = np.floor(img_q_3 / delta) * delta


    # qunatize 2
    img_q = cv2.resize(img, dsize=(2, 2), interpolation=cv2.INTER_AREA)
    img_q = (img_q//0.34)*0.34
    delta = 1.0 / nbins
    # quantize colors
    img_q *= 0.9999  # to make sure 1.0 does not appear and have a specific value
    img_q = np.floor(img_q / delta) * delta

    # draw_imgs([img, img_q_4, img_q_3, img_q])
    draw_imgs([img, img_q_4, img_q])


    plt.savefig(path)

    # plt.show()
    plt.clf()


def gif(filename, array, fps=10, scale=1.0):
    # ensure that the file has the .gif extension
    fname, _ = os.path.splitext(filename)
    filename = fname + '.gif'

    # copy into the color dimension if the images are black and white
    if array.ndim == 3:
        array = array[..., np.newaxis] * np.ones(3)

    # make the moviepy clip
    clip = ImageSequenceClip(list(array), fps=fps).resize(scale)
    clip.write_gif(filename, fps=fps)
    return clip

sys.path.append('../..')

H, W = 48, 48
ims = []
ENV_NAME = 'MiniWorld-ThreeRoomsTVRicher-v0'
env = gym.make(
    ENV_NAME,
    # is this parameter needed? used for generating DONE
    # intentionally set to > m_st so that no DONEs appear
    max_episode_steps=100,
    TV_active=True,

    # we'll need this for clustering
    obs_width=W,
    obs_height=H,

    # obs_width=48,
    # obs_height=48,
    # window_width=48,
    # window_height=48,
    domain_rand=False,
    static_eval_set_size=20,

    static_initial_position=True,
)
# ENV_NAME = 'PlaygroundRGB-v1'
# env = gym.make(
#     ENV_NAME,
#     continuous=True,
#
#     # debug_mode=True,
#     # available_room="OBJECTS",
#     # rooms_mode=False,
#     static_eval_set_size=25,
#
#     debug_mode=False,
#     colored_start_room=True,
#     air_wrong_room_distance=True,
#     rooms_mode=True,
#     debug_rooms_mode=True,
#     goal_generation_rooms=("OBJECTS",),
#     available_rooms=("START", "OBJECTS", "RANDOM", "TV"),
#     number_of_random_room_colors=5,
#     number_of_TV_room_colors=5,
#     doors=True,
#     door_size=0.5,
#     teleport_random_room=False,
#     random_color_per_rollout=True,
#     random_ball_per_rollout=False,
#     noise_random_color=False,
#     corridor_random_room=False,
#     random_ball_location=True,
#
#     random_TV_location=True,
#     random_TV_location_per_rollout=False,
#     TV_color_per_rollout=True,
#
#     white_obj_room=False,
#     gray_obj_room=True,
#     obj_room_flipped_actions=False,
#     obj_room_rotated_actions=False,
#     doors_handle=False,
#
#     block_dog=True,
#     grow=False,
#     block_water=False,
#
#     deterministic_obj=True,
#     whiten=True,
#
#     use_big_margin=False,
#     use_medium_margin=False,
#     use_custom_margin=True,
#     custom_margin_value=0.75,
#
#     use_small_objs=False,
#     use_huge_objs=False,
#     use_custom_objs=True,
#     custom_objs_ratio=1.3,
#
#     red_gripper=True,
#     static=True,
#     smooth_colors=False,
#
#     pos_step_size=0.5,
# )


pygame.init()

# seed = np.random.randint(300)
# np.random.seed(seed)
# env.seed(seed)


# cam left/right, forward/backward, right/left, pickup/drop, toggle
d = {
    "a": np.array([1/2, 0, 0, 0, 0]),
    "d": np.array([-1/2, 0, 0, 0, 0]),
    "w": np.array([0, 1, 0, 0, 0]),
    "s": np.array([0, -1, 0, 0, 0]),
    "q": np.array([0, 0, -1, 0, 0]),
    "e": np.array([0, 0, 1, 0, 0]),
    "p": np.array([0, 0, 0, 1, 0]),
    "o": np.array([0, 0, 0, -1, 0]),
    "i": np.array([0, 0, 0, 0, 1])
}
goals = []

import torch

GIF = False
img = None
for r in range(20):
    print("new goal")
    obs = env.reset()
    env_state = env.get_env_state()
    print("es:", env_state[7])

    g_ = env.get_next_static_eval_goals(1)
    g = {k: v[0] for k, v in g_.items()}

    env.set_to_goal(g)
    img = env.get_image(H, W).astype(float)

    save_img(img, "dig")

    env.set_env_state(env_state)

    if GIF:
        ims.append(img)

    hold = 0

    while True:

        while True:
            # until key in dict
            key = input()[:1]
            key = "p" if key == '' else key

            if key == "p":
                hold = 1-hold

            if key == "g" and GIF:
                gif("play_clean/playgorundrgb.gif", np.array(ims), fps=15)
                img = []
                print("saved to play_clean/playgorundrgb.gif")
                exit()

            if key in d:
                a = d[key]
                break

        a[-2] = hold
        obs, _, _, info = env.step(a)
        print("agent xya: {} box xya: {}".format(
            env.vector_to_state_dict(obs['achieved_goal'])['agent_xya'].__repr__(),
            env.vector_to_state_dict(obs['achieved_goal'])['box_xya'].__repr__(),
        ))

        if key in ["w", "s"]:
            obs, _, _, info = env.step(a)
            obs, _, _, info = env.step(a)

        im = env.get_image_obs()
        save_img(im, "dig")

        if GIF:
            ims.append(im)




