#!/bin/bash
# Our custom function

case "$1" in
  1)
    echo "Underlying imgep is Skewfit"
    exp_name="Abl_Skewfit_"
    script_name="Skewfit_ablation.py"
    ;;
  2)
    echo "Underlying imgep is CountBased"
    exp_name="Abl_CountBased_"
    script_name="CountBased_ablation.py"
    ;;
  3)
    echo "Underlying imgep is OnlineRIG"
    exp_name="Abl_OnlineRIG_"
    script_name="OnlineRIG_ablation.py"
    ;;
esac

sequential_run () {
  ngpus=`nvidia-smi -q | grep GPU\ UUID | wc -l`
  gpu=$(($1 % $ngpus))
  echo "Starting exp: $2 conf: $1 on gpu: $gpu"

  touch ./outs/error_$2_$1
  CUDA_VISIBLE_DEVICES=$gpu python -u examples/grimgep/$script_name $1 $2 2>&1 | tee -a ./outs/error_$2_$1

}


# rand
for i in {0..7}
do
	sequential_run $i $exp_name &
done

wait
echo "All done"
