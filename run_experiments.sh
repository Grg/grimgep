#!/bin/bash
# Our custom function
echo "this is playground, not 3D"
echo "exiting now..."
exit

case "$1" in
  1)
    echo "Underlying imgep is Skewfit"
    exp_name="Exp_Skewfit_"
    script_name="Skewfit_experiment.py"
    ;;
  2)
    echo "Underlying imgep is CountBased"
    exp_name="Exp_CountBased_"
    script_name="CountBased_experiment.py"
    ;;
  3)
    echo "Underlying imgep is OnlineRIG"
    exp_name="Exp_OnlineRIG_"
    script_name="OnlineRIG_experiment.py"
    ;;
esac

sequential_run () {
  ngpus=`nvidia-smi -q | grep GPU\ UUID | wc -l`
  ngpus=$(($ngpus>1?$ngpus:1)) # in case there are no gpus
  gpu=$(($1 % $ngpus))
  echo "Starting exp: $2 conf: $1 on gpu: $gpu"

  touch ./outs/error_$2_$1
  CUDA_VISIBLE_DEVICES=$gpu python -u examples/grimgep/$script_name $1 $2 2>&1 | tee -a ./outs/error_$2_$1

}


# rand
for i in {0..5}
do
	sequential_run $i $exp_name &
done

wait
echo "All done"
