import glob
from matplotlib.lines import Line2D
from sklearn.neighbors import NearestNeighbors
from collections import defaultdict, Counter
from sklearn.linear_model import LinearRegression

import time
# from sklearn.neighbors import NearestNeighbors
from sklearn.cluster import KMeans, AgglomerativeClustering
from sklearn.mixture import GaussianMixture
import numpy as np
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
plt.rcParams["figure.figsize"] = (10, 10)
from pathlib import Path
from envs.playground_env.env_params import Room

import sys

path = Path(sys.argv[1])
filenames = []
step = 10

max_iter = max([int(Path(p).stem.split("_")[-1]) for p in glob.glob(str(path) + "/vae_lats*")])
for i in range(0, max_iter, step):
        filenames.append(
        str(path/"vae_lats_{}.npy".format(i))
    )
max_iter = max_iter - max_iter % step
# fig, axs = plt.subplots(nrows=len(filenames), ncols=5)

def normalize_lp(lps):
    return lps
    normalizer = sum(lps.values())
    if normalizer > 0:
        lps = {k: v / normalizer for k, v in lps.items()}
    return lps


def calculate_ALP(
        goals_lat,
        clusters,
        perfs,
        subset_size=None,
        k=1,
        history_size=None,
        clamp=False
):
    assert len(clusters) == len(perfs) == len(goals_lat)

    uni_cl = set(clusters)
    if clamp:
        for c in uni_cl:
            inds = np.where(np.array(clusters)==c)[0]
            perfs[inds] = np.minimum(perfs[inds], np.mean(perfs[inds][:5]))

    if history_size is not None:
        assert history_size > 1
        perfs = perfs[-history_size:]
        clusters = clusters[-history_size:]
        goals_lat = goals_lat[-history_size:]

    l = len(clusters) // 2

    if subset_size is not None:
        l = subset_size

    old_goals = goals_lat[:l]
    old_perfs = perfs[:l]

    new_goals = goals_lat[-l:]
    new_perfs = perfs[-l:]

    new_clusters = clusters[-l:]

    k = min(k, l)
    k = min(k, len(old_goals))

    old_nbrs = NearestNeighbors(n_neighbors=k, algorithm='ball_tree').fit(old_goals)
    old_indices = old_nbrs.kneighbors(new_goals, return_distance=False)
    nn_old_perfs = old_perfs[old_indices].mean(axis=1)

    new_nbrs = NearestNeighbors(n_neighbors=k, algorithm='ball_tree').fit(new_goals)
    new_indices = new_nbrs.kneighbors(new_goals, return_distance=False)
    nn_new_perfs = new_perfs[new_indices].mean(axis=1)

    lps = abs(nn_new_perfs - nn_old_perfs)
    available_clusters = set(new_clusters)
    cluster_lp_dict = defaultdict(float)
    for cl in available_clusters:
        cl_indices = np.where(np.array(new_clusters) == cl)[0]
        cluster_lp_dict[cl] = lps[cl_indices].mean()

    cluster_lp_dict = normalize_lp(cluster_lp_dict)
    return cluster_lp_dict, lps

def calculate_NN_LP(goals_lat, clusters, vae_perfs,  clamp=True, history_size=None, subset_size=None, k=None):
    size = 10
    if len(vae_perfs) > size:
        clusters_steps = np.array_split(clusters, len(vae_perfs)//size)
        vae_perfs_steps = np.array_split(vae_perfs, len(vae_perfs)//size)
        goals_lat_steps = np.array_split(goals_lat, len(vae_perfs)//size)
    else:
        clusters_steps = [clusters]
        vae_perfs_steps = [vae_perfs]
        goals_lat_steps = [goals_lat]

    perfs = defaultdict(list)
    uni_cl = set(clusters)

    test_set_clusters = np.array(clusters[-10:])
    test_set_perfs = np.array(vae_perfs[-10:])
    test_set_lats = np.array(goals_lat[-10:])

    lps = {}
    for clusters_step, vae_perfs_step, goals_lat_step in zip(clusters_steps, vae_perfs_steps, goals_lat_steps):
        for cl in uni_cl:
            # cl_test_perfs = test_set_perfs[test_set_clusters == cl]
            cl_test_lats = test_set_lats[test_set_clusters == cl]

            if len(cl_test_lats) > 0:
                nbrs = NearestNeighbors(n_neighbors=1, algorithm='ball_tree').fit(goals_lat_step)
                indices = nbrs.kneighbors(cl_test_lats, return_distance=False)
                p = vae_perfs_step[indices].mean()
                perfs[cl].append(p)

    for cl in uni_cl:
        perfs[cl] = np.array(perfs[cl])
        if clamp:
            perfs[cl] = np.minimum(perfs[cl], perfs[cl][0])

        if history_size is not None:
            perfs[cl] = perfs[cl][-history_size:]

        l = max(len(perfs[cl])//2, 1)

        lps[cl] = np.abs(perfs[cl][:l].mean() - perfs[cl][-l:].mean())

    lps = normalize_lp(lps)
    return lps, None

def linreg_lp(vae_perfs, clusters, clamp=True, history_size=None):
    size = 10
    if len(vae_perfs) > size:
        clusters_steps = np.array_split(clusters, len(vae_perfs)//size)
        vae_perfs_steps = np.array_split(vae_perfs, len(vae_perfs)//size)
    else:
        clusters_steps = [clusters]
        vae_perfs_steps = [vae_perfs]

    perfs = defaultdict(list)
    uni_cl = set(clusters)
    lps = {}
    for clusters_step, vae_perfs_step in zip(clusters_steps, vae_perfs_steps):
        for cl in uni_cl:
            ps = vae_perfs_step[np.array(clusters_step) == cl]
            if len(ps) > 0:
                perfs[cl].append(ps.mean())

    for cl in uni_cl:
        perfs[cl] = np.array(perfs[cl])
        if clamp:
            perfs[cl] = np.minimum(perfs[cl], perfs[cl][0])

        if history_size is not None:
            perfs[cl] = perfs[cl][-history_size:]

        l = max(len(perfs[cl])//2, 1)

        X = np.arange(len(perfs[cl])).reshape(-1, 1)
        y = perfs[cl].reshape(-1, 1)
        lp = np.abs(LinearRegression().fit(X, y).coef_)
        lps[cl] = lp

    lps = normalize_lp(lps)
    return lps

def normal_lp(vae_perfs, clusters, clamp=True, history_size=None, size=10, med=False):
    if len(vae_perfs) > size:
        clusters_steps = np.array_split(clusters, len(vae_perfs)//size)
        vae_perfs_steps = np.array_split(vae_perfs, len(vae_perfs)//size)
    else:
        clusters_steps = [clusters]
        vae_perfs_steps = [vae_perfs]

    perfs = defaultdict(list)
    uni_cl = set(clusters)
    lps = {}
    for clusters_step, vae_perfs_step in zip(clusters_steps, vae_perfs_steps):
        for cl in uni_cl:
            ps = vae_perfs_step[np.array(clusters_step) == cl]
            if len(ps) > 0:
                perfs[cl].append(ps.mean())

    for cl in uni_cl:
        perfs[cl] = np.array(perfs[cl])
        if clamp:
            perfs[cl] = np.minimum(perfs[cl], perfs[cl][0])

        if history_size is not None:
            perfs[cl] = perfs[cl][-history_size:]

        l = max(len(perfs[cl])//2, 1)

        if med:
            lps[cl] = np.abs(np.median(perfs[cl][:l]) - np.median(perfs[cl][-l:].mean()))
        else:
            lps[cl] = np.abs(perfs[cl][:l].mean() - perfs[cl][-l:].mean())

    lps = normalize_lp(lps)
    return lps


def approx_normal_lp(vae_perfs, clusters, clamp=True):
    uni_cl = set(clusters)
    perfs = dict()
    lps = dict()
    if clamp:
        for c in uni_cl:
            inds = np.where(np.array(clusters) == c)[0]
            perfs[c] = np.minimum(vae_perfs[inds], np.mean(vae_perfs[inds][:5]))
            l = len(perfs[c])//2
            lps[c] = np.abs(perfs[c][l:].mean() - perfs[c][:l].mean())
    return lps



recs = defaultdict(list)
precs = defaultdict(list)
alp_points = []
h_lens = [0]
for iter, filename in enumerate(filenames):
    print("step:", iter*step)
    data = np.load(filename, allow_pickle=True)
    data_dict = data[()]
    rooms = data_dict["goal_rooms"]
    clusters = np.array(data_dict["clusters"])

    goals_lat = data_dict['goals_lat']
    last_states_lat = data_dict['last_states_lat']
    # vae_perfs = np.sqrt(np.linalg.norm(goals_lat - last_states_lat, ord=2, axis=1))
    vae_perfs = np.linalg.norm(goals_lat - last_states_lat, ord=2, axis=1)

    # rooms = [str(c) for c in clusters]
    lp_dict, _ = calculate_NN_LP(
            goals_lat,
            clusters,
            vae_perfs,
            subset_size=None,
            k=1,
            history_size=None,
            clamp=False,
    )

    n_lp_dict = normal_lp(
        vae_perfs,
        clusters,
        clamp=True,
        history_size=None,
    )


    # lps = [lp_dict[c] for c in rooms]
    available_clusters = list(set(clusters))
    perf_d = {}
    for cl in available_clusters:
        cl_indices = np.where(np.array(clusters[-10*step:]) == cl)[0]
        perf_d[cl] = vae_perfs[-10*step:][cl_indices].mean()

    perfs = [perf_d[c] for c in clusters]
    alps = [lp_dict[c] for c in clusters]
    lps = [n_lp_dict[c] for c in clusters]
    # perfs = vae_perfs
    NC_n_lp_dict = normal_lp(
        vae_perfs,
        clusters,
        clamp=True,
        history_size=None,
        med=True,
    )
    # NC_n_lp_dict = linreg_lp(
    #     vae_perfs,
    #     clusters,
    #     clamp=False,
    #     history_size=None,
    # )
    NC_lp_dict, _ = calculate_ALP(
        goals_lat,
        clusters,
        vae_perfs,
        subset_size=500,
        k=1,
        history_size=None,
        clamp=False,
    )
    NC_lps = [NC_n_lp_dict[c] for c in clusters]
    NC_alps = [NC_lp_dict[c] for c in clusters]

    alp_points.extend(
        [(iter*step, p, lp, alp, NC_lp, NC_alp, r) for p, lp, alp, NC_lp, NC_alp, r in zip(perfs, lps, alps, NC_lps, NC_alps, clusters)][-10*step:]
    )

    for ro in [Room.RANDOM, Room.OBJECTS, Room.START]:
        inds = [i for i, r in enumerate(rooms) if r == ro]
        cls = clusters[inds]
        if len(cls) > 0:
            most_c, n_c = Counter(cls).most_common(1)[0]
            recl = n_c/len(cls)
        else:
            recl = 0.0
        recs[ro].append(recl)

    for cl in np.unique(clusters):
        inds = [i for i, c in enumerate(clusters) if c == cl]
        rms = np.array(rooms)[inds]
        if len(rms) == 0:
            prec = 0.0
        else:
            print(str(cl)+":", Counter(rms))
            most_r, n_r = Counter(rms).most_common(1)[0]
            prec = n_r/len(rms)
        precs[cl].append(prec)

colors = ["red", "green", "black", "blue", "black", "black", "black", "black", "black", "black", "black", "black", "black", "black", "black", "black", "black", "black", "black", "black"]
for ro in [Room.RANDOM, Room.OBJECTS, Room.START]:
    r_recs = recs[ro]
    plt.plot([step*i for i in range(len(r_recs))], r_recs, color=colors[ro.value], label=ro)

plt.legend()
plt.savefig("cluster_metrics_re")
plt.clf()

for cl, ps in precs.items():
    plt.plot([step*i for i in range(len(ps))], ps, color=colors[cl], label=cl)
plt.legend()
plt.savefig("cluster_metrics_pr")
plt.clf()

fig, axs = plt.subplots(nrows=2, ncols=2)

xs, perfs, lps, alps, NC_lps, NC_alps, cls = list(zip(*alp_points))
axs[0][0].scatter(xs, lps, color=[colors[int(c) if type(c) != Room else c.value] for c in cls], s=2)
axs[0][0].title.set_text("LP")

axs[0][1].scatter(xs, alps, color=[colors[int(c) if type(c) != Room else c.value] for c in cls], s=2)
axs[0][1].title.set_text("ALP")

axs[1][0].scatter(xs, NC_lps, color=[colors[int(c) if type(c) != Room else c.value] for c in cls], s=2)
axs[1][0].title.set_text("nc_LP")

axs[1][1].scatter(xs, NC_alps, color=[colors[int(c) if type(c) != Room else c.value] for c in cls], s=2)
axs[1][1].title.set_text("nc_ALP")

plt.title(Path(path).stem)
custom_lines = [Line2D([0], [0], color=colors[c], lw=1) for c in [0, 1, 3]]
axs[0][0].legend(custom_lines, [Room(0), Room(1), Room(3)])
plt.savefig("alp_viz")
