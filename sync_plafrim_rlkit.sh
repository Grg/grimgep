#!/bin/bash
SOURCECODE=/home/flowers/Documents/projects/grimgep/grimgep3D/grimgep/
DESTINATIONCODE=gkovac@plafrim:/home/gkovac/Projects/grimgep3D/grimgep/

echo Transfering code to plafrim ...

#rsync -azvh --exclude "plots/*" --exclude "*npy" --exclude "*png" --exclude "envs/unity/*" --exclude "outs/*" --exclude "py3/*" --exclude "data" --exclude "*gif" --exclude "*mp4" --exclude "*.idea/" --exclude "*__pycache__/"  -e ssh $SOURCECODE $DESTINATIONCODE
rsync -azvh  -K  --exclude "replay_buffer_saved*" --exclude "outs*" --exclude "actions.pkl" --exclude "cache*" --exclude "replay_buffer_visualization*" --exclude "observations/*" --exclude "clustering_results*" --exclude "plots/*" --exclude "*npy"  --exclude "envs/unity/*" --exclude "outs/*" --exclude "py3/*" --exclude "data" --exclude "*gif" --exclude "*mp4" --exclude "*.idea/" --exclude "*__pycache__/"  -e ssh $SOURCECODE $DESTINATIONCODE


echo Done
