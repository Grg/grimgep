import csv
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid
import numpy as np
from collections import defaultdict
from pathlib import Path

import argparse

parser = argparse.ArgumentParser()
# parser.add_argument('--dataset-dir', type=str, default='./observations/MiniWorld-ThreeRoomsTVRicher-v0_H_320_overlay')
parser.add_argument('--model-path', type=str, default="/home/flowers/Documents/projects/grimgep/grimgep3D/grimgep/data/03-30-3D-CountBased-torch-120-single-seed-cl-UNI-pca-n-25-n-clus-25-tv-imagenet-very-small-start-in-TV-pow-1.0-tv-act-True-quant-s-3-n-4/03-30-3D_CountBased_torch_120_single_seed_cl_UNI_pca_n_25_n_clus_25_tv_imagenet_very_small_start_in_TV_pow_1.0_tv_act_True_quant_s_3_n_4_2021_03_30_21_07_18_0000--s-2864/")
parser.add_argument('--step', type=int, default=20)
args = parser.parse_args()

# csv_path="/home/flowers/Documents/projects/grimgep/grimgep3D/grimgep/data/03-30-3D-CountBased-torch-120-single-seed-cl-UNI-pca-n-25-n-clus-25-tv-imagenet-very-small-start-in-TV-pow-1.0-tv-act-True-quant-s-3-n-4/03-30-3D_CountBased_torch_120_single_seed_cl_UNI_pca_n_25_n_clus_25_tv_imagenet_very_small_start_in_TV_pow_1.0_tv_act_True_quant_s_3_n_4_2021_03_30_21_07_18_0000--s-2864/progress.csv"
step = args.step
csv_path = Path(args.model_path) / "progress.csv"




data={}
# parse csv
with open(csv_path, mode='r') as csv_file:
    csv_reader = csv.DictReader(csv_file)
    line_count = 0
    for row in csv_reader:
        line_count += 1

        if line_count == 0:
            print(f'Column names are {", ".join(row)}')

        else:
            epoch = int(row["Epoch"])
            ent_names_num_of_clus = [k for k in row.keys() if "num_of_cls" in k and ("Room" not in k  and "agent" not in k)]
            labels = [e.replace("_num_of_cls", "").replace("replay_buffer/", "") for e in ent_names_num_of_clus]

            ids=list(range(len(ent_names_num_of_clus)))
            if epoch % step == 0 and epoch != 0:
                data[epoch] = {}

                n_clusters = len([k for k in row.keys() if "cl_" in k and "num_of_ent" in k]) / (len(labels) + 1)
                assert n_clusters == int(n_clusters)
                n_clusters = int(n_clusters)

                for c in range(n_clusters):
                    data[epoch][c] = {}
                    for lab in labels:
                        data[epoch][c][lab] = row["replay_buffer/cl_{}_num_of_ent_{}".format(c, lab)]

                if (epoch // step) - 1 == 3:
                    break


for epoch, ep_data in data.items():
    print("E:", epoch)
    r = int(np.sqrt(n_clusters))
    # r=3
    c = n_clusters // r
    if r*c != n_clusters:
        r+=1

    fig = plt.figure(figsize=((1+r)*3, c*3))
    grid = ImageGrid(fig, 111,  # similar to subplot(111)
                     nrows_ncols=(r, c),  # creates 1x4 grid of axes
                     axes_pad=0.2,  # pad between axes in inch.
                     )

    max_h = max([max(map(int, c.values())) for c in ep_data.values()])

    # max_h = np.array([[float(data[epoch][c][l]) for l in labels] for c in ep_data.keys()]).max()
    for c, c_data in ep_data.items():
        print("C:", c)
        # init figure grid

        ax = grid[c]
        # ax.set_title("cl: {}".format(c))
        # num of ent_names
        ids = np.array(list(range(len(labels))), dtype=np.float32)

        ids *= max_h/10

        ax.bar(
            ids,
            [float(c_data[l]) for l in labels],
            align='center',
            width=max_h/12,
        )

        plt.sca(ax)
        plt.xticks(ids, labels, rotation=90)

    # fig.tight_layout()
    plt.show()
    exit()